/* #__flow */
export type ComplexSymbolType = symbol & { name: string } & { [key: string]: ComplexSymbolType };

const ComplexSymbol = (name: string, subsymbols: Object = {}): ComplexSymbolType =>
  Object.assign(Symbol(name), { name }, subsymbols);

export default (Object.assign({
  Abstract: ComplexSymbol('Abstract', {
    Featured: ComplexSymbol('Abstract.Featured'),
    Left: ComplexSymbol('Abstract.Left'),
    Right: ComplexSymbol('Abstract.Right')
  }),
  Action: ComplexSymbol('Action'),
  Advert: ComplexSymbol('Advert'),
  Agenda: ComplexSymbol('Agenda', {
    Item: ComplexSymbol('Agenda.Item')
  }),
  Article: ComplexSymbol('Article'),
  Autocomplete: ComplexSymbol('Autocomplete'),
  Avatar: ComplexSymbol('Avatar'),
  Badge: ComplexSymbol('Badge'),
  Ball: ComplexSymbol('Ball'),
  Banner: ComplexSymbol('Banner', {
    Page: ComplexSymbol('Banner.Page')
  }),
  Bar: ComplexSymbol('Bar'),
  Beam: ComplexSymbol('Beam', {
    Page: ComplexSymbol('Beam.Page')
  }),
  Box: ComplexSymbol('Box'),
  Breadcrumbs: ComplexSymbol('Breadcrumbs', {
    Item: ComplexSymbol('Breadcrumbs.Item')
  }),
  Button: ComplexSymbol('Button', {
    Box: ComplexSymbol('Button.Box'),
    Group: ComplexSymbol('Button.Group'),
    Selectable: ComplexSymbol('Button.Selectable', {
      Group: ComplexSymbol('Button.Selectable.Group', {
        Item: ComplexSymbol('Button.Selectable.Group.Item')
      })
    }),
    Social: ComplexSymbol('Button.Social')
  }),
  Calendar: ComplexSymbol('Calendar', {
    Picker: ComplexSymbol('Calendar.Picker'),
    Range: ComplexSymbol('Calendar.Range')
  }),
  Card: ComplexSymbol('Card', {
    Loading: ComplexSymbol('Loading'),
    Set: ComplexSymbol('Set')
  }),
  Carousel: ComplexSymbol('Carousel', {
    Item: ComplexSymbol('Carousel.Item')
  }),
  Carto: ComplexSymbol('Carto'),
  Center: ComplexSymbol('Center'),
  CharIcon: ComplexSymbol('CharIcon'),
  Chart: ComplexSymbol('Chart', {
    Axis: ComplexSymbol('Chart.Axis'),
    Bar: ComplexSymbol('Chart.Bar'),
    Bubble: ComplexSymbol('Chart.Bubble'),
    Dataset: ComplexSymbol('Chart.Dataset'),
    Doughnut: ComplexSymbol('Chart.Doughnut'),
    Legend: ComplexSymbol('Chart.Legend'),
    Line: ComplexSymbol('Chart.Line'),
    Pie: ComplexSymbol('Chart.Pie'),
    Polar: ComplexSymbol('Chart.Polar'),
    Radar: ComplexSymbol('Chart.Radar'),
    Scatter: ComplexSymbol('Chart.Scatter'),
    Title: ComplexSymbol('Chart.Title'),
    Tooltips: ComplexSymbol('Chart.Tooltips')
  }),
  Checkbox: ComplexSymbol('Checkbox', {
    Group: ComplexSymbol('Checkbox.Group', {
      Item: ComplexSymbol('Checkbox.Group.Item')
    })
  }),
  Clock: ComplexSymbol('Clock', {
    Picker: ComplexSymbol('Clock.Picker')
  }),
  Code: ComplexSymbol('Code'),
  Color: ComplexSymbol('Color', {
    Picker: ComplexSymbol('Color.Picker')
  }),
  ColorIcon: ComplexSymbol('ColorIcon'),
  Column: ComplexSymbol('Column', {
    Set: ComplexSymbol('Column.Set')
  }),
  Content: ComplexSymbol('Content'),
  Dialog: ComplexSymbol('Dialog', {
    Loading: ComplexSymbol('Dialog.Loading')
  }),
  Dimmer: ComplexSymbol('Dimmer'),
  Dots: ComplexSymbol('Dots'),
  Dropdown: ComplexSymbol('Dropdown', {
    Item: ComplexSymbol('Dropdown.Item')
  }),
  Dropzone: ComplexSymbol('Dropzone', {
    File: ComplexSymbol('Dropzone.File')
  }),
  Empty: ComplexSymbol('Empty'),
  Error: ComplexSymbol('Error'),
  Fab: ComplexSymbol('Fab', {
    Item: ComplexSymbol('Fab.Item')
  }),
  Field: ComplexSymbol('Field', {
    Group: ComplexSymbol('Field.Group')
  }),
  Fieldset: ComplexSymbol('Fieldset'),
  Figure: ComplexSymbol('Figure'),
  FileInput: ComplexSymbol('FileInput', {
    File: ComplexSymbol('FileInput.File')
  }),
  Flipper: ComplexSymbol('Flipper', {
    Page: ComplexSymbol('Flipper.Page')
  }),
  FontAwesome: ComplexSymbol('FontAwesome', {
    Brand: ComplexSymbol('FontAwesome.Brand'),
    Regular: ComplexSymbol('FontAwesome.Regular')
  }),
  Footer: ComplexSymbol('Footer'),
  Form: ComplexSymbol('Form'),
  GraphicPreview: ComplexSymbol('GraphicPreview'),
  GridSelect: ComplexSymbol('GridSelect', {
    Item: ComplexSymbol('GridSelect.Item', {
      Default: ComplexSymbol('GridSelect.Item.Default')
    })
  }),
  Hamburger: ComplexSymbol('Hamburger'),
  Header: ComplexSymbol('Header'),
  Heading: {
    H1: ComplexSymbol('Heading.H1'),
    H2: ComplexSymbol('Heading.H2'),
    H3: ComplexSymbol('Heading.H3'),
    H4: ComplexSymbol('Heading.H4'),
    H5: ComplexSymbol('Heading.H5')
  },
  Icon: ComplexSymbol('Icon'),
  Image: ComplexSymbol('Image', {
    Group: ComplexSymbol('Image.Group')
  }),
  InfoBox: ComplexSymbol('InfoBox'),
  Infotip: ComplexSymbol('Infotip'),
  Input: ComplexSymbol('Input', {
    Date: ComplexSymbol('Input.Date'),
    DateTime: ComplexSymbol('Input.DateTime'),
    Number: ComplexSymbol('Input.Number'),
    Password: ComplexSymbol('Input.Password'),
    PhoneNumber: ComplexSymbol('Input.PhoneNumber'),
    PhoneNumberPL: ComplexSymbol('Input.PhoneNumberPL'),
    PostalCodePL: ComplexSymbol('Input.PostalCodePL'),
    Time: ComplexSymbol('Input.Time')
  }),
  InteractivePlayground: ComplexSymbol('InteractivePlayground'),
  Label: ComplexSymbol('Label'),
  Layer: ComplexSymbol('Layer'),
  Leaf: ComplexSymbol('Leaf'),
  Link: ComplexSymbol('Link', {
    Set: ComplexSymbol('Link.Set')
  }),
  List: {
    Item: ComplexSymbol('List.Item'),
    Ordered: ComplexSymbol('List.Ordered'),
    Unordered: ComplexSymbol('List.Unordered')
  },
  Loader: ComplexSymbol('Loader', {
    Spinner: ComplexSymbol('Loader.Spinner', {
      Upload: ComplexSymbol('Loader.Spinner.Upload')
    })
  }),
  Main: ComplexSymbol('Main'),
  Markdown: ComplexSymbol('Markdown'),
  Menu: ComplexSymbol('Menu'),
  Message: ComplexSymbol('Message'),
  Notification: ComplexSymbol('Notification'),
  Option: ComplexSymbol('Option', {
    Selectable: ComplexSymbol('Option.Selectable', {
      Group: ComplexSymbol('Option.Selectable.Group', {
        Item: ComplexSymbol('Option.Selectable.Group.Item')
      })
    })
  }),
  PagerNavigation: ComplexSymbol('PagerNavigation'),
  Pane: ComplexSymbol('Pane', {
    Menu: ComplexSymbol('Pane.Menu')
  }),
  Panels: ComplexSymbol('Panels', {
    Panel: ComplexSymbol('Panels.Panel')
  }),
  Paragraph: ComplexSymbol('Paragraph'),
  Picker: ComplexSymbol('Picker', {
    Bool: ComplexSymbol('Picker.Bool'),
    Item: ComplexSymbol('Picker.Item')
  }),
  Pill: ComplexSymbol('Pill'),
  Playground: ComplexSymbol('Playground', {
    Dashboard: ComplexSymbol('Playground.Dashboard', {
      Buttons: ComplexSymbol('Playground.Dashboard.Buttons')
    }),
    Example: ComplexSymbol('Playground.Example', {
      Frame: ComplexSymbol('Playground.Example.Frame')
    }),
    Page: ComplexSymbol('Playground.Page'),
    Section: ComplexSymbol('Playground.Section'),
    State: ComplexSymbol('Playground.State')
  }),
  Progress: {
    Bar: ComplexSymbol('Progress.Bar'),
    Circle: ComplexSymbol('Progress.Circle'),
    Line: ComplexSymbol('Progress.Line'),
    SemiCircle: ComplexSymbol('Progress.SemiCircle')
  },
  PropList: ComplexSymbol('PropList', {
    Prop: ComplexSymbol('PropList.Prop')
  }),
  Radio: ComplexSymbol('Radio', {
    Group: ComplexSymbol('Radio.Group', {
      Item: ComplexSymbol('Radio.Group.Item')
    })
  }),
  Rating: ComplexSymbol('Rating', {
    Emoji: ComplexSymbol('Rating.Emoji'),
    Numbers: ComplexSymbol('Rating.Numbers'),
    Stars: ComplexSymbol('Rating.Stars')
  }),
  Required: ComplexSymbol('Required'),
  Section: ComplexSymbol('Section'),
  Select: ComplexSymbol('Select', {
    Bool: ComplexSymbol('Select.Bool'),
    Item: ComplexSymbol('Select.Item'),
    Number: ComplexSymbol('Select.Number'),
    Time: ComplexSymbol('Select.Time')
  }),
  Separator: ComplexSymbol('Separator'),
  Set: ComplexSymbol('Set'),
  Shirt: ComplexSymbol('Shirt'),
  SideNote: {
    Info: ComplexSymbol('SideNote.Info'),
    Link: ComplexSymbol('SideNote.Link'),
    Quote: ComplexSymbol('SideNote.Quote'),
    Warning: ComplexSymbol('SideNote.Warning'),
    Well: ComplexSymbol('SideNote.Well')
  },
  Slider: ComplexSymbol('Slider', {
    Range: ComplexSymbol('Slider.Range')
  }),
  SortableSets: ComplexSymbol('SortableSets', {
    Group: ComplexSymbol('SortableSets.Group'),
    Item: ComplexSymbol('SortableSets.Item')
  }),
  Status: ComplexSymbol('Status'),
  Steps: ComplexSymbol('Steps', {
    Step: ComplexSymbol('Steps.Step')
  }),
  Strength: ComplexSymbol('Strength'),
  Summary: ComplexSymbol('Summary'),
  Table: ComplexSymbol('Table', {
    Body: ComplexSymbol('Table.Body'),
    Cell: ComplexSymbol('Table.Cell', {
      Actions: ComplexSymbol('Table.Cell.Actions')
    }),
    Details: ComplexSymbol('Table.Details'),
    Footer: ComplexSymbol('Table.Footer'),
    Header: ComplexSymbol('Table.Header'),
    Row: ComplexSymbol('Table.Row', {
      Group: ComplexSymbol('Table.Row.Group')
    })
  }),
  Tabs: ComplexSymbol('Tabs', {
    Tab: ComplexSymbol('Tabs.Tab')
  }),
  Text: ComplexSymbol('Text', {
    Helper: ComplexSymbol('Text.Helper'),
    Primary: ComplexSymbol('Text.Primary'),
    Secondary: ComplexSymbol('Text.Secondary'),
    Subtitle: ComplexSymbol('Text.Subtitle')
  }),
  Tile: ComplexSymbol('Tile', {
    Set: ComplexSymbol('Set')
  }),
  Toast: ComplexSymbol('Toast'),
  Toggle: ComplexSymbol('Toggle'),
  ToggleView: ComplexSymbol('ToggleView'),
  Tooltip: ComplexSymbol('Tooltip'),
  Tracker: ComplexSymbol('Tracker', {
    Item: ComplexSymbol('Tracker.Item')
  }),
  Tree: ComplexSymbol('Tree', {
    Item: ComplexSymbol('Tree.Item')
  }),
  Value: ComplexSymbol('Value', {
    AccountNumber: ComplexSymbol('Value.AccountNumber'),
    Address: ComplexSymbol('Value.Address'),
    Amount: ComplexSymbol('Value.Amount'),
    Bool: ComplexSymbol('Value.Bool'),
    Color: ComplexSymbol('Value.Color'),
    Country: ComplexSymbol('Value.Country'),
    Currency: ComplexSymbol('Value.Currency'),
    Date: ComplexSymbol('Value.Date'),
    DateTime: ComplexSymbol('Value.DateTime'),
    Duration: ComplexSymbol('Value.Duration'),
    FileSize: ComplexSymbol('Value.FileSize'),
    Kind: ComplexSymbol('Value.Kind'),
    Money: ComplexSymbol('Value.Money'),
    Month: ComplexSymbol('Value.Month'),
    PhoneNumber: ComplexSymbol('Value.PhoneNumber'),
    Time: ComplexSymbol('Value.Time'),
    Week: ComplexSymbol('Value.Week'),
    Year: ComplexSymbol('Value.Year'),
    YearlessDate: ComplexSymbol('Value.YearlessDate')
  }),
  View: ComplexSymbol('View')
}): {
  [name: string]: ComplexSymbolType
});
