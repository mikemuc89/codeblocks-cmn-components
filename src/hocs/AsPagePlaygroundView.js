/* @flow */
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { compose, repeatPattern } from '@omnibly/codeblocks-cmn-utils';
import WithError, {
  type WithErrorAddedPropsType,
  type WithErrorParamsType,
  type WithErrorRequiredPropsType
} from './WithError';

export { repeatPattern };

export type PagePlaygroundViewType = WithErrorAddedPropsType;

export type AsPagePlaygroundViewAddedPropsType = WithErrorParamsType;

export type AsPagePlaygroundViewRequiredPropsType = WithErrorRequiredPropsType;

export type PagePlaygroundViewPropsType = any;

export type UpdateFieldType = (key: string, value: any) => void;

export default ({ ErrorComponent }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType): React.ComponentType<PagePlaygroundViewPropsType> => {
    const AsPagePlaygroundView = Object.assign(compose(WithError({ ErrorComponent }))(WrappedComponent));
    return AsPagePlaygroundView;
  };
