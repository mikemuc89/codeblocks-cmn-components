/* @flow */
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { compose } from '@omnibly/codeblocks-cmn-utils';
import AsLeafView, {
  type CallbackParamsType,
  type LeafViewType,
  type AsLeafViewParamsType,
  type AsLeafViewRequiredPropsType
} from './AsLeafView';
import WithFlowHistory, {
  type FlowCallbackParamsType,
  type WithFlowHistoryAddedPropsType,
  type WithFlowHistoryParamsType,
  type WithFlowHistoryRequiredPropsType
} from './WithFlowHistory';
import WithSteps, {
  type WithStepsAddedPropsType,
  type WithStepsParamsType,
  type WithStepsRequiredPropsType
} from './WithSteps';

export type {
  CallbackParamsType,
  FlowCallbackParamsType
};

export type FlowViewType = {|
  ...LeafViewType,
  ...WithStepsAddedPropsType,
  ...WithFlowHistoryAddedPropsType
|};

export type AsFlowViewParamsType = {|
  ...AsLeafViewParamsType,
  ...WithStepsParamsType,
  ...WithFlowHistoryParamsType
|};

export type AsFlowViewRequiredPropsType = {|
  ...AsLeafViewRequiredPropsType,
  ...WithStepsRequiredPropsType,
  ...WithFlowHistoryRequiredPropsType
|};

export type FlowViewPropsType = any;

export default ({
    ErrorComponent,
    api,
    callbacks,
    customHandlers,
    endStates,
    errorConfig,
    events,
    flowHistoryCallbacks,
    onSubmit,
    steps
  }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType): React.ComponentType<FlowViewPropsType> => {
    const AsFlowView = Object.assign(
      compose(
        AsLeafView({ ErrorComponent, api, callbacks, customHandlers, errorConfig, events, onSubmit }),
        WithFlowHistory({ api, endStates, flowHistoryCallbacks }),
        WithSteps({ steps })
      )(WrappedComponent),
      {
        api
      }
    );
    return AsFlowView;
  };
