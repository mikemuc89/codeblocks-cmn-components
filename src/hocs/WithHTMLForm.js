/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import Form from '../components/Form';
import { type FormType } from './WithState';

export type WithHTMLFormAddedPropsType = {};

export type WithHTMLFormParamsType = {|
  onSubmit: (form: FormType) => void
|};

export type WithHTMLFormRequiredPropsType = {|
  form: FormType
|};

export default ({ onSubmit }: WithHTMLFormParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithHTMLForm = (props: WithHTMLFormRequiredPropsType) => {
      const { form } = props;
      const onSubmit = React.useCallback(() => onSubmit(form), [form]);

      return (
        <Form onSubmit={onSubmit}>
          <WrappedComponent {...props} />
        </Form>
      );
    };
    return WithHTMLForm;
  };
