/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';

export type WithTimeoutsAddedPropsType = {|
  addTimeout: Function,
  clearTimeouts: Function
|};

export type WithTimeoutsParamsType = {};

export type WithTimeoutsRequiredPropsType = {};

export default () => (WrappedComponent: HocWrappedComponentType) => {
  const WithTimeouts = (props: WithTimeoutsRequiredPropsType) => {
    const timeouts = React.useRef([]);

    const addTimeout = React.useCallback((func: Function, delay: number) => {
      timeouts.current.push(setTimeout(func, delay));
    }, []);

    const clearTimeouts = React.useCallback(() => {
      timeouts.current.forEach(clearTimeout);
    }, []);

    return <WrappedComponent {...props} addTimeout={addTimeout} clearTimeouts={clearTimeouts} />;
  };
  return WithTimeouts;
};
