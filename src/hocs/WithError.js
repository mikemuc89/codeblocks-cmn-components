/* @flow */
import * as React from 'react';
import { type ErrorsType, type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import DefaultErrorComponent from '../components/Error';
import { type NavigateType } from './WithNavigation';

export type WithErrorAddedPropsType = {};

export type WithErrorParamsType = {|
  ErrorComponent: React.ComponentType<any>
|};

export type WithErrorRequiredPropsType = {|
  navigate: NavigateType
|};

type StateType = {|
  errors: ErrorsType
|};

export default ({ ErrorComponent = DefaultErrorComponent }: WithErrorParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => 
    (props: WithErrorRequiredPropsType) => {
      try {
        return (
          <WrappedComponent {...props} />
        );
      } catch(errors) {
        const { navigate } = props;  
        console.error(errors);

        return (
          <ErrorComponent errors={errors} navigate={navigate} />
        );
      }
    };
