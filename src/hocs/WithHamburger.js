/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import HamburgerRegisterContext from '../contexts/HamburgerRegisterContext';

export type WithHamburgerAddedPropsType = {};

export type WithHamburgerParamsType = {|
  open?: boolean
|};

export type WithHamburgerRequiredPropsType = {};

export default ({ open: initiallyOpen }: WithHamburgerParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithHamburger = (props: WithHamburgerRequiredPropsType) => {
      const [open, setOpen] = React.useState<boolean>(initiallyOpen);
      const [hamburgers, setHamburgers] = React.useState<Object>({});
      const [hamburgerIds, setHamburgerIds] = React.useState<Array<string>>([]);

      const toggle = React.useCallback(
        (forcedState?: boolean) => {
          setOpen((open: boolean) => (forcedState === undefined ? !open : Boolean(forcedState)));
        },
        [setOpen]
      );

      const subscribe = React.useCallback(
        (id: string, component: React.ComponentType<any>) => {
          setHamburgers((old: Object) => ({
            ...old,
            [id]: component
          }));
          setHamburgerIds((old: Array<string>) => [...old, id]);
        },
        [setHamburgers, setHamburgerIds]
      );

      const unsubscribe = React.useCallback(
        (id: string) => {
          setHamburgers(({ [id]: _, ...filtered }: Object) => filtered);
          setHamburgerIds((old: Array<string>) => old.filter((item: string) => item !== id));
        },
        [setHamburgers, setHamburgerIds]
      );

      const hamburger = hamburgerIds.length === 0 ? null : hamburgers[hamburgerIds[hamburgerIds.length - 1]];

      const contextValue = React.useMemo(
        () => ({ hamburger, open, subscribe, toggle, unsubscribe }),
        [hamburger, open, subscribe, toggle, unsubscribe]
      );

      return (
        <HamburgerRegisterContext.Provider value={contextValue}>
          <WrappedComponent {...props} />
        </HamburgerRegisterContext.Provider>
      );
    };
    return WithHamburger;
  };
