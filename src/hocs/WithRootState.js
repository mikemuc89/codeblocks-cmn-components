/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';

export type WithRootStateAddedPropsType = {|
  updateRootState: Function
|};

export type WithRootStateParamsType = {};

export type WithRootStateRequiredPropsType = {|
  updateState: Function
|};

export default () => (WrappedComponent: HocWrappedComponentType) => {
  const WithRootState = (props: WithRootStateRequiredPropsType) => (
    <WrappedComponent {...props} updateRootState={props.updateState} />
  );
  return WithRootState;
};
