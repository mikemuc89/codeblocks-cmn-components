/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';

export type WithContextAddedPropsType = {};

export type WithContextParamsType = {|
  Context: React.ContextType<any>,
  propName: string
|};

export type WithContextRequiredPropsType = {};

export default ({ Context, propName }: WithContextParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithContext = (props: WithContextRequiredPropsType) => (
      <Context.Consumer>{(value: any) => <WrappedComponent {...props} {...{ [propName]: value }} />}</Context.Consumer>
    );
    return WithContext;
  };
