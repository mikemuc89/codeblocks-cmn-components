/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import branch from './branch';

type WithDefaultAddedPropsType = {};

export type WithDefaultParamsType = {|
  DefaultComponent?: React.Element<any>
|};

export type WithDefaultRequiredPropsType = {|
  useDefault?: boolean
|};

export default ({ DefaultComponent = () => null }: WithDefaultParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithDefault = (props: WithDefaultRequiredPropsType) =>
      branch(props.useDefault, DefaultComponent, WrappedComponent)(props);
    return WithDefault;
  };
