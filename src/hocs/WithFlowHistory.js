/* @flow */
import * as React from 'react';
import { ACTIONS, type ApiType } from '@omnibly/codeblocks-cmn-schema/src/api';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { guid } from '@omnibly/codeblocks-cmn-utils';
import history, { addDerivedHistoryState, updateHistoryState } from '../history';
import useEventListener from '../hooks/useEventListener';
import { type FetchType } from './WithFetch';
import { type NavigateType } from './WithNavigation';
import { type UpdateStateType } from './WithState';

export type FlowCallbackParamsType = {|
  back: () => Promise<void>,
  fetch: FetchType,
  form: Object,
  navigate: NavigateType,
|};

export type WithFetchAddedPropsType = {};

export type WithFetchParamsType = {|
  api?: ApiType,
  endStates?: Array<string>,
  flowHistoryCallbacks?: { [fromStep: string]: { [toStep: string]: (params: FlowCallbackParamsType) => void } }
|};

export type WithFetchRequiredPropsType = {|
  config: Object,
  fetch: FetchType,
  form: Object,
  navigate: NavigateType,
  updateState: UpdateStateType
|};

export default ({ api, endStates = [], flowHistoryCallbacks = {} }: WithFetchParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const asyncBack = () => {
      history.back();
      return Promise.resolve();
    };

    const updateFlowHistory = (e: Event) => {
      const { payload = {}, response = {} } = e.detail;
      const { flow = {} } = history.location.state;

      const isFlowEnd = endStates.includes(response.step);
      const newFlowState = { id: response.flow_id, step: response.step };

      if (!flow.id && !flow.step) {
        if (!isFlowEnd) {
          updateHistoryState(history.location, { flow: { ...newFlowState, init: true } });
        }
      } else if (payload.action === ACTIONS.SUBMIT) {
        const { [flow.step]: possibleTransitions = {} } = flowHistoryCallbacks;

        if (flow.id === response.flow_id) {
          if (response.step !== flow.step) {
            if (isFlowEnd) {
              addDerivedHistoryState(history.location, { flow: newFlowState, gid: guid(), skip: true });
            } else if (response.step in possibleTransitions) {
              addDerivedHistoryState(history.location, { flow: newFlowState, gid: guid() });
            } else {
              updateHistoryState(history.location, { flow: undefined });
            }
          }
        } else {
          updateHistoryState(history.location, { flow: undefined });
        }
      }
    };

    const WithFlowHistory = (props: WithFetchRequiredPropsType) => {
      const {
        config: { name: viewName = '/' },
        fetch,
        form,
        navigate
      } = props;

      const handleFlowTransition = React.useCallback(
        (e: Event) => {
          const { location } = e.detail;

          if (!viewName || viewName !== location.state?.path) {
            return;
          }

          const { previousLocation } = e.detail;
          const { state: { flow: fromFlow, path: fromPath } = {} } = previousLocation;
          const { state: { flow: toFlow, path } = {} } = location;

          if (toFlow && fromFlow && fromPath === path) {
            const { id: fromId, step: fromStep } = fromFlow;
            const { id: toId, init, step: toStep } = toFlow;

            if (fromId === toId) {
              if (fromStep !== toStep) {
                const { [fromStep]: { [toStep]: callback } = {} } = flowHistoryCallbacks;
                if (callback) {
                  callback({ back: asyncBack, fetch: api && fetch(api), form, navigate });
                } else if (!init) {
                  // for every step until init or different navi id:
                  // go back until found flow init, mark every as skip
                  updateHistoryState(history.location, { hold: true, skip: true });
                  history.back();
                }
              }
            } else if (!init) {
              // for every step until init or different navi id:
              // go back until found flow init, mark every as skip
              updateHistoryState(history.location, { hold: true, skip: true });
              history.back();
            }
          } else if (toFlow && fromPath !== path) {
            // stepping into the middle of a flow from outside
            const { init } = toFlow;
            if (!init) {
              // for every step until init or different navi id:
              // go back until found flow init, mark every as skip
              updateHistoryState(history.location, { hold: true, skip: true });
              history.back();
            }
          }
        },
        [fetch, form, navigate, viewName]
      );

      useEventListener('dataFetched', updateFlowHistory, typeof document === 'undefined' ? null : document);
      useEventListener('flowTransition', handleFlowTransition, typeof document === 'undefined' ? null : document);

      return <WrappedComponent {...props} />;
    };
    return WithFlowHistory;
  };
