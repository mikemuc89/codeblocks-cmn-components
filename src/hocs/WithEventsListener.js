/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { type FetchType } from './WithFetch';
import { type NavigateType } from './WithNavigation';
import { type UpdateStateType } from './WithState';

export type EventParamsType = {|
  data: Object,
  fetch: FetchType,
  navigate: NavigateType,
  updateState: UpdateStateType
|};

export type EventHandlerType = (params: EventParamsType) => void;

export type WithEventsListenerAddedPropsType = {};

export type WithEventsListenerParamsType = {|
  events: {
    [key: string]: EventHandlerType
  }
|};

export type WithEventsListenerRequiredPropsType = {|
  fetch: FetchType,
  navigate: NavigateType,
  updateState: UpdateStateType
|};

export const triggerEvent = (name: string, data: Object = {}) => {
  const e = new CustomEvent(name, { detail: data });
  window.dispatchEvent(e);
};

export default ({ events = {} }: WithEventsListenerParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithEventsListener = (props: WithEventsListenerRequiredPropsType) => {
      const { fetch, navigate, updateState } = props;

      const addListeners = React.useCallback(() => {
        Object.entries(events).forEach(([event, handler]: [string, mixed]) => {
          if (typeof handler !== 'function') {
            throw new Error('Handler should be a function');
          }
          window.addEventListener(event, (e: Event) => {
            e.stopPropagation();
            handler({ data: e.detail, fetch, navigate, updateState });
          });
        });
      }, [fetch, navigate, updateState]);

      const removeListeners = React.useCallback(() => {
        Object.entries(events).forEach(([event, handler]: [string, mixed]) => {
          if (typeof handler !== 'function') {
            throw new Error('Handler should be a function');
          }
          window.removeEventListener(event, handler);
        });
      }, []);

      React.useEffect(() => {
        addListeners();
        return removeListeners;
      }, [addListeners, removeListeners]);

      return <WrappedComponent {...props} />;
    };
    return WithEventsListener;
  };
