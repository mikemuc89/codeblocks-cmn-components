/* @flow */
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { compose } from '@omnibly/codeblocks-cmn-utils';
import AsView, {
  type ViewType,
  type AsViewParamsType,
  type AsViewRequiredPropsType
} from './AsView';
import WithHamburger, {
  type WithHamburgerAddedPropsType,
  type WithHamburgerParamsType,
  type WithHamburgerRequiredPropsType
} from './WithHamburger';
import WithNavigation, {
  type WithNavigationAddedPropsType,
  type WithNavigationParamsType,
  type WithNavigationRequiredPropsType
} from './WithNavigation';
import WithNotifications, {
  type WithNotificationsAddedPropsType,
  type WithNotificationsParamsType,
  type WithNotificationsRequiredPropsType
} from './WithNotifications';
import WithRootState, {
  type WithRootStateAddedPropsType,
  type WithRootStateParamsType,
  type WithRootStateRequiredPropsType
} from './WithRootState';
import WithTranslation, {
  type WithTranslationAddedPropsType,
  type WithTranslationParamsType,
  type WithTranslationRequiredPropsType
} from './WithTranslation';

export type RootViewType = {|
  ...ViewType,
  ...WithHamburgerAddedPropsType,
  ...WithNavigationAddedPropsType,
  ...WithNotificationsAddedPropsType,
  ...WithRootStateAddedPropsType,
  ...WithTranslationAddedPropsType
|};

export type AsRootViewParamsType = {|
  ...AsViewParamsType,
  ...WithHamburgerParamsType,
  ...WithNavigationParamsType,
  ...WithNotificationsParamsType,
  ...WithRootStateParamsType,
  ...WithTranslationParamsType
|};

export type AsRootViewRequiredPropsType = {|
  ...AsViewRequiredPropsType,
  ...WithHamburgerRequiredPropsType,
  ...WithNavigationRequiredPropsType,
  ...WithNotificationsRequiredPropsType,
  ...WithRootStateRequiredPropsType,
  ...WithTranslationRequiredPropsType
|};

export default ({
    DefaultContent,
    ErrorComponent,
    api,
    callbacks,
    customHandlers,
    errorConfig,
    events,
    i18n,
    initialData,
    navigation
  }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const AsRootView = Object.assign(
      compose(
        WithHamburger(),
        WithNavigation({ navigation }),
        WithTranslation({ i18n }),
        AsView({ DefaultContent, ErrorComponent, api, callbacks, customHandlers, errorConfig, events, initialData }),
        WithRootState(),
        WithNotifications()
      )(WrappedComponent),
      {
        api
      }
    );
    return AsRootView;
  };
