/* @flow */
import * as React from 'react';
import { compose } from '@omnibly/codeblocks-cmn-utils';
import WithContent, {
  type WithContentAddedPropsType,
  type WithContentParamsType,
  type WithContentRequiredPropsType
} from './WithContent';
import WithError, {
  type WithErrorAddedPropsType,
  type WithErrorParamsType,
  type WithErrorRequiredPropsType
} from './WithError';
import WithEventsListener, {
  type WithEventsListenerAddedPropsType,
  type WithEventsListenerParamsType,
  type WithEventsListenerRequiredPropsType
} from './WithEventsListener';
import WithFetch, {
  type WithFetchParamsType,
  type WithFetchAddedPropsType,
  type WithFetchRequiredPropsType
} from './WithFetch';
import WithForm, {
  type WithFormParamsType,
  type WithFormAddedPropsType,
  type WithFormRequiredPropsType
} from './WithForm';
import WithState, {
  type WithStateAddedPropsType,
  type WithStateParamsType,
  type WithStateRequiredPropsType
} from './WithState';

export type ViewType = {|
  ...WithContentAddedPropsType,
  ...WithErrorAddedPropsType,
  ...WithEventsListenerAddedPropsType,
  ...WithFetchAddedPropsType,
  ...WithFormAddedPropsType,
  ...WithStateAddedPropsType
|};

export type AsViewParamsType = {|
  ...WithContentParamsType,
  ...WithErrorParamsType,
  ...WithEventsListenerParamsType,
  ...WithFetchParamsType,
  ...WithFormParamsType,
  ...WithStateParamsType
|};

export type AsViewRequiredPropsType = {|
  ...WithContentRequiredPropsType,
  ...WithErrorRequiredPropsType,
  ...WithEventsListenerRequiredPropsType,
  ...WithFetchRequiredPropsType,
  ...WithFormRequiredPropsType,
  ...WithStateRequiredPropsType
|};

export type ViewPropsType = any;

export default ({
  DefaultContent,
  ErrorComponent,
  api,
  callbacks,
  customHandlers,
  initialData,
  errorConfig,
  events
}: AsViewParamsType = {}) =>
(WrappedComponent: HocWrappedComponentType): React.ComponentType<ViewPropsType> => {
  const AsView = Object.assign(
    compose(
      WithState(),
      WithFetch({ api, callbacks }),
      WithError({ ErrorComponent }),
      WithForm({ customHandlers, errorConfig }),
      WithEventsListener({ events }),
      WithContent({ DefaultContent })
    )(WrappedComponent),
    {
      api
    }
  );
  return AsView;
};
