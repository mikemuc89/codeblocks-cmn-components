/* @flow */
import * as React from 'react';

export default (test: boolean, ComponentOnPass: React.ComponentType<any>, ComponentOnFail: React.ComponentType<any>) =>
  (props: Object) =>
    test ? <ComponentOnPass {...props} /> : ComponentOnFail ? <ComponentOnFail {...props} /> : null;
