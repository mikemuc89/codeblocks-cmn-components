/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { compose } from '@omnibly/codeblocks-cmn-utils';
import WithError, { type WithErrorAddedPropsType, type WithErrorParamsType } from './WithError';
import WithEventsListener, {
  type WithEventsListenerAddedPropsType,
  type WithEventsListenerParamsType
} from './WithEventsListener';
import WithFetch, {
  type CallbackParamsType,
  type WithFetchParamsType,
  type WithFetchAddedPropsType
} from './WithFetch';
import WithForm, { type WithFormParamsType, type WithFormAddedPropsType } from './WithForm';
import WithHTMLForm, { type WithHTMLFormParamsType, type WithHTMLFormAddedPropsType } from './WithHTMLForm';
import WithState, { type WithStateAddedPropsType, type WithStateParamsType } from './WithState';

export type { CallbackParamsType };

export type LeafViewType = {|
  ...WithErrorAddedPropsType,
  ...WithEventsListenerAddedPropsType,
  ...WithFetchAddedPropsType,
  ...WithFormAddedPropsType,
  ...WithHTMLFormAddedPropsType,
  ...WithStateAddedPropsType
|};

export type AsLeafViewParamsType = {|
  ...WithErrorParamsType,
  ...WithEventsListenerParamsType,
  ...WithFetchParamsType,
  ...WithFormParamsType,
  ...WithHTMLFormParamsType,
  ...WithStateParamsType
|};

export type AsLeafViewRequiredPropsType = {|
  ...WithErrorRequiredPropsType,
  ...WithEventsListenerRequiredPropsType,
  ...WithFetchRequiredPropsType,
  ...WithFormRequiredPropsType,
  ...WithHTMLFormRequiredPropsType,
  ...WithStateRequiredPropsType
|};

export type LeafViewPropsType = any;

export default ({
    ErrorComponent,
    api,
    callbacks,
    customHandlers,
    errorConfig,
    events,
    historyCallbacks,
    onSubmit
  }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType): React.ComponentType<LeafViewPropsType> => {
    const AsLeafView = Object.assign(
      compose(
        WithState(),
        WithFetch({ api, callbacks, historyCallbacks }),
        WithError({ ErrorComponent }),
        WithForm({ customHandlers, errorConfig }),
        WithHTMLForm({ onSubmit }),
        WithEventsListener({ events })
      )(WrappedComponent),
      {
        api
      }
    );
    return AsLeafView;
  };
