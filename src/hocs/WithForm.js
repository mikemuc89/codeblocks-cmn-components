/* @flow */
import * as React from 'react';
import defaultHandlers from '@omnibly/codeblocks-cmn-handlers';
import { type ErrorsType, type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { type UpdateStateType } from './WithState';

export type FieldHandlerType = (key: string, opts?: Object) => Object;

export type WithFormAddedPropsType = {|
  errorConfig?: { [errorKey: string]: (?ErrorsType) => string },
  fieldHandler: FieldHandlerType
|};

export type WithFormParamsType = {|
  errorConfig?: { [errorKey: string]: (?ErrorsType) => string },
  customHandlers?: { [handlerKey: string]: (?ErrorsType) => string }
|};

export type WithFormRequiredPropsType = {|
  config?: Object,
  errorConfig?: { [errorKey: string]: (?ErrorsType) => string },
  form: Object,
  updateState: UpdateStateType
|};

const WithForm =
  ({ errorConfig: paramsErrorConfig = {}, customHandlers = {} }: WithFormParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithForm = (props: WithFormRequiredPropsType) => {
      const { config: { name: viewName = '/' } = {}, errorConfig, form, updateState } = props;

      const fieldHandler = React.useCallback(
        (key: string, opts?: Object = {}) => {
          const { customErrors, fieldType, ...restOpts } = opts;
          const { fields = {} } = form;
          const options = {
            errorConfig: {
              ...errorConfig,
              ...paramsErrorConfig,
              ...customErrors
            },
            ...restOpts
          };
          const name = `${viewName}/${key}`;

          return {
            handled: true,
            name,
            ...getHandlerFunction(
              key,
              fieldType
            )({
              field: fields[key] || {},
              key,
              options,
              state: form,
              updateFields: (spec: Object) => {
                updateState({ form: { fields: spec } });
              }
            })
          };
        },
        [errorConfig, form, getHandlerFunction, updateState, viewName]
      );

      const getHandlerFunction = React.useCallback(
        (key: string, defaultFieldType?: string): FieldHandlerType => {
          const { fields = {} } = form;
          const field = fields[key] || { errors: null, value: null, widget: {} };
          const { widget: { field_type: fieldType } = {} } = field;
          const handlers = {
            ...defaultHandlers,
            ...customHandlers
          };

          return (
            handlers[fieldType] || handlers[key] || (defaultFieldType && handlers[defaultFieldType]) || handlers.str
          );
        },
        [form]
      );

      return <WrappedComponent errorConfig={errorConfig} {...props} fieldHandler={fieldHandler} />;
    };

    return WithForm;
  };

export default WithForm;
