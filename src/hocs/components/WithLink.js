/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import useAppHrefHandler from '../../hooks/useAppHrefHandler';

export type ParamsType = {|
  forceClickable?: boolean
|};
export type WithLinkType = {|
  disabled?: boolean,
  href?: string,
  hrefData?: string,
  onClick?: (Event) => void
|};
export type WithLinkExtensionType = {|
  LinkComponent: string,
  linkProps: Object
|};

export default ({ forceClickable }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithLink = React.forwardRef(
      ({ disabled, href, hrefData, onClick, ...props }: WithLinkType, forwardedRef: ReactForwardedRefType) => {
        const appHrefHandler = useAppHrefHandler({ disabled, forceClickable, href, hrefData, onClick });

        return (
          <WrappedComponent
            ref={forwardedRef}
            LinkComponent={appHrefHandler.Component}
            disabled={disabled}
            linkProps={appHrefHandler()}
            {...props}
          />
        );
      }
    );

    return WithLink;
  };
