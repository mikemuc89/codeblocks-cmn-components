/* @flow */
import * as React from 'react';
import { type ReactForwardedRefType, type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { compose } from '@omnibly/codeblocks-cmn-utils';
import { type ComplexSymbolType } from '../../componentNames';
import WithRwdProps, { type WithRwdPropsType } from './WithRwdProps';
import WithRwdVisibility, { type WithRwdVisibilityType } from './WithRwdVisibility';

export type AsComponentType = {|
  ...WithRwdPropsType,
  ...WithRwdVisibilityType
|};
export type AsComponentExtensionType = {|
  className?: string
|};
export type { ReactForwardedRefType };

export default (symbol: ComplexSymbolType) => (WrappedComponent: HocWrappedComponentType) => {
  const AsComponent: React.ComponentType<AsComponentType> = compose(
    WithRwdProps(),
    WithRwdVisibility()
  )(WrappedComponent);

  return Object.assign(AsComponent, { componentId: symbol, displayName: symbol.name });
};
