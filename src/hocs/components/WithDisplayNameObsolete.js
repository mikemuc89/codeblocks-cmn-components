/* @flow */
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';

export default (displayName: string) => (WrappedComponent: HocWrappedComponentType) => {
  const WithDisplayName = Object.assign(WrappedComponent, { displayName });
  return WithDisplayName;
};
