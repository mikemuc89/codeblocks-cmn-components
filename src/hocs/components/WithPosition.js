/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type ReactForwardedRefType } from '@omnibly/codeblocks-cmn-types';
import { POSITIONS_CORNERS as POSITIONS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';

const COMMON_STYLE = { position: 'absolute' };

export type WithPositionType = {|
  absolute?: boolean,
  position?: $Values<typeof POSITIONS>,
  size?: number
|};
export type WithPositionExtensionType = {|
  positionElementRef: ReactRefType<HTMLElement>,
  size?: number
|};

export default () => (WrappedComponent: HocWrappedComponentType) => {
  const WithPosition = React.forwardRef(
    ({ absolute, position, size, ...props }: WithPositionType, forwardedRef: ReactForwardedRefType) => {
      const positionElementRef = React.useRef(null);

      const style = React.useMemo(() => ({
        ...COMMON_STYLE,
        ...(() => {
          if (absolute) {
            return { position: 'absolute' };
          }

          if (!position) {
            return { position: 'relative' };
          }

          const horizontalPosition = unitize(-size / 2);
          const verticalPosition = unitize(-size / 2);

          if (position === POSITIONS.TOP_LEFT) {
            return { left: horizontalPosition, top: verticalPosition };
          }
          if (position === POSITIONS.TOP_RIGHT) {
            return { right: horizontalPosition, top: verticalPosition };
          }
          if (position === POSITIONS.BOTTOM_LEFT) {
            return { bottom: verticalPosition, left: horizontalPosition };
          }
          if (position === POSITIONS.BOTTOM_RIGHT) {
            return { bottom: verticalPosition, right: horizontalPosition };
          }
        })()
      }), [absolute, position, size]);

      React.useEffect(() => {
        if (positionElementRef.current) {
          setElementStyle(positionElementRef.current, style)
        }
      }, [style]);

      return <WrappedComponent positionElementRef={positionElementRef} ref={forwardedRef} size={size} {...props} />;
    }
  );

  return WithPosition;
};
