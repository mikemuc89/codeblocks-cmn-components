/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type ReactForwardedRefType } from '@omnibly/codeblocks-cmn-types';

const CHAR_ADJUSTER_SHIFT = 9000;

export type ParamsType = {|
  format: (...args: Array<any>) => string,
  regex: typeof RegExp,
  sanitize: (...args: Array<any>) => string
|};
export type WithInputFormattingType = {|
  controlElement: React.Node,
  controlRef: ReactRefType<HTMLInputElement | HTMLTextAreaElement>,
  disabled?: boolean,
  readonly?: boolean,
  setValue: SetStateCallbackType<ValueType>,
  value: ValueType
|};
export type WithInputFormattingExtensionType = {|
  controlElement: React.Node
|};

export default ({ format, regex, sanitize }: ParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const getCorrespondingCharPosition = (
      originalPosition: number,
      formattedString: string,
      sanitizedString: string
    ) => {
      const formattedLength = formattedString.length;
      const sanitizedLength = sanitizedString.length;

      if (originalPosition >= formattedLength) {
        return sanitizedLength;
      }

      let nearestSaneCharacter = originalPosition;
      while (formattedString.charCodeAt(nearestSaneCharacter) < CHAR_ADJUSTER_SHIFT) {
        nearestSaneCharacter++;
      }

      const char = formattedString[nearestSaneCharacter];
      const index = sanitizedString.indexOf(char);
      return index === -1 ? sanitizedLength : index;
    };

    const getAdjustedCursorPosition = (el: HTMLInputElement) => {
      const { value, selectionStart, selectionEnd } = el;

      const sanitized = Array.from(Array(sanitize(value).length))
        .map((_: any, idx: number) => String.fromCharCode(CHAR_ADJUSTER_SHIFT + idx))
        .join('');
      const formatted = format(sanitized);

      const newSelectionStart = getCorrespondingCharPosition(selectionStart, formatted, sanitized);
      const newSelectionEnd =
        selectionStart === selectionEnd
          ? newSelectionStart
          : getCorrespondingCharPosition(selectionEnd, formatted, sanitized);

      return { selectionEnd: newSelectionEnd, selectionStart: newSelectionStart };
    };

    const WithInputFormatting = React.forwardRef(
      ({ controlElement, ...props }: WithInputFormattingType, forwardedRef: ReactForwardedRefType) => {
        const { controlRef, disabled, readonly, setValue, value } = props;

        const previousValue = React.useRef(value);
        const preventFocusSanitize = React.useRef(false);

        React.useEffect(() => {
          const el = controlRef.current;
          el.value = format(value);
        }, [value]);

        const handleOnBlur = React.useCallback((e: SyntheticFocusEvent<HTMLInputElement>) => {
          const el = controlRef.current;
          el.value = format(el.value);
          if (controlElement.props.onBlur) {
            controlElement.props.onBlur(e);
          }
        }, [controlElement]);

        const handleOnFocus = React.useCallback((e: SyntheticFocusEvent<HTMLInputElement>) => {
          if (!preventFocusSanitize.current) {
            const el = controlRef.current;
            const { value } = el;
            const newValue = sanitize(value);
            el.value = newValue;
            el.selectionStart = 0;
            el.selectionEnd = newValue.length - 1;
          }
          if (controlElement.props.onFocus) {
            controlElement.props.onFocus(e);
          }
        }, [controlElement]);

        const handleOnMouseUp = React.useCallback(() => {
          const el = controlRef.current;
          const { selectionEnd, selectionStart } = getAdjustedCursorPosition(el);

          const { value } = el;
          const newValue = sanitize(value);
          el.value = newValue;

          el.selectionStart = selectionStart;
          el.selectionEnd = selectionEnd;
          preventFocusSanitize.current = false;
          document.removeEventListener('mouseup', handleOnMouseUp);
        }, []);

        const handleOnMouseDown = React.useCallback((e: SyntheticPointerEvent<HTMLInputElement>) => {
          preventFocusSanitize.current = true;
          document.addEventListener('mouseup', handleOnMouseUp);
        }, []);

        const handleOnInput = React.useCallback((e: SyntheticInputEvent<HTMLInputElement>) => {
          const { value } = e.target;
          if (regex && !value.match(regex)) {
            e.target.value = previousValue.current;
          } else {
            previousValue.current = e.target.value;
          }
        }, []);

        const overwrittenControlElement = React.useMemo(() => controlElement && React.cloneElement(controlElement, {
          onBlur: (disabled || readonly) ? undefined : handleOnBlur,
          onFocus: (disabled || readonly) ? undefined : handleOnFocus,
          onInput: (disabled || readonly) ? undefined : handleOnInput,
          onMouseDown: (disabled || readonly) ? undefined : handleOnMouseDown
        }), [controlElement, disabled, handleOnBlur, handleOnFocus, handleOnInput, handleOnMouseDown, readonly]);

        return (
          <WrappedComponent ref={forwardedRef} controlElement={overwrittenControlElement} {...props} />
        );
      }
    );

    return WithInputFormatting;
  };
