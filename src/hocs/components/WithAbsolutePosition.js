/* @flow */
import number from '@omnibly/codeblocks-cmn-handlers/src/handlers/number';
import * as React from 'react';

const CENTER_TYPES = Object.freeze({
  BOTH: 'BOTH',
  HORIZONTAL: 'HORIZONTAL',
  VERTICAL: 'VERTICAL'
});

const POSITION_TYPES = Object.freeze({
  ABSOLUTE: 'absolute',
  FIXED: 'fixed'
});

type WithAbsolutePositionType = {|
  bottom?: number,
  children: React.Node,
  center?: $Values<typeof CENTER_TYPES>,
  height?: number,
  left?: number,
  position?: $Values<typeof POSITION_TYPES>,
  right?: number,
  top?: number,
  width?: number
|};

const WithAbsolutePosition = ({
  bottom,
  children,
  center = CENTER_TYPES.HORIZONTAL,
  height,
  left,
  position = POSITION_TYPES.ABSOLUTE,
  right,
  top,
  width
}: WithAbsolutePositionType) => {
  const absoluteElRef = React.useRef(null);

  React.useEffect(() => {
    const el = absoluteElRef.current;
    if (el) {
      el.style.position = position;
      if (height) {
        el.style.height = unitize(height);
      }
      if (width) {
        el.style.width = unitize(width);
      }
      if (center) {
        const style = {
          [CENTER_TYPES.HORIZONTAL]: () => ({
            left: '50%',
            transform: 'translateX(-50%)'
          }),
          [CENTER_TYPES.VERTICAL]: () => ({
            top: '50%',
            transform: 'translateY(-50%)'
          }),
          [CENTER_TYPES.BOTH]: () => ({
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)'
          })
        }[center]();
        if (bottom) {
          style.transform += ` translateY(${unitize(-bottom)})`
        }
        if (left) {
          style.transform += ` translateX(${unitize(left)})`
        }
        if (right) {
          style.transform += ` translateX(${unitize(-right)})`
        }
        if (top) {
          style.transform += ` translateY(${unitize(top)})`
        }
        setElementStyle(el, style);
      } else {
        const style = {
          ...(bottom === undefined ? {} : { bottom: unitize(bottom) }),
          ...(left === undefined ? {} : { left: unitize(left) }),
          ...(right === undefined ? {} : { right: unitize(right) }),
          ...(top === undefined ? {} : { top: unitize(top) })
        };
        setElementStyle(el, style);
      }
    }
  }, [bottom, center, left, position, right, top]);

  return React.cloneElement(
    children,
    {
      ref: absoluteElRef
    }
  );
};

export default Object.assign(
  WithAbsolutePosition,
  {
    CENTER_TYPES,
    POSITION_TYPES
  }
);
