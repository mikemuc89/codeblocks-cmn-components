/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type SetStateCallbackType, type ReactRefType, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { GLOBAL_CLASS_NAMES, INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid } from '@omnibly/codeblocks-cmn-utils';
import Required from '../../components/Required';
import useControlValue from '../../hooks/useControlValue';
import useControlFocus from '../../hooks/useControlFocus';
import useControlMessages from '../../hooks/useControlMessages';

const defaultSanitizeValueForControl = (value: string) => value;

export type AsControlParamsType<ValueType> = {|
  controlTag?: string,
  controlType?: string,
  defaultMaximum?: number,
  defaultValue: ValueType,
  sanitizeValueForControl?: (value: ValueType, { multiple: boolean }) => string,
  style?: { key: string, styles: Object }
|};
export type AsControlType<ValueType> = {|
  ...WithMessagesType,
  autofocus?: boolean,
  canClear?: boolean,
  disabled?: boolean,
  focus?: boolean,
  maximum?: number,
  name?: string,
  onBlur?: (e: SyntheticFocusEvent<HTMLElement>) => void,
  onChange?: (value: ValueType) => void,
  onClear?: (value: ValueType) => void,
  onEnter?: (value: ValueType) => void,
  onFocus?: (e: SyntheticFocusEvent<HTMLElement>) => void,
  onKeyDown?: (e: SyntheticKeyboardEvent<HTMLElement>) => void,
  readonly?: boolean,
  rows?: number,
  value: ValueType
|};
export type AsControlExtensionType<ValueType> = {|
  controlElement: React.Node,
  controlRef: ReactRefType<HTMLElement>,
  messageElement: React.Node,
  multiple?: boolean,
  required?: boolean,
  requiredElement: React.Node,
  onClear?: (value: ValueType) => void,
  setValue: SetStateCallbackType<ValueType>,
  value: ValueType
|};

export default ({
  allowMultiRow = false,
  controlTag: DefaultControl = 'input',
  controlType = INPUT_TYPES.TEXT,
  defaultMaximum = 1,
  defaultValue: paramsDefaultValue,
  sanitizeValueForControl = defaultSanitizeValueForControl,
  style: { key, styles } = {}
}: AsControlParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const AsControl = React.forwardRef(
      (
        {
          alwaysVisibleRows = 1,
          autofocus,
          canClear,
          className,
          errors,
          focus: initialFocus,
          helper,
          hints,
          name,
          onBlur,
          onChange,
          onClear,
          onEnter,
          onFocus,
          onKeyDown,
          required,
          rows = 1,
          value: propsValue,
          warnings,
          ...props
        }: AsControlType,
        forwardedRef: ReactForwardedRefType
      ) => {
        const {
          disabled,
          maximum = defaultMaximum,
          readonly
        } = props;

        const Control = React.useMemo(() => allowMultiRow && controlType === INPUT_TYPES.TEXT && rows > 1 ? 'textarea' : DefaultControl, [controlType, rows]);

        const controlRef = React.useRef(null);

        const {
          handleOnChange,
          handleOnClear,
          multiple,
          sanitizedControlValue,
          setValue,
          value
        } = useControlValue({
          canClear,
          maximum,
          onChange,
          onClear,
          paramsDefaultValue,
          readonly,
          sanitizeValueForControl,
          value: propsValue
        });

        const controlName = React.useMemo(() => `${name || guid()}${controlType === INPUT_TYPES.FILE && multiple ? '[]' : ''}`, [multiple, name]);

        const {
          focus,
          handleOnBlur,
          handleOnFocus: handleOnFocusIntermediate
        } = useControlFocus({
          focus: initialFocus,
          onBlur,
          onFocus
        });

        const {
          handleOnFocus,
          messageElement,
          setErrors,
          setHelper,
          setHints,
          setWarnings
        } = useControlMessages({
          errors,
          handleOnFocus: handleOnFocusIntermediate,
          helper,
          hints,
          warnings
        });

        const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLElement>) => {
          const { keyCode } = e;

          if ([KEY_CODES.ENTER].includes(keyCode) && onEnter) {
            onEnter(e);
          }
          if (onKeyDown) {
            onKeyDown(e);
          }
        }, [onEnter, onKeyDown]);

        const controlElement = React.useMemo(() => (
          <Control
            ref={controlRef}
            className={GLOBAL_CLASS_NAMES.CONTROL}
            autoFocus={autofocus}
            checked={[INPUT_TYPES.CHECKBOX, INPUT_TYPES.RADIO].includes(controlType) ? Boolean(value) : undefined}
            disabled={disabled}
            {...([INPUT_TYPES.FILE].includes(controlType) && multiple ? { multiple: 'multiple' } : {})}
            name={controlName}
            onBlur={disabled ? undefined : handleOnBlur}
            onChange={(disabled || readonly) ? undefined : handleOnChange}
            onFocus={disabled ? undefined : handleOnFocus}
            onKeyDown={(disabled || readonly) ? undefined : handleOnKeyDown}
            readOnly={readonly}
            type={controlType}
            value={sanitizedControlValue}
          />
        ), [controlName, disabled, handleOnBlur, handleOnChange, handleOnFocus, handleOnKeyDown, readonly, sanitizedControlValue, value]);

        const requiredElement = React.useMemo(() => required ? (
          <Required className={GLOBAL_CLASS_NAMES.REQUIRED} />
        ) : null, [required]);

        return (
          <WrappedComponent
            ref={forwardedRef}
            className={cx(
              className,
              styles && key && cx.control(styles, key, { disabled, errors, focus, helper, hints, readonly, required, warnings })
            )}
            alwaysVisibleRows={allowMultiRow ? alwaysVisibleRows : 1}
            controlRef={controlRef}
            controlElement={controlElement}
            messageElement={messageElement}
            multiple={multiple}
            required={required}
            requiredElement={requiredElement}
            onClear={(disabled || readonly) ? undefined : handleOnClear}
            rows={allowMultiRow ? rows : 1}
            setErrors={setErrors}
            setHelper={setHelper}
            setHints={setHints}
            setValue={setValue}
            setWarnings={setWarnings}
            value={value}
            {...props}
          />
        );
      }
    );

    return AsControl;
  };