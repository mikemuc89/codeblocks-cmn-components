/* @flow */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { type HocWrappedComponentType, type ReactForwardedRefType } from '@omnibly/codeblocks-cmn-types';
import { GLOBAL_CLASS_NAMES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { escape, mergeRefs, setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';
import ParentCacheContext from '../../contexts/ParentCacheContext';
import Layer, { LayerContentContext, LAYER_MODES } from '../../components/Layer';
import CONFIG from '../../components/style-config';
import useAnimation, { ENTER_ANIMATIONS, EXIT_ANIMATIONS } from '../../hooks/useAnimation';
import useKeyboardSubmit from '../../hooks/useKeyboardSubmit';
import useOpenState from '../../hooks/useOpenState';
import useWindowDimensions from '../../hooks/useWindowDimensions';
import { ScrollingContext } from './AsContainer';

export { LAYER_MODES };

export const LayerContext = React.createContext({});

const { breakpoints: BREAKPOINTS } = CONFIG;

const LAYER_SHIFT = 8;
const LAYER_MARGIN = 16;
const MIN_MOBILE_TOP_SPACE = 40;

const enterAnimation = [{
  duration: 500,
  kind: ENTER_ANIMATIONS.EXPAND
}, {
  duration: 500,
  kind: ENTER_ANIMATIONS.FADE_IN
}];

const exitAnimation = [{
  duration: 500,
  kind: EXIT_ANIMATIONS.COLLAPSE
}, {
  duration: 500,
  kind: EXIT_ANIMATIONS.FADE_OUT
}];

export type ParamsType = {|
  dimmerMode?: $Values<typeof DIMMER_MODES>,
  filter?: boolean,
  height?: number,
  kind?: $Values<typeof LAYER_KINDS>,
  mode?: $Values<typeof LAYER_MODES>,
  showCoverElementInsideLayer?: boolean,
  width?: number
|};
export type WithLayerType = {|
  allowFuzzySearch?: boolean,
  children: React.Node,
  disabled?: boolean,
  forceSwitchSidesVertically?: boolean,
  forceSwitchSidesHorizontally?: boolean,
  height?: number,
  noResultsElement: React.Node,
  open?: boolean,
  readonly?: boolean,
  sustainOnPick?: boolean,
  width?: number
|};
export type WithLayerExtensionType = {|
  coverElement?: React.Node,
  layerElement: React.Node
|};

const { DIMMER_MODES, KINDS: LAYER_KINDS } = Layer;
export { DIMMER_MODES, LAYER_KINDS, LayerContentContext };

const LAYER_ROOT_ID = 'layerRoot';

export default ({
    dimmerMode = Layer.DIMMER_MODES.MOBILE,
    filter = false,
    height: defaultHeight = 240,
    kind = Layer.KINDS.ITEMS,
    layerClassName,
    mode = LAYER_MODES.VERTICAL,
    showCoverElementInsideLayer = false,
	  width: defaultWidth = 240
}: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const preparePosition = (style: Object) => ({
      bottom: undefined,
      display: undefined,
      height: undefined,
      left: undefined,
      maxHeight: undefined,
      maxWidth: undefined,
      position: undefined,
      right: undefined,
      top: undefined,
      width: undefined,
      ...style
    });

    const WithLayer = React.forwardRef(
      (
        {
          allowFuzzySearch = true,
          forceSwitchSidesVertically,
          forceSwitchSidesHorizontally,
          height = defaultHeight,
          heightByItems,
		      input,
          noResultsElement,
          open: initiallyOpen,
          sustainOnPick,
          width = defaultWidth,
          widthByItems,
          ...props
        }: WithLayerType,
        forwardedRef: ReactForwardedRefType
      ) => {
        const { children, disabled, readonly } = props;

        const coverRef = React.useRef(null);
        const layerElementRef = React.useRef();
        const inputParentElementRef = React.useRef();
        const closeHandlerAdded = React.useRef(false);
        const layerRoot = document.getElementById(LAYER_ROOT_ID) || document.body;

        const { cache = '' } = React.useContext(ParentCacheContext);

        const onLayerOpen = React.useCallback(() => {
          const el = inputParentElementRef.current;
          if (el) {
            const inputEl = el.querySelector('input:not([type="hidden"])');
            if (inputEl) {
              inputEl.focus();
            }
          }
        }, []);

        const onLayerClose = React.useCallback(() => setVisible(false), [setVisible]);

        const { hide, open, show } = useOpenState({ disabled: disabled || readonly, open: initiallyOpen, onClose: onLayerClose, onOpen: onLayerOpen });
        const { animationElementRef, setVisible } = useAnimation({ enter: enterAnimation, exit: exitAnimation, visible: open });
        const { height: windowHeight, width: windowWidth } = useWindowDimensions();
        const { left: parentScrollLeft, top: parentScrollTop } = React.useContext(ScrollingContext);

        const [cover, setCover] = React.useState(input || null);

        React.useLayoutEffect(() => {
          const el = coverRef.current;
          const layerEl = layerElementRef.current;

          if (el && layerEl && open) {
            if (mode === LAYER_MODES.FAB) {
              setElementStyle(layerEl, preparePosition({
                position: 'fixed',
                bottom: 0,
                right: 0,
                maxHeight: '100%',
                width: 'auto'
              }));
              setVisible(true);
              return;
            }

            if (windowWidth < BREAKPOINTS.laptop.from) {
              setElementStyle(layerEl, preparePosition({
                position: 'fixed',
                bottom: 0,
                left: 0,
                maxHeight: unitize(windowHeight - MIN_MOBILE_TOP_SPACE),
                right: 0
              }));
              setVisible(true);
              return;
            }
            
            const layerInputEl = layerEl.querySelector(`.${GLOBAL_CLASS_NAMES.LAYER_INPUT}`);
            const layerItemsEl = layerEl.querySelector(`.${GLOBAL_CLASS_NAMES.LAYER_ITEMS}`);
            const firstChild = layerItemsEl && Array.from(layerItemsEl.children)[0];

            const computedHeight = (() => {
              if (!open || !heightByItems || windowWidth < BREAKPOINTS.laptop.from) {
                return height;
              }
              const afterHeight = (layerItemsEl && unitize.revert(getComputedStyle(layerItemsEl.parentElement, ':after').height)) || 0;
              const beforeHeight = (layerItemsEl && unitize.revert(getComputedStyle(layerItemsEl.parentElement, ':before').height)) || 0;
              return afterHeight + beforeHeight + (layerInputEl ? layerInputEl.clientHeight : 0) + (firstChild ? firstChild.clientHeight * heightByItems : 0);
            })();

            const computedWidth = (() => {
              if (!open || !widthByItems || windowWidth < BREAKPOINTS.laptop.from) {
                return width;
              }
              return (layerInputEl ? layerInputEl.clientWidth : 0) + (firstChild ? firstChild.clientWidth * widthByItems : 0);
            })();

            const { height: elHeight, left, top, width: elWidth } = el.getBoundingClientRect();
      
            const windowScrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
            const windowScrollTop = window.pageYOffset || document.documentElement.scrollTop;

            const topY = windowScrollTop + top - parentScrollTop;
            const bottomY = topY + elHeight;
            const leftX = windowScrollLeft + left - parentScrollLeft;
            const rightX = leftX + elWidth;

            setElementStyle(layerEl, {
              [LAYER_MODES.HORIZONTAL]: () => {
                const switchSides = forceSwitchSidesHorizontally || (leftX + computedWidth + LAYER_MARGIN - LAYER_SHIFT > windowWidth && rightX + LAYER_SHIFT - computedWidth - LAYER_MARGIN > 0);

                return preparePosition({
                  position: 'absolute',
                  height: unitize(elHeight + 2 * LAYER_SHIFT),
                  maxWidth: unitize(width),
                  top: unitize(topY - LAYER_SHIFT),
                  ...(switchSides
                    ? {
                        right: unitize(rightX - LAYER_SHIFT)
                      }
                    : {
                        left: unitize(leftX - LAYER_SHIFT)
                      })
                });
              },
              [LAYER_MODES.ORIGIN]: () => {
                const switchSidesHorizontally = forceSwitchSidesHorizontally || (leftX + computedWidth + LAYER_MARGIN - LAYER_SHIFT > windowWidth && rightX + LAYER_SHIFT - computedWidth - LAYER_MARGIN > 0);
                const switchSidesVertically = forceSwitchSidesVertically || (topY + computedHeight + LAYER_MARGIN - LAYER_SHIFT > windowHeight && bottomY + LAYER_SHIFT - computedHeight - LAYER_MARGIN > 0);

                return preparePosition({
                  position: 'absolute',
                  maxHeight: unitize(computedHeight),
                  width: unitize(width),
                  ...(switchSidesHorizontally
                    ? {
                        right: unitize(rightX - LAYER_SHIFT)
                      }
                    : {
                        left: unitize(leftX - LAYER_SHIFT)
                      }),
                  ...(switchSidesVertically
                    ? {
                        bottom: unitize(bottomY - LAYER_SHIFT)
                      }
                    : {
                        top: unitize(topY - LAYER_SHIFT)
                      }),
                });
              },
              [LAYER_MODES.VERTICAL]: () => {
                const switchSides = forceSwitchSidesVertically || (topY + computedHeight + LAYER_MARGIN - LAYER_SHIFT > windowHeight && bottomY + LAYER_SHIFT - computedHeight - LAYER_MARGIN > 0);

                return preparePosition({
                  position: 'absolute',
                  left: unitize(leftX - LAYER_SHIFT),
                  maxHeight: unitize(computedHeight),
                  width: unitize(elWidth + 2 * LAYER_SHIFT),
                  ...(switchSides
                    ? {
                        bottom: unitize(bottomY - LAYER_SHIFT)
                      }
                    : {
                        top: unitize(topY - LAYER_SHIFT)
                      }),
                });
              }
            }[mode || LAYER_MODES.VERTICAL]());
            setVisible(true);
            return;
          }

          if (layerEl) {
            setElementStyle(layerEl, {
              display: 'none'
            });
            setVisible(false);
          }
        }, [forceSwitchSidesHorizontally, forceSwitchSidesVertically, height, heightByItems, open, setVisible, width, widthByItems, windowHeight, windowWidth]);
      
        const onKeyOpen = useKeyboardSubmit(show);

        React.useEffect(() => {
          if (open && !closeHandlerAdded.current) {
            window.addEventListener('click', hide);
            closeHandlerAdded.current = true;
          } else if (!open && closeHandlerAdded.current) {
            window.removeEventListener('click', hide);
            closeHandlerAdded.current = false;
          }
        }, [hide, open]);

        const coverElement = React.useMemo(() => cover ? (
          <div ref={coverRef} className={GLOBAL_CLASS_NAMES.LAYER_COVER} onClick={show} onKeyDown={onKeyOpen}>
            {cover}
          </div>
        ) : null, [cover, show, onKeyOpen]);

        const inputInsideLayerElement = React.useMemo(() => input ? (
          <div ref={inputParentElementRef} className={GLOBAL_CLASS_NAMES.LAYER_INPUT}>
            {React.cloneElement(input, {})}
          </div>
        ) : null, [input]);

        const sanitizedCache = React.useMemo(() => open ? escape(cache.toLowerCase()) : '', [cache, open]);
        const fullRegex = React.useMemo(() => new RegExp(`^${sanitizedCache}$`, 'gi'), [sanitizedCache]);
        const startingRegex = React.useMemo(() => new RegExp(`^${sanitizedCache}`, 'gi'), [sanitizedCache]);
        const containingRegex = React.useMemo(() => new RegExp(`${sanitizedCache}`, 'gi'), [sanitizedCache]);
        const fuzzyRegex = React.useMemo(() => new RegExp(`${sanitizedCache.split('').join('.*')}`, 'gi'), [sanitizedCache]);

        const filteredChildren = (() => {
          if (filter) {
            const { all = [], containing = [], full = [], fuzzy = [], starting = [] } = (open && sanitizedCache) ? React.Children.toArray(children).reduce((result, child) => {
              if (fullRegex.test(child.props.children)) {
                return {
                  ...result,
                  full: [
                    ...result.full,
                    child
                  ]
                }
              }
              if (startingRegex.test(child.props.children)) {
                return {
                  ...result,
                  starting: [
                    ...result.starting,
                    child
                  ]
                }
              }
              if (containingRegex.test(child.props.children)) {
                return {
                  ...result,
                  containing: [
                    ...result.containing,
                    child
                  ]
                }
              }
              if (allowFuzzySearch && fuzzyRegex.test(child.props.children)) {
                return {
                  ...result,
                  fuzzy: [
                    ...result.fuzzy,
                    child
                  ]
                }
              }
              return result;
            }, { all: [], containing: [], full: [], fuzzy: [], starting: [] }) : { all: children };
    
            return [
              ...all,
              ...full,
              ...starting,
              ...containing,
              ...fuzzy
            ];            
          }
          return children;
        })();

        const noResults = filter ? Array.isArray(filteredChildren) ? filteredChildren.length === 0 : Boolean(filteredChildren) : false;

        const layerPortalContent = (
          <Layer
            ref={mergeRefs(layerElementRef, animationElementRef)}
            mode={mode}
            dimmerMode={dimmerMode}
            kind={kind}
          >
            {inputInsideLayerElement}
            <div className={GLOBAL_CLASS_NAMES.LAYER_ITEMS}>
              {noResults ? noResultsElement : filteredChildren}
            </div>
          </Layer>
        );

        const layerElement = open ? ReactDOM.createPortal(layerPortalContent, layerRoot) : null;

        const contextValue = React.useMemo(() => ({
          hideLayer: hide,
          showLayer: show,
          sustainOnPick
        }), [hide, show, sustainOnPick]);

        return (
          <LayerContext.Provider value={contextValue}>
            <WrappedComponent
              ref={forwardedRef}
              layerElement={layerElement}
              coverElement={coverElement}
              setCover={setCover}
              {...props}
            />
          </LayerContext.Provider>
        );
      }
    );

    return WithLayer;
  };
