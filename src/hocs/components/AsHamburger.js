/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { guid } from '@omnibly/codeblocks-cmn-utils';
import HamburgerRegisterContext from '../../contexts/HamburgerRegisterContext';

export default () => (WrappedComponent: HocWrappedComponentType) => {
  const id = guid();

  const AsHamburger = (props: Object) => {
    const { subscribe, unsubscribe } = React.useContext(HamburgerRegisterContext);

    const renderedComponent = React.useMemo(() => <WrappedComponent {...props} />, [props]);

    React.useEffect(() => {
      subscribe(id, renderedComponent);

      return () => {
        unsubscribe(id);
      };
    }, [renderedComponent, subscribe, unsubscribe]);

    return null;
  };

  return AsHamburger;
};
