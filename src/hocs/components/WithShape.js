/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type ReactForwardedRefType } from '@omnibly/codeblocks-cmn-types';
import { SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';
import CONFIG from '../../components/style-config';

const { border: BORDER } = CONFIG;

export type ParamsType = {|
  defaultShape?: $Values<typeof SHAPES>
|};
export type WithShapeType = {|
  shape?: $Values<typeof SHAPES>
|};
export type WithShapeExtensionType = {|
  shapeElementRef: ReactRefType<HTMLElement>
|};

export default ({ defaultShape = SHAPES.SQUARE }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithShape = React.forwardRef(
      ({ shape = defaultShape, ...props }: WithShapeType, forwardedRef: ReactForwardedRefType) => {
        const shapeElementRef = React.useRef(null);

        const style = React.useMemo(() => ({
          overflow: 'hidden',
          ...{
            [SHAPES.CIRCLE]: () => ({
              borderRadius: '50%'
            }),
            [SHAPES.ROUNDED]: () => ({
              borderRadius: '20%'
            }),
            [SHAPES.SQUARE]: () => ({
              borderRadius: unitize(BORDER.radius)
            })
          }[shape]()
        }), [shape]);

        React.useEffect(() => {
          if (shapeElementRef.current) {
            setElementStyle(shapeElementRef.current, style);
          }
        }, [style]);

        return <WrappedComponent shapeElementRef={shapeElementRef} ref={forwardedRef} {...props} />;
      }
    );

    return WithShape;
  };
