/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import useMedia from '../../hooks/useMedia';

type SingleDevicePropsType = Object;
export type WithRwdPropsType = {|
  rwdProps?: {
    desktopBig?: SingleDevicePropsType,
    desktop?: SingleDevicePropsType,
    laptop?: SingleDevicePropsType,
    tablet?: SingleDevicePropsType,
    phoneL?: SingleDevicePropsType,
    phoneM?: SingleDevicePropsType,
    phoneS?: SingleDevicePropsType
  }
|};

export default () => (WrappedComponent: HocWrappedComponentType) => {
  const WithRwdProps = React.forwardRef(
    (
      { rwdProps: { desktop, desktopBig, laptop, phoneL, phoneM, phoneS, tablet } = {}, ...props }: WithRwdPropsType,
      forwardedRef: ReactForwardedRefType
    ) => {
      const { breakpoint } = useMedia();
      const rwdProps = React.useMemo(
        () =>
          ({
            desktop,
            desktopBig,
            laptop,
            phoneL,
            phoneM,
            phoneS,
            tablet
          }[breakpoint]),
        [breakpoint, desktop, desktopBig, laptop, phoneL, phoneM, phoneS, tablet]
      );

      return <WrappedComponent ref={forwardedRef} {...props} {...rwdProps} />;
    }
  );

  return WithRwdProps;
};
