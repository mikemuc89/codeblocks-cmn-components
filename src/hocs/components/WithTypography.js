/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type ReactForwardedRefType } from '@omnibly/codeblocks-cmn-types';
import { CSS_ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { FONT_WEIGHTS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';

const FONT_WEIGHT_ENUM_TO_STYLE = Object.freeze({
  [FONT_WEIGHTS.XS]: 100,
  [FONT_WEIGHTS.S]: 300,
  [FONT_WEIGHTS.M]: 500,
  [FONT_WEIGHTS.L]: 700,
  [FONT_WEIGHTS.XL]: 900
});

export type WithTypographyParamsType = {|
  defaultFontSize?: number,
  defaultFontWeight?: $Values<typeof FONT_WEIGHTS>,
  defaultLineHeight?: number
|};
export type WithTypographyType = {|
  align: $Values<typeof CSS_ALIGN_TYPES>,
  color?: string,
  fontSize?: number,
  fontWeight?: $Values<typeof FONT_WEIGHTS>,
  lineHeight?: number,
  oneline?: boolean
|};
export type WithTypographyExtensionType = {|
  positionElementRef: ReactRefType<HTMLElement>
|};

export default ({ defaultFontSize, defaultFontWeight, defaultLineHeight }: WithTypographyParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithTypography = React.forwardRef(
      (
        {
          align,
          color,
          fontSize = defaultFontSize,
          fontWeight = defaultFontWeight,
          lineHeight = defaultLineHeight,
          oneline,
          ...props
        }: WithTypographyType,
        forwardedRef: ReactForwardedRefType
      ) => {
        const typographyElementRef = React.useRef(null);

        const styles = React.useMemo(() => ({
          ...(align ? { textAlign: align } : {}),
          ...(color ? { color } : {}),
          ...(fontSize ? { fontSize: unitize(fontSize) } : {}),
          ...(fontWeight ? { fontWeight: FONT_WEIGHT_ENUM_TO_STYLE[fontWeight] } : {}),
          ...(lineHeight ? { lineHeight: unitize(lineHeight) } : {}),
          ...(oneline ? { overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' } : {})
        }), [align, fontSize, fontWeight, lineHeight, oneline]);

        React.useEffect(() => {
          if (typographyElementRef.current) {
            setElementStyle(typographyElementRef.current, styles);
          }
        }, [styles]);

        return <WrappedComponent typographyElementRef={typographyElementRef} ref={forwardedRef} {...props} />;
      }
    );

    return WithTypography;
  };
