/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import styles from './WithRwdVisibility.scss';

export type WithRwdVisibilityType = {|
  className?: string,
  hideAboveDesktop?: boolean,
  hideAboveLaptop?: boolean,
  hideAboveTablet?: boolean,
  hideAbovePhoneL?: boolean,
  hideAbovePhoneM?: boolean,
  hideAbovePhoneS?: boolean,
  hideBelowDesktopBig?: boolean,
  hideBelowDesktop?: boolean,
  hideBelowLaptop?: boolean,
  hideBelowTablet?: boolean,
  hideBelowPhoneL?: boolean,
  hideBelowPhoneM?: boolean
|};

export default () => (WrappedComponent: HocWrappedComponentType) => {
  const WithRwdVisibility = React.forwardRef(
    (
      {
        className,
        hideAboveDesktop,
        hideAboveLaptop,
        hideAboveTablet,
        hideAbovePhoneL,
        hideAbovePhoneM,
        hideAbovePhoneS,
        hideBelowDesktopBig,
        hideBelowDesktop,
        hideBelowLaptop,
        hideBelowTablet,
        hideBelowPhoneL,
        hideBelowPhoneM,
        ...props
      }: WithRwdVisibilityType,
      forwardedRef: ReactForwardedRefType
    ) => (
      <WrappedComponent
        className={cx(
          className,
          hideAboveDesktop && styles.Component__HideAboveDesktop,
          hideAboveLaptop && styles.Component__HideAboveLaptop,
          hideAboveTablet && styles.Component__HideAboveTablet,
          hideAbovePhoneL && styles.Component__HideAbovePhoneL,
          hideAbovePhoneM && styles.Component__HideAbovePhoneM,
          hideAbovePhoneS && styles.Component__HideAbovePhoneS,
          hideBelowDesktopBig && styles.Component__HideBelowDesktopBig,
          hideBelowDesktop && styles.Component__HideBelowDesktop,
          hideBelowLaptop && styles.Component__HideBelowLaptop,
          hideBelowTablet && styles.Component__HideBelowTablet,
          hideBelowPhoneL && styles.Component__HideBelowPhoneL,
          hideBelowPhoneM && styles.Component__HideBelowPhoneM
        )}
        ref={forwardedRef}
        {...props}
      />
    )
  );

  return WithRwdVisibility;
};
