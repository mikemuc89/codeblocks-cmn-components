/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type ReactForwardedRefType } from '@omnibly/codeblocks-cmn-types';
import { mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import useEventListener from '../../hooks/useEventListener';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundParamsType, type WithBackgroundType } from './WithBackground';

export const ScrollingContext = React.createContext({ left: 0, top: 0 });

export type AsContainerParamsType = {|
  ...WithBackgroundParamsType
|};
export type AsContainerType = {|
  ...WithBackgroundType,
  backgroundElementRef: ReactRefType<HTMLElement>
|};
export type AsContainerExtensionType = {|
  ...WithBackgroundType,
  containerElementRef: ReactRefType<HTMLElement>
|};

export default ({ ...params }) => (WrappedComponent: HocWrappedComponentType) => {
  const HocAwareComponent = WithBackground(params)(WrappedComponent);

  const AsContainer = React.forwardRef(({ backgroundElementRef, ...props }: Object, forwardedRef: ReactForwardedRefType) => {
    const containerElementRef = React.useRef(null);
    const { left: parentScrollLeft, top: parentScrollTop } = React.useContext(ScrollingContext);

    const [left, setLeft] = React.useState(parentScrollLeft);
    const [top, setTop] = React.useState(parentScrollTop);

    const onScroll = React.useCallback((e) => {
      const { target: { scrollLeft, scrollTop } = {} } = e;
      setLeft(parentScrollLeft + scrollLeft);
      setTop(parentScrollTop + scrollTop);
    }, [parentScrollLeft, parentScrollTop, setLeft, setTop]);

    useEventListener('scroll', onScroll, containerElementRef.current);

    const contextValue = React.useMemo(() => ({ left, top }), [left, top]);

    return (
      <ScrollingContext.Provider value={contextValue}>
        <HocAwareComponent ref={forwardedRef} containerElementRef={mergeRefs(backgroundElementRef, containerElementRef)} {...props} />
      </ScrollingContext.Provider>
    );
  });

  return AsContainer;
};
