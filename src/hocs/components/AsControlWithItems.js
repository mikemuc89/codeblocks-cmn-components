/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type SetStateCallbackType, type ReactRefType, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { GLOBAL_CLASS_NAMES, INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid } from '@omnibly/codeblocks-cmn-utils';
import Message from '../../components/Message';
import Required from '../../components/Required';
import useControlValue from '../../hooks/useControlValue';
import useControlFocus from '../../hooks/useControlFocus';
import useControlMessages from '../../hooks/useControlMessages';

type GroupItemContextType = {
  handleOnChange?: () => void
};

export const GroupItemContext = React.createContext({});

const MULTI_CHANGE_TYPES = Object.freeze({
  ALWAYS_ADD: 'AsControlWithItems.MULTI_CHANGE_TYPES.ALWAYS_ADD', // always add, even if exists
  TOGGLE: 'AsControlWithItems.MULTI_CHANGE_TYPES.TOGGLE' // add to array if not present, remove if exists
});

const SINGLE_CHANGE_TYPES = Object.freeze({
  ALWAYS_NEW: 'AsControlWithItems.SINGLE_CHANGE_TYPES.ALWAYS_ADD', // always switch to new value
  TOGGLE: 'AsControlWithItems.SINGLE_CHANGE_TYPES.TOGGLE' // reset if same as exists, change if different
});

const defaultSanitizeValueForControl = (value: string) => value;

export type AsControlWithItemsParamsType<ValueType> = {|
  changeTypeMulti?: $Values<typeof MULTI_CHANGE_TYPES>,
  changeTypeSingle?: $Values<typeof SINGLE_CHANGE_TYPES>,
  controlTag?: string,
  controlType?: string,
  defaultMaximum?: number,
  defaultValue: ValueType,
  hasCache?: boolean,
  hasFilter?: boolean,
  hasLayer?: boolean,
  hasSearch?: boolean,
  sanitizeValueForControl?: (value: ValueType, { multiple: boolean }) => string,
  style?: { key: string, styles: Object }
|};
export type AsControlWithItemsType<ValueType> = {|
  ...WithMessagesType,
  autofocus?: boolean,
  canClear?: boolean,
  children?: React.Node,
  disabled?: boolean,
  focus?: boolean,
  maximum?: number,
  name?: string,
  onBlur?: (e: SyntheticFocusEvent<HTMLElement>) => void,
  onChange?: (value: ValueType) => void,
  onClear?: (value: ValueType) => void,
  onEnter?: (value: ValueType) => void,
  onFocus?: (e: SyntheticFocusEvent<HTMLElement>) => void,
  readonly?: boolean,
  value: ValueType
|};
export type AsControlWithItemsExtensionType<ValueType> = {|
  controlElement: React.Node,
  controlRef: ReactRefType<HTMLElement>,
  messageElement: React.Node,
  requiredElement: React.Node,
  onClear?: (value: ValueType) => void,
  setValue: SetStateCallbackType<ValueType>,
  value: ValueType
|};

export const ControlWithItemsContext = React.createContext({});

export default Object.assign(({
  changeTypeMulti = MULTI_CHANGE_TYPES.ALWAYS_ADD,
  changeTypeSingle = SINGLE_CHANGE_TYPES.ALWAYS_NEW,
  controlTag: Control = 'input',
  controlType = INPUT_TYPES.HIDDEN,
  defaultMaximum = 1,
  defaultValue: paramsDefaultValue,
  hasCache,
  hasFilter,
  hasSearch,
  sanitizeValueForControl = defaultSanitizeValueForControl,
  style: { key, styles } = {},
  ...params
}: AsControlWithItemsParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const ItemControl = 'input';

    const AsControlWithItems = React.forwardRef(
      (
        {
          autofocus,
          canClear,
          children,
          className,
          errors,
          focus: initialFocus,
          helper,
          hints,
          name,
          onBlur,
          onChange,
          onClear,
          onFocus,
          required,
          value: propsValue,
          warnings,
          ...props
        }: AsControlWithItemsType,
        forwardedRef: ReactForwardedRefType
      ) => {
        const {
          disabled,
          maximum = defaultMaximum,
          readonly
        } = props;

        const controlRef = React.useRef(null);
        const itemsControlType = React.useMemo(() => maximum > 1 ? INPUT_TYPES.CHECKBOX : INPUT_TYPES.RADIO, [maximum]);
        const controlName = React.useMemo(() => name || guid(), [name]);
        const itemsControlName = React.useMemo(() => `item.${controlName}`, [controlName]);

        const {
          handleOnChange,
          handleOnClear,
          multiple,
          sanitizedControlValue,
          setValue,
          value
        } = useControlValue({
          canClear,
          maximum,
          onChange,
          onClear,
          paramsDefaultValue,
          readonly,
          sanitizeValueForControl,
          value: propsValue
        });

        const {
          focus,
          handleOnBlur,
          handleOnFocus: handleOnFocusIntermediate
        } = useControlFocus({
          focus: initialFocus,
          onBlur,
          onFocus
        });

        const {
          handleOnFocus,
          messageElement,
          setErrors,
          setHelper,
          setHints,
          setWarnings
        } = useControlMessages({
          errors,
          handleOnFocus: handleOnFocusIntermediate,
          helper,
          hints,
          warnings
        });

        const controlElement = React.useMemo(() => (
          <Control
            ref={controlRef}
            className={GLOBAL_CLASS_NAMES.CONTROL}
            autoFocus={autofocus}
            checked={[INPUT_TYPES.CHECKBOX, INPUT_TYPES.RADIO].includes(controlType) ? Boolean(value) : undefined}
            disabled={disabled}
            name={controlName}
            onChange={(disabled || readonly) ? undefined : handleOnChange}
            readOnly={readonly}
            type={controlType}
            value={sanitizedControlValue}
          />
        ), [controlName, disabled, handleOnBlur, handleOnChange, handleOnFocus, readonly, sanitizedControlValue, value]);

        const requiredElement = React.useMemo(() => required ? (
          <Required className={GLOBAL_CLASS_NAMES.REQUIRED} />
        ) : null, [required]);

        const handleOnChangeByItemId = React.useCallback((id: string) => {
          setValue((oldValue) => {
            if (multiple) {
              if (changeTypeMulti === MULTI_CHANGE_TYPES.ALWAYS_ADD) {
                return [...oldValue, id];
              }
              if (changeTypeMulti === MULTI_CHANGE_TYPES.TOGGLE) {
                return oldValue.includes(id) ? oldValue.filter((item: string) => item !== id) : [...oldValue, id];
              }
              return [id];
            }

            if (changeTypeSingle === SINGLE_CHANGE_TYPES.ALWAYS_NEW) {
              return id;
            }
            if (changeTypeSingle === SINGLE_CHANGE_TYPES.TOGGLE) {
              return oldValue === id ? null : id;
            }

            return id;
          });
        }, [multiple, setValue]);

        const handleOnRemoveByItemId = React.useCallback((id: string) => {
          setValue((oldValue) => {
            if (multiple) {
              return oldValue.filter((item: string) => item !== id);
            }
            return null;
          });
        }, [multiple, setValue]);

        const handleOnItemKeyDown = React.useCallback((e: SyntheticEvent<HTMLInputElement>) => {
          const { keyCode, target: { id } } = e;

          if ([KEY_CODES.ENTER].includes(keyCode)) {
            handleOnChangeByItemId(id);
          }
        }, [handleOnChangeByItemId]);

        const itemControlElement = React.useMemo(() => (
          <ItemControl
            className={GLOBAL_CLASS_NAMES.ITEM_CONTROL}
            disabled={disabled}
            name={itemsControlName}
            onKeyDown={handleOnItemKeyDown}
            readOnly={readonly}
            type={itemsControlType}
          />
        ), [disabled, itemsControlName, itemsControlType, readonly]);

        const contextValue = React.useMemo(() => ({
          controlSelector: `${ItemControl}.${GLOBAL_CLASS_NAMES.ITEM_CONTROL}`,
          itemControlElement,
          multiple,
          onChangeByItemId: handleOnChangeByItemId,
          onRemoveByItemId: handleOnRemoveByItemId,
          setValue,
          value
        }), [handleOnChangeByItemId, handleOnRemoveByItemId, itemControlElement, multiple, setValue, value]);

        return (
          <ControlWithItemsContext.Provider value={contextValue}>
            <WrappedComponent
              ref={forwardedRef}
              className={cx(
                className,
                styles && key && cx.control(styles, key, { disabled, errors, focus, helper, hints, readonly, required, warnings })
              )}
              controlRef={controlRef}
              controlElement={controlElement}
              messageElement={messageElement}
              multiple={multiple}
              requiredElement={requiredElement}
              onBlur={disabled ? undefined : handleOnBlur}
              onClear={disabled ? undefined : handleOnClear}
              onFocus={disabled ? undefined : handleOnFocus}
              setErrors={setErrors}
              setHelper={setHelper}
              setHints={setHints}
              setValue={setValue}
              setWarnings={setWarnings}
              value={value}
              {...props}
            >
              {children}
            </WrappedComponent>
          </ControlWithItemsContext.Provider>
        );
      }
    );

    return AsControlWithItems;
  },
  {
    MULTI_CHANGE_TYPES,
    SINGLE_CHANGE_TYPES
  }
);
