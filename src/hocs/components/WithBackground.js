/* @flow */
import * as React from 'react';
import BackgroundColorContext from '../../contexts/BackgroundColorContext';
import { type HocWrappedComponentType, type ReactForwardedRefType } from '@omnibly/codeblocks-cmn-types';
import { setElementStyle } from '@omnibly/codeblocks-cmn-utils';

export type WithBackgroundParamsType = {|
  defaultBackgroundColor?: string,
  defaultBackgroundColorFocus?: string,
  defaultBackgroundColorHover?: string
|};
export type WithBackgroundType = {|
  backgroundColor?: string,
  backgroundColorFocus?: string,
  backgroundColorHover?: string,
  backgroundImage?: string,
  backgroundImagePosition?: string,
  backgroundImageRepeat?: string,
  backgroundImageSize?: string
|};
export type WithBackgroundExtensionType = {|
  backgroundElementRef: ReactRefType<HTMLElement>
|};

export default ({
  defaultBackgroundColor,
  defaultBackgroundColorFocus,
  defaultBackgroundColorHover
}: WithBackgroundParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithBackground = React.forwardRef(
      (
        {
          backgroundColor = defaultBackgroundColor,
          backgroundColorFocus = defaultBackgroundColorFocus,
          backgroundColorHover = defaultBackgroundColorHover,
          backgroundImage,
          backgroundImagePosition,
          backgroundImageRepeat = 'no-repeat',
          backgroundImageSize = '100%',
          ...props
        }: WithBackgroundType,
        forwardedRef: ReactForwardedRefType
      ) => {
        const {
          backgroundColor: parentBackgroundColor,
          backgroundColorFocus: parentBackgroundColorFocus,
          backgroundColorHover: parentBackgroundColorHover
        } = React.useContext(BackgroundColorContext);
        const backgroundElementRef = React.useRef(null);

        const styles = React.useMemo(() => ({
          ...(backgroundColor ? { backgroundColor } : {}),
          ...(backgroundImage ? {
            backgroundImage: `url('${backgroundImage}')`,
            ...(backgroundImagePosition ? { backgroundPosition: backgroundImagePosition } : {}),
            ...(backgroundImageRepeat ? { backgroundRepeat: backgroundImageRepeat } : {}),
            ...(backgroundImageSize ? { backgroundSize: backgroundImageSize } : {}),
          } : {})
        }), [backgroundColor, backgroundImage, backgroundImagePosition, backgroundImageRepeat, backgroundImageSize]);

        React.useEffect(() => {
          if (backgroundElementRef.current) {
            setElementStyle(backgroundElementRef.current, styles);
          }
        }, [styles]);

        const contextValue = React.useMemo(() => ({
          backgroundColor: backgroundColor || parentBackgroundColor,
          backgroundColorFocus: backgroundColorFocus || parentBackgroundColorFocus,
          backgroundColorHover: backgroundColorHover || parentBackgroundColorHover
        }), [
          backgroundColor,
          backgroundColorFocus,
          backgroundColorHover,
          parentBackgroundColor,
          parentBackgroundColorFocus,
          parentBackgroundColorHover
        ]);

        return (
          <BackgroundColorContext.Provider value={contextValue}>
            <WrappedComponent backgroundElementRef={backgroundElementRef} ref={forwardedRef} {...props} />
          </BackgroundColorContext.Provider>
        );
      }
    );

    return WithBackground;
  };
