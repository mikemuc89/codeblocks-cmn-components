/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type SetStateCallbackType } from '@omnibly/codeblocks-cmn-types';
import { guid } from '@omnibly/codeblocks-cmn-utils';

const MULTI_CHANGE_TYPES = Object.freeze({
  ALWAYS_ADD: 'AsControl.MULTI_CHANGE_TYPES.ALWAYS_ADD', // always add, even if exists
  TOGGLE: 'AsControl.MULTI_CHANGE_TYPES.TOGGLE' // add to array if not present, remove if exists
});

const SINGLE_CHANGE_TYPES = Object.freeze({
  ALWAYS_NEW: 'AsControl.SINGLE_CHANGE_TYPES.ALWAYS_ADD', // always switch to new value
  TOGGLE: 'AsControl.SINGLE_CHANGE_TYPES.TOGGLE' // reset if same as exists, change if different
});

export type ParamsType = {|
  defaultMaximum?: number,
  defaultValue: any,
  generateValueCache: (value: any) => string,
  multiChangeType?: $Values<typeof MULTI_CHANGE_TYPES>,
  parseValue: (value: any) => any,
  singleChangeType?: $Values<typeof SINGLE_CHANGE_TYPES>
|};
export type AsControlType = {|
  disabled?: boolean,
  maximum?: number,
  name?: string,
  onChange?: (value: any) => void,
  readonly?: boolean,
  value: any
|};
export type AsControlExtensionType<ValueType> = {|
  controlValue: ValueType,
  disabled?: boolean,
  multiple: boolean,
  onClear: (value: ValueType) => void,
  readonly?: boolean,
  setControlValue: SetStateCallbackType<ValueType>,
  value: string | ValueType
|};

export default Object.assign(
  ({
      defaultMaximum = 1,
      defaultValue,
      generateValueCache = (value: any) => value,
      multiChangeType = MULTI_CHANGE_TYPES.ALWAYS_ADD,
      parseValue = (value: any) => value,
      singleChangeType = SINGLE_CHANGE_TYPES.ALWAYS_NEW
    }: ParamsType = {}) =>
    (WrappedComponent: HocWrappedComponentType) => {
      const AsControl = React.forwardRef(
        (
          { disabled, maximum = defaultMaximum, name, onChange, readonly, value: propsValue, ...props }: AsControlType,
          forwardedRef: ReactForwardedRefType
        ) => {
          const value = cache === undefined ? propsValue : cache;
          const multiple = React.useMemo(() => maximum > 1, [maximum]);
          const [controlValue, setControlValue] = React.useState(
            multiple ? parseValue(value) || [] : value === undefined ? defaultValue : parseValue(value)
          );
          const inputName = React.useMemo(() => name || guid(), [name]);

          React.useEffect(() => {
            setControlValue(
              multiple
                ? parseValue(value) || []
                : value === undefined
                ? defaultValue
                : value === undefined
                ? defaultValue
                : parseValue(value)
            );
          }, [multiple, setControlValue, value]);

          const onChangeHandler = React.useCallback(
            (value: any) => {
              if (readonly) {
                return;
              }

              const newValue = multiple
                ? {
                    [MULTI_CHANGE_TYPES.ALWAYS_ADD]: () => [...controlValue, value],
                    [MULTI_CHANGE_TYPES.TOGGLE]: () =>
                      controlValue.includes(value)
                        ? controlValue.filter((item: any) => item !== value)
                        : [...controlValue, value]
                  }[multiChangeType]() || value
                : {
                    [SINGLE_CHANGE_TYPES.ALWAYS_NEW]: () => value,
                    [SINGLE_CHANGE_TYPES.TOGGLE]: () => (controlValue === value ? null : value)
                  }[singleChangeType]() || value;

              if (onChange) {
                onChange(newValue);
              }
              setControlValue(newValue);
            },
            [controlValue, multiple, onChange, readonly, setControlValue]
          );

          const onClearHandler = React.useCallback(() => {
            if (!readonly) {
              const newValue = multiple ? [] : '';
              setControlValue(newValue);
              if (onChange) {
                onChange(newValue);
              }
            }
          }, [multiple, onChange, readonly, setControlValue]);

          return (
            <WrappedComponent
              ref={forwardedRef}
              controlValue={controlValue}
              disabled={disabled}
              maximum={maximum}
              multiple={multiple}
              name={inputName}
              onChange={disabled ? undefined : onChangeHandler}
              onClear={disabled ? undefined : onClearHandler}
              readonly={readonly}
              setControlValue={setControlValue}
              value={value}
              {...props}
            />
          );
        }
      );

      return AsControl;
    },
  {
    MULTI_CHANGE_TYPES,
    SINGLE_CHANGE_TYPES
  }
);
