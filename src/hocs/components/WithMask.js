/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';

export type ParamsType = {
  fixedCharacters?: Array<number>,
  mask?: string,
  regex: typeof RegExp
};
export type WithMaskType<ValueType> = {
  controlElement: React.Node,
  controlRef: ReactRefType<HTMLInputElement | HTMLTextAreaElement>,
  disabled?: boolean,
  readonly?: boolean,
  setValue: SetStateCallbackType<ValueType>,
  value: ValueType
};
export type WithMaskExtensionType = {
  controlElement: React.Node
};

export default ({ fixedCharacters = [], mask = '', regex }: ParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const firstEditableCharacter = (() => {
      let n = 0;
      while (fixedCharacters.includes(n)) {
        n++;
      }
      return n;
    })();

    const lastEditableCharacter = (() => {
      let n = mask.length;
      while (fixedCharacters.includes(n - 1)) {
        n--;
      }
      return n;
    })();

    const prepareMaskedValue = (value: string) => {
      if (mask && value) {
        return value.slice(0, mask.length).split('').reduce((result: string, char: string, idx: number) => fixedCharacters.includes(idx) ? result : `${result.slice(0, idx)}${char}${result.slice(idx + 1)}`, mask);
      }
      return mask || value || '';
    };

    const WithMask = React.forwardRef(({ controlElement, ...props }: WithMaskType, forwardedRef: ReactRefType) => {
	    const { controlRef, disabled, readonly, setValue, value = '' } = props;

      const previousSelectionStart = React.useRef(0);
      const previousSelectionEnd = React.useRef(0);
      const previousMaskedValue = React.useRef(prepareMaskedValue(value));

      const maskedValue = React.useMemo(() => prepareMaskedValue(value), [value]);

      const firstEmptyCharacter = React.useMemo(() => {
        let n = value.length;
        while (fixedCharacters.includes(n)) {
          n++;
        }
        return n;
      }, [value]);

      const lastUserCharInValue = React.useMemo(() => {
        let n = value.length;
        while (fixedCharacters.includes(n - 1)) {
          n--;
        }
        return n;
      }, [value]);

      React.useEffect(() => {
        const el = controlRef.current;
        el.selectionStart = previousSelectionStart.current;
        el.selectionEnd = previousSelectionEnd.current;
      }, [maskedValue]);

      const handleOnInput = React.useCallback((e: SyntheticEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const el = controlRef.current;
        const { current: prevSelectionStart } = previousSelectionStart;
        const { current: prevSelectionEnd } = previousSelectionEnd;

        const { inputType } = e.nativeEvent;
        const { selectionEnd, selectionStart, value } = el;

        const lengthDiff = value.length - mask.length;

        const { positionAfterInput, valueAfterInput } = (() => {
          if (['deleteContentBackward', 'deleteContentForward'].includes(inputType)) {
            let deletePosition = selectionStart;

            if (inputType === 'deleteContentBackward') {
              while (fixedCharacters.includes(deletePosition)) {
                deletePosition--;
              }
            } else if (inputType === 'deleteContentForward') {
              while (fixedCharacters.includes(deletePosition + 1)) {
                deletePosition++;
              }
            }
    
            const maskDeleteLength = deletePosition - selectionStart;
    
            const valueAfterInput = `${maskedValue.slice(0, deletePosition)}${mask.slice(deletePosition, deletePosition - lengthDiff - maskDeleteLength)}${lengthDiff === 0 ? maskedValue.slice(selectionStart - lengthDiff - maskDeleteLength) : ''}`;

            return { positionAfterInput: deletePosition, valueAfterInput };
          }

          if (['insertFromPaste', 'insertText'].includes(inputType)) {
            const startIndex = selectionStart - lengthDiff;
            const fixedCharactersLength = fixedCharacters.filter((fixedChar) => fixedChar >= startIndex && fixedChar < selectionStart).length;

            const valueAfterInput = `${maskedValue.slice(0, prevSelectionStart)}${value.slice(prevSelectionStart, selectionStart)}${lengthDiff === 0 ? maskedValue.slice(selectionStart) : ''}`;

            return { positionAfterInput: selectionStart + fixedCharactersLength, valueAfterInput };
          }
        })();

        let newSelection = positionAfterInput;

        if (lengthDiff < 0) {
          while (fixedCharacters.includes(newSelection - 1)) {
            newSelection--;
          }
        } else {
          while (fixedCharacters.includes(newSelection)) {
            newSelection++;
          }
        }

        const clearChars = valueAfterInput.split('').filter((char: string, index: number) => char !== mask[index]);

        const newValueArray = [];
        let i = 0;
        let j = 0;

        while (j < clearChars.length && newValueArray.length < mask.length) {
          if (fixedCharacters.includes(i)) {
            newValueArray.push(mask[i]);
          } else {
            newValueArray.push(clearChars[j]);
            j++;
          }
          i++;
        }

        const newValue = newValueArray.join('');

        if (regex && !newValue.match(regex)) {
          el.value = maskedValue;
          el.selectionStart = previousSelectionStart.current;
          el.selectionEnd = previousSelectionEnd.current;
        } else {
          setValue(newValue);

          el.selectionStart = newSelection;
          el.selectionEnd = newSelection;

          previousSelectionStart.current = el.selectionStart;
          previousSelectionEnd.current = el.selectionEnd;
        }
      }, [controlElement, maskedValue]);

      const setCursorPosition = React.useCallback(() => {
        const el = controlRef.current;
          
        const { selectionStart, selectionEnd } = el;
        const simpleSelection = selectionStart === selectionEnd;
        let newSelectionStart = Math.min(Math.max(selectionStart, firstEditableCharacter), firstEmptyCharacter, lastEditableCharacter); 

        if (newSelectionStart === 0 || (newSelectionStart !== lastEditableCharacter && newSelectionStart > lastUserCharInValue)) {
          while (fixedCharacters.includes(newSelectionStart)) {
            newSelectionStart++;
          }
        }

        let newSelectionEnd = simpleSelection ? newSelectionStart : Math.min(selectionEnd, firstEmptyCharacter, lastEditableCharacter);

        
        while (!simpleSelection && fixedCharacters.includes(newSelectionEnd - 1)) {
          newSelectionEnd--;
        }

        el.selectionStart = newSelectionStart;
        el.selectionEnd = newSelectionEnd;

        previousSelectionStart.current = el.selectionStart;
        previousSelectionEnd.current = el.selectionEnd;
      }, [firstEmptyCharacter, lastUserCharInValue]);

      const adjustCursorPosition = React.useCallback((e: SyntheticEvent) => {
        const el = controlRef.current;

        if (el) {
          const { ctrlKey, keyCode, shiftKey } = e;

          if (
            [
              KEY_CODES.DOWN_ARROW,
              KEY_CODES.LEFT_ARROW,
              KEY_CODES.RIGHT_ARROW,
              KEY_CODES.UP_ARROW,
              KEY_CODES.END,
              KEY_CODES.HOME
            ].includes(keyCode) ||
            (keyCode === KEY_CODES.A && ctrlKey)
          ) {
            e.preventDefault();

            const selectAll = () => {
              if (ctrlKey) {
                el.selectionStart = firstEditableCharacter;
                let selectionEnd = firstEmptyCharacter;
                while (fixedCharacters.includes(selectionEnd - 1)) {
                  selectionEnd--;
                }
                el.selectionEnd = selectionEnd;
              }
            };

            const goToEnd = () => {
              const newSelection = firstEmptyCharacter;
              el.selectionEnd = newSelection;
              if (shiftKey) {
                el.selectionStart = previousSelectionStart.current;
              } else {
                el.selectionStart = newSelection;
              }
            };

            const goToBeginning = () => {
              const newSelection = firstEditableCharacter;
              el.selectionStart = newSelection;
              if (shiftKey) {
                el.selectionEnd = previousSelectionStart.current;
              } else {
                el.selectionEnd = newSelection;
              }
            };

            const goRight = () => {
              let newSelection = Math.min(previousSelectionEnd.current + 1, firstEmptyCharacter);
              if (ctrlKey) {
                while (!fixedCharacters.includes(newSelection) && newSelection < firstEmptyCharacter) {
                  newSelection++;
                }
              }
              if (newSelection < firstEmptyCharacter) {
                while (fixedCharacters.includes(newSelection - 1) && fixedCharacters.includes(newSelection)) {
                  newSelection++;
                }
              }
              el.selectionEnd = newSelection;
              if (shiftKey) {
                el.selectionStart = previousSelectionStart.current;
              } else {
                el.selectionStart = newSelection;
              }
            };

            const goLeft = () => {
              let newSelection = Math.max(el.selectionStart - 1, firstEditableCharacter);
              if (ctrlKey) {
                while (!fixedCharacters.includes(newSelection - 1) && newSelection > 0) {
                  newSelection--;
                }
              }
              if (newSelection > firstEditableCharacter) {
                while (fixedCharacters.includes(newSelection - 1) && fixedCharacters.includes(newSelection)) {
                  newSelection--;
                }
              }
              el.selectionStart = newSelection;
              if (shiftKey) {
                el.selectionEnd = previousSelectionEnd.current;
              } else {
                el.selectionEnd = newSelection;
              }
            };

            const keyHandler = {
              [KEY_CODES.A]: selectAll,
              [KEY_CODES.DOWN_ARROW]: goToEnd,
              [KEY_CODES.LEFT_ARROW]: goLeft,
              [KEY_CODES.RIGHT_ARROW]: goRight,
              [KEY_CODES.UP_ARROW]: goToBeginning,
              [KEY_CODES.END]: goToEnd,
              [KEY_CODES.HOME]: goToBeginning
            }[keyCode];

            if (keyHandler) {
              keyHandler();
            }

            previousSelectionStart.current = el.selectionStart;
            previousSelectionEnd.current = el.selectionEnd;
          }
        }
      }, [firstEmptyCharacter]);
      
      const handleOnMouseUp = React.useCallback((e: SyntheticEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setCursorPosition();
      }, [controlElement, setCursorPosition]);
      
      const handleOnFocus = React.useCallback((e: SyntheticFocusEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if (controlElement.props.onFocus) {
          controlElement.props.onFocus(e);
        }
        setTimeout(() => {
          setCursorPosition();
        });
      }, [controlElement, setCursorPosition]);
      
      const handleOnKeyDown = React.useCallback((e: SyntheticFocusEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        adjustCursorPosition(e);
        if (controlElement.props.onKeyDown) {
          controlElement.props.onKeyDown(e);
        }
      }, [adjustCursorPosition, controlElement]);
      
      const overwrittenControlElement = React.useMemo(() => React.cloneElement(controlElement, {
        onMouseUp: (disabled || readonly) ? undefined : handleOnMouseUp,
        onFocus: (disabled || readonly) ? undefined : handleOnFocus,
        onChange: undefined,
        onInput: (disabled || readonly) ? undefined : handleOnInput,
        onKeyDown: (disabled || readonly) ? undefined : handleOnKeyDown,
        'data-value': value,
        value: maskedValue
      }), [controlElement, disabled, handleOnFocus, handleOnInput, handleOnKeyDown, handleOnMouseUp, maskedValue, readonly, value]);

      return (
        <WrappedComponent {...props} controlElement={overwrittenControlElement} />
      );
    });

    return WithMask;
  };