/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type ReactForwardedRefType } from '@omnibly/codeblocks-cmn-types';
import { KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { mergeRefs, setElementStyle } from '@omnibly/codeblocks-cmn-utils';
import CONFIG from '../../components/style-config';
import BackgroundColorContext from '../../contexts/BackgroundColorContext';
import useFocusState from '../../hooks/useFocusState';
import useHoverState from '../../hooks/useHoverState';

const {
  colors: {
    controls: { disabled: DISABLED_COLORS },
    kinds: KIND_COLORS,
    social: SOCIAL_COLORS
  }
} = CONFIG;

const KIND_STYLE = Object.freeze({
  [KINDS.DARK]: KIND_COLORS.dark,
  [KINDS.ERROR]: KIND_COLORS.error,
  [KINDS.INFO]: KIND_COLORS.info,
  [KINDS.LIGHT]: KIND_COLORS.light,
  [KINDS.LINK]: KIND_COLORS.link,
  [KINDS.MARKETING]: KIND_COLORS.marketing,
  [KINDS.PLAIN]: KIND_COLORS.plain,
  [KINDS.PRIMARY]: KIND_COLORS.primary,
  [KINDS.SECONDARY]: KIND_COLORS.secondary,
  [KINDS.SOCIAL_FACEBOOK]: SOCIAL_COLORS.facebook,
  [KINDS.SOCIAL_GOOGLE]: SOCIAL_COLORS.google,
  [KINDS.SOCIAL_INSTAGRAM]: SOCIAL_COLORS.instagram,
  [KINDS.SOCIAL_LINKEDIN]: SOCIAL_COLORS.linkedin,
  [KINDS.SOCIAL_MAIL]: SOCIAL_COLORS.mail,
  [KINDS.SOCIAL_PHONE]: SOCIAL_COLORS.phone,
  [KINDS.SOCIAL_PINTEREST]: SOCIAL_COLORS.pinterest,
  [KINDS.SOCIAL_SKYPE]: SOCIAL_COLORS.skype,
  [KINDS.SOCIAL_SNAPCHAT]: SOCIAL_COLORS.snapchat,
  [KINDS.SOCIAL_SPOTIFY]: SOCIAL_COLORS.spotify,
  [KINDS.SOCIAL_TEAMS]: SOCIAL_COLORS.teams,
  [KINDS.SOCIAL_TIKTOK]: SOCIAL_COLORS.tiktok,
  [KINDS.SOCIAL_TWITTER]: SOCIAL_COLORS.twitter,
  [KINDS.SOCIAL_YOUTUBE]: SOCIAL_COLORS.youtube,
  [KINDS.SUBMIT]: KIND_COLORS.submit,
  [KINDS.SUCCESS]: KIND_COLORS.success,
  [KINDS.WARNING]: KIND_COLORS.warning
});

export type WithThemeParamsType = {|
  backgroundProperty?: string,
  defaultBorder?: boolean,
  defaultKind?: $Values<typeof KINDS>,
  focusable?: boolean,
  hoverable?: boolean
|};
export type WithThemeType = {|
  backgroundColor?: string,
  backgroundColorDisabled?: string,
  backgroundColorFocus?: string,
  backgroundColorHover?: string,
  border?: boolean,
  borderColor?: string,
  borderColorDisabled?: string,
  borderColorFocus?: string,
  borderColorHover?: string,
  color?: string,
  colorDisabled?: string,
  colorFocus?: string,
  colorHover?: string,
  disabled?: boolean,
  focus?: boolean,
  focusable?: boolean,
  hover?: boolean,
  hoverable?: boolean,
  inverted?: boolean,
  kind?: $Values<typeof KINDS>
|};
export type WithThemeExtensionType = {|
  themeElementRef: ReactRefType<HTMLElement>,
  themeElementStyle: Object
|};

export default ({
    backgroundProperty = 'backgroundColor',
    defaultBorder = false,
    defaultKind,
    focusable: defaultFocusable = true,
    hoverable: defaultHoverable = true
  }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithTheme = React.forwardRef(
      (
        {
          backgroundColor,
          backgroundColorDisabled = backgroundColor,
          backgroundColorFocus = backgroundColor,
          backgroundColorHover = backgroundColor,
          border = defaultBorder,
          borderColor,
          borderColorDisabled = borderColor,
          borderColorFocus = borderColor,
          borderColorHover = borderColor,
          color,
          colorDisabled = color,
          colorFocus = color,
          colorHover = color,
          disabled,
          focus,
          focusable = defaultFocusable,
          hover,
          hoverable = defaultHoverable,
          inverted,
          kind = defaultKind,
          ...props
        }: WithThemeType,
        forwardedRef: ReactForwardedRefType
      ) => {
        const {
          backgroundColor: parentBackgroundColor,
          backgroundColorFocus: parentBackgroundColorFocus,
          backgroundColorHover: parentBackgroundColorHover
        } = React.useContext(BackgroundColorContext);
        const themeElementRef = React.useRef(null);
        const { focusElementRef, focused } = useFocusState({ disabled: !focusable, focus });
        const { hoverElementRef, hovered } = useHoverState({ disabled: !hoverable, hover });

        const style = React.useMemo(() => {
          const KIND_COLORS = KIND_STYLE[kind] || {};

          return {
            disabled: {
              ...DISABLED_COLORS,
              ...(backgroundColorDisabled ? { background: backgroundColorDisabled } : {}),
              ...(borderColorDisabled ? { border: borderColorDisabled } : {}),
              ...(colorDisabled ? { font: inverted ? backgroundColorDisabled : colorDisabled } : {}),
              ...(border ? {} : { border: backgroundColorDisabled || DISABLED_COLORS.background })
            },
            focus: {
              ...KIND_COLORS.focus,
              ...(inverted ? { background: parentBackgroundColorFocus } : {}),
              ...(inverted ? { font: KIND_COLORS.focus.background } : {}),
              ...(backgroundColorFocus ? { background: inverted ? parentBackgroundColorFocus : backgroundColorFocus } : {}),
              ...(borderColorFocus ? { border: borderColorFocus } : {}),
              ...(colorFocus ? { font: inverted ? backgroundColorFocus : colorFocus } : {}),
              ...(border ? {} : { border: backgroundColorFocus || (kind ? KIND_COLORS.focus.background : '') })
            },
            hover: {
              ...KIND_COLORS.hover,
              ...(inverted ? { background: parentBackgroundColorHover } : {}),
              ...(inverted ? { font: KIND_COLORS.hover.background } : {}),
              ...(backgroundColorHover ? { background: inverted ? parentBackgroundColorHover : backgroundColorHover } : {}),
              ...(borderColorHover ? { border: borderColorHover } : {}),
              ...(colorHover ? { font: inverted ? backgroundColorHover : colorHover } : {}),
              ...(border ? {} : { border: backgroundColorHover || (kind ? KIND_COLORS.hover.background : '') })
            },
            normal: {
              ...KIND_COLORS.normal,
              ...(inverted ? { background: parentBackgroundColor } : {}),
              ...(inverted ? { font: KIND_COLORS.normal.background } : {}),
              ...(backgroundColor ? { background: inverted ? parentBackgroundColor : backgroundColor } : {}),
              ...(borderColor ? { border: borderColor } : {}),
              ...(color ? { font: inverted ? backgroundColor : color } : {}),
              ...(border ? {} : { border: backgroundColor || (kind ? KIND_COLORS.normal.background : '') })
            }
          };
        }, [
          backgroundColor,
          backgroundColorDisabled,
          backgroundColorFocus,
          backgroundColorHover,
          border,
          borderColor,
          borderColorDisabled,
          borderColorFocus,
          borderColorHover,
          color,
          colorDisabled,
          colorFocus,
          colorHover,
          inverted,
          kind,
          parentBackgroundColor,
          parentBackgroundColorFocus,
          parentBackgroundColorHover
        ]);

        React.useLayoutEffect(() => {
          const el = themeElementRef.current;
          if (el) {
            if (!focusable) {
              el.tabIndex = -1;
            }
            if (disabled) {
              el.tabIndex = -1;
              if (['button', 'datalist', 'form', 'input', 'select'].includes(el.tagName.toLowerCase())) {
                el.disabled = true;
              }
            }
          }
        }, [disabled, focusable]);

        React.useLayoutEffect(() => {
          const el = themeElementRef.current;
          if (el) {
            const key = (disabled && 'disabled') || (focused && 'focus') || (hovered && 'hover') || 'normal';
            const styleForState = style[key];
            const elemenStyle = {
              [backgroundProperty]: styleForState.background,
              borderColor: styleForState.border,
              color: styleForState.font
            };

            setElementStyle(el, elemenStyle);
          }
        }, [
          disabled,
          focused,
          hovered,
          style
        ]);

        return (
          <WrappedComponent
            ref={forwardedRef}
            border={border}
            disabled={disabled}
            kind={kind}
            themeElementRef={mergeRefs(focusElementRef, hoverElementRef, themeElementRef)}
            themeElementStyle={style}
            {...props}
          />
        );
      }
    );

    return WithTheme;
  };
