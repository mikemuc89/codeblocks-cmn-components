/* @flow */
import * as React from 'react';
import { type ErrorsType, type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import styles from './WithMessages.scss';

export type ParamsType = {|
  messageClassName?: string
|};
export type WithMessagesType = {|
  errors?: ErrorsType,
  helper?: ErrorsType,
  hints?: ErrorsType,
  warnings?: ErrorsType
|};
export type WithMessageExtensionType = {|
  message: React.Node
|};

const formatMessage = (message: ErrorsType) => message.title || message;

export default ({ messageClassName }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithMessages = React.forwardRef(
      ({ errors, helper, hints, warnings, ...props }: WithMessagesType, forwardedRef: ReactForwardedRefType) => {
        const msg = errors || hints || helper || warnings;
        const message = msg && (
          <span
            className={cx(
              styles.Message,
              cx.control(styles, 'Message', { errors, helper, hints, warnings }),
              messageClassName
            )}
          >
            {formatMessage(msg)}
          </span>
        );

        return <WrappedComponent ref={forwardedRef} message={message} {...props} />;
      }
    );

    return WithMessages;
  };
