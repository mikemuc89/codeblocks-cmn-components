/* @flow */
import * as React from 'react';
import { formatFileSize } from '@omnibly/codeblocks-cmn-formatters';
import { type ErrorsType, type SetStateCallbackType } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid, unitize } from '@omnibly/codeblocks-cmn-utils';
import Button, { type ButtonType } from '../../components/Button';
import FontAwesome from '../../components/FontAwesome';
import { type LinkType } from '../../components/Link';
import { useDeepMemoizedValue } from '../../hooks/useDeepCompareHooks';

const ERRORS = Object.freeze({
  DUPLICATE_NAME: (name: string) => `You already picked file ${name}. You can't have multiple files with the same name.`,
  EMPTY_FILE_NOT_ALLOWED: (name: string) => `File ${name} is empty. Check if it is the right file you want to add.`,
  FILE_ALREADY_ADDED: () => `You already added your file. You can try to remove the old one and pick another.`,
  SINGLE_FILE_SIZE_TOO_BIG: (name: string, max: number) => `File ${name} is too big. You can only add files up to ${formatFileSize(max)}.`,
  SINGLE_NOT_UPLOADED: () => 'File was not uploaded.',
  SOME_NOT_UPLOADED: () => 'Some of the files were not uploaded.',
  SUMMARIZED_FILES_SIZE_TOO_BIG: (maxLeft: number) => `Size of all files is too big. You can only add ${formatFileSize(maxLeft)} more.`,
  TOO_MANY_FILES_ADDED: (maxLeft: number) => `You picked too many files. You can only add ${maxLeft} more.`,
  WRONG_FILE_EXTENSION: (name: string, ext: string, allowedExtensions: Array<string>) => `File ${name} is invalid - extension .${ext} is not allowed here. Try one of the following types: ${allowedExtensions.map((ext: string) => `.${ext}`).join(', ')}.`
});

const LITERALS = Object.freeze({
  EMPTY: '',
  MULTIPLE_ADD_FIRST: 'Pick your files',
  MULTIPLE_ADD_MORE: 'Add more files',
  MULTIPLE_MAXIMUM_ADDED: 'You added maximum number of files',
  SINGLE_ADD: 'Pick the file',
  SINGLE_MAXIMUM_ADDED: 'You added the file'
});

const FILE_ICONS = Object.freeze({
  ARCHIVE: <FontAwesome id="file-archive" color="#ffffff" backgroundColor="#d12c4e" />,
  AUDIO: <FontAwesome id="file-audio" color="#ffffff" backgroundColor="#25a195" />,
  CAD: <FontAwesome id="file-invoice" color="#ffffff" backgroundColor="#333333" />,
  CODE: <FontAwesome id="file-code" color="#ffffff" backgroundColor="#ec7421" />,
  CSV: <FontAwesome id="file-csv" color="#ffffff" backgroundColor="#2a6399" />,
  DATABASE: <FontAwesome id="database" color="#ffffff" backgroundColor="#2b74bd" />,
  EXECUTABLE: <FontAwesome id="file" color="#ffffff" backgroundColor="#6e4bc5" />,
  FONT: <FontAwesome id="font" color="#ffffff" backgroundColor="#333333" />,
  KEY: <FontAwesome id="key" color="#ffffff" backgroundColor="##f7cb46" />,
  PDF: <FontAwesome id="file-pdf" color="#ffffff" backgroundColor="#d11516" />,
  PLAIN: <FontAwesome id="file-alt" color="#ffffff" backgroundColor="#2a6399" />,
  PRESENTATION: <FontAwesome id="file-powerpoint" color="#ffffff" backgroundColor="#c54830" />,
  PRINTING: <FontAwesome id="file-invoice" color="#ffffff" backgroundColor="#333333" />,
  PROJECT: <FontAwesome id="file-image" color="#ffffff" backgroundColor="#225466" />,
  PUBLICATION: <FontAwesome id="file-invoice" color="#ffffff" backgroundColor="#81b315" />,
  RASTER: <FontAwesome id="file-image" color="#ffffff" backgroundColor="#84ba54" />,
  SPREADSHEET: <FontAwesome id="file-excel" color="#ffffff" backgroundColor="#206f44" />,
  TEMPORARY: <FontAwesome id="file" color="#ffffff" backgroundColor="#333333" />,
  TEXT: <FontAwesome id="file-word" color="#ffffff" backgroundColor="#2a5395" />,
  VECTOR: <FontAwesome id="file-image" color="#ffffff" backgroundColor="#ec7421" />,
  VIDEO: <FontAwesome id="file-video" color="#ffffff" backgroundColor="#e04a3a" />
});

export const getExtensionIcon = (extension: string) => {
  if (['7z', 'bz2', 'dmg', 'gz', 'gzip', 'img', 'iso', 'mdf', 'rar', 'tar', 'tgz', 'zip', 'zipx'].includes(extension)) {
    return FILE_ICONS.ARCHIVE
  }
  if (['aac', 'cda', 'flac', 'm4a', 'mid', 'midi', 'mp3', 'mpa', 'mpc', 'mpga', 'ogg', 'wav', 'wma'].includes(extension)) {
    return FILE_ICONS.AUDIO
  }
  if (['3dm', '3ds', 'dgn', 'dwg', 'dxf', 'max', 'obj', 'skp'].includes(extension)) {
    return FILE_ICONS.CAD
  }
  if (['asp', 'aspx', 'class', 'css', 'gml', 'gpx', 'htm', 'html', 'jar', 'java', 'js', 'json', 'jsx', 'kml', 'kmz', 'p', 'php', 'py', 'py3', 'pyc', 'pyo', 'pyw', 'swift', 'xhtml', 'xml', 'yaml', 'yml'].includes(extension)) {
    return FILE_ICONS.CODE
  }
  if (['csv'].includes(extension)) {
    return FILE_ICONS.CSV
  }
  if (['accda', 'accdb', 'accde', 'accdt', 'db', 'dbf', 'mdb', 'pdb', 'sql'].includes(extension)) {
    return FILE_ICONS.DATABASE
  }
  if (['air', 'apk', 'app', 'bat', 'cgi', 'com', 'deb', 'exe', 'msi', 'vb'].includes(extension)) {
    return FILE_ICONS.EXECUTABLE
  }
  if (['eot', 'fnt', 'fon', 'otf', 'ttf', 'woff', 'woff2'].includes(extension)) {
    return FILE_ICONS.FONT
  }
  if (['cer', 'crt', 'key', 'xades'].includes(extension)) {
    return FILE_ICONS.KEY
  }
  if (['pdf', 'pdfx'].includes(extension)) {
    return FILE_ICONS.PDF
  }
  if (['log', 'srt', 'txt'].includes(extension)) {
    return FILE_ICONS.PLAIN
  }
  if (['fodp', 'odp', 'pot', 'potm', 'potx', 'ppam', 'pps', 'ppsm', 'ppsx', 'ppt', 'pptm', 'pptx', 'sldm', 'sldx'].includes(extension)) {
    return FILE_ICONS.PRESENTATION
  }
  if (['ps', 'xps'].includes(extension)) {
    return FILE_ICONS.PRINTING
  }
  if (['fodg', 'odg', 'psd'].includes(extension)) {
    return FILE_ICONS.PROJECT
  }
  if (['azw', 'chm', 'epub', 'kfx', 'mobi', 'pub', 'vbk'].includes(extension)) {
    return FILE_ICONS.PUBLICATION
  }
  if (['bmp', 'dng', 'gif', 'ico', 'jp2', 'jpeg', 'jpg', 'png', 'raf', 'raw', 'tga', 'tif', 'tiff', 'webp', 'xcf'].includes(extension)) {
    return FILE_ICONS.RASTER
  }
  if (['fods', 'ods', 'xla', 'xlam', 'xll', 'xls', 'xlsb', 'xlsm', 'xlsx', 'xlt', 'xltm', 'xltx', 'xlw'].includes(extension)) {
    return FILE_ICONS.SPREADSHEET
  }
  if (['part', 'temp', 'tmp'].includes(extension)) {
    return FILE_ICONS.TEMPORARY
  }
  if (['doc', 'docb', 'docm', 'docx', 'dot', 'dotm', 'dotx', 'fodt', 'odt', 'rtf', 'wbk'].includes(extension)) {
    return FILE_ICONS.TEXT
  }
  if (['ai', 'cdr', 'eps', 'odg', 'std', 'svg', 'svgz', 'vdw', 'vsd', 'vsdm', 'vsdx', 'vss', 'vssm', 'vssx', 'vst', 'vstm', 'vstx'].includes(extension)) {
    return FILE_ICONS.VECTOR
  }
  if (['3gp', 'amv', 'avi', 'divx', 'flv', 'h264', 'm4v', 'mkv', 'mov', 'mp4', 'mpeg', 'mpg', 'ogv', 'qt', 'swf', 'vob', 'webm', 'wmv'].includes(extension)) {
    return FILE_ICONS.VIDEO
  }
};

export const FileItemContext = React.createContext({});

const FILE_EXTENSION_DELIMITER = '.';

export type WithFileInputParamsType<ValueType> = {
  buttonClassName?: string
};
export type WithFileInputType<ValueType> = {|
  allowDuplicates?: boolean,
  allowedExtensions?: Array<string>,
  buttonAdd?: React.Element<ButtonType | LinkType>,
  buttonFull?: React.Element<ButtonType | LinkType>,
  controlElement: React.Node,
  controlRef: ReactRefType<HTMLElement>,
  disabled?: boolean,
  hideButton?: boolean,
  hideSelectedFiles?: boolean,
  immediateUpload?: boolean,
  maximum?: number,
  multiple?: boolean,
  onUpload?: (file: File) => Promise<any>,
  onUploadFail?: (error: Object) => void,
  onUploadSuccess?: (result: Object) => void,
  over: initiallyOver,
  readonly?: boolean,
  requiredElement?: React.Node,
  setErrors: SetStateCallbackType<ErrorsType>,
  setValue: SetStateCallbackType<ValueType>,
  value: ValueType
|};
export type WithFileInputExtensionType<ValueType> = {|
  buttonElement: React.Node,
  controlElement: React.Node,
  onDragProps: Object,
  overElement: React.Node,
  selectedFiles: Array<{ extension: string, name: string, size: number }>
|};

export default ({
  buttonClassName
}: WithFileInputParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithFileInput = React.forwardRef(
      (
        {
          allowDuplicates,
          allowedExtensions,
          buttonAdd,
          buttonFull,
          controlElement,
          hideButton,
          hideSelectedFiles,
          immediateUpload,
          maxAllFilesSize,
          maxSingleFileSize,
          onUpload,
          onUploadFail,
          onUploadSuccess,
          over: initiallyOver,
          requiredElement,
          ...props
        }: WithFileInputType,
        forwardedRef: ReactForwardedRefType
      ) => {
        const {
          controlRef,
          disabled,
          maximum,
          multiple,
          readonly,
          setErrors,
          setValue,
          value
        } = props;

        const [over, setOver] = React.useState(initiallyOver && !disabled);

        const allowedExtensionsMemo = useDeepMemoizedValue(allowedExtensions);

        const selectedFiles = React.useMemo(() => {
          if (hideSelectedFiles || !value || value.length === 0) {
            return [];
          }
          return value.map((file: File) => {
            const parts = file.name.split(FILE_EXTENSION_DELIMITER);
            const extension = parts.pop();
            return {
              extension,
              name: parts.join(FILE_EXTENSION_DELIMITER),
              size: file.size
            }
          });
        }, [hideSelectedFiles, multiple, value]);

        const validateChange = React.useCallback((files: Array<File>) => {
          const filesToCheck = multiple ? files : [files[0]];
          const currentLength = value ? value.length : 0;
          if (filesToCheck.length + currentLength > maximum) {
            const maxLeft = maximum - currentLength;
            return ERRORS.TOO_MANY_FILES_ADDED(maxLeft);
          }
          if (maxAllFilesSize !== undefined) {
            const currentFilesSize = value ? value.reduce((sum, file) => sum + file.size, 0) : 0;
            const newFilesSize = filesToCheck.reduce((sum, file) => sum + file.size, 0);
            if (currentFilesSize + newFilesSize > maxAllFilesSize) {
              const maxLeft = maxAllFilesSize - currentFilesSize;
              return ERRORS.SUMMARIZED_FILES_SIZE_TOO_BIG(maxLeft);
            }
          }
          return filesToCheck.reduce((error, file) => {
            if (error) {
              return error;
            }
            if (file.size === 0) {
              return ERRORS.EMPTY_FILE_NOT_ALLOWED(file.name);
            }
            if (file.size > maxSingleFileSize) {
              return ERRORS.SINGLE_FILE_SIZE_TOO_BIG(file.name, maxSingleFileSize);
            }
            const valueArray = value ? multiple ? value : [value] : [];
            if (!allowDuplicates && valueArray.some(({ name }) => name === file.name)) {
              return ERRORS.DUPLICATE_NAME(file.name);
            }
            const ext = file.name.split(FILE_EXTENSION_DELIMITER).pop();
            if (!allowedExtensionsMemo.includes(ext)) {
              return ERRORS.WRONG_FILE_EXTENSION(file.name, ext, allowedExtensionsMemo);
            }
            return null;
          }, null);
        }, [allowDuplicates, allowedExtensionsMemo, maxAllFilesSize, maximum, maxSingleFileSize, multiple, value]);

        const canAddMore = React.useMemo(() => !value || (multiple && value.length < maximum), [maximum, multiple, value]);

        const uploadFiles = React.useCallback((files: Array<File>) => {
          if (onUpload) {
            return onUpload(files).then(onUploadSuccess, (errors) => {
              setErrors(ERRORS.SOME_NOT_UPLOADED());
              onUploadFail(errors);
            });
          }
          return Promise.resolve();
        }, [onUpload, onUploadFail, onUploadSuccess, setErrors]);

        const handleOnChange = React.useCallback((e: Event) => {
          setErrors(null);
          const files = Array.from(e.dataTransfer ? e.dataTransfer.files : e.target.files);
          const errors = validateChange(files);
          if (errors) {
            return setErrors(errors);
          }
          if (immediateUpload) {
            uploadFiles(multiple ? files : [files[0]]);
          }
          if (multiple) {
            return setValue((oldValue) => [...(oldValue || []), ...files])
          }
          return setValue((oldValue) => files[0]);
        }, [immediateUpload, multiple, setErrors, setValue, uploadFiles, validateChange]);
    
        const handleOnRemoveByItemIndex = React.useCallback((index?: number) => {
          setValue((oldValue: File | Array<File> | null) => oldValue && multiple ? [
            ...oldValue.slice(0, index),
            ...oldValue.slice(index + 1)
          ] : null);
        }, [multiple, setValue]);
    
        const handleButtonOnClick = React.useCallback((e: MouseEvent) => {
          const el = controlRef.current;
          if (el) {
            el.click();
          }
        }, [setErrors]);
    
        const handleButtonOnKeyDown = React.useCallback((e: KeyboardEvent) => {
          const { keyCode } = e;
    
          if ([KEY_CODES.ENTER, KEY_CODES.SPACE].includes(keyCode)) {
            const el = controlRef.current;
            if (el) {
              el.click();
            }
          }
        }, [setErrors]);

        const handleOnDragLeave = React.useCallback((e: MouseEvent) => {
          e.preventDefault();
          if (!e.target.contains(e.relatedTarget)) {
            setOver(false);
          }
        }, [setOver]);
    
        const handleOnDragOver = React.useCallback((e: MouseEvent) => {
          console.log('onDragOver', e);
          e.preventDefault();
          setOver(true);
        }, [setOver]);
    
        const handleOnDrop = React.useCallback((e: MouseEvent) => {
          e.preventDefault();
          setOver(0);
          handleOnChange(e);
        }, [handleOnChange]);
    
        const buttonText = React.useMemo(() => {
          if (hideButton || disabled) {
            return LITERALS.EMPTY;
          }
          if (multiple) {
            if (value && value.length) {
              if (value.length < maximum) {
                return LITERALS.MULTIPLE_ADD_MORE;
              }
              return hideSelectedFiles ? LITERALS.MULTIPLE_MAXIMUM : LITERALS.EMPTY;
            }
            return LITERALS.MULTIPLE_ADD_FIRST;
          }
          if (value) {
            return hideSelectedFiles ? LITERALS.SINGLE_MAXIMUM_ADDED : LITERALS.EMPTY;
          }
          return LITERALS.SINGLE_ADD;
        }, [hideButton, hideSelectedFiles, maximum, multiple, value]);
    
        const buttonChildren = React.useMemo(() => [buttonText, requiredElement], [buttonText, requiredElement]);
    
        const buttonElement = React.useMemo(() => {
          if (buttonText === LITERALS.EMPTY) {
            return null;
          }
    
          const mainProps = {
            className: buttonClassName,
            children: buttonChildren,
            disabled,
            kind: Button.KINDS.LINK
          }
          const propsCanAdd = {
            ...mainProps,
            kind: Button.KINDS.PRIMARY,
            onClick: disabled ? undefined : handleButtonOnClick
          };
    
          if (multiple) {
            if (value && value.length) {
              if (value.length < maximum) {
                return buttonAdd ? React.cloneElement(buttonAdd, propsCanAdd) : (
                  <Button {...propsCanAdd} />
                );
              }
              return buttonFull ? React.cloneElement(buttonFull, mainProps) : (
                <Button {...mainProps} />
              );
            }
            return buttonAdd ? React.cloneElement(buttonAdd, propsCanAdd) : (
              <Button {...propsCanAdd} />
            );
          }
          if (value) {
            return buttonFull ? React.cloneElement(buttonFull, mainProps) : (
              <Button {...mainProps} />
            );
          }
          return buttonAdd ? React.cloneElement(buttonAdd, propsCanAdd) : (
            <Button {...propsCanAdd} />
          );
        }, [buttonAdd, buttonChildren, buttonFull, buttonText, handleButtonOnClick, maximum, multiple, value]);

        const accept = React.useMemo(() => {
          if (!allowedExtensions) {
            return undefined;
          }
          return allowedExtensions.map((ext: string) => `.${ext}`);
        }, [allowedExtensions]);
    
        const overwrittenControlElement = React.useMemo(() => controlElement && React.cloneElement(controlElement, {
          accept,
          onChange: (disabled || readonly) ? undefined : handleOnChange,
          tabIndex: -1
        }), [accept, controlElement, disabled, handleOnChange, readonly]);

        const contextValue = React.useMemo(() => ({
          disabled,
          handleOnRemoveByItemIndex,
          readonly
        }), [disabled, handleOnRemoveByItemIndex, readonly]);

        const onDragProps = React.useMemo(() => ({
          onDrop: disabled ? undefined : handleOnDrop,
          onDragOver: disabled ? undefined : handleOnDragOver,
          onDragLeave: disabled ? undefined : handleOnDragLeave
        }), [disabled, handleOnDragLeave, handleOnDragOver, handleOnDrop]);

        const overElement = React.useMemo(() => over ? (
          <div style={{
            backgroundColor: '#666666',
            border: `${unitize(4)} dashed #000000`,
            bottom: unitize(0),
            left: unitize(-4),
            opacity: 0.1,
            position: 'absolute',
            right: unitize(-4),
            top: unitize(0)
          }}></div>
        ): null, [over]);

        return (
          <FileItemContext.Provider value={contextValue}>
            <WrappedComponent
              ref={forwardedRef}
              buttonElement={buttonElement}
              canAddMore={canAddMore}
              controlElement={overwrittenControlElement}
              onDragProps={onDragProps}
              overElement={overElement}
              selectedFiles={selectedFiles}
              {...props}
            />
          </FileItemContext.Provider>
        );
      }
    );

    return WithFileInput;
  };
