/* @flow */
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { compose } from '@omnibly/codeblocks-cmn-utils';
import AsLeafView, {
  type LeafViewType,
  type AsLeafViewParamsType,
  type AsLeafViewRequiredPropsType
} from './AsLeafView';

export type PagerViewType = LeafViewType;

export type AsPagerViewParamsType = AsLeafViewParamsType;

export type AsPagerRequiredPropsType = AsLeafViewRequiredPropsType;

export default ({
    ErrorComponent,
    api,
    callbacks,
    customHandlers,
    errorConfig,
    events,
    initialData
  }: ParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const AsPagerView = Object.assign(
      compose(AsLeafView({ ErrorComponent, api, callbacks, customHandlers, errorConfig, events, initialData }))(
        WrappedComponent
      ),
      {
        api
      }
    );
    return AsPagerView;
  };
