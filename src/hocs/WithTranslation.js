/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import TranslationContext from '../contexts/TranslationContext';

export type WithTranslationAddedPropsType = {};

export type WithTranslationParamsType = {|
  defaultLang: string,
  fallbackLang: string,
  i18n: Object
|};

export type WithTranslationRequiredPropsType = {};

export default ({ lang: defaultLang = 'pl', fallbackLang = 'en', i18n }: WithTranslationParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithTranslation = (props: WithTranslationRequiredPropsType) => {
      const [lang, setLang] = React.useState(defaultLang);

      const t = React.useCallback(
        (key: string, defaultValue: string) => {
          const literalConfig = i18n[key];
          return literalConfig ? literalConfig[lang] || literalConfig[fallbackLang] || defaultValue : defaultValue;
        },
        [lang]
      );

      const context = React.useMemo(
        () => ({
          lang,
          setLang,
          t
        }),
        [lang, setLang, t]
      );

      return (
        <TranslationContext.Provider value={context}>
          <WrappedComponent {...props} />
        </TranslationContext.Provider>
      );
    };

    return WithTranslation;
  };
