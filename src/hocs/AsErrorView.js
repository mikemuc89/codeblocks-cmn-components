/* @flow */
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';

export type ErrorViewType = {};

export type AsErrorViewParamsType = {};

export type AsErrorViewRequiredPropsType = {};

export type ErrorViewPropsType = any;

export default () => (WrappedComponent: HocWrappedComponentType): React.ComponentType<ErrorViewPropsType> => WrappedComponent;
