/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { type NavigateType } from './WithNavigation';

export type RedirectCallbackParamsType = {|
  navigate: NavigateType  
|};
export type RedirectCallbackType = (params: RedirectCallbackParamsType) => void;

export type WithRedirectAddedPropsType = {};

export type WithRedirectParamsType = {|
  onRedirect: RedirectCallbackType,
  timeout?: number
|};

export type WithRedirectRequiredPropsType = {|
  navigate: NavigateType
|};

export default ({ onRedirect, timeout = 2000 }: WithRedirectParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithRedirect = (props: WithRedirectRequiredPropsType) => {
      const timeoutRef = React.useRef(null);
      const { navigate } = props;

      React.useEffect(() => {
        timeoutRef.current = setTimeout(() => {
          onRedirect({ navigate });
        }, timeout);

        return () => clearTimeout(timeoutRef.current);
      }, [navigate]);

      return <WrappedComponent {...props} />;
    };
    return WithRedirect;
  };
