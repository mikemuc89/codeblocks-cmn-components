/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type ErrorsType } from '@omnibly/codeblocks-cmn-types';
import { update } from '@omnibly/codeblocks-cmn-utils';
import InitialDataContext from '../contexts/InitialDataContext';

export type FormType = {|
  data: Object,
  errors: ErrorsType,
  fields?: Object,
  step?: string
|};

type StateType = Object;

type UpdateStatePromiseReturnType = {|
  newState: StateType,
  oldState: StateType
|};

export type UpdateStateType = (spec: Object) => Promise<?UpdateStatePromiseReturnType>;

export type WithStateAddedPropsType = {|
  errors: ErrorsType,
  form: FormType,
  hasInitialData: boolean,
  updateState: UpdateStateType
|};

export type WithStateParamsType = {};

export type WithStateRequiredPropsType = {|
  config?: Object
|};

const getInitialState = (initialData: Object) => ({
  form: initialData || {
    data: {},
    errors: null,
    fields: {}
  }
});

export default () => (WrappedComponent: HocWrappedComponentType) => {
  const WithState = (props: WithStateRequiredPropsType) => {
    const { config: { name = '/' } = {} } = props;
    const _isMounted = React.useRef(false);

    const getInitialData = React.useContext(InitialDataContext);
    const initialData = getInitialData(name);
    const hasInitialData = Boolean(initialData);

    const [state, setState] = React.useState(getInitialState(initialData));

    React.useEffect(() => {
      _isMounted.current = true;

      return () => {
        _isMounted.current = false;
      };
    }, []);

    const updateState = React.useCallback(
      (spec: Object): Promise<any> =>
        new Promise((resolve: (?UpdateStatePromiseReturnType) => void) => {
          if (_isMounted.current) {
            setState((oldState: StateType) => {
              const newState = update(oldState, spec);
              setTimeout(() => resolve({ newState, oldState }));
              return newState;
            });
          } else {
            resolve(null);
          }
        }),
      [setState]
    );

    return (
      <WrappedComponent
        {...props}
        errors={state.form.errors}
        form={state.form}
        hasInitialData={hasInitialData}
        updateState={updateState}
      />
    );
  };
  return WithState;
};
