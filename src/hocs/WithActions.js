/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { getParamsFromLink } from '@omnibly/codeblocks-cmn-utils';
import useEventListener from '../hooks/useEventListener';

type ActionType = (params?: Object) => void;

export type WithActionsAddedPropsType = {};

export type WithActionsParamsType = {|
  actions?: { [actionKey: string]: ActionType }
|};

export type WithActionsRequiredPropsType = {};

export default ({ actions = {} }: WithActionsParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithActions = (props: WithActionsRequiredPropsType) => {
      const ref = React.useRef(null);

      const handleAction = (e: MouseEvent) => {
        const anchor = e.target instanceof HTMLElement && e.target.closest('a');
        const button = e.target instanceof HTMLElement && e.target.closest('button');
        const destination = (anchor && anchor.getAttribute('href')) || (button && button.getAttribute('value')) || '';
        const [address, action = ''] = destination.split('#');
        const { id, params } = getParamsFromLink(action);

        if (address === '' && id in actions) {
          e.stopPropagation();
          e.preventDefault();
          actions[id](params);
        }
      };

      useEventListener('click', handleAction, ref.current);

      return (
        <div ref={ref}>
          <WrappedComponent {...props} />
        </div>
      );
    };
    return WithActions;
  };
