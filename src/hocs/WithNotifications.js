/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { type UpdateStateType } from './WithState';

export type WithNotificationsAddedPropsType = {|
  hideNotification: (id: string) => void,
  notify: (notification: Object) => void
|};

export type WithNotificationsParamsType = {};

export type WithNotificationsRequiredPropsType = {|
  updateState: UpdateStateType
|};

export default () => (WrappedComponent: HocWrappedComponentType) => {
  const WithNotifications = (props: WithNotificationsRequiredPropsType) => {
    const { updateState } = props;
    const hideNotification = React.useCallback(
      (id: string) => {
        updateState({
          form: {
            data: {
              notifications: {
                $apply: (notifications: Array<Object> = []) =>
                  notifications.filter((notification: Object) => notification.id !== id)
              }
            }
          }
        });
      },
      [updateState]
    );

    const notify = React.useCallback(
      (notification: Object) => {
        updateState({
          form: {
            data: {
              notifications: {
                $apply: (notifications: Array<Object> = []) => notifications.concat([notification])
              }
            }
          }
        });
      },
      [updateState]
    );

    return <WrappedComponent {...props} hideNotification={hideNotification} notify={notify} />;
  };
  return WithNotifications;
};
