/* #__flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import history from '../history';
import { type FieldHandlerType } from './WithForm';
import { type NavigateType } from './WithNavigation';
import { type FormType } from './WithState';

export type TranslateType = (key: string, defaultValue: string) => string;

export type StepPropsType = {|
  ...FormType,
  fieldHandler: FieldHandlerType,
  loading: boolean,
  navigate: NavigateType,
  t: TranslateType
|};

export type WithStepsAddedPropsType = {|
  Content?: React.ComponentType<any>
|};

export type WithStepsParamsType = {|
  steps: { [step: string]: React.ComponentType<any> }
|};

export type WithStepsRequiredPropsType = {|
  fieldHandler: FieldHandlerType,
  form: FormType,
  loading: boolean,
  navigate: NavigateType,
  t: TranslateType
|};

export default ({ steps }: WithStepsParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithSteps = (props: WithStepsRequiredPropsType) => {
      const { form, fieldHandler, loading, navigate, t } = props;
      const Content = (form.step && steps[form.step]) || null;
      const contentProps = {
        back: history.back,
        fieldHandler,
        loading,
        navigate,
        t,
        ...form
      };
      return <WrappedComponent {...props} Content={Content && <Content {...contentProps} />} />;
    };
    return WithSteps;
  };
