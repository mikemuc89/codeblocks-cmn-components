/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import * as ERRORS from '@omnibly/codeblocks-cmn-types/src/errors';
import { update } from '@omnibly/codeblocks-cmn-utils';
import { type FieldHandlerType } from './WithForm';

const errorConfig = {
  FLD_INVD_CHARACTERS: ERRORS.ERR_FLD_INVD_CHARACTERS,
  FLD_INVD_ITEM_SELECTED: ERRORS.ERR_FLD_INVD_ITEM_SELECTED,
  FLD_INVD_RANGE: ERRORS.ERR_FLD_INVD_RANGE,
  FLD_INVD_VALUE: ERRORS.ERR_FLD_INVD_VALUE,
  FLD_INVD_VALUE_REGEX: ERRORS.ERR_FLD_INVD_VALUE_REGEX,
  FLD_MAX_DATE_EXCEEDED: ERRORS.ERR_FLD_MAX_DATE_EXCEEDED,
  FLD_MAX_LEN_EXCEEDED: ERRORS.ERR_FLD_VALUE_TOO_LONG,
  FLD_MAX_VALUE_EXCEEDED: ERRORS.ERR_FLD_VALUE_TOO_BIG,
  FLD_MIN_DATE_EXCEEDED: ERRORS.ERR_FLD_MIN_DATE_EXCEEDED,
  FLD_MIN_LEN_EXCEEDED: ERRORS.ERR_FLD_VALUE_TOO_SHORT,
  FLD_MIN_VALUE_EXCEEDED: ERRORS.ERR_FLD_VALUE_TOO_SMALL,
  FLD_VALUE_REQUIRED: ERRORS.ERR_FLD_VALUE_REQUIRED
};

export type WithMockHandlerAddedPropsType = {|
  fieldHandler: FieldHandlerType,
  state: Object,
  toggleDisabled: () => void,
  toggleErrors: () => void,
  toggleHints: () => void,
  toggleRequired: () => void,
  toggleWarnings: () => void
|};

export type WithMockHandlerParamsType = {|
  handler: ({
    field: Object,
    key: string,
    options?: Object,
    updateFields: (spec: Object, callback?: () => void) => void
  }) => Object,
  initialState: Object
|};

export type WithMockHandlerRequiredPropsType = {};

const KEY = 'TEST';
const MESSAGE = 'Lorem ipsum dolor sit amet';

export default ({ handler, initialState }: WithMockHandlerParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithMockHandler = (props: WithMockHandlerRequiredPropsType) => {
      const initState = React.useMemo(() => ({ [KEY]: initialState }), []);
      const [state, setState] = React.useState(initState);

      const updateFields = React.useCallback(
        (spec: Object) => {
          setState((oldState: Object) => update(oldState, spec));
        },
        [setState]
      );

      const toggleWidgetField = React.useCallback(
        (fieldKey: string, value?: boolean = true) =>
          setState(({ [KEY]: { widget = {}, ...field }, ...oldState }: Object) => ({
            ...oldState,
            [KEY]: {
              ...field,
              widget: {
                ...widget,
                [fieldKey]: widget[fieldKey] ? null : value
              }
            }
          })),
        [setState]
      );

      const toggleMainField = React.useCallback(
        (fieldKey: string, value: any) =>
          setState((oldState: Object) => ({
            ...oldState,
            [KEY]: {
              ...oldState[KEY],
              [fieldKey]: oldState[KEY][fieldKey] ? null : value
            }
          })),
        [setState]
      );

      const toggleDisabled = React.useCallback(() => toggleWidgetField('disabled', true), [toggleWidgetField]);
      const toggleMaxLen = React.useCallback(() => toggleWidgetField('maxLen', 8), [toggleWidgetField]);
      const toggleMinLen = React.useCallback(() => toggleWidgetField('minLen', 3), [toggleWidgetField]);
      const toggleRegex = React.useCallback(
        () => toggleWidgetField('regex', '^[0-9]{4}-[0-9]{3}-[0-9]{2}$'),
        [toggleWidgetField]
      );
      const toggleRequired = React.useCallback(() => toggleWidgetField('required', true), [toggleWidgetField]);

      const toggleErrors = React.useCallback(() => toggleMainField('errors', MESSAGE), [toggleMainField]);
      const toggleHints = React.useCallback(() => toggleMainField('hints', MESSAGE), [toggleMainField]);
      const toggleWarnings = React.useCallback(() => toggleMainField('warnings', MESSAGE), [toggleMainField]);

      const fieldHandler = React.useCallback(
        () => ({
          handled: true,
          name: 'test',
          ...handler({
            field: state[KEY] || {},
            key: KEY,
            options: {
              errorConfig
            },
            state,
            updateFields
          })
        }),
        [state, updateFields]
      );

      return (
        <WrappedComponent
          {...props}
          fieldHandler={fieldHandler}
          state={state}
          toggleDisabled={toggleDisabled}
          toggleErrors={toggleErrors}
          toggleHints={toggleHints}
          toggleMaxLen={toggleMaxLen}
          toggleMinLen={toggleMinLen}
          toggleRegex={toggleRegex}
          toggleRequired={toggleRequired}
          toggleWarnings={toggleWarnings}
        />
      );
    };
    return WithMockHandler;
  };
