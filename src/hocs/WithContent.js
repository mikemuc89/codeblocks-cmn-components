/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { useDeepMemoizedValue } from '../hooks/useDeepCompareHooks';

export type WithContentAddedPropsType = {|
  Content: React.Node
|};

export type WithContentParamsType = {|
  DefaultContent?: React.Element<any>
|};

export type WithContentRequiredPropsType = {|
  navigation: Object,
  tree: Array<string>
|};

export default ({ DefaultContent = () => null }: WithContentParamsType = {}) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithContent = (props: WithContentRequiredPropsType) => {
      const [childId, ...tree] = props.tree || [];
      const Content = React.useMemo(() => props.navigation[childId] || DefaultContent, [childId]);

      const propsMemo = useDeepMemoizedValue(props);
      const treeMemo = useDeepMemoizedValue(tree);

      const contentProps = React.useMemo(() => ({
        ...propsMemo,
        currentId: childId,
        treeMemo
      }), [childId, propsMemo, treeMemo]);

      const contentEl = React.useMemo(() => (
        <Content {...contentProps} />
      ), [Content, contentProps]);

      return <WrappedComponent {...props} tree={tree} Content={contentEl} />;
    };
    return WithContent;
  };
