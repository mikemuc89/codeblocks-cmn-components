/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType, type LocationType } from '@omnibly/codeblocks-cmn-types';
import {
  createUrlFromIdAndParams,
  getNavigationParamsFromLocation,
  getNavigationTree,
  getParamsFromLink
} from '@omnibly/codeblocks-cmn-utils';
import AppNavigationContext from '../contexts/AppNavigationContext';
import LocationContext from '../contexts/LocationContext';
import history, { useHistoryTracking, HISTORY_ACTIONS } from '../history';
import useEventListener from '../hooks/useEventListener';

type NavigateParamsType = Object;
type NavigatePromiseType = {|
  id: string,
  params?: NavigateParamsType
|};

export type NavigateType = (id: string, params?: NavigateParamsType) => Promise<?NavigatePromiseType>;

type TreeType = Array<string>;

export type WithNavigationAddedPropsType = {|
  back: Function,
  navigate: NavigateType,
  navigation: Object,
  params: Object,
  path: string,
  tree: TreeType
|};

export type WithNavigationParamsType = {|
  navigation: Object
|};

export type WithNavigationRequiredPropsType = {};

type NavigationStateType = {|
  dialogs: Array<string>,
  params?: Object,
  path: string,
  tree: TreeType
|};

const NOT_FOUND_ID = '/ERROR/404';

export default ({ navigation }: WithNavigationParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithNavigation = (props: WithNavigationRequiredPropsType) => {
      const ssrInitialLocation = React.useContext(LocationContext);

      const getParamsFromLocation = React.useCallback(
        (location: Object = ssrInitialLocation || window.location): Object => {
          try {
            const { id, pathParams, requestParams } = getNavigationParamsFromLocation(location, navigation);
            return {
              id,
              params: {
                ...requestParams,
                ...pathParams
              }
            };
          } catch (e) {
            console.warn(e);
            return {
              id: NOT_FOUND_ID,
              params: {}
            };
          }
        },
        [ssrInitialLocation]
      );

      const [state, setState] = React.useState<NavigationStateType>(
        (({ id, params }: { id: string, params: Object }) => ({
          dialogs: [],
          params,
          path: id,
          tree: getNavigationTree(id, navigation)
        }))(getParamsFromLocation())
      );

      const { historyArray, historyIndex, updateHistoryTracking } = useHistoryTracking({
        path: state.path
      });

      const setNavigationStateTimeout = React.useRef(null);

      React.useEffect(
        () =>
          history.listen(
            ({ action, location }: { action: $Values<typeof HISTORY_ACTIONS>, location: LocationType }) => {
              const { isBack, previousLocation } = updateHistoryTracking({ action, location });
              const { hold, skip } = location.state || {};
              const preventTransition = hold || skip;

              if (skip) {
                return isBack ? history.back() : history.forward();
              }

              if (!preventTransition) {
                clearTimeout(setNavigationStateTimeout.current);
                setNavigationStateTimeout.current = setTimeout(() => setNavigationState(location));
              }

              const isFlowTransition = previousLocation.state.flow || location.state.flow;

              if (action === HISTORY_ACTIONS.POP && isFlowTransition) {
                const detail = { historyArray, historyIndex, isBack, location, previousLocation };
                const evt = new CustomEvent('flowTransition', { detail });
                document.dispatchEvent(evt);
              }
            }
          ),
        [historyArray, historyIndex, setNavigationState, updateHistoryTracking]
      );

      const setNavigationState = React.useCallback(
        (location?: Object) => {
          const { id, params } = getParamsFromLocation(location);
          return setState((state: NavigationStateType) => ({
            ...state,
            params,
            path: id,
            tree: getNavigationTree(id, navigation)
          }));
        },
        [getParamsFromLocation, setState]
      );

      const addDialog = React.useCallback(
        (id: string) => setState((state: NavigationStateType) => ({ ...state, dialogs: state.dialogs.concat(id) })),
        [setState]
      );

      const removeDialog = React.useCallback(
        () => setState((state: NavigationStateType) => ({ ...state, dialogs: state.dialogs.slice(0, -1) })),
        [setState]
      );

      const navigate = React.useCallback(
        (id: string, params?: NavigateParamsType = {}) =>
          new Promise((resolve: (?{ id: string, params: Object }) => void) => {
            const evt = new CustomEvent('navigate', { detail: { id, params } });
            if (navigation[id] && navigation[id].dialog) {
              addDialog(id);
              document.dispatchEvent(evt);
              return resolve({ id, params });
            }
            try {
              const url = createUrlFromIdAndParams(id, params, navigation);
              history.push(url);
              document.dispatchEvent(evt);
              return resolve({ id, params });
            } catch (e) {
              console.warn(e, { id, params });
              return resolve(null);
            }
          }),
        [addDialog]
      );

      const handleLinkClick = React.useCallback(
        (e: MouseEvent) => {
          const element = e.target.closest('a') || e.target.closest('button');
          if (element) {
            const href = element.getAttribute('data-href') || element.getAttribute('href') || '';
            const [address, location = ''] = href.split('#');
            if (address === '') {
              if (location) {
                e.stopPropagation();
                e.preventDefault();
              }
              if (location.startsWith('@')) {
                const action = location.slice(1);
                const actions = {
                  BACK: () => history.back(),
                  CLOSE_DIALOG: () => removeDialog()
                };
                if (action in actions) {
                  actions[action]();
                } else {
                  const error = `Action ${action} not defined`;
                  console.error(error);
                  throw error;
                }
              } else if (location.startsWith('/')) {
                const { id, params } = getParamsFromLink(location);
                navigate(id, { data: params });
              }
            } else if (/(https?:)?\/\//gi.test(address)) {
              e.stopPropagation();
              e.preventDefault();
              window.open(href);
            }
          }
        },
        [navigate, removeDialog]
      );

      useEventListener('click', handleLinkClick, typeof document === 'undefined' ? null : document);

      return (
        <AppNavigationContext.Provider value={navigation}>
          <WrappedComponent
            {...props}
            back={history.back}
            navigate={navigate}
            navigation={navigation}
            params={state.params}
            path={state.path}
            tree={state.tree}
          />
          {state.dialogs.map((id: string) => {
            const Dialog = navigation[id];
            return <Dialog key={id} {...props} navigate={navigate} closeDialog={removeDialog} />;
          })}
        </AppNavigationContext.Provider>
      );
    };
    return WithNavigation;
  };
