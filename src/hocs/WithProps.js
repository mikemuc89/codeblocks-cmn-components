/* @flow */
import * as React from 'react';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';

export type WithPropsAddedPropsType = {};

export type WithPropsParamsType = {|
  injectedProps: Object
|};

export type WithPropsRequiredPropsType = {};

export default ({ injectedProps }: WithPropsParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const WithProps = (props: WithPropsRequiredPropsType) => <WrappedComponent {...injectedProps} {...props} />;
    return WithProps;
  };
