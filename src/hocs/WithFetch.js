/* @flow */
import * as React from 'react';
import { type ApiType } from '@omnibly/codeblocks-cmn-schema/src/api';
import { type HocWrappedComponentType } from '@omnibly/codeblocks-cmn-types';
import { request } from '@omnibly/codeblocks-cmn-utils';
import history from '../history';
import useEventListener from '../hooks/useEventListener';
import { type NavigateType } from './WithNavigation';
import { type UpdateStateType } from './WithState';

export type FetchType = (payload: Object) => Promise<Object>;

export type CustomFetchType = (api: ApiType) => FetchType;

export type CallbackParamsType = {|
  back: () => Promise<void>,
  fetch: FetchType,
  fetchCustom: CustomFetchType,
  form: Object,
  navigate: NavigateType
|};

export type CallbackType = (params: CallbackParamsType) => void;

export type WithFetchAddedPropsType = {|
  fetch: FetchType,
  loading?: boolean
|};

export type WithFetchParamsType = {|
  api?: ApiType,
  callbacks?: { [actionKey: string]: CallbackType }
|};

export type WithFetchRequiredPropsType = {|
  form: Object,
  hasInitialData?: boolean,
  navigate: NavigateType,
  params?: Object,
  updateState: UpdateStateType
|};

export default ({ api, callbacks = {}, historyCallbacks = {} }: WithFetchParamsType) =>
  (WrappedComponent: HocWrappedComponentType) => {
    const asyncBack = () => {
      history.back();
      return Promise.resolve();
    };

    const WithFetch = (props: WithFetchRequiredPropsType) => {
      const [loading, setLoading] = React.useState<boolean>(false);
      const { form, hasInitialData, navigate, params, updateState } = props;

      const fetch = React.useCallback(
        (api: ApiType) =>
          (payload: Object): Promise<Object> =>
            new Promise((resolve: () => void) => {
              setLoading(true);
              api(request)({
                ...form,
                ...payload
              }).then(
                (data: Object) => {
                  const evt = new CustomEvent('dataFetched', { detail: { payload, response: data } });
                  document.dispatchEvent(evt);
                  updateState({
                    form: {
                      $apply: (form: Object) => ({
                        ...form,
                        ...Object.entries(data).reduce(
                          (result: Object, [key, value]: [string, mixed]) => ({
                            ...result,
                            [key]: Array.isArray(value)
                              ? key === 'items' && form[key]
                                ? [...form[key], ...data[key]]
                                : value
                              : value instanceof Object
                              ? {
                                  ...form[key],
                                  ...value
                                }
                              : value
                          }),
                          {}
                        )
                      })
                    }
                  }).then(resolve);
                  setLoading(false);
                },
                ($set: Error) => {
                  setLoading(false);
                  updateState({
                    form: { errors: { $set } }
                  }).then(resolve);
                }
              );
            }),
        [form, setLoading, updateState]
      );

      const handleButtonClick = React.useCallback(
        (e: MouseEvent) => {
          const button = e.target.closest('button');
          if (button && !button.disabled) {
            button.disabled = true;
            const value = button.getAttribute('value') || '';
            if (value in callbacks) {
              e.stopPropagation();
              e.preventDefault();
              callbacks[value]({ back: asyncBack, fetch: api && fetch(api), fetchCustom: fetch, form, navigate }).then(
                (result: any) => {
                  button.disabled = false;
                  return result;
                },
                () => {
                  button.disabled = false;
                }
              );
            } else {
              button.disabled = false;
            }
          }
        },
        [fetch, form, navigate]
      );

      React.useEffect(() => {
        if (typeof api === 'function') {
          if (!hasInitialData) {
            fetch(api)({ meta: { data: params } });
          }
        }

        return () => {
          if (api && api.cancelable && form?.step !== 'end') {
            fetch(api)({ action: 'cancel', form });
          }
        };
      }, [fetch, form, hasInitialData, params]);

      useEventListener('click', handleButtonClick, typeof document === 'undefined' ? null : document);

      return <WrappedComponent {...props} fetch={fetch} loading={loading} />;
    };
    return WithFetch;
  };
