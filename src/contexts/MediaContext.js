/* @flow */
import * as React from 'react';
import { BREAKPOINT_NAMES } from '@omnibly/codeblocks-cmn-types/src/constants';

type MediaContextType = {|
  breakpoint?: $Values<typeof BREAKPOINT_NAMES>
|};

const DEFAULT_VALUE: MediaContextType = {
  breakpoint: undefined
};

export default React.createContext(DEFAULT_VALUE);
