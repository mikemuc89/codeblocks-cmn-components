/* @flow */
import * as React from 'react';

type NavigationType = Object;

const DEFAULT_VALUE: NavigationType = {};

export default React.createContext(DEFAULT_VALUE);
