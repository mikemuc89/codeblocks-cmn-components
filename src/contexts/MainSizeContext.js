/* @flow */
import * as React from 'react';

type MainSizeContextType = {|
  footerHeight: number,
  headerHeight: number,
  innerWidth?: number | string,
  menuHeight: number
|};

const DEFAULT_VALUE: MainSizeContextType = {
  footerHeight: 0,
  headerHeight: 0,
  innerWidth: undefined,
  menuHeight: 0
};

export default React.createContext(DEFAULT_VALUE);
