/* @flow */
import * as React from 'react';
import { type LocationType } from '@omnibly/codeblocks-cmn-types';

const DEFAULT_VALUE: ?LocationType = null;

export default React.createContext(DEFAULT_VALUE);
