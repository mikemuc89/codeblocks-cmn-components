/* @flow */
import * as React from 'react';

type BackgroundColorContextType = {|
  backgroundColor: string,
  backgroundColorFocus: string,
  backgroundColorHover: string
|};

const DEFAULT_VALUE: BackgroundColorContextType = {
  backgroundColor: '#ffffff',
  backgroundColorFocus: '#dadada',
  backgroundColorHover: '#dadada'
};

export default React.createContext(DEFAULT_VALUE);
