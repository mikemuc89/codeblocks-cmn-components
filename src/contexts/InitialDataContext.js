/* @flow */
import * as React from 'react';

const DEFAULT_VALUE: ?Object = null;

export default React.createContext(DEFAULT_VALUE);
