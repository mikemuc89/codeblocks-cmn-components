/* @flow */
import * as React from 'react';

type DialogContextType = {|
  isDialog: boolean
|};

const DEFAULT_VALUE: DialogContextType = {
  isDialog: false
};

export default React.createContext(DEFAULT_VALUE);
