/* @flow */
import * as React from 'react';

type DialogContextType = {|
  hamburger: any,
  hamburgerOpen: boolean,
  subscribe?: (id: string, component: React.ComponentType<any>) => void,
  toggleHamburger?: (forceState?: boolean) => void,
  unsubscribe?: (id: string) => void
|};

const DEFAULT_VALUE: DialogContextType = {
  hamburger: null,
  hamburgerOpen: false,
  subscribe: undefined,
  toggleHamburger: undefined,
  unsubscribe: undefined
};

export default React.createContext(DEFAULT_VALUE);
