/* @flow */
import * as React from 'react';
import { type SetStateCallbackType } from '@omnibly/codeblocks-cmn-types';

type TranslationContextType = {|
  lang: string,
  setLang?: SetStateCallbackType<string>,
  t?: (key: string, defaultValue: string) => string
|};

const DEFAULT_VALUE: TranslationContextType = {
  lang: 'pl',
  setLang: undefined,
  t: undefined
};

export default React.createContext(DEFAULT_VALUE);
