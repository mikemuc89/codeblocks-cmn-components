/* @flow */
import * as React from 'react';

type ParentCacheContextType = void | {|
  setParentCache?: $Values<typeof BREAKPOINT_NAMES>
|};

const DEFAULT_VALUE: ParentCacheContextType = {};

export default React.createContext(DEFAULT_VALUE);
