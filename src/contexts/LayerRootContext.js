/* @flow */
import * as React from 'react';
import { setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';

export const setLayerRootStyle = (node: HTMLElement) => {
  setElementStyle(node, {
    position: 'absolute',
    bottom: unitize(0),
    left: unitize(0),
    right: unitize(0),
    top: unitize(0)
  });
};

type LayerRootContextType = HTMLElement | null;

const DEFAULT_VALUE: LayerRootContextType = null;

export default React.createContext(DEFAULT_VALUE);
