/* @flow */
import * as React from 'react';
import { createBrowserHistory, createMemoryHistory } from 'history';
import { type LocationType } from '@omnibly/codeblocks-cmn-types';
import { guid } from '@omnibly/codeblocks-cmn-utils';

const history = typeof window === 'undefined' ? createMemoryHistory() : createBrowserHistory();

export const HISTORY_ACTIONS = Object.freeze({
  POP: 'POP',
  PUSH: 'PUSH',
  REPLACE: 'REPLACE'
});

export const updateHistoryState = (currentLocation: LocationType, newState: Object) => {
  const { hash, pathname, search, state } = currentLocation;
  history.replace({ hash, pathname, search }, { ...state, ...newState });
};

export const addDerivedHistoryState = (currentLocation: LocationType, newState: Object) => {
  const { hash, pathname, search, state } = currentLocation;
  history.push({ hash, pathname, search }, { ...state, ...newState });
};

export const removeForwardNavigationStack = (currentLocation: LocationType, newState?: Object = {}) => {
  const { hash, pathname, search, state } = currentLocation;
  history.replace({ hash, pathname, search }, { ...state, skip: true });
  history.push({ hash, pathname, search }, { ...state, ...newState });
};

export const createNewHistoryElement = (location: LocationType, path: string, overwriteState?: Object = {}) => {
  const { origin = '' } = typeof document === 'undefined' ? {} : document.location;
  const state = { ...(location.state || {}), gid: guid(), path, ...overwriteState };
  return { ...location, origin, state };
};

export const useHistoryTracking = ({ path }: { path: string }) => {
  const historyArray = React.useRef([]);
  const historyElement = React.useRef({});
  const historyIndex = React.useRef(null);

  React.useEffect(() => {
    if (historyIndex.current === null) {
      const length = window === 'undefined' ? 0 : window.history.length;
      const element = createNewHistoryElement(history.location, path, { flow: undefined });

      historyArray.current = [...Array.from(Array(length - 1)).map(() => null), element];
      historyElement.current = element;
      historyIndex.current = length - 1;

      updateHistoryState(history.location, element.state);
    }
  });

  const updateHistoryTracking = ({
    action,
    location
  }: {
    action: $Values<typeof HISTORY_ACTIONS>,
    location: LocationType
  }) => {
    const previousLocation = { ...historyElement.current };

    const newIndex = {
      [HISTORY_ACTIONS.POP]: () =>
        historyArray.current.findIndex((item: LocationType) => item && item.state.gid === location.state.gid),
      [HISTORY_ACTIONS.PUSH]: () => historyIndex.current + 1,
      [HISTORY_ACTIONS.REPLACE]: () => historyIndex.current
    }[action]();
    const isBack = action === HISTORY_ACTIONS.POP && (newIndex === -1 || newIndex < historyIndex.current);

    if (action === HISTORY_ACTIONS.PUSH) {
      const element = createNewHistoryElement(location, path);
      historyArray.current = [...historyArray.current.slice(0, historyIndex.current + 1), element];
      historyElement.current = element;
      historyIndex.current = newIndex;

      updateHistoryState(location, element.state);
    } else if (action === HISTORY_ACTIONS.REPLACE) {
      const element = createNewHistoryElement(location, path);

      historyArray.current = [
        ...historyArray.current.slice(0, historyIndex.current),
        element,
        ...historyArray.current.slice(historyIndex.current + 1)
      ];
      historyElement.current = element;
    } else if (action === HISTORY_ACTIONS.POP) {
      if (newIndex === -1) {
        const updateIndex = historyArray.current.lastIndexOf(null);
        const element = createNewHistoryElement(location, null, { path: location.state?.path });

        historyArray.current = [
          ...historyArray.current.slice(0, updateIndex),
          element,
          ...historyArray.current.slice(updateIndex + 1)
        ];
        historyElement.current = element;
        historyIndex.current = updateIndex;
      } else {
        const element = historyArray.current[newIndex];
        historyIndex.current = newIndex;
        historyElement.current = element;
      }
    }

    return { isBack, previousLocation };
  };

  return { historyArray, historyElement, historyIndex, updateHistoryTracking };
};

export default history;
