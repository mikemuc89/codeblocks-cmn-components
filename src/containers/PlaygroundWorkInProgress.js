/* @flow */
import * as React from 'react';
import FontAwesome from '../components/FontAwesome';
import Markdown from '../components/Markdown';
import Summary from '../components/Summary';
import View from '../components/View';
import AsPagePlaygroundView, { type PagePlaygroundViewPropsType } from '../hocs/AsPagePlaygroundView';

export default (AsPagePlaygroundView()(() => (
  <View backgroundColor="#eb7e0d" fullHeight>
    <Summary color="#ffffff" icon={<FontAwesome id="cogs" />} title="Work in progress">
      <Markdown color="#ffffff">Try again soon.</Markdown>
    </Summary>
  </View>
)): React.ComponentType<PagePlaygroundViewPropsType>);
