/* @flow */
import * as React from 'react';
import Loader from '../components/Loader';

type ViewProviderConfigType = {
  url?: string
};

type ViewProviderType = {
  config: ViewProviderConfigType,
  component: any,
  onLoad: () => Promise<Object>,
  additional?: Object
};

type ViewProviderStateType = {
  component?: any
};

export default class ViewProvider extends React.Component<
  ViewProviderType,
  ViewProviderStateType
> {
  _isMounted: boolean = false;

  state = {}

  componentDidMount() {
    this._isMounted = true;
    const { onLoad } = this.props;
    if (onLoad) {
      onLoad().then(({ default: component }) => {
        if (this._isMounted) {
          this.setState({ component });
        }
      });
    }
  }

  render() {
    const { component: SyncComponent } = this.props;
    const { component: Component = SyncComponent } = this.state || {};
    const props = Object.assign({}, this.props, { component: undefined, onLoad: undefined });

    return Component ? <Component {...props} /> : <Loader.Spinner />;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
}

// /* @flow */
// import * as React from 'react';
// import Loader from '../components/Loader';

// type ViewProviderType = {|
//   component: any,
//   onLoad: () => Promise<Object>
// |};

// const ViewProvider = ({ component: SyncComponent, onLoad, ...props }: ViewProviderType) => {
//   const isMounted = React.useRef(false);
//   const [Component, setComponent] = React.useState<React.ComponentType<any> | null>(null);

//   React.useEffect(() => {
//     isMounted.current = true;
//     if (onLoad) {
//       onLoad().then(({ default: component }: Object) => {
//         if (isMounted.current) {
//           setComponent(component);
//         }
//       });
//     }

//     return () => {
//       isMounted.current = false;
//     };
//   }, [onLoad]);

//   const ComponentToRender = Component || SyncComponent;

//   return ComponentToRender ? <ComponentToRender {...props} /> : <Loader.Spinner />;
// };

// export default (ViewProvider: React.ComponentType<ViewProviderType>);
