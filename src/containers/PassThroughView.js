/* @flow */
import * as React from 'react';
import AsView, { type ViewPropsType, type ViewType } from '../hocs/AsView';

const View = ({ Content }: ViewType) => Content;

export default (AsView()(View): React.ComponentType<ViewPropsType>);
