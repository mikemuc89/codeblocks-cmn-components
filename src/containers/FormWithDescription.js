/* @flow */
import * as React from 'react';
import Card from '../components/Card';
import Column from '../components/Column';
import Content from '../components/Content';
import useMedia, { BREAKPOINT_NAMES } from '../hooks/useMedia';

const FormWithDescription = ({ addition, badge, children, description, postScriptum }) => {
  const { breakpoint } = useMedia();
  const isMobileView = React.useMemo(() => [
    BREAKPOINT_NAMES.PHONE_L,
    BREAKPOINT_NAMES.PHONE_M,
    BREAKPOINT_NAMES.PHONE_S,
    BREAKPOINT_NAMES.TABLET
  ].includes(breakpoint), [breakpoint]);

  return (
    <Column.Set>
      <Column>
        <Card badge={isMobileView ? badge : undefined} addition={addition} postScriptum={isMobileView ? postScriptum : undefined}>
          {isMobileView && description}
          {children}
        </Card>
      </Column>
      {!isMobileView && (
        <Column>
          <Content>
            {badge}
            {description}
            {postScriptum}
          </Content>
        </Column>
      )}
    </Column.Set>
  );
};

export default FormWithDescription;
