/* @flow */
import * as React from 'react';
import LayerRootContext from '../contexts/LayerRootContext';
import styles from './LayerRootContainer.scss';

const LayerRootContainer = ({ children }) => {
  const layerRoot = React.useRef(null);
  const [layerRootElement, setLayerRootElement] = React.useState(null);

  const onPreventScroll = React.useCallback((e: Event) => {
    e.stopPropagation();
    e.preventDefault();
    e.returnValue = false;
    return false;
  }, []);

  React.useEffect(() => {
    if (layerRootElement) {
      layerRootElement.addEventListener('mousewheel', onPreventScroll);
    }
    return () => {
      if (layerRootElement) {
        layerRootElement.removeEventListener('mousewheel', onPreventScroll);
      }
    };
  }, [layerRootElement, onPreventScroll]);
  
  React.useEffect(() => {
    const el = layerRoot.current;
    if (el) {
      setLayerRootElement(el);
    }
    return () => {
      setLayerRootElement(null);
    }
  }, [setLayerRootElement]);

  return (
    <>
      <LayerRootContext.Provider value={layerRootElement}>
        {children}
      </LayerRootContext.Provider>
      <div ref={layerRoot} className={styles.LayerRoot} />
    </>
  );
};

export default LayerRootContainer;
