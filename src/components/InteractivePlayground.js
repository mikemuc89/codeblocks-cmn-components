/* @flow */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, unitize } from '@omnibly/codeblocks-cmn-utils';
import MainSizeContext from '../contexts/MainSizeContext';
import useOpenState from '../hooks/useOpenState';
import Card from './Card';
import CharIcon from './CharIcon';
import ColorIcon from './ColorIcon';
import Dialog from './Dialog';
import Field from './Field';
import FontAwesome from './FontAwesome';
import Hamburger from './Hamburger';
import Icon from './Icon';
import Image from './Image';
import Label from './Label';
import View from './View';
import styles from './InteractivePlayground.scss';

const CardContainer = ({ children }: { children: ReactChildren }) => (
  <View>
    <Card>{children}</Card>
  </View>
);

const DialogMiniContainer = ({ children }: { children: ReactChildren }) => <Dialog mini>{children}</Dialog>;

const DialogNanoContainer = ({ children }: { children: ReactChildren }) => <Dialog nano>{children}</Dialog>;

const DialogContainer = ({ children }: { children: ReactChildren }) => <Dialog>{children}</Dialog>;

const FieldContainer = ({ children }: { children: ReactChildren }) => <Field>{children}</Field>;

const FieldSideLabelContainer = ({ children }: { children: ReactChildren }) => (
  <Field label={<Label position={Label.POSITIONS.LEFT}>Label</Label>}>{children}</Field>
);

const FieldTopLabelContainer = ({ children }: { children: ReactChildren }) => (
  <Field label={<Label position={Label.POSITIONS.TOP}>Label</Label>}>{children}</Field>
);

const HamburgerContainer = ({ children }: { children: ReactChildren }) => <Hamburger>{children}</Hamburger>;

const ViewContainer = ({ children }: { children: ReactChildren }) => <View>{children}</View>;

const ViewPaddedContainer = ({ children }: { children: ReactChildren }) => <View padding={16}>{children}</View>;

const charIconExample = <CharIcon text="AB" />;

const colorIconExample = <ColorIcon color="#ffd102" />;

const fontAwesomeExample = <FontAwesome id="bell-slash" />;

const iconExample = <Icon src={require('../assets/images/icons/cookies-normal.svg')} />;

const PROP_TYPES = Object.freeze({
  BOOLEAN: 'InteractivePlayground.PROP_TYPES.BOOLEAN',
  CALLBACK: 'InteractivePlayground.PROP_TYPES.CALLBACK',
  CHOICE: 'InteractivePlayground.PROP_TYPES.CHOICE',
  ICON: 'InteractivePlayground.PROP_TYPES.ICON',
  IMAGE: 'InteractivePlayground.PROP_TYPES.IMAGE',
  INPUT: 'InteractivePlayground.PROP_TYPES.INPUT',
  KINDS: 'InteractivePlayground.PROP_TYPES.KINDS',
  NUMBER_INPUT: 'InteractivePlayground.PROP_TYPES.NUMBER_INPUT',
  PARENT: 'InteractivePlayground.PROP_TYPES.PARENT',
  TEXT: 'InteractivePlayground.PROP_TYPES.TEXT'
});

const ICON_ITEM_KEYS = Object.freeze({
  CHAR_ICON: 'CharIcon',
  COLOR_ICON: 'ColorIcon',
  FONT_AWESOME: 'FontAwesome',
  ICON: 'Icon'
});

const ICON_ITEM_KEYS_TO_VALUE = Object.freeze({
  CHAR_ICON: charIconExample,
  COLOR_ICON: colorIconExample,
  FONT_AWESOME: fontAwesomeExample,
  ICON: iconExample
});

const KIND_ITEM_KEYS = Object.freeze({
  DARK: 'Dark',
  ERROR: 'Error',
  INFO: 'Info',
  LIGHT: 'Light',
  MARKETING: 'Marketing',
  PLAIN: 'Plain',
  PRIMARY: 'Primary',
  SECONDARY: 'Secondary',
  SUBMIT: 'Submit',
  SUCCESS: 'Success',
  WARNING: 'Warning'
});

const PARENT_ITEM_KEYS = Object.freeze({
  CARD: 'Card',
  DIALOG_MINI: 'Dialog (mini)',
  DIALOG_NANO: 'Dialog (nano)',
  DIALOG_NORMAL: 'Dialog (normal)',
  FIELD: 'Field',
  FIELD_LABEL_SIDE: 'Field (side label)',
  FIELD_LABEL_TOP: 'Field (top label)',
  HAMBURGER: 'Hamburger',
  VIEW: 'View',
  VIEW_PADDED: 'View (padded)'
});

const PARENT_ITEM_KEYS_TO_VALUE = Object.freeze({
  CARD: CardContainer,
  DIALOG_MINI: DialogMiniContainer,
  DIALOG_NANO: DialogNanoContainer,
  DIALOG_NORMAL: DialogContainer,
  FIELD: FieldContainer,
  FIELD_LABEL_SIDE: FieldSideLabelContainer,
  FIELD_LABEL_TOP: FieldTopLabelContainer,
  HAMBURGER: HamburgerContainer,
  VIEW: ViewContainer,
  VIEW_PADDED: ViewPaddedContainer
});

const TEXT_ITEM_KEYS = Object.freeze({
  EMPTY: 'Empty',
  LONG: 'Long',
  MARKDOWN: 'Markdown',
  NONE: 'None',
  SHORT: 'Short',
  UNDEFINED: 'Undefined'
});

const TEXT_ITEM_KEYS_TO_VALUE = Object.freeze({
  EMPTY: '',
  LONG: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed venenatis nulla sit amet iaculis porta. Vestibulum et sagittis metus. Nulla vitae velit turpis. Phasellus efficitur porttitor luctus. Vivamus vitae lacinia enim. Praesent ultricies felis non facilisis venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer tempor sagittis erat. Vestibulum vel ultrices metus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ut metus neque. Morbi eleifend ante lectus, id ultrices dolor egestas vitae. Praesent vitae tellus id tortor elementum viverra. Proin quis dolor ac eros mattis semper vitae eget nisi.',
  MARKDOWN: `## Lorem ipsum dolor sit amet
Consectetur adipiscing elit. Sed venenatis nulla sit amet iaculis porta. Vestibulum et sagittis metus. **Nulla vitae velit turpis.** Phasellus efficitur porttitor luctus.
Vivamus vitae lacinia enim. Praesent ultricies felis non facilisis venenatis.`,
  NONE: null,
  SHORT: 'Lorem ipsum dolor sit amet',
  UNDEFINED: undefined
});

const DEFAULT_PROPS = {
  [PROP_TYPES.BOOLEAN]: {},
  [PROP_TYPES.CHOICE]: {},
  [PROP_TYPES.CALLBACK]: {
    value: true
  },
  [PROP_TYPES.ICON]: {
    items: ICON_ITEM_KEYS,
    itemsResolver: ICON_ITEM_KEYS_TO_VALUE,
    value: ''
  },
  [PROP_TYPES.IMAGE]: {},
  [PROP_TYPES.INPUT]: {},
  [PROP_TYPES.NUMBER_INPUT]: {},
  [PROP_TYPES.KINDS]: {
    items: KIND_ITEM_KEYS,
    itemsResolver: KINDS
  },
  [PROP_TYPES.PARENT]: {
    items: PARENT_ITEM_KEYS,
    itemsResolver: PARENT_ITEM_KEYS_TO_VALUE,
    value: 'VIEW'
  },
  [PROP_TYPES.TEXT]: {
    items: TEXT_ITEM_KEYS,
    itemsResolver: TEXT_ITEM_KEYS_TO_VALUE
  }
};

const MOCK_CALLBACK = (e: Event, key: string) => {
  /* eslint-disable-next-line no-console */
  console.log(key, e);
};

const DEVICES = Object.freeze({
  DESKTOP: 'Desktop',
  IPHONE_12: 'iPhone 12',
  RESPONSIVE: 'Responsive'
});

const DEVICE_PARAMS = Object.freeze({
  DESKTOP: {
    height: '100%',
    width: '100%'
  },
  IPHONE_12: {
    height: 800,
    width: 500
  },
  RESPONSIVE: {
    height: '100%',
    width: '100%'
  }
});

const DEVICE_CONFIG = {
  device: { items: DEVICES, type: PROP_TYPES.CHOICE },
  flip: { type: PROP_TYPES.BOOLEAN }
};

const deviceToParams = ({
  device: { value: device } = {},
  flip: { value: flip = false } = {}
}: {
  device: { value: $Keys<typeof DEVICES> },
  flip: { value?: boolean }
} = {}) => {
  const sanitizedDevice = device && device in DEVICES ? device : 'RESPONSIVE';
  const { height, width } = DEVICE_PARAMS[sanitizedDevice];

  return {
    height: flip ? unitize(width) : unitize(height),
    resizeable: ['RESPONSIVE'].includes(sanitizedDevice),
    width: flip ? unitize(height) : unitize(width)
  };
};

const copyStylesToFrame = (frameDocument: HTMLElement) => {
  document.head.querySelectorAll('link,style').forEach((node: HTMLElement) => {
    frameDocument.head.appendChild(node.cloneNode(true));
  });
};

const parseValue = (val?: ?string) => {
  if ([undefined, 'true'].includes(val)) {
    return true;
  }
  if (['false'].includes(val)) {
    return false;
  }
  if (['null', 'none'].includes(val)) {
    return null;
  }
  if (/^\d+$/.test(val)) {
    return parseInt(val);
  }
  if (/^\d+\.\d+$/.test(val)) {
    return parseFloat(val);
  }
  return decodeURI(val);
};

const parseDocQuery = (query: string = window.location.search) => {
  const stripped = query.startsWith('?') ? query.slice(1) : query;
  const params = stripped.split('&').reduce((result: Object, item: string) => {
    const [key, val] = item.split('=');
    if (key in result) {
      return {
        ...result,
        [key]: [...result[key], parseValue(val)]
      };
    }
    return {
      ...result,
      [key]: parseValue(val)
    };
  }, {});
  return params;
};

const prepareInitialState = (config: Object, viewConfig: Object) => {
  const queryProps = parseDocQuery();

  return {
    ...Object.entries(config).reduce(
      (result: Object, [key, value]: [string, mixed]) => {
        if (typeof value !== 'object') {
          throw new Error('Item value should be an object');
        }
        const { type, ...defaultProps } = value;
        const props = DEFAULT_PROPS[type];
        const queryValue = queryProps[key];

        return {
          ...result,
          [key]: {
            ...props,
            ...defaultProps,
            ...(queryValue === undefined ? {} : { value: queryValue })
          }
        };
      },
      {}
    ),
    ...Object.entries(viewConfig).reduce(
      (result: Object, [key, value]: [string, mixed]) => {
        if (typeof value !== 'object') {
          throw new Error('Item value should be an object');
        }
        const { type, ...defaultProps } = value;
        const props = DEFAULT_PROPS[type];
        const queryValue = queryProps[key];

        return {
          ...result,
          [key]: {
            ...props,
            ...defaultProps,
            ...(queryValue === undefined ? {} : { value: queryValue })
          }
        };
      },
      {}
    )
  };
};

const preparePropsFromState = (config: Object, state: Object) =>
  Object.entries(config).reduce(
    (result: Object, [key, value]: [string, mixed]) => {
      if (typeof value !== 'object') {
        throw new Error('Item value should be an object');
      }
      const { type, ...defaultProps } = value;
      return ({
        ...result,
        [key]: {
          [PROP_TYPES.BOOLEAN]: ({ value }: { value: boolean }) => value,
          [PROP_TYPES.CALLBACK]: ({ value }: { value: (e: Event) => void }) =>
            value ? (e: Event) => MOCK_CALLBACK(e, key) : undefined,
          [PROP_TYPES.CHOICE]: ({ itemsResolver, value }: { itemsResolver: Object, value: string }) =>
            value ? (itemsResolver ? itemsResolver[value] : value) : undefined,
          [PROP_TYPES.KINDS]: ({ value }: { value: $Values<typeof KINDS> }) => value,
          [PROP_TYPES.ICON]: ({ itemsResolver, value }: { itemsResolver: Object, value: string }) =>
            value ? (itemsResolver ? itemsResolver[value] : value) : undefined,
          [PROP_TYPES.IMAGE]: ({ value }: { value: string }) => (value ? <Image src={value} /> : undefined),
          [PROP_TYPES.INPUT]: ({ value }: { value: string }) => value,
          [PROP_TYPES.NUMBER_INPUT]: ({ value }: { value: string | number }) =>
            isNaN(parseInt(value)) ? undefined : parseInt(value),
          [PROP_TYPES.PARENT]: ({
            itemsResolver,
            value
          }: {
            itemsResolver: Object,
            value: $Values<typeof PARENT_ITEM_KEYS>
          }) => (value ? (itemsResolver ? itemsResolver[value] : value) : ViewContainer),
          [PROP_TYPES.TEXT]: ({
            itemsResolver,
            value
          }: {
            itemsResolver: Object,
            value: $Values<typeof TEXT_ITEM_KEYS>
          }) => (value ? (itemsResolver ? itemsResolver[value] : value) : undefined)
        }[type](state[key])
      })
    },
    {}
  );

const prepareQueryParamsFromState = (state: Object) =>
  Object.entries(state)
    .reduce((result: Array<string>, [key, { value }]: [string, mixed]) => {
      if (value === undefined) {
        return result;
      }
      if (value === null) {
        return [...result, `${key}=null`];
      }
      if (value === true) {
        return [...result, `${key}`];
      }
      return [...result, `${key}=${encodeURI(value.toString())}`];
    }, [])
    .join('&');

type FieldType<ValueType> = {|
  id: string,
  label?: string,
  onChange: (id: string, newValue: ValueType) => void,
  value: ValueType
|};

const Bool = ({ id, label, onChange, value = false }: FieldType<boolean>) => {
  const handleChange = React.useCallback(
    (e: Event) => {
      if (onChange) {
        const newValue = e.target.checked;
        onChange(id, newValue);
      }
    },
    [id, onChange]
  );

  return (
    <span className={cx(styles.Control__Bool)}>
      <label>{label}</label>
      <input checked={value} onChange={handleChange} type="checkbox" value="" />
    </span>
  );
};

const ImageField = ({ id, label, onChange, value = '' }: FieldType<string>) => {
  const handleChange = React.useCallback(
    (e: Event) => {
      if (onChange) {
        const newValue = e.target.value;
        onChange(id, newValue);
      }
    },
    [id, onChange]
  );

  const handleClick = React.useCallback(() => {
    if (onChange) {
      onChange(id, 'https://picsum.photos/200/300');
    }
  }, [id, onChange]);

  return (
    <span className={cx(styles.Control__Image)}>
      <label>{label}</label>
      <input onChange={handleChange} type="text" value={value} />
      <a onClick={handleClick}>random</a>
    </span>
  );
};

const Input = ({ id, label, onChange, value = '' }: FieldType<string>) => {
  const handleChange = React.useCallback(
    (e: Event) => {
      if (onChange) {
        const newValue = e.target.value;
        onChange(id, newValue);
      }
    },
    [id, onChange]
  );

  return (
    <span className={cx(styles.Control__Input)}>
      <label>{label}</label>
      <input onChange={handleChange} type="text" value={value} />
    </span>
  );
};

const NumberInput = ({ id, label, onChange, value = '' }: FieldType<string | number>) => {
  const handleChange = React.useCallback(
    (e: Event) => {
      if (onChange) {
        const newValue = e.target.value;
        if (/^\d*$/.test(newValue)) {
          onChange(id, newValue);
        }
      }
    },
    [id, onChange]
  );

  return (
    <span className={cx(styles.Control__NumberInput)}>
      <label>{label}</label>
      <input onChange={handleChange} type="text" value={value} />
    </span>
  );
};

type OptionsFieldType<ValueType, ItemType> = {|
  ...FieldType<ValueType>,
  items: Array<{ [key: string]: ItemType }>
|};

const Select = ({ id, items = [], label, onChange, value = '' }: OptionsFieldType<string, string>) => {
  const handleChange = React.useCallback(
    (e: Event) => {
      if (onChange) {
        const newValue = e.target.value;
        onChange(id, newValue);
      }
    },
    [id, onChange]
  );

  return (
    <span className={cx(styles.Control__Select)}>
      <label>{label}</label>
      <select onChange={handleChange} value={value}>
        <option value="">-</option>
        {Object.entries(items).map(([key, value]: [string, mixed]) => (
          <option key={key} value={key}>
            {value.toString()}
          </option>
        ))}
      </select>
    </span>
  );
};

type FormType = {|
  config: Object,
  handler: (key: string) => Object,
  state: Object
|};

const Form = ({ config, handler, state }: FormType) => (
  <div className={styles.Form}>
    {Object.entries(config).map(([key, { type }]: [string, { type: $Values<typeof PROP_TYPES> }]) => (
      <div key={key} className={styles.Item}>
        {((Component: React.ComponentType<any>) => (
          <Component label={key} {...handler(key)} />
        ))(
          {
            [PROP_TYPES.BOOLEAN]: Bool,
            [PROP_TYPES.CALLBACK]: Bool,
            [PROP_TYPES.CHOICE]: Select,
            [PROP_TYPES.KINDS]: Select,
            [PROP_TYPES.ICON]: Select,
            [PROP_TYPES.IMAGE]: ImageField,
            [PROP_TYPES.INPUT]: Input,
            [PROP_TYPES.NUMBER_INPUT]: NumberInput,
            [PROP_TYPES.PARENT]: Select,
            [PROP_TYPES.TEXT]: Select
          }[type]
        )}
      </div>
    ))}
  </div>
);

const PANE_TITLE_HEIGHT = 24;

type PaneType = {|
  children: ReactChildren,
  open?: boolean,
  title: string
|};

const Pane = ({ children, open: initiallyOpen, title }: PaneType) => {
  const { open, onToggle } = useOpenState({ open: initiallyOpen });

  return (
    <div className={cx(styles.Pane, cx.open(styles, 'Pane', { open }))}>
      {open ? (
        <>
          <span className={styles.CloseButton} onClick={onToggle}>
            x
          </span>
          <span className={styles.OpenTitle} onClick={onToggle}>
            {title}
          </span>
          <div className={styles.FormWrapper}>{children}</div>
        </>
      ) : (
        <span className={styles.Title} onClick={onToggle}>
          {title}
        </span>
      )}
    </div>
  );
};

const StatePane = ({ open, ...props }: FormType & { open?: boolean }) => (
  <Pane open={open} title="Control state">
    <Form {...props} />
  </Pane>
);

const ViewPane = ({ open, ...props }: FormType & { open?: boolean }) => (
  <Pane open={open} title="View config">
    <Form {...props} />
  </Pane>
);

export type InteractivePlaygroundType = {|
  children: ReactChildren,
  className?: string,
  config: Object,
  label: string
|};

const InteractivePlayground = ({ children, className, config, label }: InteractivePlaygroundType) => {
  const frameLoaded = React.useRef(false);
  const { headerHeight, menuHeight, footerHeight } = React.useContext(MainSizeContext);

  const [frameDocument, setFrameDocument] = React.useState(null);

  const handleOnLoad = React.useCallback(
    (e: Event) => {
      if (!e.defaultPrevented && !frameLoaded.current) {
        const frameDocument = e.target.contentDocument;
        setFrameDocument(frameDocument);
        copyStylesToFrame(frameDocument);
        frameLoaded.current = true;
      }
    },
    [setFrameDocument]
  );

  const [state, setState] = React.useState(prepareInitialState(config, DEVICE_CONFIG));

  const viewHeightDeductions = headerHeight + menuHeight + footerHeight + 3 * PANE_TITLE_HEIGHT;

  React.useEffect(() => {
    const { origin, pathname } = document.location;
    const queryParams = prepareQueryParamsFromState(state);
    window.history.replaceState(null, document.title, `${origin}${pathname}?${queryParams}`);
  }, [state]);

  const handleWorkStateChange = React.useCallback(
    (id: string, value: any) => {
      setState((state: Object) => ({
        ...state,
        [id]: {
          ...state[id],
          value
        }
      }));
    },
    [setState]
  );

  const workHandler = React.useCallback(
    (id: string) => ({
      id,
      onChange: handleWorkStateChange,
      ...state[id]
    }),
    [handleWorkStateChange, state]
  );

  const { device, flip, ...stateForProps } = state;
  const { resizeable, ...style } = deviceToParams({ device, flip });

  const { parent: ParentComponent, ...childrenProps } = preparePropsFromState(config, stateForProps);

  const inFrameElement = <ParentComponent>{children(childrenProps, handleWorkStateChange)}</ParentComponent>;

  return (
    <div className={cx(styles.InteractivePlayground, resizeable && styles.InteractivePlayground__Resizeable)}>
      <div className={styles.Panes}>
        <div className={styles.Label}>{label}</div>
        <ViewPane config={DEVICE_CONFIG} handler={workHandler} state={{ device, flip }} />
        <StatePane config={config} handler={workHandler} state={state} />
      </div>
      <div className={styles.FrameWrapper} style={{ height: `calc(100vh - ${unitize(viewHeightDeductions)})` }}>
        <iframe srcDoc="<!DOCTYPE html>" style={style} className={styles.Frame} onLoad={handleOnLoad}>
          {frameDocument && ReactDOM.createPortal(inFrameElement, frameDocument.body)}
        </iframe>
      </div>
    </div>
  );
};

export default (
  Object.assign(InteractivePlayground, {
    PROP_TYPES
  }): React.ComponentType<InteractivePlaygroundType> & {
    PROP_TYPES: Object
  }
);
