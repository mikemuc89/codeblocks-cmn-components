/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import styles from './Steps.scss';

type TriangleType = {|
  active?: boolean,
  className?: string,
  disabled?: boolean
|};

const Triangle = React.forwardRef(({ active, className, disabled }: TriangleType, forwardedRef: ReactForwardedRefType) => (
  <div
    ref={forwardedRef}
    className={cx(styles.Triangle, cx.control(styles, 'Triangle', { active, disabled }), className)}
  >
    <div className={styles.Body} />
    <div className={styles.Border} />
  </div>
));

type StepOwnPropsType = {|
  active?: boolean,
  children: ReactChildren,
  disabled?: boolean  
|};

type WrappedStepType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...StepOwnPropsType
|};

const Step = React.forwardRef(
  ({ LinkComponent, active, children, className, disabled, linkProps }: WrappedStepType, forwardedRef: ReactForwardedRefType) => (
    <LinkComponent
      ref={forwardedRef}
      className={cx(styles.Step, cx.control(styles, 'Step', { active, disabled }), className)}
      {...(active ? { 'data-active': true } : {})}
      {...linkProps}
    >
      {children}
      <Triangle active={active} disabled={disabled} />
    </LinkComponent>
  )
);

export type StepType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...StepOwnPropsType
|};

type StepsOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactElementsChildren<StepType>,
  lastArrow?: boolean  
|};

type WrappedStepsType = {|
  ...AsComponentExtensionType,
  ...StepsOwnPropsType
|};

const Steps = React.forwardRef(
  ({ align = ALIGN_TYPES.LEFT, border, className, children, lastArrow }: WrappedStepsType, forwardedRef: ReactForwardedRefType) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Steps,
        cx.alignSingle(styles, 'Steps', { align }),
        lastArrow && styles.Steps__LastArrow,
        border && styles.Steps__Border,
        className
      )}
    >
      {children}
    </div>
  )
);

export type StepsType = {|
  ...AsComponentType,
  ...StepsOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Steps)(Steps), {
    Step: AsComponent(ComponentNames.Steps.Step)(WithLink()(Step))
  }, {
    ALIGN_TYPES
  }): React.ComponentType<StepsType> & {
    Step: React.ComponentType<StepType>
  } & {
    ALIGN_TYPES: Object
  }
);
