/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type IconPropType, type ImagePropType } from '../types';
import styles from './Empty.scss';

const ICON_SIZE = 80;
const ICON_INNER_SIZE = 100;

type EmptyOwnPropsType = {|
  children: ReactChildren,
  image: ImagePropType | IconPropType,
  title: string
|};

type WrappedEmptyType = {|
  ...AsComponentExtensionType,
  ...EmptyOwnPropsType
|};

const Empty = React.forwardRef(({ children, className, image, title }: WrappedEmptyType, forwardedRef: ReactForwardedRefType) => (
  <div ref={forwardedRef} className={cx(styles.Empty, className)}>
    {image &&
      React.cloneElement(image, {
        alt: 'Brak danych',
        className: styles.Image,
        size: ICON_SIZE,
        ...{
          [ComponentNames.CharIcon]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: ICON_INNER_SIZE
          }
        }[image.type.componentId]
      })}
    <div className={styles.Title}>{title}</div>
    {children && <div className={styles.Content}>{children}</div>}
  </div>
));

export type EmptyType = {|
  ...AsComponentType,
  ...EmptyOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Empty)(Empty), {}): React.ComponentType<EmptyType>
);
