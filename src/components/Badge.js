/* #__flow */
import * as React from 'react';
import { KINDS, POSITIONS_CORNERS as POSITIONS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithPosition, { type WithPositionExtensionType, type WithPositionType } from '../hocs/components/WithPosition';
import { type IconPropType } from '../types';
import styles from './Badge.scss';

type BadgeOwnPropsType = {|
  children: IconPropType
|};

type WrappedBadgeType = {|
  ...AsComponentExtensionType,
  ...WithPositionExtensionType,
  ...BadgeOwnPropsType
|};

const Badge = React.forwardRef(
  ({ children, className, positionElementRef, ...props }: WrappedBadgeType, forwardedRef: ReactForwardedRefType) =>
    React.cloneElement(children, {
      className: cx(styles.Badge, children.props.className, className),
      ref: mergeRefs(forwardedRef, positionElementRef),
      ...props
    })
);

export type BadgeType = {|
  ...AsComponentType,
  ...WithPositionType,
  ...BadgeOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Badge)(WithPosition()(Badge)), {
    KINDS,
    POSITIONS,
    SHAPES
  }): React.ComponentType<BadgeType> & {
    KINDS: Object,
    POSITIONS: Object,
    SHAPES: Object
  }
);
