/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { ALIGN_TYPES, KINDS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, processChildren } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import AsControlWithItems, { ControlWithItemsContext, GroupItemContext, type AsControlWithItemsExtensionType, type AsControlWithItemsType } from '../hocs/components/AsControlWithItems';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import { type IconPropType } from '../types';
import FontAwesome from './FontAwesome';
import styles from './Option.scss';

const ICON_SIZE = 38;
const ICON_INNER_SIZE = 16;

const ARROW_KINDS = Object.freeze({
  CHECK: 'OPTION.ARROW_KINDS.CHECK',
  CLOSE: 'OPTION.ARROW_KINDS.CLOSE',
  EMPTY: 'OPTION.ARROW_KINDS.EMPTY',
  OPEN: 'OPTION.ARROW_KINDS.OPEN',
  REMOVE: 'OPTION.ARROW_KINDS.REMOVE'
});

const ARROW_KIND_TO_COMPONENT = Object.freeze({
  [ARROW_KINDS.CLOSE]: <FontAwesome id="chevron-up" />,
  [ARROW_KINDS.CHECK]: <FontAwesome id="check" />,
  [ARROW_KINDS.EMPTY]: <FontAwesome id="empty" />,
  [ARROW_KINDS.OPEN]: <FontAwesome id="chevron-down" />,
  [ARROW_KINDS.REMOVE]: <FontAwesome id="times" />
});

type OptionOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  action?: IconPropType,
  arrow?: IconPropType,
  border?: boolean,
  children: ReactChildren,
  clickable?: boolean,
  disabled?: boolean,
  icon?: IconPropType,
  onClick?: (e: MouseEvent) => void,
  subtitle?: string,
  verticalTop?: boolean
|};

export type WrappedOptionType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...OptionOwnPropsType
|};

const Option = React.forwardRef(
  (
    {
      align = ALIGN_TYPES.LEFT,
      action,
      arrow,
      border = true,
      children,
      className,
      clickable = true,
      disabled,
      icon,
      onClick,
      subtitle,
      themeElementRef,
      verticalTop
    }: WrappedOptionType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const Component = clickable ? 'button' : 'div';

    const actionElement =
      !disabled &&
      action &&
      React.cloneElement(action, {
        className: styles.Action,
        size: ICON_SIZE,
        ...{
          [ComponentNames.CharIcon]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: ICON_INNER_SIZE
          }
        }[action.type.componentId]
      });

    const arrowElement =
      !disabled &&
      arrow &&
      React.cloneElement(ARROW_KIND_TO_COMPONENT[arrow], {
        className: styles.Arrow,
        innerSize: ICON_INNER_SIZE,
        size: ICON_SIZE
      });

    const iconElement =
      icon &&
      React.cloneElement(icon, {
        border: icon.props.border === undefined ? true : icon.props.border,
        className: styles.Icon,
        focusable: false,
        hoverable: false,
        size: icon.props.size || ICON_SIZE,
        shape: icon.props.shape || SHAPES.SQUARE
      });

    return (
      <Component
        ref={mergeRefs(forwardedRef, themeElementRef)}
        className={cx(
          styles.Option,
          action && styles.Option__WithAction,
          arrow && styles.Option__WithArrow,
          cx.alignSingle(styles, 'Option', { align }),
          cx.control(styles, 'Option', { disabled }),
          border && styles.Option__Border,
          verticalTop && styles.Option__VerticalTop,
          className
        )}
        onClick={disabled ? undefined : onClick}
      >
        <div className={styles.Content}>
          {iconElement}
          <div className={styles.Text}>
            <div className={styles.Title}>{children}</div>
            {subtitle && <div className={styles.Subtitle}>{subtitle}</div>}
          </div>
        </div>
        {actionElement}
        {arrowElement}
      </Component>
    );
  }
);

export type OptionType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...OptionOwnPropsType
|};

type WrappedSelectableType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithThemeExtensionType,
  ...OptionOwnPropsType
|};

const Selectable = React.forwardRef(({
  border = true,
  className,
  controlElement,
  disabled,
  icon,
  messageElement = null,
  pull,
  readonly,
  requiredElement = null,
  setValue,
  value,
  ...props
}: WrappedSelectableType, forwardedRef) => {
  const { handleGroupOnChange } = React.useContext(GroupItemContext);
  const toggleValue = React.useCallback(() => setValue((oldValue) => !oldValue), [setValue]);
  const valueUpdater = React.useMemo(() => handleGroupOnChange || toggleValue, [handleGroupOnChange, toggleValue]);

  const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const { keyCode } = e;

    if ([KEY_CODES.ENTER].includes(keyCode)) {
      valueUpdater();
    }

    if (controlElement.props.onKeyDown) {
      controlElement.props.onKeyDown(e);
    }
  }, [controlElement.props.onKeyDown, valueUpdater]);

  const handleOnClick = React.useCallback((e: SnytheticEvent<HTMLElement>) => {
    e.stopPropagation();
    valueUpdater();
  }, [valueUpdater]);

  return (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Selectable,
        className
      )}
    >
      {controlElement}
      <Option
        {...props}
        onClick={(disabled || readonly) ? undefined : handleOnClick}
        onKeyDown={(disabled || readonly) ? undefined : handleOnKeyDown}
        arrow={value ? ARROW_KINDS.CHECK : undefined}
        border={border}
        clickable={false}
      />
      {messageElement}
      {requiredElement}
    </div>
  );
});

export type SelectableType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithThemeExtensionType,
  ...OptionOwnPropsType
|};

type ItemOwnPropsType = {|
  id: string  
|};

type WrappedItemType = {|
  ...WrappedOptionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(({
  className,
  disabled: itemDisabled,
  id,
  ...props
}: WrappedItemType, forwardedRef: ReactForwardedRefType) => {
  const itemRootRef = React.useRef(null);
  const { controlSelector, itemControlElement, onChangeByItemId, value, ...contextProps } = React.useContext(ControlWithItemsContext);
  const selected = React.useMemo(() => value.includes(id), [id, value]);

  const handleOnChange = React.useCallback(() => {
    onChangeByItemId(id);
  }, [id, onChangeByItemId]);

  const focusNextItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const el = itemRootRef.current;
    if (el) {
      const nextEl = el.nextElementSibling;
      if (nextEl) {
        e.preventDefault();
        nextEl.querySelector(controlSelector).focus();
      }
    }
  }, [controlSelector]);

  const focusPrevItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const el = itemRootRef.current;
    if (el) {
      const nextEl = el.previousElementSibling;
      if (nextEl) {
        e.preventDefault();
        nextEl.querySelector(controlSelector).focus();
      }
    }
  }, [controlSelector]);

  const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const { keyCode } = e;

    if ([KEY_CODES.UP_ARROW, KEY_CODES.LEFT_ARROW].includes(keyCode)) {
      focusPrevItem(e);
    }
    if ([KEY_CODES.DOWN_ARROW, KEY_CODES.RIGHT_ARROW].includes(keyCode)) {
      focusNextItem(e);
    }
    if ([KEY_CODES.TAB].includes(keyCode)) {
      if (e.shiftKey) {
        focusPrevItem(e);
      } else {
        focusNextItem(e);
      }
    }

    if (itemControlElement.props.onKeyDown) {
      itemControlElement.props.onKeyDown(e);
    }
  }, [itemControlElement.props.onKeyDown, focusNextItem, focusPrevItem]);

  const disabled = React.useMemo(() => itemDisabled || itemControlElement.props.disabled, [itemDisabled, itemControlElement.props.disabled]);

  const controlElement = React.cloneElement(itemControlElement, {
    disabled,
    id,
    onChange: disabled ? undefined : handleOnChange,
    onKeyDown: disabled ? undefined : handleOnKeyDown
  });

  const contextValue = React.useMemo(() => ({
    handleGroupOnChange: handleOnChange
  }), [handleOnChange]);

  return (
    <div ref={mergeRefs(forwardedRef, itemRootRef)} className={cx(styles.Item, className)}>
      <GroupItemContext.Provider value={contextValue}>
        <Selectable {...props} {...contextProps} border={false} disabled={disabled} controlElement={controlElement} value={selected} />
      </GroupItemContext.Provider>
    </div>
  );
});

export type ItemType = {|
  ...WrappedCheckboxType,
  ...ItemOwnPropsType
|};

type GroupOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactElementsChildren<ItemType>
|};

type WrappedGroupType = {|
  ...AsComponentExtensionType,
  ...AsControlWithItemsExtensionType,
  ...GroupOwnPropsType
|};

const Group = React.forwardRef(
  (
    {
      align = ALIGN_TYPES.LEFT,
      border = true,
      children,
      className,
      controlElement,
      disabled,
      kind,
      messageElement,
      onBlur,
      onFocus,
      requiredElement,
      setValue,
      value
    }: WrappedGroupType,
    forwardedRef: ReactForwardedRefType
  ) => {
    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Group,
          border && styles.Group__Border,
          className
        )}
        onBlur={onBlur}
        onFocus={onFocus}
      >
        {controlElement}
        <div className={styles.Content}>
          {processChildren(
            children,
            (child) => ({
              align: child.props.align || align,
              disabled: child.props.disabled || disabled,
              kind: child.props.kind || kind
            })
          )}
        </div>
        {messageElement}
        {requiredElement}
      </div>
    );
  }
);

export type GroupType = {|
  ...AsComponentType,
  ...AsControlWithItemsType,
  ...GroupOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Option)(WithTheme()(Option)), {
    Selectable: Object.assign(AsComponent(ComponentNames.Option.Selectable)(AsControl({
      controlType: INPUT_TYPES.CHECKBOX,
      defaultValue: false,
      sanitizeValueForControl: () => '',
      style: { key: 'Selectable', styles }
    })(WithTheme()(Selectable))), {
      Group: Object.assign(AsComponent(ComponentNames.Option.Selectable.Group)(AsControlWithItems({
        changeTypeMulti: AsControlWithItems.MULTI_CHANGE_TYPES.TOGGLE,
        defaultMaximum: 1,
        sanitizeValueForControl: JSON.stringify,
        style: { key: 'Group', styles }
      })(Group)), {
        Item: AsComponent(ComponentNames.Option.Selectable.Group.Item)(Item)
      })
    })
  }, {
    ALIGN_TYPES,
    ARROW_KINDS,
    KINDS
  }): React.ComponentType<OptionType> & {
    Selectable: React.ComponentType<SelectableType> & {
      Group: React.ComponentType<GroupType> & {
        Item: React.ComponentType<ItemType>
      }
    }
  } & {
    ALIGN_TYPES: Object,
    ARROW_KINDS: Object,
    KINDS: Object
  }
);
