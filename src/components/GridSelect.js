/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType } from '../hocs/components/AsComponent';
import useItems from '../hooks/useItems';
import useLayer from '../hooks/useLayer';
import { type IconPropType, type ImagePropType } from '../types';
import Icon from './Icon';
import Layer from './Layer';
import Message from './Message';
import Required from './Required';
import { SelectContext } from './Select';
import styles from './GridSelect.scss';

const ARROW_KINDS = Object.freeze({
  CLOSE: 'CLOSE',
  OPEN: 'OPEN'
});

type ItemOwnPropsType = {|
  arrow?: $Values<typeof ARROW_KINDS>,
  backgroundColor?: string,
  children: ReactChildren,
  disabled?: boolean,
  focusable?: boolean,
  icon?: IconPropType,
  id: string,
  image?: ImagePropType,
  onClick?: (e: MouseEvent) => void,
  subtitle?: string
|}

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...ItemOwnPropsType
|};

export const Item = React.forwardRef(
  (
    { arrow, backgroundColor, children, className, disabled, focusable, icon, id, image, onClick, subtitle }: WrappedItemType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { onChange, value } = React.useContext(SelectContext);
    const checked = React.useMemo(() => value !== null && value === id, [value, id]);

    const onClickHandler = React.useCallback(
      (e: MouseEvent) => {
        if (onClick) {
          onClick(e);
        } else if (onChange) {
          onChange(id);
        }
      },
      [id, onChange, onClick]
    );

    return (
      <button
        ref={forwardedRef}
        {...(focusable && !disabled ? {} : { tabIndex: -1 })}
        className={cx(styles.Item, cx.control(styles, 'Item', { disabled }), className)}
        onClick={onClickHandler}
      >
        {(icon || image) && (
          <div className={styles.IconWrapper}>
            <Icon backgroundColor={backgroundColor} className={styles.Icon} id={icon} image={image} outline />
          </div>
        )}
        <div className={styles.TextWrapper}>
          <div className={styles.Text}>{children}</div>
          {subtitle && <div className={styles.TextSecondary}>{subtitle}</div>}
        </div>
        {checked && <Icon className={styles.Check} id="check" />}
        {arrow && (
          <Icon
            className={styles.Arrow}
            id={
              {
                [ARROW_KINDS.CLOSE]: 'chevron-up',
                [ARROW_KINDS.OPEN]: 'chevron-down'
              }[arrow]
            }
          />
        )}
      </button>
    );
  }
);

const DefaultItem = React.forwardRef(({ children = 'Wybierz', ...props }: WrappedItemType, forwardedRef: ReactForwardedRefType) => (
  <Item ref={forwardedRef} id={null} {...props}>
    {children}
  </Item>
));

export type ItemType = {|
  ...AsComponentType,
  ...ItemOwnPropsType
|};

type GridSelectOwnPropsType = {|
  ...WithMessagesType,
  children: ReactElementsChildren<ItemType>,
  columns?: number,
  label?: string,
  disabled?: boolean,
  focus?: boolean,
  items: Array<Object>,
  maxHeight?: number,
  onBlur?: (e: SyntheticFocusEvent<HTMLDivElement>) => void,
  onChange?: (value: string) => void,
  onFocus?: (e: SyntheticFocusEvent<HTMLDivElement>) => void,
  open?: boolean,
  value: ?string
|};

type WrappedGridSelectType = {|
  ...AsComponentExtensionType,
  ...GridSelectOwnPropsType
|};

const GridSelect = React.forwardRef(
  (
    {
      children,
      className,
      columns = 2,
      label = 'Wybierz',
      disabled,
      errors,
      focus,
      helper,
      hints,
      items: propsItems,
      maxHeight = 240,
      onBlur,
      onChange,
      onFocus,
      open: initiallyOpen,
      required,
      value,
      warnings
    }: WrappedGridSelectType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const select = React.useRef(null);
    const { open, onHide, onShow, wrapperRef } = useLayer({ disabled, open: initiallyOpen });
    const { items, selected } = useItems({ children, items: propsItems, value });

    const contextValue = React.useMemo(
      () => ({
        onChange,
        value
      }),
      [onChange, value]
    );

    return (
      <div
        ref={mergeRefs(forwardedRef, wrapperRef)}
        className={cx(
          styles.Wrapper,
          cx.control(styles, 'Wrapper', { disabled, errors, focus, hints, warnings }),
          cx.open(styles, 'Wrapper', { open }),
          styles[`Wrapper__Size_${columns}`],
          className
        )}
      >
        <div ref={select} onBlur={onBlur} onFocus={onFocus} className={styles.GridSelect}>
          {value ? (
            (({ id, data: { name, text = name, ...selected } }: { id: string, data: Object }) => (
              <Item
                className={styles.MainItem}
                {...selected}
                id={id}
                disabled={disabled}
                arrow={open ? ARROW_KINDS.CLOSE : ARROW_KINDS.OPEN}
                onClick={onShow}
                focusable
              >
                {text}
              </Item>
            ))(selected)
          ) : (
            <DefaultItem
              className={styles.MainItem}
              disabled={disabled}
              arrow={open ? ARROW_KINDS.CLOSE : ARROW_KINDS.OPEN}
              onClick={onShow}
              focusable
            >
              {label}
            </DefaultItem>
          )}
        </div>
        {required && <Required className={styles.Required} />}
        <Message errors={errors} helper={helper} hints={hints} warnings={warnings} />
        <Layer className={styles.Layer} onHide={onHide} open={open}>
          <SelectContext.Provider value={contextValue}>
            {children ||
              items.map(({ id, data: { name, text = name, ...item } }: { id: string, data: Object }) => (
                <Item key={id} {...item} id={id} focusable={open}>
                  {text}
                </Item>
              ))}
          </SelectContext.Provider>
        </Layer>
      </div>
    );
  }
);

export type GridSelectType = {|
  ...AsComponentType,
  ...GridSelectOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.GridSelect)(GridSelect), {
    Item: Object.assign(AsComponent(ComponentNames.GridSelect.Item)(Item), {
      Default: AsComponent(ComponentNames.GridSelect.Item.Default)(DefaultItem)
    })
  }): React.ComponentType<GridSelectType> & {
    Item: React.ComponentType<ItemType> & {
      Default: React.ComponentType<ItemType>
    }
  }
);
