/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES, POSITIONS_VERTICAL as POSITIONS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import styles from './Tabs.scss';

const TAB_POSITION_TO_CLASS = Object.freeze({
  [POSITIONS.BOTTOM]: styles.Tabs__Bottom,
  [POSITIONS.TOP]: styles.Tabs__Top
});

const DEFAULT_ICON_SIZE = 24;
const DEFAULT_ICON_INNER_SIZE = 16;

type TabOwnPropsType = {|
  active?: boolean,
  children: ReactChildren,
  disabled?: boolean  
|};

type WrappedTabType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...TabOwnPropsType
|};

const Tab = React.forwardRef(
  ({ LinkComponent, active, children, className, disabled, linkProps }: WrappedTabType, forwardedRef: ReactForwardedRefType) => (
    <LinkComponent
      ref={forwardedRef}
      className={cx(styles.Tab, cx.control(styles, 'Tab', { active, disabled }), className)}
      {...(active ? { 'data-active': true } : {})}
      {...linkProps}
    >
      {React.Children.map(
        children,
        (child: React.Node) =>
          child &&
          (isBasicReactChild(child) ? (
            <span className={styles.Content}>{child}</span>
          ) : (
            React.cloneElement(child, {
              className: cx(
                child.props.className,
                {
                  [ComponentNames.CharIcon]: styles.Icon,
                  [ComponentNames.ColorIcon]: styles.Icon,
                  [ComponentNames.FontAwesome]: styles.Icon,
                  [ComponentNames.FontAwesome.Brand]: styles.Icon,
                  [ComponentNames.FontAwesome.Regular]: styles.Icon,
                  [ComponentNames.Icon]: styles.Icon
                }[child.type.componentId]
              ),
              ...{
                [ComponentNames.CharIcon]: {
                  innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                  size: child.props.size || DEFAULT_ICON_SIZE
                },
                [ComponentNames.ColorIcon]: {
                  innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                  size: child.props.size || DEFAULT_ICON_SIZE
                },
                [ComponentNames.FontAwesome]: {
                  innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                  size: child.props.size || DEFAULT_ICON_SIZE
                },
                [ComponentNames.FontAwesome.Brand]: {
                  innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                  size: child.props.size || DEFAULT_ICON_SIZE
                },
                [ComponentNames.FontAwesome.Regular]: {
                  innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                  size: child.props.size || DEFAULT_ICON_SIZE
                },
                [ComponentNames.Icon]: {
                  size: child.props.size || DEFAULT_ICON_SIZE
                }
              }[child.type.componentId]
            })
          ))
      )}
    </LinkComponent>
  )
);

export type TabType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...TabOwnPropsType
|};

type TabsOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactElementsChildren<TabType>,
  position?: $Values<typeof POSITIONS>,
  verticalCenter?: boolean
|};

type WrappedTabsType = {|
  ...AsComponentExtensionType,
  ...TabsOwnPropsType
|};

const Tabs = React.forwardRef(
  (
    { align = ALIGN_TYPES.LEFT, border, className, children, position = POSITIONS.TOP, verticalCenter }: WrappedTabsType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Tabs,
        cx.alignSingle(styles, 'Tabs', { align }),
        TAB_POSITION_TO_CLASS[position],
        border && styles.Tabs__Border,
        verticalCenter && styles.Tabs__VerticalCenter,
        className
      )}
    >
      {children}
    </div>
  )
);

export type TabsType = {|
  ...AsComponentType,
  ...TabsOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Tabs)(Tabs), {
    Tab: AsComponent(ComponentNames.Tabs.Tab)(WithLink()(Tab))
  }, {
    ALIGN_TYPES,
    POSITIONS
  }): React.ComponentType<TabsType> & {
    Tab: React.ComponentType<TabType>
  } & {
    ALIGN_TYPES: Object,
    POSITIONS: Object
  }
);
