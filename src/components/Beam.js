/* #__flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES, KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import styles from './Beam.scss';

const ICON_SIZE = 32;
const ICON_INNER_SIZE = 19;

type BeamOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: ReactChildren,
  clickable?: boolean
|};

type WrappedBeamType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...BeamOwnPropsType
|};

const Beam = React.forwardRef(
  (
    { align = ALIGN_TYPES.LEFT, children, clickable, className, themeElementRef }: BeamType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { innerWidth } = React.useContext(MainSizeContext);

    return (
      <div
        ref={mergeRefs(forwardedRef, themeElementRef)}
        className={cx(styles.Beam, align && cx.alignSingle(styles, 'Beam', { align }), className)}
      >
        <div className={styles.Inner} style={{ width: unitize(innerWidth) }}>
          {React.Children.map(children, (child: React.Node) =>
            isBasicReactChild(child) ? (
              <span className={styles.Content}>{child}</span>
            ) : (
              React.cloneElement(child, {
                className: cx(
                  child.props.className,
                  {
                    [ComponentNames.Button]: styles.Button,
                    [ComponentNames.CharIcon]: styles.Icon,
                    [ComponentNames.ColorIcon]: styles.Icon,
                    [ComponentNames.FontAwesome]: styles.Icon,
                    [ComponentNames.FontAwesome.Brand]: styles.Icon,
                    [ComponentNames.FontAwesome.Regular]: styles.Icon,
                    [ComponentNames.Icon]: styles.Icon,
                    [ComponentNames.Link]: styles.Link
                  }[child.type.componentId] || styles.Content
                ),
                ...{
                  [ComponentNames.Button]: {
                    align: null,
                    narrow: true
                  },
                  [ComponentNames.CharIcon]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.ColorIcon]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.FontAwesome]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.FontAwesome.Brand]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.FontAwesome.Regular]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.Icon]: {
                    size: ICON_SIZE
                  },
                  [ComponentNames.Text]: {
                    lineHeight: 32
                  },
                  [ComponentNames.Text.Primary]: {
                    lineHeight: 32
                  },
                  [ComponentNames.Text.Secondary]: {
                    lineHeight: 28
                  }
                }[child.type.componentId]
              })
            )
          )}
        </div>
      </div>
    );
  }
);

export type BeamType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...BeamOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Beam)(WithTheme({ defaultKind: KINDS.PRIMARY, focusable: false, hoverable: false })(Beam)),
    {
      ALIGN_TYPES,
      KINDS
    }
  ): React.ComponentType<BeamType> & {
    ALIGN_TYPES: Object,
    KINDS: Object
  }
);
