/* @flow */
import * as React from 'react';
import { formatProgressLabel } from '@omnibly/codeblocks-cmn-formatters';
import { CIRCULAR_DIRECTIONS, KINDS, POSITIONS, PROGRESS_LABEL_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, getPercent } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import styles from './Progress.scss';

const OUTLINE_ID = 'outline';

const CIRCLE_START_TYPES_TO_ROTATION = {
  [POSITIONS.BOTTOM]: 90,
  [POSITIONS.LEFT]: 180,
  [POSITIONS.RIGHT]: 0,
  [POSITIONS.TOP]: -90
};

const SEMICIRCLE_START_TYPES = Object.freeze({
  LEFT: 'Progress.SEMICIRCLE_START_TYPES.LEFT',
  RIGHT: 'Progress.SEMICIRCLE_START_TYPES.RIGHT'
});

const SEMICIRCLE_START_TYPES_TO_ROTATION = {
  [SEMICIRCLE_START_TYPES.LEFT]: 180,
  [SEMICIRCLE_START_TYPES.RIGHT]: 0
};

type BaseProgressOwnPropsType = {|
  max?: number,
  min?: number,
  value?: number  
|};

type WrappedBaseProgressType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...BaseProgressOwnPropsType
|};

type BarOwnPropsType = {|
  labelType?: $Values<typeof PROGRESS_LABEL_TYPES>  
|};

type WrappedBarType = {|
  ...WrappedBaseProgressType,
  ...BarOwnPropsType
|};

const Bar = React.forwardRef(
  (
    { className, labelType = PROGRESS_LABEL_TYPES.NONE, max = 100, min = 0, themeElementRef, value = 0 }: WrappedBarType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div ref={forwardedRef} className={cx(styles.Bar, className)}>
      <div className={styles.Content}>
        <div
          ref={themeElementRef}
          className={styles.Percent}
          style={{ width: `${getPercent({ max, min, value })}%` }}
        />
        {labelType !== PROGRESS_LABEL_TYPES.NONE && (
          <div className={styles.Label}>
            <span>{formatProgressLabel({ labelType, max, min, value })}</span>
          </div>
        )}
      </div>
    </div>
  )
);

type CircleOwnPropsType = {|
  direction?: $Values<typeof CIRCULAR_DIRECTIONS>,
  hideBackground?: boolean,
  labelType?: $Values<typeof PROGRESS_LABEL_TYPES>,
  size?: number,
  startType?: $Values<typeof POSITIONS>
|};

type WrappedCircleType = {|
  ...WrappedBaseProgressType,
  ...CircleOwnPropsType
|};

const Circle = React.forwardRef(
  (
    {
      className,
      direction = CIRCULAR_DIRECTIONS.CLOCKWISE,
      hideBackground,
      labelType = PROGRESS_LABEL_TYPES.NONE,
      max = 100,
      min = 0,
      size = 120,
      startType = POSITIONS.TOP,
      themeElementRef,
      value = 0
    }: WrappedCircleType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const halfSize = size / 2;
    const radius = (size * 5) / 12;
    const circumference = 2 * Math.PI * radius;
    const dash = (circumference * getPercent({ max, min, value })) / 100;
    const rotate = CIRCLE_START_TYPES_TO_ROTATION[startType];
    const transform = {
      [CIRCULAR_DIRECTIONS.CLOCKWISE]: rotate === 0 ? '' : `rotate(${rotate} ${halfSize} ${halfSize})`,
      [CIRCULAR_DIRECTIONS.COUNTERCLOCKWISE]: `translate(${halfSize} ${halfSize}) scale(1, -1) translate(${-halfSize} ${-halfSize})${
        rotate === 0 ? '' : ` rotate(${-rotate} ${halfSize} ${halfSize})`
      }`
    }[direction];

    return (
      <div ref={forwardedRef} className={cx(styles.Circle, className)}>
        <svg viewBox={`0 0 ${size} ${size}`} height={size} width={size}>
          <defs>
            <circle id={OUTLINE_ID} cx={halfSize} cy={halfSize} r={radius} />
          </defs>
          {!hideBackground && <use href={`#${OUTLINE_ID}`} className={styles.Background} />}
          <use
            ref={themeElementRef}
            href={`#${OUTLINE_ID}`}
            className={styles.Percent}
            strokeDasharray={`${dash} ${circumference}`}
            transform={transform}
          />
        </svg>
        {labelType !== PROGRESS_LABEL_TYPES.NONE && (
          <div className={styles.Label}>
            <span>{formatProgressLabel({ labelType, max, min, value })}</span>
          </div>
        )}
      </div>
    );
  }
);

type LineOwnPropsType = {|
  completed?: boolean
|};

type WrappedLineType = {|
  ...WrappedBaseProgressType,
  ...LineOwnPropsType
|};

const Line = React.forwardRef(
  ({ className, completed, max = 100, min = 0, themeElementRef, value = 0 }: WrappedLineType, forwardedRef: ReactForwardedRefType) => {
    const width = React.useMemo(() => getPercent({ max, min, value }), [max, min, value]);
    const hidden = React.useMemo(() => completed || width >= 100 || width <= 0, [completed, width]);

    return (
      <div ref={forwardedRef} className={cx(styles.Line, hidden && styles.Line__Hidden, className)}>
        <div ref={themeElementRef} className={styles.Percent} style={{ width: `${width}%` }} />
      </div>
    );
  }
);

type SemiCircleOwnPropsType = {|
  direction?: $Values<typeof CIRCULAR_DIRECTIONS>,
  hideBackground?: boolean,
  labelType?: $Values<typeof PROGRESS_LABEL_TYPES>,
  size?: number,
  startType?: $Values<typeof SEMICIRCLE_START_TYPES>
|};

type WrappedSemiCircleType = {|
  ...WrappedBaseProgressType,
  ...SemiCircleOwnPropsType
|};

const SemiCircle = React.forwardRef(
  (
    {
      className,
      direction = CIRCULAR_DIRECTIONS.CLOCKWISE,
      hideBackground,
      labelType = PROGRESS_LABEL_TYPES.NONE,
      max = 100,
      min = 0,
      size = 120,
      startType = SEMICIRCLE_START_TYPES.LEFT,
      themeElementRef,
      value = 0
    }: WrappedSemiCircleType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const halfSize = size / 2;
    const radius = (size * 5) / 12;
    const circumference = Math.PI * radius;
    const dash = (circumference * getPercent({ max, min, value })) / 100;
    const rotate = SEMICIRCLE_START_TYPES_TO_ROTATION[startType];
    const transform = `rotate(${rotate} ${halfSize} ${halfSize})${
      startType === SEMICIRCLE_START_TYPES.RIGHT ? `translate(0 ${size}) scale(1,-1)` : ''
    }`;

    return (
      <div ref={forwardedRef} className={cx(styles.SemiCircle, className)}>
        <svg height={halfSize} width={size} viewport={`0 0 ${halfSize} ${size}`}>
          <defs>
            <circle id={OUTLINE_ID} cx={halfSize} cy={halfSize} r={radius} />
          </defs>
          {!hideBackground && <use href={`#${OUTLINE_ID}`} className={styles.Background} />}
          <use
            ref={themeElementRef}
            href={`#${OUTLINE_ID}`}
            className={styles.Percent}
            strokeDasharray={`${dash} ${circumference}`}
            transform={transform}
          />
        </svg>
        {labelType !== PROGRESS_LABEL_TYPES.NONE && (
          <div className={styles.Label}>
            <span>{formatProgressLabel({ labelType, max, min, value })}</span>
          </div>
        )}
      </div>
    );
  }
);

type BaseProgressType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...BaseProgressOwnPropsType
|};

export type BarType = {|
  ...BaseProgressType,
  ...BarOwnPropsType
|};

export type CircleType = {|
  ...BaseProgressType,
  ...CircleOwnPropsType
|};

export type LineType = {|
  ...BaseProgressType,
  ...LineOwnPropsType
|};

export type SemiCircleType = {|
  ...BaseProgressType,
  ...SemiCircleOwnPropsType
|};

export default (
  {
    Bar: Object.assign(
      AsComponent(ComponentNames.Progress.Bar)(WithTheme({ defaultKind: KINDS.PRIMARY, hoverable: false })(Bar)),
      {
        KINDS,
        LABEL_TYPES: PROGRESS_LABEL_TYPES
      }
    ),
    Circle: Object.assign(
      AsComponent(ComponentNames.Progress.Circle)(
        WithTheme({ backgroundProperty: 'stroke', defaultKind: KINDS.PRIMARY, hoverable: false })(Circle)
      ),
      {
        DIRECTIONS: CIRCULAR_DIRECTIONS,
        KINDS,
        LABEL_TYPES: PROGRESS_LABEL_TYPES,
        START_TYPES: POSITIONS
      }
    ),
    Line: Object.assign(
      AsComponent(ComponentNames.Progress.Line)(WithTheme({ defaultKind: KINDS.PRIMARY, hoverable: false })(Line)),
      {
        KINDS
      }
    ),
    SemiCircle: Object.assign(
      AsComponent(ComponentNames.Progress.SemiCircle)(
        WithTheme({ backgroundProperty: 'stroke', defaultKind: KINDS.PRIMARY, hoverable: false })(SemiCircle)
      ),
      {
        DIRECTIONS: CIRCULAR_DIRECTIONS,
        KINDS,
        LABEL_TYPES: PROGRESS_LABEL_TYPES,
        START_TYPES: SEMICIRCLE_START_TYPES
      }
    )
  }: {
    Bar: React.ComponentType<BarType> & {
      KINDS: Object,
      LABEL_TYPES: Object
    },
    Circle: React.ComponentType<CircleType> & {
      DIRECTIONS: Object,
      KINDS: Object,
      LABEL_TYPES: Object,
      START_TYPES: Object
    },
    Line: React.ComponentType<LineType> & {
      KINDS: Object
    },
    SemiCircle: React.Component & {
      DIRECTIONS: Object,
      KINDS: Object,
      LABEL_TYPES: Object,
      START_TYPES: Object
    }
  }
);
