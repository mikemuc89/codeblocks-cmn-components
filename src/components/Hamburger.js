/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { KINDS, POSITIONS_CORNERS as POSITIONS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild, mergeRefs, unitize, stopPropagationCallback } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsHamburger from '../hocs/components/AsHamburger';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import {
  type BadgePropType,
  type FooterPropType,
  type HeaderPropType,
  type ImagePropType,
  type MenuPropType
} from '../types';
import CONFIG from '../components/style-config';
import styles from './Hamburger.scss';

const {
  colors: {
    background: BACKGROUND_COLORS
  }
} = CONFIG;

const DEFAULT_BADGE_SIZE = 64;
const DEFAULT_BADGE_INNER_SIZE = 40;
const DEFAULT_FOOTER_SIZE = 40;
const DEFAULT_HEADER_SIZE = 40;
const DEFAULT_IMAGE_SIZE = 160;

type HamburgerOwnPropsType = {|
  addition?: string,
  badge?: BadgePropType,
  children: ReactChildren,
  footer?: FooterPropType,
  header?: HeaderPropType,
  image?: ImagePropType,
  menu?: MenuPropType,
  onClose?: () => void,
  right?: boolean,
  width?: number  
|};

type WrappedHamburgerType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...HamburgerOwnPropsType
|};

const Hamburger = React.forwardRef(
  (
    { addition, backgroundElementRef, badge, children, className, footer, header, image, menu, onClose, right, width = 600 }: WrappedHamburgerType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div
      ref={forwardedRef}
      className={cx(styles.Hamburger, right ? styles.Hamburger__Right : styles.Hamburger__Left, className)}
      onClick={onClose}
    >
      <span tabIndex={0} onClick={onClose} className={styles.Close} />
      <div
        ref={backgroundElementRef}
        style={{ ...(width ? { width: unitize(width) } : {}) }}
        className={styles.Content}
        onClick={stopPropagationCallback}
      >
        {badge &&
          React.cloneElement(badge, {
            className: styles.Badge,
            focusable: false,
            hoverable: false,
            kind: badge.props.kind || KINDS.PRIMARY,
            shape: badge.props.shape || SHAPES.CIRCLE,
            size: badge.props.size || DEFAULT_BADGE_SIZE,
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
              },
              [ComponentNames.ColorIcon]: {
                innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
              },
              [ComponentNames.FontAwesome]: {
                innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
              }
            }[badge.type.componentId]
          })}
        {menu &&
          React.cloneElement(menu, {
            className: styles.Menu,
            ...{
              [ComponentNames.Dropdown]: {
                accent: menu.props.accent || false,
                border: menu.props.border || false
              },
              [ComponentNames.Infotip]: {
                accent: menu.props.accent || false,
                border: menu.props.border || false
              },
              [ComponentNames.Fab]: {
                position: menu.props.position || POSITIONS.TOP_RIGHT
              }
            }[menu.type.componentId]
          })}
        {image &&
          React.cloneElement(image, {
            className: styles.Image,
            height: image.props.height || DEFAULT_IMAGE_SIZE
          })}
        {header &&
          React.cloneElement(header, {
            className: styles.Header,
            height: header.props.height || DEFAULT_HEADER_SIZE
          })}
        <div className={styles.Inner}>
          {children &&
            React.Children.map(children, (child: React.Node) =>
              isBasicReactChild(child) ? (
                <div className={styles.Text}>{child}</div>
              ) : (
                React.cloneElement(child, {
                  className: {
                    [ComponentNames.Banner]: styles.Banner,
                    [ComponentNames.Heading.H1]: styles.Heading,
                    [ComponentNames.Heading.H2]: styles.Heading,
                    [ComponentNames.Heading.H3]: styles.Heading,
                    [ComponentNames.Heading.H4]: styles.Heading,
                    [ComponentNames.Heading.H5]: styles.Heading,
                    [ComponentNames.Markdown]: styles.Text,
                    [ComponentNames.Text]: styles.Text
                  }[child.type.componentId]
                })
              )
            )}
        </div>
        {footer &&
          React.cloneElement(footer, {
            className: styles.Footer,
            height: footer.props.height || DEFAULT_FOOTER_SIZE
          })}
        {addition && <div className={styles.Addition}>{addition}</div>}
      </div>
    </div>
  )
);

export type HamburgerType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...HamburgerOwnPropsType
|};

export const BaseHamburger: HamburgerType = AsComponent(ComponentNames.Hamburger)(WithBackground({
  defaultBackgroundColor: BACKGROUND_COLORS.hamburger.normal,
  defaultBackgroundColorFocus: BACKGROUND_COLORS.hamburger.focus,
  defaultBackgroundColorHover: BACKGROUND_COLORS.hamburger.hover
})(Hamburger));

export default (
  Object.assign(AsHamburger()(BaseHamburger), {}): React.ComponentType<HamburgerType>
);
