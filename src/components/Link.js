/* @flow */
import * as React from 'react';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import styles from './Link.scss';

const ICON_SIZE = 22;
const ICON_INNER_SIZE = 16;

export type WrappedLinkType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType
|};

const Link = React.forwardRef(
  ({ LinkComponent, active, children, className, linkProps }: WrappedLinkType, forwardedRef: ReactForwardedRefType) => (
    <LinkComponent ref={forwardedRef} className={cx(styles.Link, active && styles.Link__Active, className)} {...linkProps}>
      {React.Children.map(children, (child: React.Node) =>
        isBasicReactChild(child) ? (
          <span className={styles.Text}>{child}</span>
        ) : (
          React.cloneElement(child, {
            className: cx(
              child.props.className,
              {
                [ComponentNames.CharIcon]: styles.Icon,
                [ComponentNames.ColorIcon]: styles.Icon,
                [ComponentNames.FontAwesome]: styles.Icon,
                [ComponentNames.FontAwesome.Brand]: styles.Icon,
                [ComponentNames.FontAwesome.Regular]: styles.Icon,
                [ComponentNames.Icon]: styles.Icon
              }[child.type.componentId] || styles.Text
            ),
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: child.props.innerSize || ICON_INNER_SIZE,
                size: child.props.size || ICON_SIZE
              },
              [ComponentNames.ColorIcon]: {
                innerSize: child.props.innerSize || ICON_INNER_SIZE,
                size: child.props.size || ICON_SIZE
              },
              [ComponentNames.FontAwesome]: {
                innerSize: child.props.innerSize || ICON_INNER_SIZE,
                size: child.props.size || ICON_SIZE
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: child.props.innerSize || ICON_INNER_SIZE,
                size: child.props.size || ICON_SIZE
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: child.props.innerSize || ICON_INNER_SIZE,
                size: child.props.size || ICON_SIZE
              },
              [ComponentNames.Icon]: {
                size: child.props.size || ICON_SIZE
              }
            }[child.type.componentId]
          })
        )
      )}
    </LinkComponent>
  )
);

export type LinkType = {|
  ...AsComponentType,
  ...WithLinkType
|};

type SetOwnPropsType = {|
  align: $Values<typeof ALIGN_TYPES>,
  children: ReactElementsChildren<LinkType>,
  vertical?: boolean
|};

type WrappedSetType = {|
  ...AsComponentExtensionType,
  ...SetOwnPropsType
|};

const Set = React.forwardRef(
  ({ align = ALIGN_TYPES.LEFT, children, className, separators = true, vertical }: WrappedSetType, forwardedRef: ReactForwardedRefType) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Set,
        cx.alignSingle(styles, 'Set', { align }),
        cx.orientation(styles, 'Set', { vertical }),
        separators && styles.Set__Separators,
        className
      )}
    >
      {children}
    </div>
  )
);

export type SetType = {|
  ...AsComponentType,
  ...SetOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Link)(WithLink()(Link)), {
    Set: AsComponent(ComponentNames.Link.Set)(Set)
  }, {
    ALIGN_TYPES
  }): React.ComponentType<LinkType> & {
    Set: React.ComponentType<SetType>,
  } & {
    ALIGN_TYPES: Object
  }
);
