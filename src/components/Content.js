/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Content.scss';

type ContentOwnPropsType = {|
  children?: ReactChildren,
|};

type WrappedContentType = {|
  ...AsComponentExtensionType,
  ...ContentOwnPropsType
|};

const Content = React.forwardRef(({
  children,
  className
}: WrappedViewType,
forwardedRef: ReactForwardedRefType) => (
  <div
    ref={forwardedRef}
    className={cx(styles.Content, className)}
  >
    {children}
  </div>
));

export type ContentType = {|
  ...AsComponentType,
  ...ContentOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Content)(Content), {}): React.ComponentType<ContentType>
);
