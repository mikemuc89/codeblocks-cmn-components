/* @flow */
import * as React from 'react';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { KEY_CODES, INPUT_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, debounced, guid, setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import ParentCacheContext from '../contexts/ParentCacheContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import { type IconPropType } from '../types';
import FontAwesome from './FontAwesome';
import Pill from './Pill';
import styles from './Set.scss';

const CLEAR_ICON_INNER_SIZE = 12;
const DEFAULT_ICON_SIZE = 38;
const DEFAULT_ICON_INNER_SIZE = 20;

const ON_SCROLL_DEBOUNCE_TIME = 50;

type RemoveIconType = {|
  index: number,
  onRemove: (index: number) => void
|};

const RemoveIcon = React.forwardRef(
  ({ index, onRemove }: RemoveIconType, forwardedRef: ReactForwardedRefType) => {
    const handleOnClick = React.useCallback(
      (e: MouseEvent) => {
        e.stopPropagation();
        e.preventDefault();
        onRemove(index);
      },
      [index, onRemove]
    );

    return <FontAwesome size={24} innerSize={12} id="times" onClick={handleOnClick} />;
  }
);

type SetOwnPropsType = {|
  action?: IconPropType,
  allowSpacesInValues?: boolean,
  alwaysVisibleRows?: number,
  autocompleteType?: string,
  availableRows?: number,
  icon?: IconPropType,
  onClick?: SyntheticEvent<HTMLDivElement>,
  onKeyDown?: SyntheticKeyboardEvent<HTMLInputElement>,
  placeholder?: string,
  rows?: number
|};

type WrappedSetType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...SetOwnPropsType
|};

const Set = React.forwardRef(
  (
    {
      action,
      allowSpacesInValues = true,
      alwaysVisibleRows = 1,
      className,
      controlElement,
      controlRef,
      disabled,
      icon,
      kind = KINDS.PRIMARY,
      messageElement = null,
      onClear,
      onClick,
      onKeyDown,
      placeholder,
      readonly,
      requiredElement = null,
      rows = 1,
      setValue,
      value
    }: WrappedSetType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { setParentCache } = React.useContext(ParentCacheContext);
    const [cache, setCache] = React.useState(parentCache);
    const borderElementRef = React.useRef(null);
    const inputElementRef = React.useRef(null);

    React.useEffect(() => {
      if (setParentCache) {
        setParentCache(cache);
      }
    }, [cache, setParentCache]);

    const focusInput = React.useCallback(() => {
      const el = inputElementRef.current;
      if (el) {
        el.focus();
      }
    }, []);

    const getInputHeight = React.useCallback((rows: number) => {
      const el = borderElementRef.current;
      if (el) {
        const { borderBottomWidth, borderTopWidth, lineHeight, paddingBottom, paddingTop } = getComputedStyle(el);
        return unitize.revert(lineHeight) * rows + [
          borderBottomWidth,
          borderTopWidth,
          paddingBottom,
          paddingTop
        ].reduce((sum, value) => sum + unitize.revert(value), 0);
      }
      return 0;
    }, []);

    const handleOnInput = React.useCallback((e: SyntheticChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
      setCache(e.target.value);
      if (rows > 1) {
        const el = borderElementRef.current;
        if (el) {
          el.style.height = '';
          const { borderBottomWidth, borderTopWidth } = getComputedStyle(el);
          const height = el.scrollHeight + unitize.revert(borderTopWidth) + unitize.revert(borderTopWidth);
          setElementStyle(el, { height: unitize(height) });
        }
      }
      const el2 = inputElementRef.current;
      if (el2) {
        setElementStyle(el2, { flexGrow: '0', minWidth: unitize(0), width: unitize(0) });
        el2.style.width = unitize(0);
        const newWidth = el2.scrollWidth;
        setElementStyle(el2, { flexGrow: '', minWidth: '', width: unitize(newWidth) });
      }
    }, [rows, setCache]);

    React.useEffect(() => {
      const el = borderElementRef.current;
      if (el) {
        const height = getInputHeight(alwaysVisibleRows);
        setElementStyle(el, { minHeight: unitize(height) });
      }
    }, [alwaysVisibleRows, getInputHeight]);

    React.useEffect(() => {
      const el = borderElementRef.current;
      if (el) {
        const height = getInputHeight(rows);
        setElementStyle(el, { maxHeight: unitize(height) });
      }
    }, [getInputHeight, rows]);

    const handleOnRemoveByIndex = React.useCallback((index: number) => {
      setValue((oldValue) => {
        return [...oldValue.slice(0, index), ...oldValue.slice(index + 1)];
      });
    }, [setValue]);

    const handleActionOnClick = React.useCallback((...args: Array<any>) => {
      if (action.props.onClick) {
        action.props.onClick(...args);
        focusInput();
      }
    }, [action, focusInput]);

    const actionElement = React.useMemo(() => action && !disabled ? React.cloneElement(action, {
      className: cx(styles.Action, action.props.className),
      onClick: handleActionOnClick,
      ...{
        [ComponentNames.CharIcon]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.ColorIcon]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome.Brand]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome.Regular]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.Icon]: {
          width: action.props.width || DEFAULT_ICON_SIZE
        }
      }[action.type.componentId]
    }) : null, [action, disabled, handleActionOnClick]);

    const handleOnClear = React.useCallback(() => {
      onClear();
      setTimeout(() => {
        focusInput();
      });
    }, [focusInput, onClear]);
  
    const handleOnScroll = React.useMemo(() => debounced(
      (e) => {
        const el = borderElementRef.current;
        if (el) {
          const { lineHeight } = getComputedStyle(el);
          const multiple = unitize.revert(lineHeight);
          el.scrollTop = Math.round(el.scrollTop / multiple) * multiple;
        }
      },
      ON_SCROLL_DEBOUNCE_TIME
    ), [getInputHeight]);

    const clearElement = React.useMemo(() => value.length && onClear ? (
      <FontAwesome
        className={styles.Clear}
        id="times"
        onClick={handleOnClear}
        innerSize={CLEAR_ICON_INNER_SIZE}
        size={DEFAULT_ICON_SIZE}
      />
    ) : null, [handleOnClear, onClear, value]);

    const iconElement = React.useMemo(() => icon ? React.cloneElement(icon, {
      className: cx(styles.Icon, icon.props.className),
      ...{
        [ComponentNames.CharIcon]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.ColorIcon]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome.Brand]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome.Regular]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.Icon]: {
          width: icon.props.width || DEFAULT_ICON_SIZE
        }
      }[icon.type.componentId]
    }) : null, [icon]);

    const handleOnKeyDown = React.useCallback((e: KeyboardEvent) => {
      const { selectionEnd, selectionStart, value: targetValue } = e.target;
      e.stopPropagation();
      const trimmedValue = targetValue.trim();

      if (onKeyDown) {
        onKeyDown(e);
      }

      if (
        [KEY_CODES.ENTER].includes(e.keyCode) ||
        (trimmedValue && !allowSpacesInValues && [KEY_CODES.SPACE].includes(e.keyCode)) ||
        (trimmedValue && [KEY_CODES.TAB].includes(e.keyCode))
      ) {
        e.preventDefault();
      }

      if (trimmedValue && (
        [KEY_CODES.ENTER, KEY_CODES.TAB].includes(e.keyCode) ||
        (!allowSpacesInValues && [KEY_CODES.SPACE].includes(e.keyCode))
      )) {
        setValue((oldValue) => [...oldValue, trimmedValue]);
        setCache('');
        focusInput();
        return;
      }

      if ([KEY_CODES.BACKSPACE].includes(e.keyCode) && selectionStart === 0 && selectionEnd === 0) {
        setValue((oldValue) => oldValue.slice(0, oldValue.length - 1));
        focusInput();
        return;
      }

      setCache(targetValue);
    }, [allowSpacesInValues, cache, focusInput, onKeyDown, setCache, setValue]);

    const inputElement = React.useMemo(() => (
      <input
        ref={inputElementRef}
        className={styles.Input}
        disabled={disabled}
        onInput={(disabled || readonly) ? undefined : handleOnInput}
        onKeyDown={(disabled || readonly) ? undefined : handleOnKeyDown}
        type={INPUT_TYPES.TEXT}
        value={cache}
      />
    ), [cache, disabled, handleOnInput, handleOnKeyDown, readonly, value]);

    const selectedElements = value && (
      <>
        {value.map((item: string, index: number) => (
          <Pill
            key={index}
            className={styles.Selected}
            action={disabled ? undefined : <RemoveIcon index={index} onRemove={handleOnRemoveByIndex} />}
            disabled={disabled}
            kind={kind}
            clickable={!disabled}
          >
            {item}
          </Pill>
        ))}
      </>
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Set,
          actionElement && styles.Set__WithAction,
          clearElement && value && styles.Set__WithClear,
          iconElement && styles.Set__WithIcon,
          !value.length && !cache && placeholder && styles.Set__Placeholder,
          className
        )}
      >
        <span className={styles.Before} onClick={focusInput} />
        <div className={styles.Border} onClick={focusInput} onScroll={handleOnScroll}>
          {controlElement}
          {iconElement}
          {selectedElements}
          {inputElement}
          <span className={styles.Actions}>
            {clearElement}
            {actionElement}
          </span>
        </div>
        <span className={styles.After} onClick={focusInput} />
        {messageElement}
        {requiredElement}
      </div>
    );
  }
);

export type SetType = {|
  ...AsComponentType,
  ...AsControlType,
  ...SetOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(
      ComponentNames.Set
    )(
      AsControl({
        allowMultiRow: true,
        controlType: INPUT_TYPES.HIDDEN,
        defaultMaximum: Infinity,
        defaultValue: [],
        sanitizeValueForControl: JSON.stringify,
        style: { key: 'Set', styles }
      })(
        Set
      )
    ), {
    KINDS
  }): React.ComponentType<SetType> & {
    KINDS: Object
  }
);
