/* @flow */
import * as React from 'react';
import { type ReactElementsChildren, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, guid } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import AsControlWithItems, { ControlWithItemsContext, GroupItemContext, type AsControlWithItemsExtensionType, type AsControlWithItemsType } from '../hocs/components/AsControlWithItems';
import { type IconPropType } from '../types';
import FontAwesome from './FontAwesome';
import styles from './Radio.scss';

const ICON_SIZE = 14;
const ICON_INNER_SIZE = 8;

type RadioOwnPropsType = {|
  icon?: IconPropType,
  pull?: boolean
|};

type WrappedRadioType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...RadioOwnPropsType
|};

const Radio = React.forwardRef(
  (
    {
      className,
      controlElement,
      disabled,
      icon,
      messageElement = null,
      pull,
      readonly,
      requiredElement = null,
      setValue,
      value
    }: WrappedRadioType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const checkValue = React.useCallback(() => setValue(true), [setValue]);

    const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
      const { keyCode } = e;

      if ([KEY_CODES.ENTER].includes(keyCode)) {
        checkValue();
      }

      if (controlElement.props.onKeyDown) {
        controlElement.props.onKeyDown(e);
      }
    }, [checkValue, controlElement.props.onKeyDown]);

    const handleOnClick = React.useCallback((e: SnytheticEvent<HTMLElement>) => {
      e.stopPropagation();
      checkValue();
    }, [checkValue]);

    const iconElement = value ? icon ? (
      React.cloneElement(icon, {
        className: styles.Icon,
        disabled,
        onClick: (disabled || readonly) ? undefined : handleOnClick,
        size: icon.props.size || ICON_SIZE,
        ...{
          [ComponentNames.CharIcon]: {
            focusable: false,
            innerSize: icon.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.ColorIcon]: {
            focusable: false,
            innerSize: icon.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome]: {
            focusable: false,
            innerSize: icon.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            focusable: false,
            innerSize: icon.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            focusable: false,
            innerSize: icon.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.Icon]: {
            focusable: false
          }
        }[icon.type.componentId]
      })
    ) : (
      <FontAwesome
        className={styles.Icon}
        disabled={disabled}
        focusable={false}
        innerSize={ICON_INNER_SIZE}
        id="check"
        onClick={(disabled || readonly) ? undefined : handleOnClick}
        size={ICON_SIZE}
      />
    ) : null;

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Radio,
          pull && styles.Radio__Pull,
          className
        )}
      >
        {React.cloneElement(controlElement, {
          onKeyDown: (disabled || readonly) ? undefined : handleOnKeyDown
        })}
        <div className={styles.Dummy} onClick={disabled ? undefined : handleOnClick}>
          {iconElement}
        </div>
        {messageElement}
        {requiredElement}
      </div>
    );
  }
);

type ItemOwnPropsType = {|
  children: string,
  disabled?: boolean,
  id: string  
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...WrappedRadioType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(
  ({ children, className, disabled: itemDisabled, id, ...props }: WrappedItemType, forwardedRef: ReactForwardedRefType) => {
    const itemRootRef = React.useRef(null);
    const { controlSelector, itemControlElement, onChangeByItemId, value, ...contextProps } = React.useContext(ControlWithItemsContext);
    const selected = React.useMemo(() => value === id, [id, value]);
  
    const handleOnChange = React.useCallback(() => {
      onChangeByItemId(id);
    }, [id, onChangeByItemId]);

    const focusNextItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
      e.preventDefault();

      const el = itemRootRef.current;
      if (el) {
        const nextEl = el.nextElementSibling || el.parentElement.firstChild;
        if (nextEl) {
          nextEl.querySelector(controlSelector).focus();
        }
      }
    }, [controlSelector]);
  
    const focusPrevItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
      e.preventDefault();

      const el = itemRootRef.current;
      if (el) {
        const nextEl = el.previousElementSibling || el.parentElement.lastChild;
        if (nextEl) {
          nextEl.querySelector(controlSelector).focus();
        }
      }
    }, [controlSelector]);
  
    const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
      const { keyCode } = e;
  
      if ([KEY_CODES.UP_ARROW, KEY_CODES.LEFT_ARROW].includes(keyCode)) {
        focusPrevItem(e);
      }
      if ([KEY_CODES.DOWN_ARROW, KEY_CODES.RIGHT_ARROW].includes(keyCode)) {
        focusNextItem(e);
      }
  
      if (itemControlElement.props.onKeyDown) {
        itemControlElement.props.onKeyDown(e);
      }
    }, [itemControlElement.props.onKeyDown, focusNextItem, focusPrevItem]);
  
    const disabled = React.useMemo(() => itemDisabled || itemControlElement.props.disabled, [itemDisabled, itemControlElement.props.disabled]);
  
    const controlElement = React.cloneElement(itemControlElement, {
      disabled,
      id,
      onChange: disabled ? undefined : handleOnChange,
      onKeyDown: disabled ? undefined : handleOnKeyDown
    });
  
    const contextValue = React.useMemo(() => ({
      handleGroupOnChange: handleOnChange
    }), [handleOnChange]);

    return (
      <div ref={mergeRefs(forwardedRef, itemRootRef)} className={cx(styles.Item, className)}>
        <label className={styles.Label}>{children}</label>
        <GroupItemContext.Provider value={contextValue}>
          <Radio {...props} {...contextProps} controlElement={controlElement} value={selected} />
        </GroupItemContext.Provider>
      </div>
    );
  }
);

type GroupOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: ReactElementsChildren<ItemType>
|};

type WrappedGroupType = {|
  ...AsComponentExtensionType,
  ...AsControlWithItemsExtensionType,
  ...GroupOwnPropsType
|};

const Group = React.forwardRef(
  (
    {
      align = ALIGN_TYPES.LEFT,
      children,
      className,
      controlElement,
      messageElement,
      onBlur,
      onFocus,
      requiredElement,
      setValue,
      value
    }: WrappedGroupType,
    forwardedRef: ReactForwardedRefType
  ) => {
    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Group,
          align && cx.alignSingle(styles, 'Group', { align }),
          className
        )}
        onBlur={onBlur}
        onFocus={onFocus}
      >
        {controlElement}
        <div className={styles.Content}>
          {children}
        </div>
        {messageElement}
        {requiredElement}
      </div>
    );
  }
);

export type RadioType = {|
  ...AsComponentType,
  ...AsControlType,
  ...RadioOwnPropsType
|};

export type ItemType = {|
  ...AsComponentType,
  ...RadioOwnPropsType,
  ...ItemOwnPropsType
|};

export type GroupType = {|
  ...AsComponentType,
  ...AsControlWithItemsType,
  ...GroupOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Radio)(AsControl({
    controlType: INPUT_TYPES.RADIO,
    defaultValue: false,
    sanitizeValueForControl: () => '',
    style: { key: 'Radio', styles }
  })(Radio)), {
    Group: Object.assign(AsComponent(ComponentNames.Radio.Group)(AsControlWithItems({
      changeTypeSingle: AsControlWithItems.MULTI_CHANGE_TYPES.ALWAYS_NEW,
      defaultValue: null,
      sanitizeValueForControl: (value: string) => value,
      style: { key: 'Group', styles }
    })(Group)), {
      Item: AsComponent(ComponentNames.Radio.Group.Item)(Item)
    }, {
      ALIGN_TYPES
    })
  }): React.ComponentType<RadioType> & {
    Item: React.ComponentType<ItemType>
  } & {
    ALIGN_TYPES: Object
  }
);
