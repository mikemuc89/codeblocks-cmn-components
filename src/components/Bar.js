/* #__flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES, KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import { type ActionPropType } from '../types';
import styles from './Bar.scss';

const ICON_SIZE = 24;
const ICON_INNER_SIZE = 16;

type BarOwnPropsType = {|
  align?: $Value<typeof ALIGN_TYPES>,
  action?: ActionPropType,
  border?: boolean,
  children: ReactChildren,
  clickable?: boolean
|};

export type WrappedBarType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...BarOwnPropsType
|};

const Bar = React.forwardRef(
  (
    { align = ALIGN_TYPES.LEFT, action, border = true, children, clickable, className, themeElementRef }: WrappedBarType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const Component = clickable ? 'button' : 'div';

    const actionElement =
      action &&
      React.cloneElement(action, {
        className: cx(
          styles.Action,
          {
            [ComponentNames.Button]: styles.Button,
            [ComponentNames.Link]: styles.Link
          }[action.type.componentId]
        ),
        ...{
          [ComponentNames.Button]: {
            align: null,
            narrow: true
          },
          [ComponentNames.CharIcon]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.Icon]: {
            size: ICON_SIZE
          }
        }[action.type.componentId]
      });

    return (
      <Component
        ref={forwardedRef}
        className={cx(
          styles.Bar,
          align && cx.alignSingle(styles, 'Bar', { align }),
          border && styles.Bar__Border,
          className
        )}
      >
        <div ref={themeElementRef} className={styles.Inner}>
          {React.Children.map(children, (child: React.Node) =>
            isBasicReactChild(child) ? (
              <span className={styles.Content}>{child}</span>
            ) : (
              React.cloneElement(child, {
                className: cx(
                  child.props.className,
                  {
                    [ComponentNames.Button]: styles.Button,
                    [ComponentNames.CharIcon]: styles.Icon,
                    [ComponentNames.ColorIcon]: styles.Icon,
                    [ComponentNames.FontAwesome]: styles.Icon,
                    [ComponentNames.FontAwesome.Brand]: styles.Icon,
                    [ComponentNames.FontAwesome.Regular]: styles.Icon,
                    [ComponentNames.Icon]: styles.Icon,
                    [ComponentNames.Link]: styles.Link
                  }[child.type.componentId] || styles.Content
                ),
                ...{
                  [ComponentNames.Button]: {
                    align: null,
                    narrow: true
                  },
                  [ComponentNames.CharIcon]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.ColorIcon]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.FontAwesome]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.FontAwesome.Brand]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.FontAwesome.Regular]: {
                    innerSize: ICON_INNER_SIZE,
                    size: ICON_SIZE
                  },
                  [ComponentNames.Icon]: {
                    size: ICON_SIZE
                  }
                }[child.type.componentId]
              })
            )
          )}
          {actionElement}
        </div>
      </Component>
    );
  }
);

export type BarType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...BarOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Bar)(WithTheme()(Bar)), {
    ALIGN_TYPES,
    KINDS
  }): React.ComponentType<BarType> & {
    ALIGN_TYPES: Object,
    KINDS: Object
  }
);
