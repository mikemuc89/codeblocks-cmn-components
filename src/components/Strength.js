/* @flow */
import * as React from 'react';
import { formatPasswordStrength } from '@omnibly/codeblocks-cmn-formatters';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Strength.scss';

type StrengthOwnPropsType = {|
  score: ?number,
  description?: boolean
|};

type WrappedStrengthType = {|
  ...AsComponentExtensionType,
  ...StrengthOwnPropsTypeP
|};

const Strength = React.forwardRef(
  (
    { className, description = true, score }: WrappedStrengthType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div ref={forwardedRef} className={cx(styles.Strength, score !== null && styles[`Strength__${score}`], className)}>
      <div className={styles.Content}>
        <span />
        <span />
        <span />
        <span />
        <span />
      </div>
      {description && (
        <div className={styles.Description}>
          {formatPasswordStrength(score)}
        </div>
      )}
    </div>
  )
);

export type StrengthType = {|
  ...AsComponentType,
  ...StrengthOwnPropsType
|};

export default (Object.assign(
  AsComponent(ComponentNames.Strength)(Strength),
  {}
): React.ComponentType<StrengthType>);
