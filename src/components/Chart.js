/* #__flow */
import * as React from 'react';
import ChartJS from 'chart.js';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Chart.scss';

const DEFAULT_CONFIG = {
  datasets: [],
  legend: {
    display: false
  },
  title: {
    display: false
  },
  tooltips: {
    enabled: false
  }
};

const FIXED_CONFIG = {
  maintainAspectRatio: false,
  responsive: true
};

const CHART_KINDS = Object.freeze({
  BAR: 'bar',
  BUBBLE: 'bubble',
  DOUGHNUT: 'doughnut',
  LINE: 'line',
  PIE: 'pie',
  POLAR: 'polarArea',
  RADAR: 'radar',
  SCATTER: 'scatter'
});

const SCALE_TYPES = Object.freeze({
  RADIAL: 'radial',
  X: 'x',
  Y: 'y'
});

type AxisType = Object;

const Axis = (props: AxisType) => null;

type DatasetType = Object;

const Dataset = (props: DatasetType) => null;

type LegendType = Object;

const Legend = (props: LegendType) => null;

type TitleType = Object;

const Title = (props: TitleType) => null;

type TooltipsType = Object;

const Tooltips = (props: TooltipsType) => null;

type ChartChildType = AxisType | DatasetType | LegendType | TitleType | TooltipsType;

type UseChartType = {|
  children: ReactElementsChildren<ChartChildType>,
  kind?: $Values<typeof CHART_KINDS>,
  labels: Array<string>,
  padding?: Object
|};

const useChart = ({ children, kind, labels, padding: componentPadding, ...options }: UseChartType) => {
  const padding = React.useMemo(
    () => ({ bottom: 16, left: 16, right: 16, top: 0, ...componentPadding }),
    [componentPadding]
  );

  const chartRef = React.useRef(null);
  const [chart, setChart] = React.useState(null);

  React.useEffect(() => {
    if (!chart) {
      const { datasets, legend, scale, scales, title, tooltips } = React.Children.toArray(children).reduce(
        (result: Object, child: React.Element<ChartChildType>) =>
          ({
            Axis: ({ kind, ...props }: Object) =>
              kind === SCALE_TYPES.RADIAL
                ? { ...result, scale: { ...props } }
                : ((axisKey: $Values<typeof SCALE_TYPES>) => ({
                    ...result,
                    scales: {
                      ...result.scales,
                      [axisKey]: [...(result.scales ? result.scales[axisKey] : []), ...props]
                    }
                  }))(
                    {
                      [SCALE_TYPES.X]: 'xAxes',
                      [SCALE_TYPES.Y]: 'yAxes'
                    }[kind]
                  ),
            Dataset: (props: Object) => ({ ...result, datasets: [...result.datasets, { ...props }] }),
            Legend: (props: Object) => ({ ...result, legend: { display: true, ...props } }),
            Title: ({ children, text = children, ...props }: Object) => ({
              ...result,
              title: { display: true, text, ...props }
            }),
            Tooltips: (props: Object) => ({ ...result, tooltips: { enabled: true, ...props } })
          }[child.type.displayName](child.props)),
        DEFAULT_CONFIG
      );

      setChart(
        new ChartJS(chartRef.current, {
          data: { datasets, labels },
          options: {
            layout: { padding },
            ...options,
            legend,
            title,
            tooltips,
            ...(scale ? { scale } : {}),
            ...(scales ? { scales } : {}),
            ...FIXED_CONFIG
          },
          type: kind
        })
      );
    }
  }, [chart, children, kind, labels, padding, options]);

  return { chart, chartRef, ctx: chartRef.current && chartRef.current.getContext('2d') };
};

type ChartOwnPropsType = {|
  ...UseChartType,
  height?: number  
|};

type WrappedChartType = {|
  ...AsComponentExtensionType,
  ...ChartOwnPropsType
|};

const Chart = React.forwardRef(({ className, height, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => {
  const { chartRef } = useChart(props);

  return (
    <div ref={forwardedRef} className={cx(styles.Chart, className)} style={{ height: unitize(height) }}>
      <canvas ref={chartRef} className={styles.Canvas} />
    </div>
  );
});

const Bar = React.forwardRef(({ children, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => (
  <Chart ref={forwardedRef} {...props} kind={CHART_KINDS.BAR}>
    {children}
  </Chart>
));

const Bubble = React.forwardRef(({ children, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => (
  <Chart ref={forwardedRef} {...props} kind={CHART_KINDS.BUBBLE}>
    {children}
  </Chart>
));

const Doughnut = React.forwardRef(({ children, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => (
  <Chart ref={forwardedRef} {...props} kind={CHART_KINDS.DOUGHNUT}>
    {children}
  </Chart>
));

const Line = React.forwardRef(({ children, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => (
  <Chart ref={forwardedRef} {...props} kind={CHART_KINDS.LINE}>
    {children}
  </Chart>
));

const Pie = React.forwardRef(({ children, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => (
  <Chart ref={forwardedRef} {...props} kind={CHART_KINDS.PIE}>
    {children}
  </Chart>
));

const Polar = React.forwardRef(({ children, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => (
  <Chart ref={forwardedRef} {...props} kind={CHART_KINDS.POLAR}>
    {children}
  </Chart>
));

const Radar = React.forwardRef(({ children, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => (
  <Chart ref={forwardedRef} {...props} kind={CHART_KINDS.RADAR}>
    {children}
  </Chart>
));

const Scatter = React.forwardRef(({ children, ...props }: WrappedChartType, forwardedRef: ReactForwardedRefType) => (
  <Chart ref={forwardedRef} {...props} kind={CHART_KINDS.SCATTER}>
    {children}
  </Chart>
));

export type ChartType = {|
  ...AsComponentType,
  ...ChartOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Chart)(Chart), {
      Axis: Object.assign(AsComponent(ComponentNames.Chart.Axis)(Axis), { KINDS: SCALE_TYPES }),
      Dataset: AsComponent(ComponentNames.Chart.Dataset)(Dataset),
      Legend: AsComponent(ComponentNames.Chart.Legend)(Legend),
      Title: AsComponent(ComponentNames.Chart.Title)(Title),
      Tooltips: AsComponent(ComponentNames.Chart.Tooltips)(Tooltips)
    }, {
      Bar: AsComponent(ComponentNames.Chart.Bar)(Bar),
      Bubble: AsComponent(ComponentNames.Chart.Bubble)(Bubble),
      Doughnut: AsComponent(ComponentNames.Chart.Doughnut)(Doughnut),
      Line: AsComponent(ComponentNames.Chart.Line)(Line),
      Pie: AsComponent(ComponentNames.Chart.Pie)(Pie),
      Polar: AsComponent(ComponentNames.Chart.Polar)(Polar),
      Radar: AsComponent(ComponentNames.Chart.Radar)(Radar),
      Scatter: AsComponent(ComponentNames.Chart.Scatter)(Scatter)
    }, {
      KINDS: CHART_KINDS
    }
  ): React.ComponentType<ChartType> & {
    Axis: React.ComponentType<any>,
    Dataset: React.ComponentType<any>,
    Legend: React.ComponentType<any>,
    Title: React.ComponentType<any>,
    Tooltips: React.ComponentType<any>
  } & {
    Bar: React.ComponentType<ChartType>,
    Bubble: React.ComponentType<ChartType>,
    Doughnut: React.ComponentType<ChartType>,
    Line: React.ComponentType<ChartType>,
    Pie: React.ComponentType<ChartType>,
    Polar: React.ComponentType<ChartType>,
    Radar: React.ComponentType<ChartType>,
    Scatter: React.ComponentType<ChartType>
  } & {
    KINDS: Object
  }
);
