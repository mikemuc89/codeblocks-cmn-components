/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import useOpenState from '../hooks/useOpenState';
import styles from './ToggleView.scss';

type ToggleViewOwnPropsType = {|
  children: ReactChildren,
  closeMessage?: string,
  open?: boolean,
  openMessage?: string
|};

type WrappedToggleViewType = {|
  ...AsComponentExtensionType,
  ...ToggleViewOwnPropsType
|};

const ToggleView = React.forwardRef(
  (
    { children, className, closeMessage = 'ukryj', open: initiallyOpen, openMessage = 'pokaż' }: WrappedToggleViewType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { open, onToggle } = useOpenState({ open: initiallyOpen });
    const message = React.useMemo(() => (open ? closeMessage : openMessage), [closeMessage, open, openMessage]);

    return (
      <div ref={forwardedRef} className={cx(styles.ToggleView, className)}>
        <div className={styles.Message} onClick={onToggle}>
          {message}
        </div>
        {open && <div className={styles.Content}>{children}</div>}
      </div>
    );
  }
);

export type ToggleViewType = {|
  ...AsComponentType,
  ...ToggleViewOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.ToggleView)(ToggleView), {}): React.ComponentType<ToggleViewType>
);
