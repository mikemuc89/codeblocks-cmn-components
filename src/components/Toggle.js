/* @flow */
import * as React from 'react';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import { type IconPropType } from '../types';
import Message from './Message';
import Required from './Required';
import styles from './Toggle.scss';
import boolean from '@omnibly/codeblocks-cmn-schema/src/schema/fields/boolean';

const ICON_SIZE = 22;
const ICON_INNER_SIZE = 14;

type ToggleOwnPropsType = {|
  iconOff?: IconPropType,
  iconOn?: IconPropType,
  pull?: boolean,
  small?: boolean
|};

type WrappedToggleType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...ToggleOwnPropsType
|};

const Toggle = React.forwardRef(
  (
    {
      className,
      controlElement,
      disabled,
      iconOff,
      iconOn,
      messageElement = null,
      pull,
      readonly,
      requiredElement = null,
      setValue,
      small,
      value
    }: WrappedToggleType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const toggleValue = React.useCallback(() => setValue((oldValue) => !oldValue), [setValue]);

    const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
      const { keyCode } = e;

      if ([KEY_CODES.ENTER].includes(keyCode)) {
        toggleValue();
      }
    }, [toggleValue]);

    const handleOnClick = React.useCallback((e: SnytheticEvent<HTMLElement>) => {
      e.stopPropagation();
      toggleValue();
    }, [toggleValue]);

    const iconOffElement = iconOff && (
      React.cloneElement(iconOff, {
        className: styles.IconOff,
        disabled,
        onClick: (disabled || readonly) ? undefined : handleOnClick,
        size: iconOff.props.size || ICON_SIZE,
        ...{
          [ComponentNames.CharIcon]: {
            innerSize: iconOff.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: iconOff.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: iconOff.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: iconOff.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: iconOff.props.innerSize || ICON_INNER_SIZE
          }
        }[iconOff.type.componentId]
      })
    );

    const iconOnElement = iconOn && (
      React.cloneElement(iconOn, {
        className: styles.IconOff,
        disabled,
        onClick: (disabled || readonly) ? undefined : handleOnClick,
        size: iconOn.props.size || ICON_SIZE,
        ...{
          [ComponentNames.CharIcon]: {
            innerSize: iconOn.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: iconOn.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: iconOn.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: iconOn.props.innerSize || ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: iconOn.props.innerSize || ICON_INNER_SIZE
          }
        }[iconOn.type.componentId]
      })
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Toggle,
          value ? styles.Toggle__On : styles.Toggle__Off,
          pull && styles.Toggle__Pull,
          small && styles.Toggle__Small,
          className
        )}
      >
        {React.cloneElement(controlElement, {
          onKeyDown: (disabled || readonly) ? undefined : (e: SyntheticKeyboardEvent<HTMLInputElement>) => {
            handleOnKeyDown(e);
            controlElement.props.onKeyDown(e);
          }
        })}
        <div className={styles.Switch} onClick={disabled ? undefined : handleOnClick}>
          <span className={styles.Button} />
          {iconOffElement}
          {iconOnElement}
        </div>
        {messageElement}
        {requiredElement}
      </div>
    );
  }
);

export type ToggleType = {|
  ...AsComponentType,
  ...AsControlExtensionType,
  ...ToggleOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Toggle)(AsControl({
    controlType: INPUT_TYPES.CHECKBOX,
    defaultValue: false,
    sanitizeValueForControl: () => '',
    style: { key: 'Toggle', styles }
  })(Toggle)), {}): React.ComponentType<ToggleType>
);
