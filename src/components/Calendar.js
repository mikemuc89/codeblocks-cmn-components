/* #__flow */
import * as React from 'react';
import { formatMonthName, formatWeekdayAbbreviation } from '@omnibly/codeblocks-cmn-formatters';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { HTML_TAGS, INPUT_TYPES, KEY_CODES, WEEKDAYS } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid, padString, repeatPattern } from '@omnibly/codeblocks-cmn-utils';
import { deltaDays, deltaMonths, deltaYears, deltaWeeks, formatISO, getFirstDayOfMonth, getFirstDayOfWeek, isToday, parseDate, DAY, WEEK_LENGTH, type InputDate } from '@omnibly/codeblocks-cmn-dateutils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import WithLayer, {
  LayerContentContext,
  LAYER_KINDS,
  LAYER_MODES,
  type WithLayerExtensionType, type WithLayerType
} from '../hocs/components/WithLayer';
import FontAwesome from './FontAwesome';
import Input from './Input';
import styles from './Calendar.scss';

const ICON_SIZE = 32;
const ICON_INNER_SIZE = 12;
const WEEKS_VISIBLE = 6;

const prepareControlValue = (value?: InputDate) => formatISO(value || new Date(), { representation: 'date' });
/* eslint-disable-next-line no-empty-function */
const dummyHandler = () => {};

const CalendarSelectionContext = React.createContext(null);

type CalendarOwnPropsType = {|
  border?: boolean,
  initialDate?: InputDate,
  maxDate?: InputDate,
  minDate?: InputDate,
  showOtherMonthDays?: boolean,
  value?: string,
  weekStart?: $Values<typeof WEEKDAYS>
|};

type WrappedCalendarType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType<string | typeof Date | null>,
  ...CalendarOwnPropsType
|};

const Calendar = React.forwardRef(
  (
    {
      border = true,
      className,
      controlElement,
      disabled,
      initialDate,
      maxDate,
      messageElement,
      minDate,
      multiple,
      requiredElement,
      setValue,
      showOtherMonthDays = true,
      value,
      weekStart = WEEKDAYS.MONDAY
    }: WrappedCalendarType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const [visibleDate, setVisibleDate] = React.useState(prepareControlValue(initialDate || (value ? (Array.isArray(value) ? value[0] : value) : undefined)));

    const daysParentElementRef = React.useRef(null);
    const selection = React.useContext(CalendarSelectionContext);

    React.useEffect(() => {
      const el = daysParentElementRef.current;
      if (el) {
        const day = el.querySelector(`input[id="${visibleDate}"]`);
        if (day) {
          day.focus();
        }
      }
    }, [visibleDate]);

    const year = React.useMemo(() => new Date(visibleDate).getFullYear(), [visibleDate]);
    const month = React.useMemo(() => new Date(visibleDate).getMonth(), [visibleDate]);

    const onChangeToCurrentDate = React.useCallback(() => setValue(visibleDate), [setValue, visibleDate]);

    const onMoveToNextDay = React.useCallback(() => setVisibleDate((date: InputDate) => prepareControlValue(deltaDays(date, 1))), [setVisibleDate]);
    const onMoveToNextMonth = React.useCallback(() => setVisibleDate((date: InputDate) => prepareControlValue(deltaMonths(date, 1))), [setVisibleDate]);
    const onMoveToNextWeek = React.useCallback(() => setVisibleDate((date: InputDate) => prepareControlValue(deltaWeeks(date, 1))), [setVisibleDate]);
    const onMoveToNextYear = React.useCallback(() => setVisibleDate((date: InputDate) => prepareControlValue(deltaYears(date, 1))), [setVisibleDate]);
    const onMoveToPreviousDay = React.useCallback(() => setVisibleDate((date: InputDate) => prepareControlValue(deltaDays(date, -1))), [setVisibleDate]);
    const onMoveToPreviousMonth = React.useCallback(() => setVisibleDate((date: InputDate) => prepareControlValue(deltaMonths(date, -1))), [setVisibleDate]);
    const onMoveToPreviousWeek = React.useCallback(() => setVisibleDate((date: InputDate) => prepareControlValue(deltaWeeks(date, -1))), [setVisibleDate]);
    const onMoveToPreviousYear = React.useCallback(() => setVisibleDate((date: InputDate) => prepareControlValue(getYearDelta(date, -1))), [setVisibleDate]);

    const onKeyDown = React.useCallback((e: KeyboardEvent) => {
      const { keyCode, target } = e;
      if (target.tagName === HTML_TAGS.INPUT) {
        switch (keyCode) {
          case KEY_CODES.UP_ARROW:
            e.preventDefault();
            onMoveToPreviousWeek();
            break;
          case KEY_CODES.DOWN_ARROW:
            e.preventDefault();
            onMoveToNextWeek();
            break;
          case KEY_CODES.RIGHT_ARROW:
            e.preventDefault();
            onMoveToNextDay();
            break;
          case KEY_CODES.LEFT_ARROW:
            e.preventDefault();
            onMoveToPreviousDay();
            break;
          case KEY_CODES.PAGE_UP:
            e.preventDefault();
            onMoveToPreviousMonth();
            break;
          case KEY_CODES.PAGE_DOWN:
            e.preventDefault();
            onMoveToNextMonth();
            break;
          case KEY_CODES.ENTER:
          case KEY_CODES.SPACE:
            e.preventDefault();
            onChangeToCurrentDate();
            break;
          default:
          //
        }
      }
    }, [
      onChangeToCurrentDate,
      onMoveToNextDay,
      onMoveToNextMonth,
      onMoveToNextWeek,
      onMoveToPreviousDay,
      onMoveToPreviousMonth,
      onMoveToPreviousWeek
    ]);

    const yearHeaderElement = (
      <div className={styles.Header}>
        <FontAwesome
          className={cx(styles.Button, styles.Button__Back)}
          onClick={disabled ? undefined : onMoveToPreviousYear}
          id="chevron-left"
          size={ICON_SIZE}
          innerSize={ICON_INNER_SIZE}
        />
        <span className={styles.Label}>{year}</span>
        <FontAwesome
          className={cx(styles.Button, styles.Button__Forward)}
          onClick={disabled ? undefined : onMoveToNextYear}
          id="chevron-right"
          size={ICON_SIZE}
          innerSize={ICON_INNER_SIZE}
        />
      </div>
    );

    const monthHeaderElement = (
      <div className={styles.Header}>
        <FontAwesome
          className={cx(styles.Button, styles.Button__Back)}
          onClick={disabled ? undefined : onMoveToPreviousMonth}
          id="chevron-left"
          size={ICON_SIZE}
          innerSize={ICON_INNER_SIZE}
        />
        <span className={styles.Label}>{formatMonthName(month)}</span>
        <FontAwesome
          className={cx(styles.Button, styles.Button__Forward)}
          onClick={disabled ? undefined : onMoveToNextMonth}
          id="chevron-right"
          size={ICON_SIZE}
          innerSize={ICON_INNER_SIZE}
        />
      </div>
    );

    const weekdayElements = repeatPattern(WEEK_LENGTH, (index: number) => (
      <span key={index} className={styles.Weekday}>
        {formatWeekdayAbbreviation((weekStart + index) % WEEK_LENGTH)}
      </span>
    ));

    const dayElements = React.useMemo(() => {
      const today = prepareControlValue();
      const firstDayOfPage = getFirstDayOfWeek(getFirstDayOfMonth(), { weekStart });

      const firstDayOfPageYear = firstDayOfPage.getFullYear();
      const firstDayOfPageMonth = firstDayOfPage.getMonth();
      const firstDayOfPageDay = firstDayOfPage.getDate();

      const maxDateObj = maxDate && parseDate(maxDate);
      const minDateObj = minDate && parseDate(minDate);

      const selectionBottomObj = selection && new Date(selection.bottom);
      const selectionTopObj = selection && new Date(selection.top);

      return repeatPattern(WEEK_LENGTH * WEEKS_VISIBLE, (index: number) => {
        const dateObj = new Date(firstDayOfPageYear, firstDayOfPageMonth, firstDayOfPageDay + index);
        const date = prepareControlValue(dateObj);
        const isSelected = multiple ? value && value.includes(date) : value === date;
        const onClick = (e: MouseEvent) => {
          e.preventDefault();
          e.stopPropagation();
          setValue(date);
        };
        const isOtherMonth = month !== dateObj.getMonth();
        const day = !dateObj || (!showOtherMonthDays && isOtherMonth) ? '' : dateObj.getDate();
        const isDisabled = disabled || (maxDateObj && dateObj > maxDateObj) || (minDateObj && dateObj < minDateObj);
        const isBetweenSelection = selection && selectionBottomObj < dateObj && selectionTopObj > dateObj;

        return (
          <div
            key={date}
            className={cx(
              styles.Day,
              isBetweenSelection && styles.Day__BetweenSelection,
              isDisabled && styles.Day__Disabled,
              isToday(dateObj) && styles.Day__Today,
              isSelected && styles.Day__Selected
            )}
          >
            {day && React.cloneElement(controlElement, {
              checked: isSelected,
              id: date
            })}
            <span className={styles.DayNumber} onClick={isDisabled || !day ? undefined : onClick}>
              {day}
            </span>
          </div>
        );
      });
    }, [
      disabled,
      maxDate,
      minDate,
      month,
      selection,
      setValue,
      showOtherMonthDays,
      value,
      weekStart,
      year
    ]);

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Calendar,
          border && styles.Calendar__Border,
          className
        )}
      >
        {controlElement}
        <div className={styles.Content}>
          {yearHeaderElement}
          {monthHeaderElement}
          <div className={styles.Weekdays}>{weekdayElements}</div>
          <div ref={daysParentElementRef} className={styles.Days} onKeyDown={onKeyDown}>
            {dayElements}
          </div>
        </div>
        {requiredElement}
        {messageElement}
      </div>
    );
  }
);

type RangeOwnPropsType = {|
  ...WithMessagesType,
  border?: boolean,
  disabled?: boolean,
  initialDate?: Date | string,
  maxDate?: string,
  minDate?: string,
  name?: string,
  onBlur: (e: SyntheticFocusEvent<HTMLDivElement>) => void,
  onChange: (value: string) => void,
  onFocus: (e: SyntheticFocusEvent<HTMLDivElement>) => void,
  readonly?: boolean,
  required?: boolean,
  showOtherMonthDays?: boolean,
  value: string,
  weekStart: $Values<typeof WEEKDAYS>
|};

type WrappedRangeType = {|
  ...AsComponentExtensionType,
  ...RangeOwnPropsType
|};

const Range = React.forwardRef(
  (
    {
      border = true,
      className,
      disabled,
      errors,
      helper,
      hints,
      initialDate,
      maxDate,
      messageElement,
      minDate,
      name,
      onBlur,
      onChange,
      onFocus,
      readonly,
      requiredElement,
      showOtherMonthDays = true,
      value,
      warnings,
      weekStart = WEEKDAYS.MONDAY
    }: WrappedRangeType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const [controlValue, setControlValue] = React.useState({
      bottom: value && value.bottom && prepareControlValue(value.bottom),
      top: value && value.top && prepareControlValue(value.top)
    });
    const [currentDate] = React.useState(
      initialDate
        ? {
            bottom: prepareControlValue(initialDate),
            top: prepareControlValue(getMonthDelta(initialDate, 1))
          }
        : {
            bottom: controlValue.bottom || prepareControlValue(new Date()),
            top: controlValue.top
              ? new Date(controlValue.bottom).getMonth() === new Date(controlValue.top).getMonth()
                ? prepareControlValue(getMonthDelta(controlValue.top, 1))
                : controlValue.top
              : prepareControlValue(getMonthDelta(new Date(), 1))
          }
    );
    const inputName = React.useMemo(() => name || guid(), [name]);
    const onChangeHandler = React.useCallback(
      (date: Date | string) => {
        if (!readonly) {
          const clickedValue = date[date.length - 1];
          const newValue =
            !controlValue.bottom || (controlValue.bottom && controlValue.top)
              ? { bottom: clickedValue, top: null }
              : new Date(clickedValue) < new Date(controlValue.bottom)
              ? { bottom: clickedValue, top: controlValue.bottom }
              : { bottom: controlValue.bottom, top: clickedValue };
          if (onChange) {
            onChange(newValue);
          }
          setControlValue(newValue);
        }
      },
      [controlValue.bottom, controlValue.top, onChange, readonly]
    );
    const childValue = React.useMemo(() => [controlValue.bottom, controlValue.top].filter(Boolean), [controlValue]);
    const selection = React.useMemo(
      () => (controlValue.bottom && controlValue.top ? controlValue : null),
      [controlValue]
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Range,
          cx.control(styles, 'Range', { disabled, errors, helper, hints, warnings }),
          border && styles.Range__Border,
          className
        )}
        onBlur={onBlur}
        onFocus={onFocus}
      >
        <CalendarSelectionContext.Provider value={selection}>
          <Calendar
            border={border}
            disabled={disabled}
            initialDate={currentDate.bottom}
            maxDate={maxDate}
            maximum={3}
            minDate={minDate}
            name={`calendarFrom.${inputName}`}
            onChange={onChangeHandler}
            readonly={readonly}
            showOtherMonthDays={showOtherMonthDays}
            value={childValue}
            weekStart={weekStart}
          />
          <Calendar
            border={border}
            disabled={disabled}
            initialDate={currentDate.top}
            maxDate={maxDate}
            maximum={3}
            minDate={minDate}
            name={`calendarTo.${inputName}`}
            onChange={onChangeHandler}
            readonly={readonly}
            showOtherMonthDays={showOtherMonthDays}
            value={childValue}
            weekStart={weekStart}
          />
        </CalendarSelectionContext.Provider>
        {requiredElement}
        {messageElement}
      </div>
    );
  }
);

const WrappedCalendar = AsComponent(ComponentNames.Calendar)(
  AsControl({
    style: { key: 'Calendar', styles },
    defaultValue: null,
    controlType: INPUT_TYPES.HIDDEN,
    sanitizeValueForControl: (value: string) => value
  })(Calendar)
);

type PickerOwnPropsType = {|
  ...WithMessagesType,
  disabled?: boolean,
  canClear?: boolean,
  focus?: boolean,
  name?: string,
  onBlur?: (e: SyntheticFocusEvent<HTMLDivElement>) => void,
  onChange?: (value: string) => void,
  onClick?: (e: MouseEvent) => void,
  onFocus?: (e: SyntheticFocusEvent<HTMLDivElement>) => void,
  open?: boolean,
  placeholder?: string,
  readonly?: boolean,
  required?: boolean,
  value: string
|};

type WrappedPickerWrapperType = {|
  ...AsComponentExtensionType,
  ...WithLayerExtensionType,
  ...PickerOwnPropsType
|};

const PickerWrapper = React.forwardRef(
  (
    {
      className,
      disabled,
      canClear,
      errors,
      focus,
      helper,
      hints,
      layerElement,
      layerTriggerElementRef,
      messageElement,
      name,
      onBlur,
      onChange,
      onClick,
      onClear,
      onFocus,
      open,
      placeholder,
      readonly,
      requiredElement,
      value,
      warnings
    }: WrappedPickerWrapperType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const inputElement = React.useMemo(
      () => (
        <Input.Date
          ref={layerTriggerElementRef}
          className={styles.Input}
          canClear={canClear}
          disabled={disabled}
          focus={focus}
          name={name}
          onChange={onChange}
          onClear={onClear}
          placeholder={placeholder}
          readonly={readonly}
          value={cache}
        />
      ),
      [cache, canClear, disabled, focus, layerTriggerElementRef, name, onChange, onClear, placeholder, readonly]
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Picker,
          cx.control(styles, 'Picker', { disabled, errors, focus, helper, hints, warnings }),
          className
        )}
        onBlur={disabled ? undefined : onBlur}
        onFocus={disabled ? undefined : onFocus}
      >
        {inputElement}
        {requiredElement}
        {messageElement}
        <LayerContentContext.Provider value={inputElement}>{layerElement}</LayerContentContext.Provider>
      </div>
    );
  }
);

const PickerWrapperWithLayer = WithLayer({
  height: 346,
  kind: LAYER_KINDS.CLOCK,
  mode: LAYER_MODES.VERTICAL,
  showTriggerElement: true
})(PickerWrapper);

type WrappedPickerType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType<string | typeof Date | null>,
  ...WithLayerExtensionType,
  ...PickerOwnPropsType
|};

const Picker = React.forwardRef(
  ({ disabled, name, onChange, setControlValue, value, ...props }: WrappedPickerType, forwardedRef: ReactForwardedRefType) => {
    const [cache, setCache] = React.useState(value);

    const onInputChangeHandler = React.useCallback(
      (value: string) => {
        setControlValue(value);
        setCache(value);
        if (onChange) {
          onChange(value);
        }
      },
      [onChange, setCache, setControlValue]
    );

    const onCalendarChangeHandler = React.useCallback(
      (value: string) => {
        setCache(value);
        if (onChange) {
          onChange(value);
        }
      },
      [onChange, setCache]
    );

    return (
      <PickerWrapperWithLayer
        ref={forwardedRef}
        disabled={disabled}
        name={`input.${name}`}
        onChange={disabled ? undefined : onInputChangeHandler}
        value={value}
        {...props}
      >
        <WrappedCalendar
          border={false}
          disabled={disabled}
          name={`calendar.${name}`}
          onChange={disabled ? undefined : onCalendarChangeHandler}
          value={value}
        />
      </PickerWrapperWithLayer>
    );
  }
);

export type CalendarType = {|
  ...AsComponentType,
  ...AsControlnType<string | typeof Date | null>,
  ...CalendarOwnPropsType
|};

export type PickerType = {|
  ...AsComponentType,
  ...AsControlType<string | typeof Date | null>,
  ...WithLayerType,
  ...PickerOwnPropsType
|};

export type RangeType = {|
  ...AsComponentType,
  ...RangeOwnPropsType
|};

export default (
  Object.assign(WrappedCalendar, {
    Picker: AsComponent(ComponentNames.Calendar.Picker)(
      AsControl({ parseValue: prepareControlValue, singleChangeType: AsControl.SINGLE_CHANGE_TYPES.ALWAYS_NEW })(Picker)
    ),
    Range: AsComponent(ComponentNames.Calendar.Range)(Range)
  }): React.ComponentType<CalendarType> & {
    Picker: React.ComponentType<PickerType>,
    Range: React.ComponentType<RangeType>
  }
);
