/* @flow */
import * as React from 'react';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Loader.scss';

const DEFAULT_LOADER_TEXT = 'Loading in progress';

const getElementHeight = (elm: HTMLElement) => {
  const { paddingBottom, paddingTop } = getComputedStyle(elm);
  return elm.scrollHeight - parseInt(paddingBottom) - parseInt(paddingTop);
};

type SpinnerOwnPropsType = {|
  children?: string  
|};

type WrappedSpinnerType = {|
  ...AsComponentExtensionType,
  ...SpinnerOwnPropsType
|};

const Spinner = React.forwardRef(({ children, className }: WrappedSpinnerType, forwardedRef: ReactForwardedRefType) => (
  <div ref={forwardedRef} className={cx(styles.Spinner, className)}>
    <div className={styles.Content}>
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
    </div>
    {children && <div className={styles.Message}>{children}</div>}
  </div>
));

const Upload = React.forwardRef(({ children, className }: WrappedSpinnerType, forwardedRef: ReactForwardedRefType) => (
  <div ref={forwardedRef} className={cx(styles.Upload, className)}>
    <div className={styles.Content}>
      <div />
      <div />
      <div />
    </div>
    {children && <div className={styles.Message}>{children}</div>}
  </div>
));

type LoaderOwnPropsType = {|
  duration?: number,
  forceLoadingHeight?: boolean,
  loadingClassName?: string,
  loadingHeight?: number,
  loading?: boolean,
  message?: string,
  preserveParentHeight?: boolean  
|};

type WrappedLoaderType = {|
  ...AsComponentExtensionType,
  ...LoaderOwnPropsType
|};

const Loader = React.forwardRef(
  (
    {
      className,
      duration = 500,
      forceLoadingHeight,
      loadingClassName,
      loadingHeight,
      loading,
      message = DEFAULT_LOADER_TEXT,
      preserveParentHeight
    }: WrappedLoaderType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const ref = React.useRef(null);
    const afterLoadingTimeout = React.useRef(null);

    React.useEffect(() => {
      if (ref.current) {
        const parent = ref.current.parentElement;
        const height = getElementHeight(parent);

        if (preserveParentHeight) {
          parent.style.transition = null;
        } else {
          parent.style.transition = `height ${duration}ms ease-in-out`;
        }

        if (loading) {
          clearTimeout(afterLoadingTimeout.current);
          if (!preserveParentHeight) {
            parent.style.height = `${forceLoadingHeight ? loadingHeight : Math.max(height, loadingHeight)}px`;
          }
          ref.current.style.display = 'block';
          setTimeout(() => {
            ref.current.style.opacity = 1;
          });
        } else {
          ref.current.style.transition = `opacity ${duration}ms ease-in-out`;
          ref.current.style.opacity = 0;
          parent.style.overflow = 'hidden';
          if (!preserveParentHeight) {
            parent.style.height = `${height}px`;
          }
          afterLoadingTimeout.current = setTimeout(() => {
            if (ref.current) {
              ref.current.style.display = 'none';
              parent.style.overflow = null;
              parent.style.height = null;
            }
          }, duration);
        }
      }
    }, [duration, forceLoadingHeight, loading, loadingHeight, preserveParentHeight]);

    return (
      <div
        ref={mergeRefs(ref, forwardedRef)}
        className={cx(styles.Loader, loading && styles.Loader__Loading, className, loading && loadingClassName)}
      >
        <Spinner>{message}</Spinner>
      </div>
    );
  }
);

export type SpinnerType = {|
  ...AsComponentType,
  ...SpinnerOwnPropsType
|};

export type LoaderType = {|
  ...AsComponentType,
  ...LoaderOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Loader)(Loader), {
    Spinner: Object.assign(AsComponent(ComponentNames.Loader.Spinner)(Spinner), {
      Upload: AsComponent(ComponentNames.Loader.Spinner.Upload)(Upload)
    })
  }): React.ComponentType<LoaderType> & {
    Spinner: React.ComponentType<SpinnerType> & {
      Upload: React.ComponentType<SpinnerType>
    }
  }
);
