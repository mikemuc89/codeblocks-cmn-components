/* #__flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import useEventListener from '../hooks/useEventListener';
import styles from './Carousel.scss';

const RESUME_HANDLER_DELAY = 500;

type ItemOwnPropsType = {|
  children: ReactChildren  
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(({ backgroundElementRef, children, className }: WrappedItemType, forwardedRef: ReactForwardedRefType) => (
  <div
    ref={mergeRefs(forwardedRef, backgroundElementRef)}
    className={cx(styles.Item, className)}
  >
    {children}
  </div>
));

export type ItemType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...ItemOwnPropsType
|};

type CarouselOwnPropsType = {|
  buttons?: boolean,
  children: ReactElementsChildren<ItemType>,
  height?: number  
|};

type WrappedCarouselType = {|
  ...AsComponentExtensionType,
  ...CarouselOwnPropsType
|};

const Carousel = React.forwardRef(
  ({ buttons = true, children, className, height = 240 }: WrappedCarouselType, forwardedRef: ReactForwardedRefType) => {
    const contentRef = React.useRef(null);
    const scrollRef = React.useRef(null);
    const [width, setWidth] = React.useState(0);
    const childrenCount = React.Children.count(children);
    const delta = React.useMemo(() => width / childrenCount, [childrenCount, width]);

    const handleScroll = React.useCallback(
      (e: Event) => {
        const el = scrollRef.current;
        if (el) {
          const scroll = el.scrollLeft;
          if (scroll < width || scroll >= width * 2) {
            el.scrollLeft = width + (scroll % width);
          }
        }
      },
      [width]
    );

    const { pauseHandler, resumeHandler } = useEventListener('scroll', handleScroll, scrollRef.current);

    const onScrollToNext = React.useCallback(() => {
      const el = scrollRef.current;
      if (el) {
        pauseHandler();
        if (el.scrollLeft + delta >= width * 2) {
          el.scrollLeft = el.scrollLeft - width;
        }
        el.scrollTo({ behavior: 'smooth', left: el.scrollLeft + delta });
        resumeHandler({ delay: RESUME_HANDLER_DELAY });
      }
    }, [delta, pauseHandler, resumeHandler, width]);

    const onScrollToPrev = React.useCallback(() => {
      const el = scrollRef.current;
      if (el) {
        pauseHandler();
        if (el.scrollLeft - delta < width) {
          el.scrollLeft = el.scrollLeft + width;
        }
        el.scrollTo({ behavior: 'smooth', left: el.scrollLeft - delta });
        resumeHandler({ delay: RESUME_HANDLER_DELAY });
      }
    }, [delta, pauseHandler, resumeHandler, width]);

    React.useLayoutEffect(() => {
      if (contentRef.current) {
        setWidth(parseInt(getComputedStyle(contentRef.current).width) * childrenCount);
        scrollRef.current.scrollLeft = width;
      }
    }, [childrenCount, width]);

    return (
      <div
        ref={mergeRefs(contentRef, forwardedRef, scrollRef)}
        className={cx(styles.Carousel, className)}
        style={{ height: unitize(height) }}
      >
        {buttons && (
          <div className={styles.PreviousButton} onClick={onScrollToPrev}>
            &lt;
          </div>
        )}
        {Array(3)
          .fill()
          .map(() => children)}
        {buttons && (
          <div className={styles.NextButton} onClick={onScrollToNext}>
            &gt;
          </div>
        )}
      </div>
    );
  }
);

export type CarouselType = {|
  ...AsComponentType,
  ...CarouselOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Carousel)(Carousel), {
    Item: AsComponent(ComponentNames.Carousel.Item)(WithBackground()(Item))
  }): React.ComponentType<CarouselType> & {
    Item: React.ComponentType<ItemType>
  }
);
