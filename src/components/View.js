/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsContainer, { type AsContainerExtensionType, type AsContainerType } from '../hocs/components/AsContainer';
import styles from './View.scss';

type ViewOwnPropsType = {|
  children?: ReactChildren,
  fullHeight?: boolean,
  height?: number,
  noOverflow?: boolean,
  width?: number
|};

type WrappedViewType = {|
  ...AsComponentExtensionType,
  ...AsContainerExtensionType,
  ...ViewOwnPropsType
|};

const View = React.forwardRef(({
  containerElementRef,
  children,
  className,
  fullHeight,
  height,
  noOverflow,
  width
}: WrappedViewType,
forwardedRef: ReactForwardedRefType) => {
    const { innerWidth } = React.useContext(MainSizeContext);

    return (
      <div
        ref={mergeRefs(forwardedRef, containerElementRef)}
        className={cx(styles.View, fullHeight && styles.View__FullHeight, noOverflow && styles.View__NoOverflow, className)}
        style={{
          width: unitize(innerWidth),
          ...(height
            ? {
                maxHeight: unitize(height),
                overflowY: 'auto'
              }
            : {}),
          ...(width
            ? {
                width: unitize(width)
              }
            : {})
        }}
      >
        {children}
      </div>
    );
  }
);

export type ViewType = {|
  ...AsComponentType,
  ...AsContainerType,
  ...ViewOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.View)(AsContainer()(View)), {}): React.ComponentType<ViewType>
);
