/* @flow */
import * as React from 'react';
import { LABEL_POSITIONS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import Required from './Required';
import styles from './Label.scss';

const LABEL_POSITION_TO_CLASS = Object.freeze({
  [LABEL_POSITIONS.BOTTOM]: styles.Label__Bottom,
  [LABEL_POSITIONS.BOTTOM_CENTER]: styles.Label__BottomCenter,
  [LABEL_POSITIONS.BOTTOM_RIGHT]: styles.Label__BottomRight,
  [LABEL_POSITIONS.LEFT]: styles.Label__Left,
  [LABEL_POSITIONS.RIGHT]: styles.Label__Right,
  [LABEL_POSITIONS.TOP]: styles.Label__Top,
  [LABEL_POSITIONS.TOP_CENTER]: styles.Label__TopCenter,
  [LABEL_POSITIONS.TOP_RIGHT]: styles.Label__TopRight
});

type LabelOwnPropsType = {|
  children: string,
  oneline?: string,
  position?: $Values<typeof LABEL_POSITIONS>,
  required?: string
|};

type WrappedLabelType = {|
  ...AsComponentExtensionType,
  ...LabelOwnPropsType
|};

const Label = React.forwardRef(
  (
    { children, className, oneline, position = LABEL_POSITIONS.TOP, required }: WrappedLabelType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <label
      ref={forwardedRef}
      className={cx(styles.Label, LABEL_POSITION_TO_CLASS[position], oneline && styles.Label__Oneline, className)}
    >
      {children}
      {required && <Required className={styles.Required} />}
    </label>
  )
);

export type LabelType = {|
  ...AsComponentType,
  ...LabelOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Label)(Label), {
    POSITIONS: LABEL_POSITIONS
  }): React.ComponentType<LabelType> & {
    POSITIONS: Object
  }
);
