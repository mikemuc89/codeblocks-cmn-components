/* @flow */
import * as React from 'react';
import { CSS_ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { FONT_WEIGHTS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTypography, { type WithTypographyExtensionType, type WithTypographyType } from '../hocs/components/WithTypography';
import styles from './Text.scss';

type WrappedTextType = {|
  ...AsComponentExtensionType,
  ...WithTypographyExtensionType
|};

const Text = React.forwardRef(({ children, className, typographyElementRef }: WrappedTextType, forwardedRef: ReactForwardedRefType) => (
  <span ref={mergeRefs(forwardedRef, typographyElementRef)} className={cx(styles.Text, className)}>
    {children}
  </span>
));

const Helper = React.forwardRef(
  ({ children, className, typographyElementRef }: WrappedTextType, forwardedRef: ReactForwardedRefType) => (
    <span ref={mergeRefs(forwardedRef, typographyElementRef)} className={cx(styles.Helper, className)}>
      {children}
    </span>
  )
);

const Primary = React.forwardRef(
  ({ children, className, typographyElementRef }: WrappedTextType, forwardedRef: ReactForwardedRefType) => (
    <span ref={mergeRefs(forwardedRef, typographyElementRef)} className={cx(styles.Primary, className)}>
      {children}
    </span>
  )
);

const Secondary = React.forwardRef(
  ({ children, className, typographyElementRef }: WrappedTextType, forwardedRef: ReactForwardedRefType) => (
    <span ref={mergeRefs(forwardedRef, typographyElementRef)} className={cx(styles.Secondary, className)}>
      {children}
    </span>
  )
);

const Subtitle = React.forwardRef(
  ({ children, className, typographyElementRef }: WrappedTextType, forwardedRef: ReactForwardedRefType) => (
    <span ref={mergeRefs(forwardedRef, typographyElementRef)} className={cx(styles.Subtitle, className)}>
      {children}
    </span>
  )
);

export type TextType = {|
  ...AsComponentType,
  ...WithTypographyType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Text)(
      WithTypography({ defaultFontSize: 15, defaultFontWeight: FONT_WEIGHTS.S, defaultLineHeight: 24 })(Text)
    ),
    {
      Helper: AsComponent(ComponentNames.Text.Helper)(
        WithTypography({ defaultFontSize: 14, defaultFontWeight: FONT_WEIGHTS.XS, defaultLineHeight: 24 })(Helper)
      ),
      Primary: AsComponent(ComponentNames.Text.Primary)(
        WithTypography({ defaultFontSize: 15, defaultFontWeight: FONT_WEIGHTS.M, defaultLineHeight: 24 })(Primary)
      ),
      Secondary: AsComponent(ComponentNames.Text.Secondary)(
        WithTypography({ defaultFontSize: 12, defaultFontWeight: FONT_WEIGHTS.XS, defaultLineHeight: 24 })(Secondary)
      ),
      Subtitle: AsComponent(ComponentNames.Text.Subtitle)(
        WithTypography({ defaultFontSize: 14, defaultFontWeight: FONT_WEIGHTS.S, defaultLineHeight: 24 })(Subtitle)
      )
    }, {
      ALIGN_TYPES: CSS_ALIGN_TYPES,
      FONT_WEIGHTS
    }
  ): React.ComponentType<TextType> & {
    Helper: React.ComponentType<TextType>,
    Primary: React.ComponentType<TextType>,
    Secondary: React.ComponentType<TextType>,
    Subtitle: React.ComponentType<TextType>
  } & {
    ALIGN_TYPES: Object,
    FONT_WEIGHTS: Object
  }
);
