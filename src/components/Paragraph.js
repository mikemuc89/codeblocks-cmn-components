/* @flow */
import * as React from 'react';
import { CSS_ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { FONT_WEIGHTS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTypography, { type WithTypographyExtensionType } from '../hocs/components/WithTypography';
import styles from './Paragraph.scss';

type ParagraphOwnPropsType = {|
  summary?: boolean
|};

type WrappedParagraphType = {|
  ...AsComponentExtensionType,
  ...WithTypographyExtensionType,
  ...ParagraphOwnPropsType
|};

const Paragraph = React.forwardRef(
  ({ children, className, summary, typographyElementRef }: WrappedParagraphType, forwardedRef: ReactForwardedRefType) => (
    <p
      ref={mergeRefs(forwardedRef, typographyElementRef)}
      className={cx(styles.Paragraph, summary && styles.Paragraph__Summary, className)}
    >
      {children}
    </p>
  )
);

export type ParagraphType = {|
  ...AsComponentType,
  ...WithTypographyType,
  ...ParagraphOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Paragraph)(WithTypography()(Paragraph)), {
    ALIGN_TYPES: CSS_ALIGN_TYPES,
    FONT_WEIGHTS
  }): React.ComponentType<ParagraphType> & {
    ALIGN_TYPES: Object,
    FONT_WEIGHTS: Object
  }
);
