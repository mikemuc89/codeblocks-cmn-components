/* #__flow */
import * as React from 'react';
import { cx, repeatPattern } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Dots.scss';

export type DotType = {|
  backgroundColor?: string,
  className?: string,
  filled?: boolean  
|};

const Dot = React.forwardRef(
  ({ backgroundColor = '#aaaaaa', className, filled }: DotType, forwardedRef: ReactForwardedRefType) => (
    <span
      ref={forwardedRef}
      className={cx(styles.Dot, className)}
      style={{ ...(backgroundColor ? { ...(filled ? { backgroundColor } : {}), borderColor: backgroundColor } : {}) }}
    />
  )
);

type DotsOwnPropsType = {|
  backgroundColor?: string,
  max?: number,
  showEmpty?: boolean,
  value: number  
|};

type WrappedDotsType = {|
  ...AsComponentExtensionType,
  ...DotsOwnPropsType
|};

const Dots = React.forwardRef(
  ({ backgroundColor = '#aaaaaa', className, max = 10, showEmpty, value }: WrappedDotsType, forwardedRef: ReactForwardedRefType) => (
    <div ref={forwardedRef} className={cx(styles.Dots, className)}>
      {repeatPattern(Math.min(value, max), (index: number) => (
        <Dot key={`filled.${index}`} backgroundColor={backgroundColor} filled />
      ))}
      {showEmpty &&
        repeatPattern(Math.max(max - value, 0), (index: number) => (
          <Dot key={`empty.${index}`} backgroundColor={backgroundColor} filled={false} />
        ))}
    </div>
  )
);

export type DotsType = {|
  ...AsComponentType,
  ...DotsOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Dots)(Dots), {}): React.ComponentType<DotsType>
);
