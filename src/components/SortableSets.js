/* @flow */
import * as React from 'react';
import Sortable, { MultiDrag, Swap } from 'sortablejs';
import { type ReactChildren, type ReactElementsChildren, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType } from '../hocs/components/AsComponent';
import Message from './Message';
import Required from './Required';
import styles from './SortableSets.scss';

type OnChangeParamsType = {|
  fromGroup: string,
  fromIndex: number,
  toGroup: string,
  toIndex: number
|};
type UseSortableType = {|
  disabled?: boolean,
  delay?: number,
  handle?: boolean,
  multiDrag?: boolean,
  name?: string,
  pull?: boolean,
  put?: boolean,
  swap?: boolean,
  onChange?: (params: OnChangeParamsType) => void
|};

const useSortable = ({
  disabled,
  delay = 500,
  handle = false,
  multiDrag,
  name,
  pull = true,
  put = true,
  swap,
  onChange
}: UseSortableType) => {
  const sortableRoot = React.useRef(null);
  const [sortable, setSortable] = React.useState(null);
  const handleChange = React.useCallback(
    (e: Event) => {
      if (onChange) {
        const { from, to, oldIndex: fromIndex, newIndex: toIndex } = e;
        onChange({ fromGroup: from.dataset.id, fromIndex, toGroup: to.dataset.id, toIndex });
      }
    },
    [onChange]
  );

  const plugins = [multiDrag && new MultiDrag(), swap && new Swap()].filter(Boolean);
  if (plugins.length) {
    Sortable.mount(...plugins);
  }

  React.useEffect(() => {
    if (!sortable && sortableRoot.current) {
      setSortable(
        new Sortable(sortableRoot.current, {
          animation: 150,
          delay,
          delayOnTouchOnly: true,
          disabled,
          draggable: `.${styles.ItemWrapper}`,
          filter: `.${styles.ItemWrapper__Disabled}`,
          ghostClass: styles.ItemWrapper__Ghost,
          group: {
            name,
            pull,
            put
          },
          onEnd: handleChange,
          selectedClass: styles.ItemWrapper__Selected,
          ...(handle ? { handle: `.${styles.Handle}` } : {}),
          ...(multiDrag ? { fallbackTolerance: 3, multiDrag } : {}),
          ...(swap ? { swap, swapClass: styles.ItemWrapper__Swap } : {})
        })
      );
    }
  }, [delay, disabled, handle, multiDrag, sortable, swap, name, pull, put, handleChange]);

  return { sortable, sortableRoot };
};

type SortableSetsContextType = {|
  delay?: boolean,
  disabled?: boolean,
  handle?: boolean,
  multiDrag?: boolean,
  onChange?: (params: OnChangeParamsType) => void,
  swap?: boolean
|};

const SortableSetsContext = React.createContext({
  delay: undefined,
  disabled: false,
  handle: false,
  multiDrag: false,
  onChange: undefined,
  swap: false
});

const SortableSetsGroupContext = React.createContext({
  disabled: false
});

type ItemOwnPropsType = {|
  children: ReactChildren,
  disabled?: boolean,
  id: string  
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(({ children, className, disabled, id }: WrappedItemType, forwardedRef: ReactForwardedRefType) => {
  const { disabled: rootDisabled, handle } = React.useContext(SortableSetsContext);
  const { disabled: groupDisabled } = React.useContext(SortableSetsGroupContext);

  return (
    <div
      ref={forwardedRef}
      className={cx(
        styles.ItemWrapper,
        (disabled || groupDisabled || rootDisabled) && styles.ItemWrapper__Disabled,
        handle && styles.ItemWrapper__WithHandle,
        className
      )}
    >
      <div className={styles.Item}>
        {handle && <span className={styles.Handle}>&gt;</span>}
        {children}
      </div>
    </div>
  );
});

export type ItemType = {|
  ...AsComponentType,
  ...ItemOwnPropsType
|};

type GroupOwnPropsType = {|
  children: ReactElementsChildren<ItemType>,
  disabled?: boolean,
  pull?: boolean,
  put?: boolean,
  sort?: boolean,
  title?: string,
  id: string
|};

type WrappedGroupType = {|
  ...AsComponentExtensionType,
  ...GroupOwnPropsType
|};

const Group = React.forwardRef(
  (
    { children, className, disabled, pull = true, put = true, sort = true, title, id }: WrappedGroupType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const {
      delay,
      disabled: sortableDisabled,
      handle,
      multiDrag,
      name,
      onChange,
      swap
    } = React.useContext(SortableSetsContext);
    const { sortableRoot } = useSortable({
      delay,
      disabled: sortableDisabled || disabled,
      handle,
      multiDrag,
      name,
      onChange,
      pull,
      put,
      swap
    });

    const contextValue = React.useMemo(() => ({ disabled }), [disabled]);

    return (
      <div ref={forwardedRef} className={cx(styles.GroupWrapper, className)}>
        <div className={styles.Title}>{title}</div>
        <SortableSetsGroupContext.Provider value={contextValue}>
          <div ref={sortableRoot} data-id={id} className={styles.Group}>
            {children}
          </div>
        </SortableSetsGroupContext.Provider>
      </div>
    );
  }
);

export type GroupType = {|
  ...AsComponentType,
  ...GroupOwnPropsType
|};

type SortableSetsOwnPropsType = {|
  ...WithMessagesType,
  children: ReactElementsChildren<GroupType>,
  delay?: number,
  disabled?: boolean,
  handle?: boolean,
  multiDrag?: boolean,
  name?: string,
  swap?: boolean,
  onBlur?: (e: FocusEvent) => void,
  onChange?: (params: OnChangeParamsType) => void,
  onFocus?: (e: FocusEvent) => void
|};

type WrappedSortableSetsType = {|
  ...AsComponentExtensionType,
  ...SortableSetsOwnPropsType
|};

const SortableSets = React.forwardRef(
  (
    {
      children,
      delay,
      disabled,
      errors,
      handle,
      helper,
      hints,
      multiDrag,
      name,
      swap,
      onBlur,
      onChange,
      onFocus,
      required,
      warnings
    }: WrappedSortableSetsType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const contextValue = React.useMemo(
      () => ({
        delay,
        disabled,
        handle,
        multiDrag,
        name,
        onChange,
        swap
      }),
      [delay, disabled, handle, multiDrag, name, onChange, swap]
    );

    return (
      <div className={styles.Wrapper}>
        <div className={styles.SortableSets}>
          <SortableSetsContext.Provider value={contextValue}>{children}</SortableSetsContext.Provider>
        </div>
        {required && <Required className={styles.Required} />}
        <Message errors={errors} helper={helper} hints={hints} warnings={warnings} />
      </div>
    );
  }
);

export type SortableSetsType = {|
  ...AsComponentType,
  ...SortableSetsOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.SortableSets)(SortableSets), {
    Group: AsComponent(ComponentNames.SortableSets.Group)(Group),
    Item: AsComponent(ComponentNames.SortableSets.Item)(Item)
  }): React.ComponentType<SortableSetsType> & {
    Group: React.ComponentType<GroupType>,
    Item: React.ComponentType<ItemType>
  }
);
