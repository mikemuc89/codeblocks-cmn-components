/* #__flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import {
  KINDS,
  POSITIONS_CORNERS as POSITIONS,
  POSITIONS_VERTICAL as TAB_POSITIONS,
  SHAPES
} from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild, mergeRefs, stopPropagationCallback, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import DialogContext from '../contexts/DialogContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import {
  type BadgePropType,
  type FooterPropType,
  type HeaderPropType,
  type ImagePropType,
  type MenuPropType,
  type TabsPropType
} from '../types';
import CONFIG from '../components/style-config';
import styles from './Dialog.scss';

const {
  colors: {
    background: BACKGROUND_COLORS
  }
} = CONFIG;

const DEFAULT_BADGE_SIZE = 64;
const DEFAULT_BADGE_INNER_SIZE = 40;
const DEFAULT_FOOTER_SIZE = 48;
const DEFAULT_HEADER_SIZE = 48;
const DEFAULT_IMAGE_SIZE = 240;

type DialogOwnPropsType = {|
  addition?: string,
  badge?: BadgePropType,
  canClose?: boolean,
  children: ReactChildren,
  footer?: FooterPropType,
  header?: HeaderPropType,
  height?: number,
  image?: ImagePropType,
  menu?: MenuPropType,
  mini?: boolean,
  nano?: boolean,
  onClose?: () => void,
  tabs?: TabsPropType  
|};

type WrappedDialogType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...DialogOwnPropsType
|};

const Dialog = React.forwardRef(
  (
    {
      addition,
      backgroundElementRef,
      badge,
      canClose = true,
      children,
      className,
      footer,
      header,
      height,
      image,
      menu,
      mini,
      nano,
      onClose,
      tabs
    }: WrappedDialogType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Dialog,
        canClose && styles.Dialog__CanClose,
        mini && styles.Dialog__Mini,
        nano && styles.Dialog__Nano,
        className
      )}
      onClick={canClose ? onClose : undefined}
    >
      <DialogContext.Provider value={React.useMemo(() => ({ isDialog: true }), [])}>
        <span tabIndex={0} onClick={onClose} className={styles.DimmerClose} />
        <div ref={backgroundElementRef} className={styles.Content} onClick={stopPropagationCallback}>
          <span tabIndex={0} onClick={onClose} className={styles.Close} />
          {badge &&
            React.cloneElement(badge, {
              className: styles.Badge,
              focusable: false,
              hoverable: false,
              kind: badge.props.kind || KINDS.PRIMARY,
              shape: badge.props.shape || SHAPES.CIRCLE,
              size: badge.props.size || DEFAULT_BADGE_SIZE,
              ...{
                [ComponentNames.CharIcon]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                },
                [ComponentNames.ColorIcon]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                },
                [ComponentNames.FontAwesome]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                },
                [ComponentNames.FontAwesome.Brand]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                },
                [ComponentNames.FontAwesome.Regular]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                }
              }[badge.type.componentId]
            })}
          {menu &&
            React.cloneElement(menu, {
              className: styles.Menu,
              ...{
                [ComponentNames.Dropdown]: {
                  accent: menu.props.accent || false,
                  border: menu.props.border || false
                },
                [ComponentNames.Infotip]: {
                  accent: menu.props.accent || false,
                  border: menu.props.border || false
                },
                [ComponentNames.Fab]: {
                  position: menu.props.position || POSITIONS.TOP_RIGHT
                }
              }[menu.type.componentId]
            })}
          {image &&
            React.cloneElement(image, {
              className: styles.Image,
              height: image.props.height || DEFAULT_IMAGE_SIZE
            })}
          {header &&
            React.cloneElement(header, {
              className: styles.Header,
              height: header.props.height || DEFAULT_HEADER_SIZE,
              stretch: true
            })}
          {tabs &&
            React.cloneElement(tabs, {
              border: false,
              className: styles.Tabs,
              position: TAB_POSITIONS.TOP
            })}
          <div
            style={{
              ...(badge && !image && !header && !tabs
                ? { paddingTop: unitize((badge.props.size || DEFAULT_BADGE_SIZE) / 2 + 8) }
                : {})
            }}
            className={styles.Inner}
          >
            {children &&
              React.Children.map(children, (child: React.Node) =>
                isBasicReactChild(child) ? (
                  <div className={styles.Text}>{child}</div>
                ) : (
                  React.cloneElement(child, {
                    className: {
                      [ComponentNames.Banner]: styles.Banner,
                      [ComponentNames.Form]: styles.Form,
                      [ComponentNames.Heading.H1]: styles.Heading,
                      [ComponentNames.Heading.H2]: styles.Heading,
                      [ComponentNames.Heading.H3]: styles.Heading,
                      [ComponentNames.Heading.H4]: styles.Heading,
                      [ComponentNames.Heading.H5]: styles.Heading,
                      [ComponentNames.Paragraph]: styles.Text,
                      [ComponentNames.Markdown]: styles.Text,
                      [ComponentNames.Summary]: styles.Summary,
                      [ComponentNames.Text]: styles.Text
                    }[child.type.componentId]
                  })
                )
              )}
          </div>
          {footer &&
            React.cloneElement(footer, {
              className: styles.Footer,
              height: footer.props.height || DEFAULT_FOOTER_SIZE,
              stretch: true

            })}
          {addition && <div className={styles.Addition}>{addition}</div>}
        </div>
      </DialogContext.Provider>
    </div>
  )
);

const DialogLoading = React.forwardRef(({ children, className, ...props }: WrappedDialogType, forwardedRef: ReactForwardedRefType) => (
  <Dialog ref={forwardedRef} className={cx(styles.Dialog__Loading, className)} canClose={false} {...props}>
    {children}
  </Dialog>
));

export type DialogType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...DialogOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Dialog)(WithBackground({
    defaultBackgroundColor: BACKGROUND_COLORS.dialog.normal,
    defaultBackgroundColorFocus: BACKGROUND_COLORS.dialog.focus,
    defaultBackgroundColorHover: BACKGROUND_COLORS.dialog.hover
  })(Dialog)), {
    Loading: Object.assign(AsComponent(ComponentNames.Dialog.Loading)(DialogLoading), {})
  }): React.ComponentType<DialogType> & {
    Loading: React.ComponentType<DialogType>
  }
);
