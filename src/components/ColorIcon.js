/* #__flow */
import * as React from 'react';
import { SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import WithShape, { type WithShapeExtensionType, type WithShapeType } from '../hocs/components/WithShape';
import styles from './ColorIcon.scss';

const DEFAULT_SIZE = 40;
const DEFAULT_ICON_SIZE_RATIO = 0.65;

type ColorIconOwnPropsType = {|
  border?: boolean,
  color: string,
  label?: string,
  sample?: string,
  size?: number,
  innerSize?: number
|};

type WrappedColorIconType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...WithShapeExtensionType,
  ...WithThemeExtensionType,
  ...ColorIconOwnPropsType
|};

const ColorIcon = React.forwardRef(
  (
    {
      LinkComponent,
      border,
      className,
      color,
      label,
      linkProps,
      sample,
      shapeElementRef,
      size = DEFAULT_SIZE,
      innerSize = size * DEFAULT_ICON_SIZE_RATIO
    }: WrappedColorIconType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <LinkComponent
      ref={forwardedRef}
      className={cx(styles.Wrapper, border && styles.Wrapper__Border, className)}
      title={label}
      {...linkProps}
      style={{
        height: unitize(size),
        width: unitize(size),
        ...linkProps.style
      }}
    >
      <span
        ref={shapeElementRef}
        className={styles.ColorIcon}
        style={{ backgroundColor: color, height: unitize(innerSize), width: unitize(innerSize) }}
      >
        {sample}
      </span>
    </LinkComponent>
  )
);

export type ColorIconType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...WithShapeType,
  ...WithThemeType,
  ...ColorIconOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.ColorIcon)(WithLink()(WithShape({ defaultShape: SHAPES.CIRCLE })(ColorIcon))),
    {
      SHAPES
    }
  ): React.ComponentType<ColorIconType> & {
    SHAPES: Object
  }
);
