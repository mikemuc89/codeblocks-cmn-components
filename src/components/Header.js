/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import CONFIG from '../components/style-config';
import styles from './Header.scss';

const {
  colors: {
    background: BACKGROUND_COLORS
  }
} = CONFIG;

type HeaderOwnPropsType = {|
  align: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactChildren,
  height?: number,
  stretch?: boolean
|};

type WrappedHeaderType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...HeaderOwnPropsType
|};

const Header = React.forwardRef(
  (
    { align = ALIGN_TYPES.LEFT, backgroundElementRef, border, children, className, height, stretch }: WrappedHeaderType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { innerWidth } = React.useContext(MainSizeContext);

    return (
      <header
        ref={mergeRefs(forwardedRef, backgroundElementRef)}
        className={cx(
          styles.Header,
          cx.alignSingle(styles, 'Header', { align }),
          border && styles.Header__Border,
          stretch && styles.Header__Stretch,
          className
        )}
        style={{
          ...(height ? { [stretch ? 'minHeight' : 'height']: unitize(height) } : {})
        }}
      >
        <div className={styles.Content} style={{ width: unitize(innerWidth) }}>
          {children}
        </div>
      </header>
    );
  }
);

export type HeaderType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...HeaderOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Header)(WithBackground({
    defaultBackgroundColor: BACKGROUND_COLORS.header.normal,
    defaultBackgroundColorFocus: BACKGROUND_COLORS.header.focus,
    defaultBackgroundColorHover: BACKGROUND_COLORS.header.hover
  })(Header)), {
    ALIGN_TYPES
  }): React.ComponentType<HeaderType> & {
    ALIGN_TYPES: Object
  }
);
