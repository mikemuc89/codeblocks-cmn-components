/* #__flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import Agenda from './Agenda';
import Code from './Code';
import Heading from './Heading';
import Image from './Image';
import Paragraph from './Paragraph';
import SideNote from './SideNote';
import styles from './Article.scss';

type ElementType = {|
  kind: string,
  params?: Object
|};

type ArticleOwnPropsType = {|
  data?: Array<ElementType>
|};

type WrappedArticleType = {|
  ...AsComponentExtensionType,
  ...ArticleOwnPropsType
|};

const Article = React.forwardRef(({ className, data = [] }: WrappedArticleType, forwardedRef: ReactForwardedRefType) => (
  <div ref={forwardedRef} className={cx(styles.Article, className)}>
    {data.map(
      ({ kind, params: { text, ...params } = {} }: ElementType, idx: number) =>
        ({
          code: (
            <Code key={idx} {...params}>
              {text}
            </Code>
          ),
          image: <Image key={idx} {...params} />,
          info: <SideNote.Info key={idx}>{text}</SideNote.Info>,
          link: (
            <SideNote.Link key={idx} {...params}>
              {text}
            </SideNote.Link>
          ),
          paragraph: <Paragraph key={idx}>{text}</Paragraph>,
          quote: (
            <SideNote.Quote key={idx} {...params}>
              {text}
            </SideNote.Quote>
          ),
          sect1: (
            <Heading.H1 key={idx} {...params}>
              {text}
            </Heading.H1>
          ),
          sect2: (
            <Heading.H2 key={idx} {...params}>
              {text}
            </Heading.H2>
          ),
          sect3: (
            <Heading.H3 key={idx} {...params}>
              {text}
            </Heading.H3>
          ),
          sect4: (
            <Heading.H4 key={idx} {...params}>
              {text}
            </Heading.H4>
          ),
          sect5: (
            <Heading.H5 key={idx} {...params}>
              {text}
            </Heading.H5>
          ),
          summary: (
            <Paragraph key={idx} summary>
              {text}
            </Paragraph>
          ),
          toc: <Agenda key={idx} {...params} />, // todo generate children from params
          warning: (
            <SideNote.Warning key={idx} {...params}>
              {text}
            </SideNote.Warning>
          ),
          well: (
            <SideNote.Well key={idx} {...params}>
              {text}
            </SideNote.Well>
          )
        }[kind.toLowerCase()])
    )}
  </div>
));

export type ArticleType = {|
  ...AsComponentType,
  ...ArticleOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Article)(Article), {}): React.ComponentType<ArticleType>
);
