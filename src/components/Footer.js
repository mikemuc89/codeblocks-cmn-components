/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import CONFIG from '../components/style-config';
import styles from './Footer.scss';

const {
  colors: {
    background: BACKGROUND_COLORS
  }
} = CONFIG;

type FooterOwnPropsType = {|
  align: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactChildren,
  height?: number,
  stretch?: boolean
|};

type WrappedFooterType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...FooterOwnPropsType
|};

const Footer = React.forwardRef(
  (
    { align = ALIGN_TYPES.LEFT, backgroundElementRef, border, children, className, height, stretch }: WrappedFooterType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { innerWidth } = React.useContext(MainSizeContext);

    return (
      <footer
        ref={mergeRefs(forwardedRef, backgroundElementRef)}
        className={cx(
          styles.Footer,
          cx.alignSingle(styles, 'Footer', { align }),
          border && styles.Footer__Border,
          stretch && styles.Footer__Stretch,
          className
        )}
        style={{
          ...(height ? { [stretch ? 'minHeight' : 'height']: unitize(height) } : {})
        }}
      >
        <div className={styles.Content} style={{ width: unitize(innerWidth) }}>
          {children}
        </div>
      </footer>
    );
  }
);

export type FooterType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...FooterOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Footer)(WithBackground({
    defaultBackgroundColor: BACKGROUND_COLORS.footer.normal,
    defaultBackgroundColorFocus: BACKGROUND_COLORS.footer.focus,
    defaultBackgroundColorHover: BACKGROUND_COLORS.footer.hover
  })(Footer)), {
    ALIGN_TYPES
  }): React.ComponentType<FooterType> & {
    ALIGN_TYPES: Object
  }
);
