/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Section.scss';

type SectionOwnPropsType = {|
  border?: boolean,
  children: ReactChildren,
  title?: string  
|};

type WrappedSectionType = {|
  ...AsComponentExtensionType,
  ...SectionOwnPropsType
|};

const Section = React.forwardRef(({ border, children, className, title }: WrappedSectionType, forwardedRef: ReactForwardedRefType) => (
  <section ref={forwardedRef} className={cx(styles.Section, border && styles.Section__Border, className)}>
    {title && <div className={styles.Title}>{title}</div>}
    <div className={styles.Content}>{children}</div>
  </section>
));

export type SectionType = {|
  ...AsComponentType,
  ...SectionOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Section)(Section), {}): React.ComponentType<SectionType>
);
