/* @flow */
import * as React from 'react';
import { cx, md, mergeRefs, MessageFormat } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import WithTypography, { type WithTypographyExtensionType, type WithTypographyType } from '../hocs/components/WithTypography';
import styles from './Markdown.scss';

type WrappedMarkdownType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...WithTypographyExtensionType
|};

const Markdown = React.forwardRef(
  ({ LinkComponent, children, className, linkProps, typographyElementRef, ...props }: WrappedMarkdownType, forwardedRef: ReactForwardedRefType) => (
    <LinkComponent
      ref={mergeRefs(forwardedRef, typographyElementRef)}
      className={cx(styles.Markdown, className)}
      dangerouslySetInnerHTML={{
        __html: md.render(new MessageFormat(children ? children.replace(/(\n)\s+\| /g, '$1') : '').format(props))
      }}
      {...linkProps}
    />
  )
);

export type MarkdownType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...WithTypographyType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Markdown)(WithLink()(WithTypography()(Markdown))), {}): React.ComponentType<MarkdownType>
);
