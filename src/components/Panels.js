/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import { type IconPropType } from '../types';
import FontAwesome from './FontAwesome';
import styles from './Panels.scss';

const DEFAULT_ICON_SIZE = 40;
const DEFAULT_ICON_INNER_SIZE = 14;

type PanelOwnPropsType = {|
  children: ReactChildren,
  icon: IconPropType,
  onToggle: (e: MouseEvent) => void,
  open?: boolean,
  title: string  
|};

type WrappedPanelType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...PanelOwnPropsType
|};

const Panel = React.forwardRef(
  ({ children, className, icon, onToggle, open, themeElementRef, title }: WrappedPanelType, forwardedRef: ReactForwardedRefType) => {
    const iconElement =
      icon &&
      React.cloneElement(icon, {
        className: styles.Icon,
        ...{
          [ComponentNames.CharIcon]: {
            innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
            size: icon.props.size || DEFAULT_ICON_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
            size: icon.props.size || DEFAULT_ICON_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
            size: icon.props.size || DEFAULT_ICON_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
            size: icon.props.size || DEFAULT_ICON_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
            size: icon.props.size || DEFAULT_ICON_SIZE
          },
          [ComponentNames.Icon]: {
            size: icon.props.size || DEFAULT_ICON_SIZE
          }
        }[icon.type.componentId]
      });

    return (
      <div ref={forwardedRef} className={cx(styles.Panel, cx.open(styles, 'Panel', { open }), className)}>
        <button ref={themeElementRef} type="button" className={styles.Title} onClick={onToggle}>
          {iconElement}
          <span className={styles.Text}>{title}</span>
          <FontAwesome
            className={styles.Icon}
            size={DEFAULT_ICON_SIZE}
            innerSize={DEFAULT_ICON_INNER_SIZE}
            id={open ? 'chevron-up' : 'chevron-down'}
          />
        </button>
        <div className={styles.Content}>{children}</div>
      </div>
    );
  }
);

export type PanelType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...PanelOwnPropsType
|};

type PanelsOwnPropsType = {|
  children: ReactElementsChildren<PanelsType>,
  kind?: $Values<typeof KINDS>,
  multi?: boolean  
|};

type WrappedPanelsType = {|
  ...AsComponentExtensionType,
  ...PanelsOwnPropsType
|};

const Panels = React.forwardRef(({ children, className, kind, multi }: WrappedPanelsType, forwardedRef: ReactForwardedRefType) => {
  const [openIndexes, setOpenIndexes] = React.useState<Array<number>>(
    React.Children.toArray(children).filter((child: React.Element<PanelType>) => Boolean(child.props.open))
  );

  const onToggle = (idx: number) => {
    if (openIndexes.includes(idx)) {
      setOpenIndexes((oldState: Array<number>) => oldState.filter((item: number) => item !== idx));
    } else {
      setOpenIndexes((oldState: Array<number>) => (multi ? [...oldState, idx] : [idx]));
    }
  };

  return (
    <div ref={forwardedRef} className={cx(styles.Panels, className)}>
      {React.Children.map(children, (child: React.Element<PanelType>, index: number) =>
        React.cloneElement(child, {
          kind,
          onToggle: () => onToggle(index),
          open: openIndexes.includes(index)
        })
      )}
    </div>
  );
});

export type PanelsType = {|
  ...AsComponentType,
  ...PanelsOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Panels)(Panels), {
    Panel: AsComponent(ComponentNames.Panels.Panel)(WithTheme({ defaultKind: KINDS.PRIMARY })(Panel))
  }, {
    KINDS
  }): React.ComponentType<PanelsType> & {
    Panel: React.ComponentType<PanelType>
  } & {
    KINDS: Object
  }
);
