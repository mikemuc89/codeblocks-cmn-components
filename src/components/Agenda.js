/* #__flow */
import * as React from 'react';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, scrollTo } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Agenda.scss';

type ItemOwnPropsType = {|
  children?: ReactElementsChildren<any>,
  id?: string,
  title: string
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(({ children, className, id, title }: WrappedItemType, forwardedRef: ReactForwardedRefType) => {
  const handleOnClick = React.useCallback(
    (e: MouseEvent) => {
      e.stopPropagation();
      scrollTo(id);
    },
    [id]
  );

  return (
    <div ref={forwardedRef} className={cx(styles.Item, className)}>
      <a className={styles.Title} id={`TOC:${id}`} onClick={handleOnClick}>
        {title}
      </a>
      <div className={styles.SubItems}>{children}</div>
    </div>
  );
});

export type ItemType = {|
  ...AsComponentType,
  ...ItemOwnPropsType
|};

type AgendaOwnPropsType = {|
  children: ReactElementsChildren<ItemType>
|};

type WrappedAgendaType = {|
  ...AsComponentExtensionType,
  ...AgendaOwnPropsType
|};

const Agenda = React.forwardRef(({ children, className }: WrappedAgendaType, forwardedRef: ReactForwardedRefType) => (
  <div ref={forwardedRef} className={cx(styles.Agenda, className)}>
    {children}
  </div>
));

export type AgendaType = {|
  ...AsComponentType,
  ...AgendaOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Agenda)(Agenda), {
    Item: AsComponent(ComponentNames.Agenda.Item)(Item)
  }): React.ComponentType<AgendaType> & {
    Item: React.ComponentType<ItemType>
  }
);
