/* @flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Required.scss';

type RequiredOwnPropsType = {|
  children?: string
|};

type WrappedRequiredType = {|
  ...AsComponentExtensionType,
  ...RequiredOwnPropsType
|};

const Required = React.forwardRef(({ children = '*', className }: WrappedRequiredType, forwardedRef: ReactForwardedRefType) => (
  <span ref={forwardedRef} className={cx(styles.Required, className)}>
    {children}
  </span>
));

export type RequiredType = {|
  ...AsComponentType,
  ...RequiredOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Required)(Required), {}): React.ComponentType<RequiredType>
);
