/* #__flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import {
  KINDS,
  POSITIONS_CORNERS as POSITIONS,
  POSITIONS_VERTICAL as TAB_POSITIONS,
  SHAPES
} from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, processChildren, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import { type BadgePropType, type ImagePropType, type MenuPropType, type TabsPropType } from '../types';
import CONFIG from '../components/style-config';
import styles from './Card.scss';

const {
  colors: {
    background: BACKGROUND_COLORS
  }
} = CONFIG;

const DEFAULT_BADGE_SIZE = 80;
const DEFAULT_BADGE_INNER_SIZE = 40;
const DEFAULT_IMAGE_SIZE = 320;

type CardOwnPropsType = {|
  addition?: string,
  badge?: BadgePropType,
  children: ReactChildren,
  image?: ImagePropType,
  menu?: MenuPropType,
  postScriptum?: string,
  stretch?: boolean,
  tabs?: TabsPropType  
|};

type WrappedCardType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...CardOwnPropsType
|};

const Card = React.forwardRef(
  ({ addition, backgroundElementRef, badge, children, className, image, menu, postScriptum, stretch, tabs }: WrappedCardType, forwardedRef: ReactForwardedRefType) => {
    const tabsOutside = tabs && tabs.props.border;
    const tabsElement =
      tabs &&
      React.cloneElement(tabs, {
        className: cx(
          styles.Tabs,
          {
            [TAB_POSITIONS.BOTTOM]: styles.Tabs__Bottom,
            [TAB_POSITIONS.TOP]: styles.Tabs__Top
          }[tabs.props.position || TAB_POSITIONS.TOP],
          tabsOutside ? styles.Tabs__Outside : styles.Tabs__Inside
        )
      });

    return (
      <div
        ref={forwardedRef}
        style={{ ...(badge ? { marginTop: unitize((badge.props.size || DEFAULT_BADGE_SIZE) * 0.5) } : {}) }}
        className={cx(styles.Card, menu && styles.Card__WithMenu, stretch && styles.Card__Stretch, className)}
      >
        {tabsOutside && tabsElement}
        <div ref={backgroundElementRef} className={styles.Content}>
          {badge &&
            React.cloneElement(badge, {
              absolute: true,
              className: styles.Badge,
              focusable: false,
              hoverable: false,
              kind: badge.props.kind || KINDS.PRIMARY,
              shape: badge.props.shape || SHAPES.CIRCLE,
              size: badge.props.size || DEFAULT_BADGE_SIZE,
              ...{
                [ComponentNames.CharIcon]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                },
                [ComponentNames.ColorIcon]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                },
                [ComponentNames.FontAwesome]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                },
                [ComponentNames.FontAwesome.Brand]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                },
                [ComponentNames.FontAwesome.Regular]: {
                  innerSize: badge.props.innerSize || DEFAULT_BADGE_INNER_SIZE
                }
              }[badge.type.componentId]
            })}
          {menu &&
            React.cloneElement(menu, {
              className: styles.Menu,
              ...{
                [ComponentNames.Dropdown]: {
                  accent: menu.props.accent || false,
                  border: menu.props.border || false
                },
                [ComponentNames.Infotip]: {
                  accent: menu.props.accent || false,
                  border: menu.props.border || false
                },
                [ComponentNames.Fab]: {
                  position: menu.props.position || POSITIONS.TOP_RIGHT
                }
              }[menu.type.componentId]
            })}
          {image &&
            React.cloneElement(image, {
              className: styles.Image,
              height: image.props.height || DEFAULT_IMAGE_SIZE
            })}
          {!tabsOutside && tabsElement}
          <div
            style={{
              ...(badge && !image ? { marginTop: unitize((badge.props.size || DEFAULT_BADGE_SIZE) * 0.5) } : {})
            }}
            className={styles.Inner}
          >
            {processChildren(
              children,
              (child, type) => ({
                className: cx(child.props.className, {
                  [ComponentNames.Banner]: styles.Banner,
                  [ComponentNames.Button.Box]: styles.ButtonBox,
                  [ComponentNames.Heading.H1]: styles.Heading,
                  [ComponentNames.Heading.H2]: styles.Heading,
                  [ComponentNames.Heading.H3]: styles.Heading,
                  [ComponentNames.Heading.H4]: styles.Heading,
                  [ComponentNames.Heading.H5]: styles.Heading,
                  [ComponentNames.Markdown]: styles.Text,
                  [ComponentNames.Text]: styles.Text
                }[type]),
                ...({
                  [ComponentNames.Button.Box]: {
                    bottom: child.props.bottom === undefined ? (addition ? 0 : 8) : child.props.bottom
                  }
                }[type])
              }),
              ({ children }) => <div className={styles.Text}>{children}</div>
            )}
          </div>
          {addition && <div className={styles.Addition}>{addition}</div>}
        </div>
        {postScriptum && <div className={styles.PostScriptum}>{postScriptum}</div>}
      </div>
    );
  }
);

export type CardType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...CardOwnPropsType
|};

type SetOwnPropsType = {|
  children: ReactElementsChildren<CardType>,
  columns?: number  
|};

type WrappedSetType = {|
  ...AsComponentExtensionType,
  ...SetOwnPropsType
|};

const Set = React.forwardRef(({ children, className, columns = 4 }: WrappedSetType, forwardedRef: ReactForwardedRefType) => (
  <div ref={forwardedRef} className={cx(styles.Set, styles[`Set__Columns_${columns}`], className)}>
    {children}
  </div>
));

export type SetType = {|
  ...AsComponentType,
  ...SetOwnPropsType
|};

const Loading = React.forwardRef(({ children, className, ...props }: WrappedCardType, forwardedRef: ReactForwardedRefType) => (
  <Card ref={forwardedRef} className={cx(styles.Loading)} {...props}>{children}</Card>
));

export default (
  Object.assign(AsComponent(ComponentNames.Card)(WithBackground({
    defaultBackgroundColor: BACKGROUND_COLORS.card.normal,
    defaultBackgroundColorFocus: BACKGROUND_COLORS.card.focus,
    defaultBackgroundColorHover: BACKGROUND_COLORS.card.hover
  })(Card)), {
    Set: AsComponent(ComponentNames.Card.Set)(Set)
  }, {
    Loading: AsComponent(ComponentNames.Card.Loading)(Loading)
  }): React.ComponentType<CardType> & {
    Set: React.ComponentType<SetType>
  } & {
    Loading: React.ComponentType<CardType>
  }
);
