/* @flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import Markdown from './Markdown';
import styles from './SideNote.scss';

type SideNoteChildrenType = string | Array<string>;

const processChildren = (children: SideNoteChildrenType) =>
  Array.isArray(children) ? children.join('\n') : children;

type InfoOwnPropsType = {|
  children: SideNoteChildrenType
|};

type WrappedInfoType = {|
  ...AsComponentExtensionType,
  ...InfoOwnPropsType
|};

const Info = React.forwardRef(({ children, className }: WrappedInfoType, forwardedRef: ReactForwardedRefType) => (
  <Markdown ref={forwardedRef} className={cx(styles.Info, className)}>
    {processChildren(children)}
  </Markdown>
));

type LinkOwnPropsType = {|
  children: SideNoteChildrenType,
  external?: boolean,
  title?: string,
  url: string
|};

type WrappedLinkType = {|
  ...AsComponentExtensionType,
  ...LinkOwnPropsType
|};

const Link = React.forwardRef(
  ({ children, className, external = true, title, url }: WrappedLinkType, forwardedRef: ReactForwardedRefType) => (
    <div ref={forwardedRef} className={cx(styles.Link, className)}>
      <a className={styles.Anchor} href={url} rel="noreferrer" target={external ? '_blank' : '_self'}>
        {title || url}
      </a>
      {children && <Markdown className={styles.Text}>{processChildren(children)}</Markdown>}
    </div>
  )
);

type QuoteOwnPropsType = {|
  author?: string,
  children: SideNoteChildrenType,
  source?: string
|};

type WrappedQuoteType = {|
  ...AsComponentExtensionType,
  ...QuoteOwnPropsType
|};

const Quote = React.forwardRef(({ author, children, className, source }: WrappedQuoteType, forwardedRef: ReactForwardedRefType) => (
  <div ref={forwardedRef} className={cx(styles.Quote, className)}>
    <Markdown className={styles.Text}>{processChildren(children)}</Markdown>
    <div className={styles.AuthorSource}>
      {author && <span className={styles.Author}>{author}</span>}
      {source && <span className={styles.Source}>{source}</span>}
    </div>
  </div>
));

type WarningOwnPropsType = {|
  children: SideNoteChildrenType
|};

type WrappedWarningType = {|
  ...AsComponentExtensionType,
  ...WarningOwnPropsType
|};

const Warning = React.forwardRef(({ children, className }: WrappedWarningType, forwardedRef: ReactForwardedRefType) => (
  <Markdown ref={forwardedRef} className={cx(styles.Warning, className)}>
    {processChildren(children)}
  </Markdown>
));

type WellOwnPropsType = {|
  children: SideNoteChildrenType
|};

type WrappedWellType = {|
  ...AsComponentExtensionType,
  ...WellOwnPropsType
|};

const Well = React.forwardRef(({ children, className }: WrappedWellType, forwardedRef: ReactForwardedRefType) => (
  <Markdown ref={forwardedRef} className={cx(styles.Well, className)}>
    {processChildren(children)}
  </Markdown>
));

export type InfoType = {|
  ...AsComponentType,
  ...InfoOwnPropsType
|};

export type LinkType = {|
  ...AsComponentType,
  ...LinkOwnPropsType
|};

export type QuoteType = {|
  ...AsComponentType,
  ...QuoteOwnPropsType
|};

export type WarningType = {|
  ...AsComponentType,
  ...WarningOwnPropsType
|};

export type WellType = {|
  ...AsComponentType,
  ...WellOwnPropsType
|};

export default ({
  Info: Object.assign(AsComponent(ComponentNames.SideNote.Info)(Info), {}),
  Link: Object.assign(AsComponent(ComponentNames.SideNote.Link)(Link), {}),
  Quote: Object.assign(AsComponent(ComponentNames.SideNote.Quote)(Quote), {}),
  Warning: Object.assign(AsComponent(ComponentNames.SideNote.Warning)(Warning), {}),
  Well: Object.assign(AsComponent(ComponentNames.SideNote.Well)(Well), {})
}: {
  Info: React.ComponentType<InfoType>,
  Link: React.ComponentType<LinkType>,
  Quote: React.ComponentType<QuoteType>,
  Warning: React.ComponentType<WarningType>,
  Well: React.ComponentType<WellType>
});
