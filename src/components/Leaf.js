/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { POSITIONS_VERTICAL as POSITIONS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Leaf.scss';

type LeafOwnPropsType = {|
  children: ReactChildren,
  height?: number,
  left?: number,
  position: $Values<typeof POSITIONS>,
  right?: number,
  showFooter?: boolean,
  showHeader?: boolean,
  width?: number
|};

type WrappedLeafType = {|
  ...AsComponentExtensionType,
  ...LeafOwnPropsType
|};

const Leaf = React.forwardRef(
  (
    { children, className, height, left, position = POSITIONS.TOP, right, showFooter, showHeader, width }: WrappedLeafType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { footerHeight, headerHeight, menuHeight } = React.useContext(MainSizeContext);
    const contentStyle = React.useMemo(
      () => ({
        ...(left === undefined ? {} : { left: unitize(left) }),
        ...(right === undefined ? {} : { right: unitize(right) }),
        ...{
          [POSITIONS.TOP]: {
            top: unitize(headerHeight + menuHeight)
          },
          [POSITIONS.BOTTOM]: {
            bottom: unitize(footerHeight)
          }
        }[position],
        ...(width === undefined ? {} : { width: unitize(width) })
      }),
      [footerHeight, headerHeight, left, menuHeight, position, right, width]
    );

    const innerStyle = React.useMemo(
      () => ({
        ...(height === undefined
          ? { maxHeight: `calc(100vh - ${unitize(footerHeight + menuHeight + headerHeight)})` }
          : { maxHeight: unitize(Math.min(height, window.innerHeight - footerHeight - menuHeight - headerHeight)) })
      }),
      [footerHeight, menuHeight, headerHeight, height]
    );

    const dimmerStyle = React.useMemo(
      () => ({
        ...(showHeader || position === POSITIONS.TOP ? { top: unitize(headerHeight + menuHeight) } : { top: 0 }),
        ...(showFooter || position === POSITIONS.BOTTOM ? { bottom: unitize(footerHeight) } : { bottom: 0 })
      }),
      [footerHeight, headerHeight, menuHeight, position, showFooter, showHeader]
    );

    return (
      <div ref={forwardedRef} className={cx(styles.Leaf, className)}>
        <div className={styles.Dimmer} style={dimmerStyle} />
        <div className={styles.Content} style={contentStyle}>
          <div className={styles.Inner} style={innerStyle}>
            {children}
          </div>
        </div>
      </div>
    );
  }
);

export type LeafType = {|
  ...AsComponentType,
  ...LeafOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Leaf)(Leaf), {
    POSITIONS
  }): React.ComponentType<LeafType> & {
    POSITIONS: Object
  }
);
