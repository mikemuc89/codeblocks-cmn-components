/* @flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Status.scss';

export const STATUSES = Object.freeze({
  ACTIVE: 'Status.STATUSES.ACTIVE',
  BLOCKED: 'Status.STATUSES.BLOCKED',
  CURRENT: 'Status.STATUSES.CURRENT',
  DONE: 'Status.STATUSES.DONE',
  ERROR: 'Status.STATUSES.ERROR',
  WAITING: 'Status.STATUSES.WAITING'
});

const STATUS_TO_CLASS = Object.freeze({
  [STATUSES.ACTIVE]: styles.Status__Active,
  [STATUSES.BLOCKED]: styles.Status__Blocked,
  [STATUSES.CURRENT]: styles.Status__Current,
  [STATUSES.DONE]: styles.Status__Done,
  [STATUSES.ERROR]: styles.Status__Error,
  [STATUSES.WAITING]: styles.Status__Waiting
});

type StatusOwnPropsType = {|
  children?: string,
  status: $Values<typeof STATUSES>
|};

export type WrappedStatusType = {|
  ...AsComponentExtensionType,
  ...StatusOwnPropsType
|};

const Status = React.forwardRef(
  ({ children = '', className, status = STATUSES.WAITING }: WrappedStatusType, forwardedRef: ReactForwardedRefType) => (
    <div ref={forwardedRef} className={cx(styles.Status, STATUS_TO_CLASS[status], className)}>
      <div className={styles.Checkbox} />
      <label className={styles.Label}>{children}</label>
    </div>
  )
);

export type StatusType = {|
  ...AsComponentType,
  ...StatusOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Status)(Status), {
    STATUSES
  }): React.ComponentType<StatusType> & {
    STATUSES: Object
  }
);
