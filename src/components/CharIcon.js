/* #__flow */
import * as React from 'react';
import { KINDS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import WithShape, { type WithShapeExtensionType, type WithShapeType } from '../hocs/components/WithShape';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import styles from './CharIcon.scss';
import boolean from '@omnibly/codeblocks-cmn-schema/src/schema/fields/boolean';

const DEFAULT_SIZE = 40;
const DEFAULT_ICON_SIZE_RATIO = 0.55;

type CharIconOwnPropsType = {|
  border?: boolean,
  label?: string,
  size?: number,
  innerSize?: number,
  inset?: boolean,
  text: string
|};

type WrappedCharIconType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...WithShapeExtensionType,
  ...WithThemeExtensionType,
  ...CharIconOwnPropsType
|};

const CharIcon = React.forwardRef(
  (
    {
      LinkComponent,
      border,
      className,
      inset,
      label,
      linkProps,
      shapeElementRef,
      themeElementRef,
      size = DEFAULT_SIZE,
      innerSize = size * DEFAULT_ICON_SIZE_RATIO,
      text
    }: WrappedCharIconType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <LinkComponent
      ref={mergeRefs(forwardedRef, shapeElementRef, themeElementRef)}
      className={cx(styles.Wrapper, border && styles.Wrapper__Border, inset && styles.Wrapper__Inset, className)}
      title={label}
      {...linkProps}
      style={{
        fontSize: unitize(innerSize),
        height: unitize(size),
        lineHeight: unitize(size),
        width: unitize(size),
        ...linkProps.style
      }}
    >
      <span className={styles.CharIcon}>{text}</span>
    </LinkComponent>
  )
);

export type CharIconType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...WithShapeType,
  ...WithThemeType,
  ...CharIconOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.CharIcon)(WithLink()(WithShape({ defaultShape: SHAPES.CIRCLE })(WithTheme()(CharIcon)))),
    {
      KINDS,
      SHAPES
    }
  ): React.ComponentType<CharIconType> & {
    KINDS: Object,
    SHAPES: Object
  }
);
