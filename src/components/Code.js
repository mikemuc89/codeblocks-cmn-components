/* #__flow */
import * as React from 'react';
import hljs from 'highlight.js';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import useOpenState from '../hooks/useOpenState';
import styles from './Code.scss';

type CodeOwnPropsType = {|
  children: string,
  closeMessage?: string,
  collapsible?: boolean,
  highlightedLines?: Array<number>,
  language?: string,
  open?: boolean,
  openMessage?: string,
  title: string  
|};

type WrappedCodeType = {|
  ...AsComponentExtensionType,
  ...CodeOwnPropsType
|};

const Code = React.forwardRef(
  (
    {
      children,
      className,
      closeMessage = 'Ukryj',
      collapsible = true,
      highlightedLines = [],
      language = 'txt',
      open: initiallyOpen = true,
      openMessage = 'Pokaż',
      title
    }: WrappedCodeType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { open, onToggle } = useOpenState({ disabled: !collapsible, open: initiallyOpen });
    const codeEl = React.useRef(null);

    React.useEffect(() => {
      const el = codeEl.current;
      if (el) {
        const html = hljs
          .highlight(children, { language })
          .value.split('\n')
          .map(
            (line: string, idx: number) =>
              `<span class="${styles.Line}${
                highlightedLines.includes(idx) ? ` ${styles.Line__Highlighted}` : ''
              }">${line}</span>`
          )
          .join('');
        el.innerHTML = html;
      }
    }, [children, highlightedLines, language]);

    const label = React.useMemo(() => (open ? closeMessage : openMessage), [closeMessage, open, openMessage]);

    return (
      <div ref={forwardedRef} className={cx(styles.Code, cx.open(styles, 'Code', { open }), className)}>
        <div className={styles.Title} onClick={collapsible ? onToggle : undefined}>
          {title}
        </div>
        {collapsible && (
          <div tabIndex={0} className={styles.Label} onClick={collapsible ? onToggle : undefined}>
            {label}
          </div>
        )}
        <pre className={styles.Pre}>
          <code ref={codeEl} className={styles.Content}>
            {children}
          </code>
        </pre>
      </div>
    );
  }
);

export type CodeType = {|
  ...AsComponentType,
  ...CodeOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Code)(Code), {}): React.ComponentType<CodeType>
);
