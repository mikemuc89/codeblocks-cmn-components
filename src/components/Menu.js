/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES, VERTICAL_ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import CONFIG from '../components/style-config';
import styles from './Menu.scss';

const {
  colors: {
    background: BACKGROUND_COLORS
  }
} = CONFIG;

const ICON_VERTICAL_MARGIN = 2 * 8;

type MenuOwnPropsType = {|
  align: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactChildren,
  height?: number,
  top: number,
  verticalAlign?: $Values<typeof VERTICAL_ALIGN_TYPES>
|};

type WrappedMenuType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...MenuOwnPropsType
|};

const Menu = React.forwardRef(
  (
    {
      align = ALIGN_TYPES.LEFT,
      backgroundElementRef,
      border,
      children,
      className,
      height,
      top,
      verticalAlign = VERTICAL_ALIGN_TYPES.MIDDLE
    }: WrappedMenuType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { innerWidth } = React.useContext(MainSizeContext);

    return (
      <nav
        ref={mergeRefs(forwardedRef, backgroundElementRef)}
        className={cx(
          styles.Menu,
          cx.alignSingle(styles, 'Menu', { align }),
          cx.verticalAlignSingle(styles, 'Menu', { verticalAlign }),
          border && styles.Menu__Border,
          className
        )}
        style={{
          ...(height ? { height: unitize(height) } : {}),
          ...(top ? { top: unitize(top) } : {})
        }}
      >
        <div className={styles.Content} style={{ width: unitize(innerWidth) }}>
          {React.Children.map(
            children,
            (child: React.Node) =>
              child &&
              (isBasicReactChild(child) ? (
                <span className={styles.Content}>{child}</span>
              ) : (
                React.cloneElement(child, {
                  className: cx(
                    child.props.className,
                    {
                      [ComponentNames.CharIcon]: styles.Icon,
                      [ComponentNames.ColorIcon]: styles.Icon,
                      [ComponentNames.FontAwesome]: styles.Icon,
                      [ComponentNames.FontAwesome.Brand]: styles.Icon,
                      [ComponentNames.FontAwesome.Regular]: styles.Icon,
                      [ComponentNames.Icon]: styles.Icon,
                      [ComponentNames.Image]: styles.Image,
                      [ComponentNames.Link]: styles.Link,
                      [ComponentNames.Link.Set]: styles.LinkSet,
                      [ComponentNames.Tabs]: styles.Tabs
                    }[child.type.componentId]
                  ),
                  ...{
                    [ComponentNames.CharIcon]: {
                      size: child.props.size || height - ICON_VERTICAL_MARGIN
                    },
                    [ComponentNames.ColorIcon]: {
                      size: child.props.size || height - ICON_VERTICAL_MARGIN
                    },
                    [ComponentNames.FontAwesome]: {
                      size: child.props.size || height - ICON_VERTICAL_MARGIN
                    },
                    [ComponentNames.FontAwesome.Brand]: {
                      size: child.props.size || height - ICON_VERTICAL_MARGIN
                    },
                    [ComponentNames.FontAwesome.Regular]: {
                      size: child.props.size || height - ICON_VERTICAL_MARGIN
                    },
                    [ComponentNames.Icon]: {
                      size: child.props.size || height - ICON_VERTICAL_MARGIN
                    },
                    [ComponentNames.Image]: {
                      height: child.props.size || height - ICON_VERTICAL_MARGIN
                    }
                  }[child.type.componentId]
                })
              ))
          )}
        </div>
      </nav>
    );
  }
);

export type MenuType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...MenuOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Menu)(WithBackground({
    defaultBackgroundColor: BACKGROUND_COLORS.menu.normal,
    defaultBackgroundColorFocus: BACKGROUND_COLORS.menu.focus,
    defaultBackgroundColorHover: BACKGROUND_COLORS.menu.hover
  })(Menu)), {
    ALIGN_TYPES
  }): React.ComponentType<MenuType> & {
    ALIGN_TYPES: Object
  }
);
