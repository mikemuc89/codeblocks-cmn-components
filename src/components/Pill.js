/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import { type ActionPropType, type IconPropType } from '../types';
import styles from './Pill.scss';

const ICON_SIZE = 24;
const ICON_INNER_SIZE = 12;

type PillOwnPropsType = {|
  action?: ActionPropType,
  children: ReactChildren,
  clickable?: boolean,
  handle: IconPropType  
|};

type WrappedPillType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...PillOwnPropsType
|};

const Pill = React.forwardRef(
  ({ action, children, clickable, className, handle, themeElementRef }: WrappedPillType, forwardedRef: ReactForwardedRefType) => {
    const Component = clickable ? 'button' : 'div';

    const actionElement =
      action &&
      React.cloneElement(action, {
        className: cx(styles.Action),
        ...{
          [ComponentNames.Button]: {
            align: null,
            narrow: true
          },
          [ComponentNames.CharIcon]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.Icon]: {
            size: ICON_SIZE
          }
        }[action.type.componentId]
      });

    const handleElement =
      handle &&
      React.cloneElement(handle, {
        className: cx(styles.Handle),
        ...{
          [ComponentNames.CharIcon]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: ICON_INNER_SIZE,
            size: ICON_SIZE
          },
          [ComponentNames.Icon]: {
            size: ICON_SIZE
          }
        }[handle.type.componentId]
      });

    return (
      <Component ref={forwardedRef} className={cx(styles.Pill, className)}>
        <div ref={themeElementRef} className={styles.Inner}>
          {handleElement}
          {React.Children.map(children, (child: React.Node) =>
            child && (
              isBasicReactChild(child) ? (
                <span className={styles.Content}>{child}</span>
              ) : (
                React.cloneElement(child, {
                  className: cx(
                    child.props.className,
                    {
                      [ComponentNames.CharIcon]: styles.Icon,
                      [ComponentNames.ColorIcon]: styles.Icon,
                      [ComponentNames.FontAwesome]: styles.Icon,
                      [ComponentNames.FontAwesome.Brand]: styles.Icon,
                      [ComponentNames.FontAwesome.Regular]: styles.Icon,
                      [ComponentNames.Icon]: styles.Icon
                    }[child.type.componentId] || styles.Content
                  ),
                  ...{
                    [ComponentNames.CharIcon]: {
                      innerSize: ICON_INNER_SIZE,
                      size: ICON_SIZE
                    },
                    [ComponentNames.ColorIcon]: {
                      innerSize: ICON_INNER_SIZE,
                      size: ICON_SIZE
                    },
                    [ComponentNames.FontAwesome]: {
                      innerSize: ICON_INNER_SIZE,
                      size: ICON_SIZE
                    },
                    [ComponentNames.FontAwesome.Brand]: {
                      innerSize: ICON_INNER_SIZE,
                      size: ICON_SIZE
                    },
                    [ComponentNames.FontAwesome.Regular]: {
                      innerSize: ICON_INNER_SIZE,
                      size: ICON_SIZE
                    },
                    [ComponentNames.Icon]: {
                      size: ICON_SIZE
                    }
                  }[child.type.componentId]
                })
              )
            )
          )}
          {actionElement}
        </div>
      </Component>
    );
  }
);

export type PillType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...PillOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Pill)(WithTheme()(Pill)), {
    KINDS
  }): React.ComponentType<PillType> & {
    KINDS: Object
  }
);
