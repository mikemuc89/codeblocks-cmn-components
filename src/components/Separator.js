/* @flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Separator.scss';

type SeparatorOwnPropsType = {|
  children?: string,
  inline?: boolean,
  narrow?: boolean
|};

type WrappedSeparatorType = {|
  ...AsComponentExtensionType,
  ...SeparatorOwnPropsType
|};

const Separator = React.forwardRef(
  ({ children, className, inline, narrow }: WrappedSeparatorType, forwardedRef: ReactForwardedRefType) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Separator,
        inline && styles.Separator__Inline,
        (inline || narrow) && styles.Separator__Narrow,
        className
      )}
    >
      <hr className={styles.Line} />
      {children && (
        <>
          <span className={styles.Content}>{children}</span>
          <hr className={styles.Line} />
        </>
      )}
    </div>
  )
);

export type SeparatorType = {|
  ...AsComponentType,
  ...SeparatorOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Separator)(Separator), {}): React.ComponentType<SeparatorType>
);
