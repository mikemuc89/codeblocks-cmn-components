/* #__flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import Animation from './Animation';
import styles from './Dimmer.scss';

const MODES = Object.freeze({
  ALWAYS: 'WithLayer.DIMMER_MODES.ALWAYS',
  MOBILE: 'WithLayer.DIMMER_MODES.MOBILE',
  NEVER: 'WithLayer.DIMMER_MODES.NEVER'
});

const MODE_TO_CLASS = Object.freeze({
  [MODES.ALWAYS]: styles.Dimmer__Always,
  [MODES.MOBILE]: styles.Dimmer__Mobile,
  [MODES.NEVER]: styles.Dimmer__Never
});

type DimmerOwnPropsType = {|
  children: ReactChildren,
  mode?: $Values<typeof MODES>,
  onClick?: (e: Event) => void
|};

type WrappedDimmerType = {|
  ...AsComponentExtensionType,
  ...DimmerOwnPropsType
|};

const Dimmer = React.forwardRef(
  ({ children, className, mode = MODES.MOBILE, onClick }: WrappedDimmerType, forwardedRef: ReactForwardedRefType) => {
    const dimmerElementRef = React.useRef(null);
    const onPreventScroll = React.useCallback((e: Event) => {
      e.stopPropagation();
      e.preventDefault();
      e.returnValue = false;
      return false;
    }, []);

    React.useEffect(() => {
      const el = dimmerElementRef.current;
      if (el) {
        el.addEventListener('mousewheel', onPreventScroll);
      }
      return () => {
        if (el) {
          el.removeEventListener('mousewheel', onPreventScroll);
        }
      };
    }, [onPreventScroll]);

    return (
      <div ref={mergeRefs(forwardedRef, dimmerElementRef)} className={cx(styles.Dimmer, MODE_TO_CLASS[mode])} onClick={onClick} />
    );
  }
);

export type DimmerType = {|
  ...AsComponentType,
  ...DimmerOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Dimmer)(Dimmer), {
    MODES
  }): React.ComponentType<DimmerType> & {
    MODES: Object
  }
);
