/* #__flow */
import * as React from 'react';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink from '../hocs/components/WithLink';
import Link, { type LinkType, type WrappedLinkType } from './Link';
import styles from './Breadcrumbs.scss';

type WrappedItemType = WrappedLinkType;

export type ItemType = LinkType;

const Item = React.forwardRef(({ children, className, ...props }: WrappedItemType, forwardedRef: ReactForwardedRefType) => (
  <Link className={cx(styles.Item, className)} {...props}>
    {children}
  </Link>
));

type BreadcrumbsOwnPropsType = {|
  children: ReactElementsChildren<ItemType>,
  separator?: string
|};

type WrappedBreadcrumbsType = {|
  ...AsComponentExtensionType,
  ...BreadcrumbsOwnPropsType
|};

const Breadcrumbs = React.forwardRef(
  ({ children, className, separator = '/' }: WrappedBreadcrumbsType, forwardedRef: ReactForwardedRefType) => (
    <div ref={forwardedRef} className={cx(styles.Breadcrumbs, className)}>
      {React.Children.map(children, (child: any, index: number) => (
        <>
          {index !== 0 && <span className={styles.Separator}>{separator}</span>}
          {React.cloneElement(child, {})}
        </>
      ))}
    </div>
  )
);

export type BreadcrumbsType = {|
  ...AsComponentType,
  ...BreadcrumbsOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Breadcrumbs)(Breadcrumbs), {
    Item: AsComponent(ComponentNames.Breadcrumbs.Item)(WithLink()(Item))
  }): React.ComponentType<BreadcrumbsType> & {
    Item: React.ComponentType<ItemType>
  }
);
