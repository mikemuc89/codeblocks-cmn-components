/* @flow */
import * as React from 'react';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type ImagePropType } from '../types';
import styles from './Figure.scss';

type FigureOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  caption: string,
  children: ImagePropType,
  counter?: boolean  
|};

type WrappedFigureType = {|
  ...AsComponentExtensionType,
  ...FigureOwnPropsType
|};

const Figure = React.forwardRef(
  ({ align = ALIGN_TYPES.CENTER, caption, children, className, counter }: WrappedFigureType, forwardedRef: ReactForwardedRefType) => (
    <figure
      ref={forwardedRef}
      className={cx(
        styles.Figure,
        cx.alignSingle(styles, 'Figure', { align }),
        counter && styles.Figure__Counter,
        className
      )}
    >
      {children &&
        React.cloneElement(children, {
          className: styles.Image
        })}
      <figcaption className={styles.Caption}>{caption}</figcaption>
    </figure>
  )
);

export type FigureType = {|
  ...AsComponentType,
  ...FigureOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Figure)(Figure), {
    ALIGN_TYPES
  }): React.ComponentType<FigureType> & {
    ALIGN_TYPES: Object
  }
);
