/* #__flow */
import * as React from 'react';
import { type ReactElementsChildren, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid, reactInnerText } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import ParentCacheContext from '../contexts/ParentCacheContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControlWithItems, { ControlWithItemsContext, GroupItemContext, type AsControlWithItemsExtensionType, type AsControlWithItemsType } from '../hocs/components/AsControlWithItems';
import WithLayer, { LAYER_MODES, type WithLayerExtensionType, type WithLayerType } from '../hocs/components/WithLayer';
import FontAwesome from './FontAwesome';
import Input from './Input';
import { Item } from './Select';
import Set from './Set';
import styles from './Autocomplete.scss';

const NoSearchResultsElement = () => (
  <div className={styles.NoSearchResults}>
    <FontAwesome className={styles.Icon} align={FontAwesome.ALIGN_TYPES.CENTER} id="ban" size={40} innerSize={24} />
    <span className={styles.Text}>No search results</span>
  </div>
);

type AutocompleteBaseOwnPropsType = {|
  selectedElement?: string | React.Node
|};

type WrappedAutocompleteBaseType = {|
  ...AsComponentExtensionType,
  ...AsControlWithItemsExtensionType,
  ...WithLayerExtensionType,
  ...AutocompleteOwnPropsType
|};

const AutocompleteBase = React.forwardRef(
  (
    {
      className,
      controlElement,
      coverElement,
      layerElement,
      messageElement,
      requiredElement
    }: WrappedAutocompleteType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Autocomplete,
        className
      )}
    >
      {controlElement}
      {coverElement}
      {requiredElement}
      {messageElement}
      {layerElement}
    </div>
  )
);

export type AutocompleteBaseType = {|
  ...AsComponentType,
  ...AsControlWithItemsType,
  ...WithLayerType,
  ...AutocompleteBaseOwnPropsType
|};

type WrappedAutocompleteWithLayerType ={|
  ...WithLayer,
  ...AutocompleteBaseOwnPropsType
|};

const AutocompleteWithLayer = WithLayer({ layerClassName: styles.Layer, mode: LAYER_MODES.VERTICAL })(AutocompleteBase);

const AutocompleteInput = React.forwardRef((props: WrappedAutocompleteWithLayerType, forwardedRef: ReactForwardedRefType) => {
  const [cache, setCache] = React.useState('');
  const { children, value } = props;

  const contextValue = React.useMemo(() => ({
    cache,
    setParentCache: setCache
  }), [cache, setCache]);

  React.useEffect(() => {
    setCache((oldValue) => {
      if (!value) {
        return oldValue;
      }
      const foundChild = React.Children.toArray(children).find((child) => child.props.id === value);
      return foundChild ? foundChild.props.children : oldValue;
    });
  }, [setCache, value]);

  const input = React.useMemo(() => (
    <Input className={styles.Input} />
  ), []);

  const noResultsElement = React.useMemo(() => (
    <NoSearchResultsElement />
  ));

  return (
    <ParentCacheContext.Provider value={contextValue}>
      <AutocompleteWithLayer ref={forwardedRef} input={input} noResultsElement={noResultsElement} {...props} />
    </ParentCacheContext.Provider>
  );
});

const AutocompleteSet = React.forwardRef((props: WrappedAutocompleteWithLayerType, forwardedRef: ReactForwardedRefType) => {
  const [cache, setCache] = React.useState('');
  const { value } = props;

  const contextValue = React.useMemo(() => ({
    cache,
    setParentCache: setCache
  }), [cache, setCache]);

  React.useEffect(() => {
    setCache((oldValue) => {
      if (!value) {
        return oldValue;
      }
      const foundChild = React.Children.toArray(children).find((child) => child.props.id === value);
      return foundChild ? foundChild.props.children : oldValue;
    });
  }, [setCache, value]);

  const input = React.useMemo(() => (
    <Set className={styles.Set} value={value} />
  ), [value]);

  const noResultsElement = React.useMemo(() => (
    <NoSearchResultsElement />
  ));

  return (
    <ParentCacheContext.Provider value={contextValue}>
      <AutocompleteWithLayer ref={forwardedRef} input={input} noResultsElement={noResultsElement} {...props} />
    </ParentCacheContext.Provider>
  );
});

export default {
  Input: AsControlWithItems({
    changeTypeSingle: AsControlWithItems.SINGLE_CHANGE_TYPES.ALWAYS_NEW,
    defaultMaximum: 1,
    defaultValue: '',
    sanitizeValueForControl: (value: string) => value,
    style: { key: 'Autocomplete', styles }
  })(AutocompleteInput),
  Item,
  Set: AsControlWithItems({
    changeTypeSingle: AsControlWithItems.SINGLE_CHANGE_TYPES.ALWAYS_NEW,
    changeTypeMulti: AsControlWithItems.MULTI_CHANGE_TYPES.TOGGLE,
    defaultMaximum: Infinity,
    defaultValue: null,
    sanitizeValueForControl: JSON.stringify,
    style: { key: 'Autocomplete', styles }
  })(AutocompleteSet),
};
