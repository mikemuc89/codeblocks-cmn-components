/* @flow */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, stopPropagationCallback } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import LayerRootContext from '../contexts/LayerRootContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import useImage from '../hooks/useImage';
import useOpenState from '../hooks/useOpenState';
import Dimmer from './Dimmer';
import styles from './Image.scss';

type ImageOwnPropsType = {|
  alt?: string,
  border?: boolean,
  fill?: boolean,
  height?: number,
  label?: string,
  width?: number,
  src: string
|};

type WrappedImageType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...WithLinkExtensionType,
  ...ImageOwnPropsType
|};

const Image = React.forwardRef(
  (
    {
      LinkComponent,
      alt,
      backgroundElementRef,
      border,
      className,
      fill,
      height,
      label,
      linkProps,
      open: initiallyOpen,
      openable = false,
      themeElementRef,
      width,
      src
    }: WrappedImageType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { hide, open, show } = useOpenState({ disabled: !openable, open: initiallyOpen });
    const { image, imageContainerRef } = useImage(src, { fill, height, width });
    const imageEl = image && (
      <img className={styles.Img} alt={alt} src={image.src} />
    );
    const layerRoot = React.useContext(LayerRootContext);

    const layerPortalContent = openable && open ? (
      <>
        <Dimmer mode={Dimmer.MODES.ALWAYS} />
        <div className={styles.Layer} onClick={hide}>
          {React.cloneElement(imageEl, { onClick: stopPropagationCallback })}
        </div>
      </>
    ) : null

    const layerElement = layerRoot && open
      ? ReactDOM.createPortal(layerPortalContent, layerRoot)
      : null;

    return (
      <>
        <LinkComponent
          ref={mergeRefs(forwardedRef, backgroundElementRef, imageContainerRef, themeElementRef)}
          className={cx(styles.Image, border && styles.Image__Border, className)}
          title={label}
          {...linkProps}
          {...(openable ? { onClick: show, tabIndex: 0 } : {})}
          style={{ ...linkProps.style, ...(openable ? { cursor: 'pointer' } : {}) }}
        >
          {imageEl}
        </LinkComponent>
        {layerElement}
      </>
    );
  }
);

export type ImageType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...WithLinkType,
  ...ImageOwnPropsType
|};

type GroupOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: ReactElementsChildren<ImageType>,
  size?: number
|};

type WrappedGroupType = {|
  ...AsComponentExtensionType,
  ...GroupOwnPropsType
|};

const Group = React.forwardRef(
  (
    {
      align = ALIGN_TYPES.CENTER,
      children,
      className,
      size = 160
    }: WrappedGroupType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const count = React.Children.count(children);
    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Group,
          align && cx.alignSingle(styles, 'Group', { align }),
          count <= 4 ? styles[`Group__${count}_Children`] : styles.Group__MoreChildren,
          className
        )}
      >
        {React.Children.map(children, (child: React.ElementType<ImageType>, index: number) => (
          <React.Fragment key={index}>
            {index !== 0 && <div className={styles.Separator} />}
            {React.cloneElement(child, {
              height: size,
              openable: true,
              width: size
            })}
          </React.Fragment>
        ))}

      </div>
    );
  }
);

export type GroupType = {|
  ...AsComponentType,
  ...GroupOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Image)(WithLink()(WithBackground()(Image))
  ), {
    Group: Object.assign(
      AsComponent(ComponentNames.Image.Group)(Group),
      {
        ALIGN_TYPES
      }
    )
  }): React.ComponentType<ImageType> & {
    Group: React.ComponentType<GroupType> & {
      ALIGN_TYPES: Object
    }
  }
);
