/* @flow */
module.exports = {
  animations: {
    fade: {
      delay: 0,
      function: 'ease-in-out',
      property: 'opacity',
      time: 500
    },
    slide: {
      delay: 0,
      function: 'ease-in-out',
      property: 'height',
      time: 500
    },
    width: {
      delay: 0,
      function: 'ease-in-out',
      property: 'width',
      time: 500
    }
  },
  border: {
    color: '#aaaaaa',
    colorDark: '#888888',
    colorLight: '#eeeeee',
    radius: 2,
    radiusCircle: '50%',
    radiusRounded: '20%',
    width: 1
  },
  breakpoints: {
    desktop: {
      contentWidth: 1200,
      dialog: {
        mini: {
          imageHeight: 240,
          width: 640
        },
        nano: {
          imageHeight: 160,
          width: 400
        },
        normal: {
          imageHeight: 320,
          width: 1000
        }
      },
      footerHeight: 32,
      from: 1280,
      headerHeight: 40,
      innerWidth: 1136,
      menuHeight: 64,
      menuLogoHeight: 48,
      menuPadding: 32,
      sidePadding: 32,
      to: 1799
    },
    desktopBig: {
      contentWidth: 1600,
      dialog: {
        mini: {
          imageHeight: 160,
          width: 720
        },
        nano: {
          imageHeight: 120,
          width: 480
        },
        normal: {
          imageHeight: 240,
          width: 1200
        }
      },
      footerHeight: 32,
      from: 1800,
      headerHeight: 40,
      innerWidth: 1536,
      menuHeight: 64,
      menuLogoHeight: 48,
      menuPadding: 32,
      sidePadding: 32,
      to: Infinity
    },
    laptop: {
      contentWidth: '100%',
      dialogWidth: {
        mini: {
          imageHeight: 240,
          width: 640
        },
        nano: {
          imageHeight: 160,
          width: 400
        },
        normal: {
          imageHeight: 320,
          width: 920
        }
      },
      footerHeight: 32,
      from: 960,
      headerHeight: 40,
      innerWidth: '100%',
      menuHeight: 64,
      menuLogoHeight: 48,
      menuPadding: 16,
      sidePadding: 16,
      to: 1279
    },
    phoneL: {
      contentWidth: '100%',
      dialog: {
        mini: {
          imageHeight: 160,
          width: '100%'
        },
        nano: {
          imageHeight: 160,
          width: '100%'
        },
        normal: {
          imageHeight: 160,
          width: '100%'
        }
      },
      footerHeight: 16,
      from: 480,
      headerHeight: 40,
      innerWidth: '100%',
      menuHeight: 40,
      menuLogoHeight: 24,
      menuPadding: 8,
      sidePadding: 0,
      to: 639
    },
    phoneM: {
      contentWidth: '100%',
      dialogWidth: {
        mini: {
          imageHeight: 120,
          width: '100%'
        },
        nano: {
          imageHeight: 120,
          width: '100%'
        },
        normal: {
          imageHeight: 120,
          width: '100%'
        }
      },
      footerHeight: 16,
      from: 320,
      headerHeight: 40,
      innerWidth: '100%',
      menuHeight: 40,
      menuLogoHeight: 24,
      menuPadding: 8,
      sidePadding: 0,
      to: 479
    },
    phoneS: {
      contentWidth: '100%',
      dialogWidth: {
        mini: {
          imageHeight: 120,
          width: '100%'
        },
        nano: {
          imageHeight: 120,
          width: '100%'
        },
        normal: {
          imageHeight: 120,
          width: '100%'
        }
      },
      footerHeight: 16,
      from: 0,
      headerHeight: 40,
      innerWidth: '100%',
      menuHeight: 40,
      menuLogoHeight: 24,
      menuPadding: 8,
      sidePadding: 0,
      to: 319
    },
    tablet: {
      contentWidth: '100%',
      dialog: {
        mini: {
          imageHeight: 200,
          width: 600
        },
        nano: {
          imageHeight: 120,
          width: 400
        },
        normal: {
          imageHeight: 280,
          width: 600
        }
      },
      footerHeight: 16,
      from: 640,
      headerHeight: 40,
      innerWidth: '100%',
      menuHeight: 40,
      menuLogoHeight: 24,
      menuPadding: 8,
      sidePadding: 8,
      to: 959
    }
  },
  colors: {
    background: {
      accent: {
        focus: '#dddddd',
        hover: '#dddddd',
        normal: '#eeeeee'
      },
      card: {
        focus: '#eeeeee',
        hover: '#eeeeee',
        normal: '#ffffff'
      },
      common: {
        focus: '#eeeeee',
        hover: '#eeeeee',
        normal: '#ffffff'
      },
      dialog: {
        focus: '#eeeeee',
        hover: '#eeeeee',
        normal: '#ffffff'
      },
      dimmer: {
        focus: 'rgba(30,30,30,0.6)',
        hover: 'rgba(30,30,30,0.6)',
        normal: 'rgba(50,50,50,0.6)'
      },
      footer: {
        focus: '#eeeeee',
        hover: '#eeeeee',
        normal: '#ffffff'
      },
      hamburger: {
        focus: '#eeeeee',
        hover: '#eeeeee',
        normal: '#ffffff'
      },
      header: {
        focus: '#eeeeee',
        hover: '#eeeeee',
        normal: '#ffffff'
      },
      menu: {
        focus: '#eeeeee',
        hover: '#eeeeee',
        normal: '#ffffff'
      },
      pane: {
        focus: '#eeeeee',
        hover: '#eeeeee',
        normal: '#ffffff'
      }
    },
    boxes: {
      helper: {
        background: '#f6f6f6',
        border: '#aaaaaa',
        font: '#000000'
      },
      info: {
        background: '#dcf0fa',
        border: '#114274',
        font: '#000000'
      },
      link: {
        background: '#c8e6aa',
        border: '#647355',
        font: '#000000'
      },
      quote: {
        background: '#fff5cd',
        border: '#ffcd14',
        font: '#000000'
      },
      warning: {
        background: '#e6bebe',
        border: '#963c3c',
        font: '#000000'
      },
      well: {
        background: '#dadada',
        border: '#666666',
        font: '#000000'
      }
    },
    controls: {
      disabled: {
        background: '#dddddd',
        border: '#aaaaaa',
        font: '#555555'
      },
      disabledAccent: {
        background: '#555555',
        border: '#aaaaaa',
        font: '#333333'
      },
      focus: {
        background: '#eeeeee',
        border: '#3296fa',
        font: '#000000'
      },
      hover: {
        background: '#eeeeee',
        border: '#bbbbbb',
        font: '#000000'
      },
      normal: {
        background: '#ffffff',
        border: '#aaaaaa',
        font: '#000000'
      }
    },
    kinds: {
      dark: {
        focus: {
          background: '#505050',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#505050',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#282828',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      error: {
        focus: {
          background: '#9a0e0e',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#9a0e0e',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#dc1414',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      info: {
        focus: {
          background: '#0064c8',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#0064c8',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#0078f0',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      light: {
        focus: {
          background: '#eeeeee',
          border: '#aaaaaa',
          font: '#000000'
        },
        hover: {
          background: '#eeeeee',
          border: '#aaaaaa',
          font: '#000000'
        },
        normal: {
          background: '#ffffff',
          border: '#aaaaaa',
          font: '#000000'
        }
      },
      link: {
        focus: {
          background: 'transparent',
          border: 'transparent',
          font: '#4e897a'
        },
        hover: {
          background: 'transparent',
          border: 'transparent',
          font: '#4e897a'
        },
        normal: {
          background: 'transparent',
          border: 'transparent',
          font: '#62ab98'
        }
      },
      marketing: {
        focus: {
          background: '#f0aa1e',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#f0aa1e',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#fabe50',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      plain: {
        focus: {
          background: '#eeeeee',
          border: '#aaaaaa',
          font: '#000000'
        },
        hover: {
          background: '#eeeeee',
          border: '#aaaaaa',
          font: '#000000'
        },
        normal: {
          background: '#ffffff',
          border: '#aaaaaa',
          font: '#000000'
        }
      },
      primary: {
        focus: {
          background: '#4e897a',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#4e897a',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#62ab98',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      secondary: {
        focus: {
          background: '#9e580e',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#9e580e',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#e17d14',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      submit: {
        focus: {
          background: '#2a547e',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#2a547e',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#3c78b4',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      success: {
        focus: {
          background: '#236923',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#236923',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#329632',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      warning: {
        focus: {
          background: '#e6823c',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#e6823c',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#f0963c',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      }
    },
    main: {
      link: {
        focus: '#3c78b4',
        hover: '#3c78b4',
        normal: '#000000'
      },
      primary: {
        focus: '#2a547e',
        hover: '#2a547e',
        normal: '#3c78b4'
      },
      secondary: {
        focus: '#9e580e',
        hover: '#9e580e',
        normal: '#e17d14'
      }
    },
    messages: {
      disabled: '#aaaaaa',
      error: '#c85050',
      helper: '#888888',
      hint: '#465a8c',
      placeholder: '#cccccc',
      required: '#c85050',
      warning: '#f0963c'
    },
    social: {
      facebook: {
        focus: {
          background: '#156ad9',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#156ad9',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#1877f2',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      google: {
        focus: {
          background: '#b31412',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#b31412',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#ea4335',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      instagram: {
        focus: {
          background: '#af2591',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#af2591',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#c32aa3',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      linkedin: {
        focus: {
          background: '#006fa2',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#006fa2',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#007bb5',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      mail: {
        focus: {
          background: '#eeeeee',
          border: '#aaaaaa',
          font: '#666666'
        },
        hover: {
          background: '#eeeeee',
          border: '#aaaaaa',
          font: '#666666'
        },
        normal: {
          background: '#ffffff',
          border: '#aaaaaa',
          font: '#333333'
        }
      },
      phone: {
        focus: {
          background: '#474747',
          border: '#aaaaaa',
          font: '#b2b2b2'
        },
        hover: {
          background: '#474747',
          border: '#aaaaaa',
          font: '#b2b2b2'
        },
        normal: {
          background: '#333333',
          border: '#aaaaaa',
          font: '#aaaaaa'
        }
      },
      pinterest: {
        focus: {
          background: '#aa071a',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#aa071a',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#bd081c',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      skype: {
        focus: {
          background: '#009ed8',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#009ed8',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#00aff0',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      snapchat: {
        focus: {
          background: '#f5f22e',
          border: '#aaaaaa',
          font: '#000000'
        },
        hover: {
          background: '#f5f22e',
          border: '#aaaaaa',
          font: '#000000'
        },
        normal: {
          background: '#fffc00',
          border: '#aaaaaa',
          font: '#000000'
        }
      },
      spotify: {
        focus: {
          background: '#1bc155',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#1bc155',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#1ed760',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      teams: {
        focus: {
          background: '#484f9f',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#484f9f',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '464eb8',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      tiktok: {
        focus: {
          background: '#191919',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#191919',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#010101',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      twitter: {
        focus: {
          background: '#1a90d9',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#1a90d9',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#1da1f2',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      },
      youtube: {
        focus: {
          background: '#e50000',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        hover: {
          background: '#e50000',
          border: '#aaaaaa',
          font: '#ffffff'
        },
        normal: {
          background: '#ff0000',
          border: '#aaaaaa',
          font: '#ffffff'
        }
      }
    },
    table: {
      cell: {
        accent: '#fafafa',
        border: '#dddddd',
        normal: '#ffffff'
      },
      details: {
        background: '#fafafa'
      },
      header: {
        accent: '#fafafa',
        border: '#dddddd',
        normal: '#ffffff'
      }
    },
    values: {
      checked: '#3c78b4',
      off: '#dc1414',
      on: '#329632'
    }
  },
  components: {
    colorPicker: {
      sidePadding: 8,
      topPadding: 4
    },
    empty: {
      size: 200
    },
    img: {
      borderPadding: 6
    },
    rating: {
      border: '#000000',
      hover: '#f8e27c',
      selected: '#ffd102'
    },
    toast: {
      margin: 16,
      width: 240
    },
    tooltip: {
      tip: {
        height: 12,
        sideMargin: 16,
        width: 20
      },
      width: 240
    }
  },
  dom: {
    dimmer: {
      element: 'div',
      id: 'dimmer',
      z: 2
    },
    layerRoot: {
      element: 'div',
      id: 'layer-root',
      z: 3
    },
    root: {
      element: 'div',
      id: 'root',
      z: 1
    },
    tooltipRoot: {
      element: 'div',
      id: 'tooltip-root',
      z: 4
    }
  },
  images: {
    icons: {
      cookies: {
        disabled: require('../assets/images/icons/cookies-disabled.svg'),
        hover: require('../assets/images/icons/cookies-hover.svg'),
        normal: require('../assets/images/icons/cookies-normal.svg')
      },
      minus: {
        disabled: require('../assets/images/icons/minus-disabled.svg'),
        hover: require('../assets/images/icons/minus-hover.svg'),
        normal: require('../assets/images/icons/minus-normal.svg')
      },
      plus: {
        disabled: require('../assets/images/icons/plus-disabled.svg'),
        hover: require('../assets/images/icons/plus-hover.svg'),
        normal: require('../assets/images/icons/plus-normal.svg')
      },
      tick: {
        disabled: require('../assets/images/icons/tick-disabled.svg'),
        hover: require('../assets/images/icons/tick-hover.svg'),
        normal: require('../assets/images/icons/tick-normal.svg')
      },
      x: {
        disabled: require('../assets/images/icons/x-disabled.svg'),
        hover: require('../assets/images/icons/x-hover.svg'),
        normal: require('../assets/images/icons/x-normal.svg')
      }
    },
    kvs: {
      noData: require('../assets/images/kvs/no-data.svg')
    }
  },
  scrollbar: {
    control: {
      size: 4,
      sizeFF: 'thin',
      thumb: {
        active: '#32465a',
        hover: '#32465a',
        inactive: '#aaaaaa',
        normal: '#43709d'
      },
      track: {
        active: '#ffffff',
        hover: '#ffffff',
        inactive: '#ffffff',
        normal: '#ffffff'
      }
    },
    controlDisabled: {
      size: 4,
      sizeFF: 'thin',
      thumb: {
        active: '#32465a',
        hover: '#32465a',
        inactive: '#aaaaaa',
        normal: '#43709d'
      },
      track: {
        active: '#dddddd',
        hover: '#dddddd',
        inactive: '#dddddd',
        normal: '#dddddd'
      }
    },
    dialog: {
      size: 4,
      sizeFF: 'thin',
      thumb: {
        active: '#32465a',
        hover: '#32465a',
        inactive: '#aaaaaa',
        normal: '#43709d'
      },
      track: {
        active: '#ffffff',
        hover: '#ffffff',
        inactive: '#ffffff',
        normal: '#ffffff'
      }
    },
    page: {
      size: 4,
      sizeFF: 'thin',
      thumb: {
        active: '#32465a',
        hover: '#32465a',
        inactive: '#aaaaaa',
        normal: '#43709d'
      },
      track: {
        active: '#eeeeee',
        hover: '#eeeeee',
        inactive: '#eeeeee',
        normal: '#eeeeee'
      }
    }
  },
  stack: [
    'minus',
    'zero',
    'common',
    'significant',
    'menu',
    'importantMenu',
    'controls',
    'fab',
    'loader',
    'tooltip',
    'toast',
    'overlay',
    'dimmer',
    'dialog',
    'layer',
    'hamburger'
  ],
  text: {
    colors: {
      helper: '#999999',
      inverted: '#ffffff',
      normal: '#000000',
      semi: '#333333'
    },
    fonts: {
      icons: {
        family: 'Font Awesome 5 Free'
      },
      monospace: {
        family: 'courier'
      },
      sans: {
        family: 'Open Sans'
      },
      serif: {
        family: 'times'
      }
    },
    styles: {
      acknowledgment: {
        align: 'center',
        emphasizedLinks: true,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 12,
        fontWeight: 'xs',
        lineHeight: 14,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [0, 0, 0, 0]
      },
      block: {
        align: 'justify',
        emphasizedLinks: true,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 'xs',
        lineHeight: 22,
        margin: [24, 24, 24, 104],
        oneline: false,
        padding: [24, 24, 24, 24]
      },
      button: {
        align: 'center',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 'm',
        lineHeight: 38,
        margin: [0, 0, 0, 0],
        oneline: true,
        padding: [0, 12, 0, 12]
      },
      code: {
        align: 'left',
        emphasizedLinks: false,
        font: 'monospace',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 'xs',
        lineHeight: 24,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [0, 0, 0, 0]
      },
      control: {
        align: 'left',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 's',
        lineHeight: 38,
        margin: [0, 0, 0, 0],
        oneline: true,
        padding: [0, 32, 0, 8]
      },
      h1: {
        align: 'left',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 32,
        fontWeight: 'm',
        lineHeight: 40,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [8, 0, 8, 0]
      },
      h2: {
        align: 'left',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 28,
        fontWeight: 'm',
        lineHeight: 32,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [8, 0, 8, 0]
      },
      h3: {
        align: 'left',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 24,
        fontWeight: 's',
        lineHeight: 32,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [8, 0, 8, 0]
      },
      h4: {
        align: 'left',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 20,
        fontWeight: 's',
        lineHeight: 24,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [8, 0, 8, 0]
      },
      h5: {
        align: 'left',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: true,
        fontSize: 18,
        fontWeight: 'xs',
        lineHeight: 24,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [8, 0, 8, 0]
      },
      indent: {
        align: 'justify',
        emphasizedLinks: true,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 'xs',
        lineHeight: 22,
        margin: [0, 0, 0, 24],
        oneline: false,
        padding: [6, 0, 6, 0]
      },
      input: {
        align: 'left',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 's',
        lineHeight: 32,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [3, 8, 3, 8]
      },
      label: {
        align: 'right',
        emphasizedLinks: true,
        font: 'sans',
        fontColor: '#333333',
        fontItalic: false,
        fontSize: 12,
        fontWeight: 'xs',
        lineHeight: 32,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [0, 0, 0, 0]
      },
      list: {
        align: 'left',
        emphasizedLinks: true,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 'xs',
        lineHeight: 22,
        margin: [0, 0, 0, 24],
        oneline: false,
        padding: [0, 0, 0, 0]
      },
      paragraph: {
        align: 'justify',
        emphasizedLinks: true,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 'xs',
        lineHeight: 22,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [6, 0, 6, 0]
      },
      subscript: {
        align: 'center',
        emphasizedLinks: true,
        font: 'sans',
        fontColor: '#333333',
        fontItalic: false,
        fontSize: 12,
        fontWeight: 'xs',
        lineHeight: 14,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [0, 0, 0, 0]
      },
      subtitle: {
        align: 'center',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 16,
        fontWeight: 'xs',
        lineHeight: 24,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [12, 8, 0, 8]
      },
      summary: {
        align: 'justify',
        emphasizedLinks: true,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 14,
        fontWeight: 'l',
        lineHeight: 22,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [16, 0, 16, 0]
      },
      title: {
        align: 'center',
        emphasizedLinks: false,
        font: 'sans',
        fontColor: '#000000',
        fontItalic: false,
        fontSize: 20,
        fontWeight: 'm',
        lineHeight: 32,
        margin: [0, 0, 0, 0],
        oneline: false,
        padding: [8, 0, 8, 0]
      }
    },
    weights: {
      l: 700,
      m: 400,
      s: 300,
      xl: 900,
      xs: 100
    }
  }
};
