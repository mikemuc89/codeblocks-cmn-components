/* #__flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs, processChildren, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsContainer, { type AsContainerExtensionType, type AsContainerType } from '../hocs/components/AsContainer';
import styles from './Column.scss';

type ColumnOwnPropsType = {|
  children: ReactChildren,
  colCount?: number,
  colSpan?: number,
  leftSeparator?: boolean,
  padding?: number,
  rightSeparator?: boolean,
  verticalCenter?: boolean
|};

type WrappedColumnType = {|
  ...AsComponentExtensionType,
  ...AsContainerExtensionType,
  ...ColumnOwnPropsType
|};

const Column = React.forwardRef(
  (
    { containerElementRef, children, className, colCount = 2, colSpan = 1, leftSeparator, padding, rightSeparator, verticalCenter }: WrappedColumnType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div
      ref={mergeRefs(forwardedRef, containerElementRef)}
      className={cx(
        styles.Column,
        styles[`Column_${colSpan}_${colCount}`],
        leftSeparator && styles.Column__LeftSep,
        rightSeparator && styles.Column__RightSep,
        verticalCenter && styles.Column__VerticalCenter,
        className
      )}
      style={{
        ...(padding ? { paddingLeft: unitize(padding), paddingRight: unitize(padding) } : {})
      }}
    >
      {children}
    </div>
  )
);

export type ColumnType = {|
  ...AsComponentType,
  ...AsContainerType,
  ...ColumnOwnPropsType
|};

type SetOwnPropsType = {|
  children: ReactElementsChildren<ColumnType>,
  fullHeight?: boolean,
  separators?: boolean,
  outer?: boolean
|};

type WrappedSetType = {|
  ...AsComponentExtensionType,
  ...SetOwnPropsType
|};

const Set = React.forwardRef(({ children, className, fullHeight, separators, outer }: WrappedSetType, forwardedRef: ReactForwardedRefType) => {
  const sumColSpan = React.useMemo(
    () =>
      (Array.isArray(children) ? children : [children]).reduce(
        (sum: number, child: React.Element<ColumnType>) => sum + (parseInt(child ? child.props.colSpan : 0) || 1),
        0
      ),
    [children]
  );

  return (
    <div
      ref={forwardedRef}
      className={cx(styles.Set, fullHeight && styles.Set__FullHeight, outer && styles.Set__Outer, separators && styles.Set__Separators, className)}
    >
      {React.Children.map(children, (child: React.Element<ColumnType>) =>
        child && React.cloneElement(child, {
          colCount: sumColSpan,
          ...(separators && { leftSeparator: undefined, rightSeparator: undefined })
        })
      )}
    </div>
  );
});

export type SetType = {|
  ...AsComponentType,
  ...SetOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Column)(AsContainer()(Column)), {
    Set: AsComponent(ComponentNames.Column.Set)(Set)
  }): React.ComponentType<ColumnType> & {
    Set: React.ComponentType<SetType>
  }
);
