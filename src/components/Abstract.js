/* #__flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import Markdown from './Markdown';
import styles from './Abstract.scss';

const TITLE_POSITION_TYPES = Object.freeze({
  CONTENT: 'Abstract.TITLE_POSITION_TYPES.CONTENT',
  TOP: 'Abstract.TITLE_POSITION_TYPES.TOP'
});

type AbstractOwnPropsType = {|
  author?: string,
  children: string,
  date?: string,
  image?: string,
  subtitle?: string,
  title: string
|}

type WrappedAbstractType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...AbstractOwnPropsType
|};

type GeneralAbstractOwnPropsType = {|
  ...AbstractOwnPropsType,
  titlePositionType?: $Values<typeof TITLE_POSITION_TYPES>
|};

type WrappedGeneralAbstractType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...GeneralAbstractOwnPropsType
|};

const Abstract = React.forwardRef(
  (
    {
      LinkComponent,
      author,
      children,
      className,
      date,
      linkProps,
      image,
      subtitle,
      title,
      titlePositionType = TITLE_POSITION_TYPES.TOP,
      ...props
    }: WrappedGeneralAbstractType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const titleEl = <div className={styles.Title}>{title}</div>;

    return (
      <LinkComponent ref={forwardedRef} className={cx(styles.Abstract, className)} {...linkProps}>
        {titlePositionType === TITLE_POSITION_TYPES.TOP && titleEl}
        {image &&
          React.cloneElement(image, {
            className: styles.Image,
            height: image.props.height || 160
          })}
        <div className={styles.Content}>
          {titlePositionType === TITLE_POSITION_TYPES.CONTENT && titleEl}
          {subtitle && <div className={styles.Subtitle}>{subtitle}</div>}
          {author && <div className={styles.Author}>{author}</div>}
          {date && <div className={styles.Date}>{date}</div>}
          <Markdown className={styles.Text} {...props}>
            {children}
          </Markdown>
        </div>
      </LinkComponent>
    );
  }
);

const WrappedAbstract = AsComponent(ComponentNames.Abstract)(WithLink()(Abstract));

const Featured = React.forwardRef(({ children, className, ...props }: WrappedAbstractType, forwardedRef: ReactForwardedRefType) => (
  <WrappedAbstract
    ref={forwardedRef}
    className={cx(styles.Featured, className)}
    {...props}
    titlePositionType={TITLE_POSITION_TYPES.CONTENT}
  >
    {children}
  </WrappedAbstract>
));

const Left = React.forwardRef(({ children, className, ...props }: WrappedAbstractType, forwardedRef: ReactForwardedRefType) => (
  <WrappedAbstract
    ref={forwardedRef}
    className={cx(styles.Left, className)}
    {...props}
    titlePositionType={TITLE_POSITION_TYPES.CONTENT}
  >
    {children}
  </WrappedAbstract>
));

const Right = React.forwardRef(({ children, className, ...props }: WrappedAbstractType, forwardedRef: ReactForwardedRefType) => (
  <WrappedAbstract
    ref={forwardedRef}
    className={cx(styles.Right, className)}
    {...props}
    titlePositionType={TITLE_POSITION_TYPES.CONTENT}
  >
    {children}
  </WrappedAbstract>
));

export type AbstractType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...AbstractOwnPropsType
|};

export type GeneralAbstractType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...GeneralAbstractOwnPropsType
|};

export default (
  Object.assign(
    WrappedAbstract, {
      Featured: AsComponent(ComponentNames.Abstract.Featured)(Featured),
      Left: AsComponent(ComponentNames.Abstract.Left)(Left),
      Right: AsComponent(ComponentNames.Abstract.Right)(Right),
    }, {
      TITLE_POSITION_TYPES
    }
  ): React.ComponentType<GeneralAbstractType> & {
    Featured: React.ComponentType<AbstractType>,
    Left: React.ComponentType<AbstractType>,
    Right: React.ComponentType<AbstractType>
  } & {
    TITLE_POSITION_TYPES: Object
  }
);
