/* @flow */
import * as React from 'react';
import { ALIGN_TYPES, KINDS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import WithShape, { type WithShapeExtensionType, type WithShapeType } from '../hocs/components/WithShape';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import useImage from '../hooks/useImage';
import styles from './Icon.scss';

const DEFAULT_SIZE = 40;

type IconOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  alt?: string,
  border?: boolean,
  height?: number,
  label?: string,
  size?: number,
  width?: number,
  src: string
|};

type WrappedIconType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...WithShapeExtensionType,
  ...WithThemeExtensionType,
  ...IconOwnPropsType
|};

const Icon = React.forwardRef(
  (
    {
      LinkComponent,
      align,
      alt,
      border,
      className,
      height,
      label,
      linkProps,
      shapeElementRef,
      themeElementRef,
      size = DEFAULT_SIZE,
      width,
      src
    }: WrappedIconType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { image, imageContainerRef } = useImage(src, { height, size, width });

    return (
      <LinkComponent
        ref={mergeRefs(forwardedRef, imageContainerRef, shapeElementRef, themeElementRef)}
        className={cx(styles.Icon, cx.alignSingle(styles, 'Icon', { align }), border && styles.Icon__Border, className)}
        title={label}
        {...linkProps}
      >
        {image && <img className={styles.Img} alt={alt} src={image.src} />}
      </LinkComponent>
    );
  }
);

export type IconType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...WithShapeType,
  ...WithThemeType,
  ...IconOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Icon)(WithLink()(WithShape()(WithTheme()(Icon)))), {
    ALIGN_TYPES,
    KINDS,
    SHAPES
  }): React.ComponentType<IconType> & {
    ALIGN_TYPES: Object,
    KINDS: Object,
    SHAPES: Object
  }
);
