/* @flow */
import * as React from 'react';
import { type ErrorsType, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Message.scss';

const formatMessage = (message: ErrorsType) => (message && typeof message === 'object' ? message.title : message);

type MessageOwnPropsType = {|
  ...WithMessagesType,
  oneline?: boolean,
  text?: ErrorsType
|};

type WrappedMessageType = {|
  ...AsComponentExtensionType,
  ...MessageOwnPropsType
|};

const Message = React.forwardRef(
  (
    { className, errors, helper, hints, oneline, warnings, text = errors || warnings || hints || helper }: WrappedMessageType,
    forwardedRef: ReactForwardedRefType
  ) =>
    text ? (
      <span
        ref={forwardedRef}
        className={cx(
          styles.Message,
          cx.control(styles, 'Message', { errors, helper, hints, warnings }),
          oneline && styles.Message__Oneline,
          className
        )}
      >
        {formatMessage(text)}
      </span>
    ) : null
);

export type MessageType = {|
  ...AsComponentType,
  ...MessageOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Message)(Message), {}): React.ComponentType<MessageType>
);
