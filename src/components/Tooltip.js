/* @flow */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { KINDS, POSITIONS_AROUND as POSITIONS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isFixedElement, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import useEventListener from '../hooks/useEventListener';
import useOpenState from '../hooks/useOpenState';
import FontAwesome from './FontAwesome';
import { components } from './style-config';
import styles from './Tooltip.scss';

type TooltipOwnPropsType = {|
  canClose?: boolean,
  children: ReactChildren,
  content: ReactChildren,
  distance?: number,
  duration?: number,
  onClose?: () => void,
  open?: boolean,
  position: $Values<typeof POSITIONS>,
  timeout?: number
|};

type WrappedTooltipType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...TooltipOwnPropsType
|};

const Tooltip = React.forwardRef(
  (
    {
      canClose,
      children,
      className,
      content,
      distance = 8,
      duration,
      onClose,
      open: initiallyOpen,
      position = POSITIONS.BOTTOM,
      themeElementRef,
      themeElementStyle,
      timeout = 1000
    }: WrappedTooltipType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { hide, open, show } = useOpenState({ open: initiallyOpen });
    const childRef = React.useRef(null);
    const tipRef = React.useRef(null);
    const tooltipRef = React.useRef(null);

    React.useEffect(() => {
      setTimeout(onShow, timeout);
    }, [onShow, timeout]);

    React.useEffect(() => {
      if (open && duration > 0) {
        setTimeout(hide, duration);
      }
    }, [duration, hide, open]);

    const updateTooltipPosition = React.useCallback(() => {
      if (!childRef.current || !tooltipRef.current) {
        return null;
      }
      const childRect = childRef.current.getBoundingClientRect();
      const { clientHeight: tooltipHeight, clientWidth: tooltipWidth } = tooltipRef.current;
      const { clientHeight: childHeight, clientWidth: childWidth } = childRef.current;
      const { height: configTipHeight, width: configTipWidth, sideMargin } = components.tooltip.tip;
      const tipRotateDiff = (configTipWidth - configTipHeight) / 2;

      const { scrollLeft, scrollTop } = document.scrollingElement;

      const style = {
        [POSITIONS.BOTTOM]: () => ({
          left: childRect.left + (childWidth - tooltipWidth) / 2 + scrollLeft,
          tipLeft: tooltipWidth / 2 - configTipWidth / 2,
          tipTop: -configTipHeight,
          top: childRect.top + childHeight + distance + configTipHeight + scrollTop,
          transform: 'rotate(0deg)'
        }),
        [POSITIONS.BOTTOM_LEFT]: () => ({
          left: childRect.left + scrollLeft,
          tipLeft: tooltipWidth / 2 > configTipWidth + sideMargin ? sideMargin : tooltipWidth / 2 - configTipWidth / 2,
          tipTop: -configTipHeight,
          top: childRect.top + childHeight + distance + configTipHeight + scrollTop,
          transform: 'rotate(0deg)'
        }),
        [POSITIONS.BOTTOM_RIGHT]: () => ({
          left: childRect.left + childWidth - tooltipWidth + scrollLeft,
          tipLeft:
            tooltipWidth / 2 > configTipWidth + sideMargin
              ? tooltipWidth - configTipWidth - sideMargin
              : tooltipWidth / 2 - configTipWidth / 2,
          tipTop: -configTipHeight,
          top: childRect.top + childHeight + distance + configTipHeight + scrollTop,
          transform: 'rotate(0deg)'
        }),
        [POSITIONS.LEFT]: () => ({
          left: childRect.left - tooltipWidth - distance - configTipHeight + scrollLeft,
          tipLeft: tooltipWidth - tipRotateDiff,
          tipTop: tooltipHeight / 2 - configTipWidth / 2,
          top: childRect.top + childHeight / 2 - tooltipHeight / 2 + scrollTop,
          transform: 'rotate(90deg)'
        }),
        [POSITIONS.LEFT_BOTTOM]: () => ({
          left: childRect.left - tooltipWidth - distance - configTipHeight + scrollLeft,
          tipLeft: tooltipWidth - tipRotateDiff,
          tipTop:
            tooltipHeight / 2 > configTipWidth + sideMargin
              ? tooltipHeight - configTipWidth - sideMargin
              : tooltipHeight / 2 - configTipWidth / 2,
          top: childRect.top + childHeight - tooltipHeight + scrollTop,
          transform: 'rotate(90deg)'
        }),
        [POSITIONS.LEFT_TOP]: () => ({
          left: childRect.left - tooltipWidth - distance - configTipHeight + scrollLeft,
          tipLeft: tooltipWidth - tipRotateDiff,
          tipTop: tooltipHeight / 2 > configTipWidth + sideMargin ? sideMargin : tooltipHeight / 2 - configTipWidth / 2,
          top: childRect.top + scrollTop,
          transform: 'rotate(90deg)'
        }),
        [POSITIONS.RIGHT]: () => ({
          left: childRect.left + childWidth + distance + configTipHeight + scrollLeft,
          tipLeft: -configTipHeight - tipRotateDiff,
          tipTop: tooltipHeight / 2 - configTipWidth / 2,
          top: childRect.top + childHeight / 2 - tooltipHeight / 2 + scrollTop,
          transform: 'rotate(270deg)'
        }),
        [POSITIONS.RIGHT_BOTTOM]: () => ({
          left: childRect.left + childWidth + distance + configTipHeight + scrollLeft,
          tipLeft: -configTipHeight - tipRotateDiff,
          tipTop:
            tooltipHeight / 2 > configTipWidth + sideMargin
              ? tooltipHeight - configTipWidth - sideMargin
              : tooltipHeight / 2 - configTipWidth / 2,
          top: childRect.top + childHeight - tooltipHeight + scrollTop,
          transform: 'rotate(270deg)'
        }),
        [POSITIONS.RIGHT_TOP]: () => ({
          left: childRect.left + childWidth + distance + configTipHeight + scrollLeft,
          tipLeft: -configTipHeight - tipRotateDiff,
          tipTop: tooltipHeight / 2 > configTipWidth + sideMargin ? sideMargin : tooltipHeight / 2 - configTipWidth / 2,
          top: childRect.top + scrollTop,
          transform: 'rotate(270deg)'
        }),
        [POSITIONS.TOP]: () => ({
          left: childRect.left + childWidth / 2 - tooltipWidth / 2 + scrollLeft,
          tipLeft: tooltipWidth / 2 - configTipWidth / 2,
          tipTop: tooltipHeight,
          top: childRect.top - tooltipHeight - distance - configTipHeight + scrollTop,
          transform: 'rotate(180deg)'
        }),
        [POSITIONS.TOP_LEFT]: () => ({
          left: childRect.left + scrollLeft,
          tipLeft: tooltipWidth / 2 > configTipWidth + sideMargin ? sideMargin : tooltipWidth / 2 - configTipWidth / 2,
          tipTop: tooltipHeight,
          top: childRect.top - tooltipHeight - distance - configTipHeight + scrollTop,
          transform: 'rotate(180deg)'
        }),
        [POSITIONS.TOP_RIGHT]: () => ({
          left: childRect.left + childWidth - tooltipWidth + scrollLeft,
          tipLeft:
            tooltipWidth / 2 > configTipWidth + sideMargin
              ? tooltipWidth - configTipWidth - sideMargin
              : tooltipWidth / 2 - configTipWidth / 2,
          tipTop: tooltipHeight,
          top: childRect.top - tooltipHeight - distance - configTipHeight + scrollTop,
          transform: 'rotate(180deg)'
        })
      }[position]();

      tooltipRef.current.style.left = unitize(style.left);
      tooltipRef.current.style.top = unitize(style.top);
      if (isFixedElement(childRef.current)) {
        tooltipRef.current.style.position = 'fixed';
      }
      tipRef.current.style.left = unitize(style.tipLeft);
      tipRef.current.style.top = unitize(style.tipTop);
      tipRef.current.style.transform = style.transform;
    }, [distance, position]);

    React.useEffect(() => {
      setTimeout(updateTooltipPosition, timeout);
    }, [timeout, updateTooltipPosition]);

    useEventListener('resize', updateTooltipPosition, window);

    const preparedChildren = typeof children === 'string' ? <span className={styles.Text}>{children}</span> : children;

    const handleOnClose = React.useCallback(() => {
      hide();
      if (onClose) {
        onClose();
      }
    }, [hide, onClose]);

    return (
      <>
        {ReactDOM.createPortal(
          <div
            ref={mergeRefs(forwardedRef, themeElementRef, tooltipRef)}
            className={cx(styles.Tooltip, open && styles.Tooltip__Open, className)}
          >
            <div className={styles.Content}>{content}</div>
            {canClose && (
              <FontAwesome className={styles.Close} id="times" size={16} innerSize={10} onClick={handleOnClose} />
            )}
            <svg
              ref={tipRef}
              className={styles.Tip}
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="12"
              viewBox="0 0 20 12"
              version="1.1"
            >
              <path
                className={styles.TipFill}
                fill={themeElementStyle.backgroundColor}
                d="m 0.64354146,12 8.51847984,-10.9548 0.8459577,-0.3908 0.872403,0.4437 8.486351,10.9019 z"
              />
            </svg>
          </div>,
          document.body
        )}
        {React.cloneElement(React.Children.only(preparedChildren), { ref: childRef })}
      </>
    );
  }
);

export type TooltipType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...TooltipOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Tooltip)(
      WithTheme({ defaultKind: KINDS.PRIMARY, focusable: false, hoverable: false })(Tooltip)
    ),
    {
      KINDS,
      POSITIONS
    }
  ): React.ComponentType<TooltipType> & {
    KINDS: Object,
    POSITIONS: Object
  }
);
