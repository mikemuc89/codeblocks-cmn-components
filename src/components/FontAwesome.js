/* @flow */
/* eslint-disable */
import * as React from 'react';
import { ALIGN_TYPES, KINDS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import WithShape, { type WithShapeExtensionType, type WithShapeType } from '../hocs/components/WithShape';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import styles from './FontAwesome.scss';
import faStyles from '@fortawesome/fontawesome-free/scss/fontawesome.scss';
import faStylesBrands from '@fortawesome/fontawesome-free/scss/brands.scss';
import faStylesRegular from '@fortawesome/fontawesome-free/scss/regular.scss';
import faStylesSolid from '@fortawesome/fontawesome-free/scss/solid.scss';

const DEFAULT_SIZE = 40;
const DEFAULT_ICON_SIZE_RATIO = 0.55;

const VARIANTS = Object.freeze({
  BRAND: 'FONT_AWESOME.VARIANTS.BRAND',
  REGULAR: 'FONT_AWESOME.VARIANTS.REGULAR',
  SOLID: 'FONT_AWESOME.VARIANTS.SOLID'
});

const VARIANT_TO_STYLE = Object.freeze({
  [VARIANTS.BRAND]: faStylesBrands.fab,
  [VARIANTS.REGULAR]: faStylesRegular.far,
  [VARIANTS.SOLID]: faStylesSolid.fas
});

type FontAwesomeOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  id: string,
  inset?: boolean,
  label?: string,
  size?: number,
  innerSize?: number
|};

type WrappedFontAwesomeType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...WithShapeExtensionType,
  ...WithThemeExtensionType,
  ...FontAwesomeOwnPropsType
|};

type FontAwesomeBaseOwnPropsType = {|
  ...FontAwesomeOwnPropsType,
  variant?: $Values<typeof VARIANTS>
|};

type WrappedFontAwesomeBaseType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...WithShapeExtensionType,
  ...WithThemeExtensionType,
  ...FontAwesomeBaseOwnPropsType
|};

const FontAwesomeBase = React.forwardRef(
  (
    {
      LinkComponent,
      align,
      className,
      id,
      inset,
      label,
      linkProps,
      shapeElementRef,
      themeElementRef,
      size = DEFAULT_SIZE,
      innerSize = size * DEFAULT_ICON_SIZE_RATIO,
      variant = VARIANTS.SOLID
    }: WrappedFontAwesomeBaseType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <LinkComponent
      ref={mergeRefs(forwardedRef, shapeElementRef, themeElementRef)}
      className={cx(styles.Wrapper, cx.alignSingle(styles, 'Wrapper', { align }), inset && styles.Wrapper__Inset, className)}
      title={label}
      {...linkProps}
      style={{
        ...linkProps.style,
        fontSize: unitize(innerSize),
        height: unitize(size),
        lineHeight: unitize(size),
        width: unitize(size)
      }}
    >
      <i className={cx(styles.FontAwesome, VARIANT_TO_STYLE[variant], faStyles[`fa-${id}`])}></i>
    </LinkComponent>
  )
);

const FontAwesome = React.forwardRef((props: WrappedFontAwesomeType, forwardedRef: ReactForwardedRefType) => (
  <FontAwesomeBase ref={forwardedRef} {...props} variant={VARIANTS.SOLID} />
));

const FontAwesomeBrand = React.forwardRef((props: WrappedFontAwesomeType, forwardedRef: ReactForwardedRefType) => (
  <FontAwesomeBase ref={forwardedRef} {...props} variant={VARIANTS.BRAND} />
));

const FontAwesomeRegular = React.forwardRef((props: WrappedFontAwesomeType, forwardedRef: ReactForwardedRefType) => (
  <FontAwesomeBase ref={forwardedRef} {...props} variant={VARIANTS.REGULAR} />
));

export type FontAwesomeType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...WithShapeType,
  ...WithThemeType,
  ...FontAwesomeOwnPropsType
|};

export type FontAwesomeBaseType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...WithShapeType,
  ...WithThemeType,
  ...FontAwesomeBaseOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.FontAwesome)(WithLink()(WithShape()(WithTheme()(FontAwesome)))), {
      Brand: Object.assign(
        AsComponent(ComponentNames.FontAwesome.Brand)(WithLink()(WithShape()(WithTheme()(FontAwesomeBrand)))),
        {
          ALIGN_TYPES,
          KINDS,
          SHAPES
        }
      ),
      Regular: Object.assign(
        AsComponent(ComponentNames.FontAwesome.Regular)(WithLink()(WithShape()(WithTheme()(FontAwesomeRegular)))),
        {
          ALIGN_TYPES,
          KINDS,
          SHAPES
        }
      )
    }, {
      ALIGN_TYPES,
      KINDS,
      SHAPES
    }
  ): React.ComponentType<FontAwesomeType> & {
    Brand: React.ComponentType<FontAwesomeType> & {
      ALIGN_TYPES: Object,
      KINDS: Object,
      SHAPES: Object
    },
    Regular: React.ComponentType<FontAwesomeType> & {
      ALIGN_TYPES: Object,
      KINDS: Object,
      SHAPES: Object
    }
  } & {
    ALIGN_TYPES: Object,
    KINDS: Object,
    SHAPES: Object
  }
);
