/* #__flow */
import * as React from 'react';
import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import { cx, guid } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Carto.scss';

type CartoOwnPropsType = {|
  x?: number,
  y?: number,
  zoom?: number
|};

type WrappedCartoType = {|
  ...AsComponentExtensionType,
  ...CartoOwnPropsType
|};

const Carto = React.forwardRef(
  ({ className, x = 10000, y = 10000, zoom = 12 }: WrappedCartoType, forwardedRef: ReactForwardedRefType) => {
    const id = React.useMemo(() => guid(), []);
    /* eslint-disable-next-line no-unused-vars */
    const mapRef = React.useRef(
      new Map({
        layers: [
          new TileLayer({
            source: new OSM()
          })
        ],
        target: id,
        view: new View({
          center: [x, y],
          zoom
        })
      })
    );

    return (
      <div ref={forwardedRef} className={cx(styles.Carto, className)}>
        <div id={id} className={styles.Content} />
      </div>
    );
  }
);

export type CartoType = {|
  ...AsComponentType,
  ...CartoOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Carto)(Carto), {}): React.ComponentType<CartoType>
);
