/* @flow */
import * as React from 'react';
import { formatFileSize } from '@omnibly/codeblocks-cmn-formatters';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import WithFileInput, { FileItemContext, getExtensionIcon, type WithFileInputExtensionType, type WithFileInputType } from '../hocs/components/WithFileInput';
import { type IconPropType } from '../types';
import FontAwesome from './FontAwesome';
import styles from './FileInput.scss';

const ICON_SIZE = 24;
const ICON_INNER_SIZE = 16;

type FileItemOwnPropsType = {|
  extension: string,
  index?: number,
  name: string,
  removable?: boolean,
  size: number
|};

type WrappedFileItemType = {|
  ...AsComponentExtensionType,
  ...FileItemOwnPropsType
|};

const FileItem = React.forwardRef(
  ({ extension, index, name, removable = true, size }: WrappedFileItemType, forwardedRef: ReactForwardedRefType) => {
    const { disabled, handleOnRemoveByItemIndex, readonly } = React.useContext(FileItemContext);

    const showRemoveIcon = React.useMemo(() => !disabled && !readonly && removable, [disabled, readonly, removable]);

    const handleOnRemove = React.useCallback(() => {
      handleOnRemoveByItemIndex(index);
    }, [index, handleOnRemoveByItemIndex]);

    const fileIconElement = React.useMemo(() => {
      const baseIcon = getExtensionIcon(extension);
      return React.cloneElement(baseIcon, {
        className: cx(baseIcon.props.className, styles.Icon),
        innerSize: baseIcon.props.innerSize || ICON_INNER_SIZE,
        size: baseIcon.props.size || ICON_SIZE
      });
    }, [extension]);

    return (
      <div ref={forwardedRef} className={cx(styles.File)}>
        <span className={styles.Actions}>
          {showRemoveIcon && (
            <FontAwesome
              className={styles.Remove}
              id="times"
              onClick={handleOnRemove}
              size={ICON_SIZE}
              innerSize={ICON_INNER_SIZE}
            />
          )}
        </span>
        <span className={styles.Name}>
          {fileIconElement}
          {name}
        </span>
        <span className={styles.Extension}>
          {extension}
        </span>
        <span className={styles.Size}>{formatFileSize(size)}</span>
      </div>
    );
  }
);

type FileInputOwnPropsType = {};

type WrappedFileInputType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithFileInputExtensionType,
  ...FileInputOwnPropsType
|};

const FileInput = React.forwardRef(
  (
    {
      buttonElement,
      className,
      controlElement,
      onDragProps,
      overElement,
      messageElement = null,
      selectedFiles
    }: WrappedFileInputType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div ref={forwardedRef} className={cx(styles.FileInput, className)} {...onDragProps}>
      {controlElement}
      {selectedFiles.length > 0 && (
        <div className={styles.Files}>
          {selectedFiles.map((params: File, index: number) => (
            <FileItem key={index} index={index} {...params} />
          ))}
        </div>
      )}
      {buttonElement}
      {messageElement}
      {overElement}
    </div>
  )
);

export type FileItemType = {|
  ...AsComponentType,
  ...FileItemOwnPropsType
|};

export type FileInputType = {|
  ...AsComponentType,
  ...AsControlType,
  ...WithFileInputType,
  ...FileInputOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(
      ComponentNames.FileInput
    )(
      AsControl({
        controlType: INPUT_TYPES.FILE,
        defaultValue: null,
        sanitizeValueForControl: () => '',
        style: { key: 'FileInput', styles }
      })(
        WithFileInput({
          buttonClassName: styles.Button
        })(
          FileInput
        )
      )
    ),
    {
      File: Object.assign(
        AsComponent(
          ComponentNames.FileInput.File
        )(FileItem),
        {}
      )
    }
  ): React.ComponentType<FileInputType> & {
    File: React.ComponentType<FileItemType>
  }
);
