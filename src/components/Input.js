/* @flow */
import * as React from 'react';
import { formatPhoneNumber } from '@omnibly/codeblocks-cmn-formatters';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { AUTOCOMPLETE_TYPES, INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, debounced, guid, mergeRefs, setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import ParentCacheContext from '../contexts/ParentCacheContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import WithInputFormatting, { type WithInputFormattingExtensionType, type WithInputFormattingType } from '../hocs/components/WithInputFormatting';
import WithMask, { type WithMaskExtensionType, type WithMaskType } from '../hocs/components/WithMask';
import { type IconPropType } from '../types';
import FontAwesome from './FontAwesome';
import styles from './Input.scss';

const CLEAR_ICON_INNER_SIZE = 12;
const DEFAULT_ICON_SIZE = 38;
const DEFAULT_ICON_INNER_SIZE = 20;

const ON_SCROLL_DEBOUNCE_TIME = 50;

const DATE_PART_REGEX = new RegExp('^\\d{0,4}(-(\\d{0,2}(-(\\d{0,2})?)?)?)?$', 'gi');
const TIME_PART_REGEX = new RegExp('^\\d{0,2}(:(\\d{0,2})?)?$', 'gi');
const DATE_TIME_PART_REGEX = new RegExp(
  '^\\d{0,4}(-(\\d{0,2}(-(\\d{0,2})?)?)?)?$( (\\d{0,2}(:(\\d{0,2})?)?)?)?$',
  'gi'
);
const INTEGER_REGEX = new RegExp('^\\d*$', 'gi');
const PHONE_NUMBER_PART_REGEX = new RegExp('^\\d{0,15}$', 'gi');
const PHONE_NUMBER_PL_PART_REGEX = new RegExp('^\\d{0,9}$', 'gi');
const POSTAL_CODE_PL_PART_REGEX = new RegExp('^\\d{0,2}(-(\\d{0,3})?)?$', 'gi');

type InputOwnPropsType = {|
  action?: IconPropType,
  alwaysVisibleRows?: number,
  autocompleteType?: string,
  availableRows?: number,
  icon?: IconPropType,
  onClick?: SyntheticEvent<HTMLInputElement | HTMLTextAreaElement>,
  placeholder?: string,
  rows?: number
|};

type WrappedInputType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithInputFormattingExtensionType,
  ...WithMaskExtensionType,
  ...InputOwnPropsType
|};

const Input = React.forwardRef(
  (
    {
      action,
      alwaysVisibleRows = 1,
      autocompleteType,
      className,
      controlElement,
      controlRef,
      disabled,
      icon,
      messageElement = null,
      onClear,
      onClick,
      placeholder,
      readonly,
      requiredElement = null,
      rows = 1,
      setValue,
      value
    }: WrappedInputType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { cache, setParentCache } = React.useContext(ParentCacheContext);

    React.useEffect(() => {
      if (setParentCache) {
        setParentCache(value);
      }
    }, [value, setParentCache]);

    const focusInput = React.useCallback(() => {
      const el = controlRef.current;
      if (el) {
        el.focus();
      }
    }, []);

    const getInputHeight = React.useCallback((rows: number) => {
      const el = controlRef.current;
      if (el) {
        const { borderBottomWidth, borderTopWidth, lineHeight, paddingBottom, paddingTop } = getComputedStyle(el);
        return unitize.revert(lineHeight) * rows + [
          borderBottomWidth,
          borderTopWidth,
          paddingBottom,
          paddingTop
        ].reduce((sum, value) => sum + unitize.revert(value), 0);
      }
      return 0;
    }, []);

    const handleOnClick = React.useCallback((e: SyntheticEvent<HTMLInputElement | HTMLTextAreaElement>) => {
      if (onClick) {
        onClick(e);
      }
    }, [onClick]);

    const handleOnInput = React.useCallback((e: SyntheticChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
      if (controlElement.props.onInput) {
        controlElement.props.onInput(e);
      }
      if (controlElement.props.onChange) {
        controlElement.props.onChange(e);
      }
      if (rows > 1) {
        const el = controlRef.current;
        if (el) {
          el.style.height = '';
          const { borderBottomWidth, borderTopWidth } = getComputedStyle(el);
          const height = el.scrollHeight + unitize.revert(borderTopWidth) + unitize.revert(borderTopWidth);
          setElementStyle(el, { height: unitize(height) });
        }
      }
    }, [controlElement, rows]);

    React.useEffect(() => {
      const el = controlRef.current;
      if (el) {
        const height = getInputHeight(alwaysVisibleRows);
        setElementStyle(el, { minHeight: unitize(height) });
      }
    }, [alwaysVisibleRows, getInputHeight]);

    React.useEffect(() => {
      const el = controlRef.current;
      if (el) {
        const height = getInputHeight(rows);
        setElementStyle(el, { maxHeight: unitize(height) });
      }
    }, [getInputHeight, rows]);

    const handleActionOnClick = React.useCallback((...args: Array<any>) => {
      if (action.props.onClick) {
        action.props.onClick(...args);
        focusInput();
      }
    }, [action, focusInput]);

    const actionElement = React.useMemo(() => action && !disabled ? React.cloneElement(action, {
      className: cx(styles.Action, action.props.className),
      onClick: handleActionOnClick,
      ...{
        [ComponentNames.CharIcon]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.ColorIcon]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome.Brand]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome.Regular]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: action.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.Icon]: {
          width: action.props.width || DEFAULT_ICON_SIZE
        }
      }[action.type.componentId]
    }) : null, [action, disabled, handleActionOnClick]);

    const handleOnClear = React.useCallback(() => {
      onClear();
      setTimeout(() => {
        focusInput();
      });
    }, [focusInput, onClear]);

    const handleOnScroll = React.useMemo(() => debounced(
      (e) => {
        const el = controlRef.current;
        if (el) {
          const { lineHeight } = getComputedStyle(el);
          const multiple = unitize.revert(lineHeight);
          el.scrollTop = Math.round(el.scrollTop / multiple) * multiple;
        }
      },
      ON_SCROLL_DEBOUNCE_TIME
    ), [getInputHeight]);

    const clearElement = React.useMemo(() => value && onClear ? (
      <FontAwesome
        className={styles.Clear}
        id="times"
        onClick={handleOnClear}
        innerSize={CLEAR_ICON_INNER_SIZE}
        size={DEFAULT_ICON_SIZE}
      />
    ) : null, [handleOnClear, onClear, value]);

    const iconElement = React.useMemo(() => icon ? React.cloneElement(icon, {
      className: cx(styles.Icon, icon.props.className),
      ...{
        [ComponentNames.CharIcon]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.ColorIcon]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome.Brand]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.FontAwesome.Regular]: {
          innerSize: DEFAULT_ICON_INNER_SIZE,
          size: icon.props.size || DEFAULT_ICON_SIZE
        },
        [ComponentNames.Icon]: {
          width: icon.props.width || DEFAULT_ICON_SIZE
        }
      }[icon.type.componentId]
    }) : null, [icon]);

    const overwrittenControlElement = React.useMemo(() => React.cloneElement(controlElement, {
      autoComplete: autocompleteType,
      onChange: undefined,
      onClick: (disabled || readonly) ? undefined : handleOnClick,
      onInput: (disabled || readonly) ? undefined : handleOnInput,
      onScroll: handleOnScroll,
      placeholder,
      value: cache || value
    }), [autocompleteType, cache, controlElement, handleOnClick, handleOnScroll, placeholder, value]);
    
    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Input,
          actionElement && styles.Input__WithAction,
          clearElement && value && styles.Input__WithClear,
          iconElement && styles.Input__WithIcon,
          !value && placeholder && styles.Input__Placeholder,
          className
        )}
      >
        {iconElement}
        <span className={styles.Before} onClick={focusInput} />
        {overwrittenControlElement}
        <span className={styles.After} onClick={focusInput} />
        <span className={styles.Actions}>
          {clearElement}
          {actionElement}
        </span>
        {messageElement}
        {requiredElement}
      </div>
    );
  }
);

const Password = React.forwardRef(({ className, ...props }: WrappedInputType, forwardedRef) => (
  <Input className={cx(styles.Password, className)} {...props} type={INPUT_TYPES.PASSWORD} />
));

const InputNumber = React.forwardRef(({ className, ...props }: WrappedInputType, forwardedRef) => {
  const { disabled, readonly, setValue, value } = props;

  const onChangeValue = React.useCallback(
    (delta: number) => {
      setValue((old: number) => {
        return old + delta;
      });
    },
    [setValue]
  );

  const handleOnIncrement = React.useCallback(() => {
    onChangeValue(1);
  }, [onChangeValue]);

  const handleOnDecrement = React.useCallback(() => {
    onChangeValue(-1);
  }, [onChangeValue]);

  return (
    <div ref={forwardedRef} className={cx(styles.InputNumber, className)}>
      <button type="button" className={styles.Decrement} onClick={(disabled || readonly) ? undefined : handleOnDecrement}>
        -
      </button>
      <Input
        {...props}
        availableRows={1}
        rows={1}
      />
      <button type="button" className={styles.Increment} onClick={(disabled || readonly) ? undefined : handleOnIncrement}>
        +
      </button>
    </div>
  );
});

export type InputType = {|
  ...AsComponentType,
  ...AsControlType,
  ...WithInputFormattingType,
  ...WithMaskType,
  ...InputOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(
      ComponentNames.Input
    )(
      AsControl({
        allowMultiRow: true,
        controlType: INPUT_TYPES.TEXT,
        defaultValue: '',
        sanitizeValueForControl: (value: string) => value,
        style: { key: 'Input', styles }
      })(
        Input
      )
    ), {
    Date: AsComponent(
      ComponentNames.Input.Date
    )(
      AsControl({
        controlType: INPUT_TYPES.TEXT,
        defaultValue: '',
        sanitizeValueForControl: (value: string) => value,
        style: { key: 'Input', styles }
      })(
        WithMask({
          fixedCharacters: [4, 7],
          mask: '____-__-__',
          regex: DATE_PART_REGEX
        })(
          Input
        )
      )
    ),
    DateTime: AsComponent(
      ComponentNames.Input.DateTime
    )(
      AsControl({
        controlType: INPUT_TYPES.TEXT,
        defaultValue: '',
        sanitizeValueForControl: (value: string) => value,
        style: { key: 'Input', styles }
      })(
        WithMask({
          fixedCharacters: [4, 7, 10, 13],
          mask: '____-__-__ __:__',
          regex: DATE_TIME_PART_REGEX
        })(
          Input
        )
      )
    ),
    Number: AsComponent(ComponentNames.Input.Number)(AsControl({
      allowMultiRow: false,
      controlType: INPUT_TYPES.TEXT,
      defaultValue: 0,
      sanitizeValueForControl: (value: number) => value.toString(),
      style: { key: 'Input', styles }
    })(
      WithInputFormatting({ format: (value: string) => value, regex: INTEGER_REGEX, sanitize: (value: string) => value })(
        InputNumber
      )
    )),
    Password: AsComponent(
      ComponentNames.Input.Password
    )(
      AsControl({
        controlType: INPUT_TYPES.PASSWORD,
        defaultValue: '',
        sanitizeValueForControl: (value: string) => value,
        style: { key: 'Input', styles }
      })(
        Input
      )
    ),
    PhoneNumber: AsComponent(
      ComponentNames.Input.PhoneNumber
    )(
      AsControl({
        controlType: INPUT_TYPES.TEXT,
        defaultValue: '',
        sanitizeValueForControl: (value: string) => value,
        style: { key: 'Input', styles }
      })(
        WithInputFormatting({
          format: formatPhoneNumber,
          regex: PHONE_NUMBER_PART_REGEX,
          sanitize: formatPhoneNumber.sanitize
        })(
          Input
        )
      )
    ),
    PhoneNumberPL: AsComponent(
      ComponentNames.Input.PhoneNumber
    )(
      AsControl({
        controlType: INPUT_TYPES.TEXT,
        defaultValue: '',
        sanitizeValueForControl: (value: string) => value,
        style: { key: 'Input', styles }
      })(
        WithInputFormatting({
          format: formatPhoneNumber,
          regex: PHONE_NUMBER_PL_PART_REGEX,
          sanitize: formatPhoneNumber.sanitize
        })(
          Input
        )
      )
    ),
    PostalCodePL: AsComponent(
      ComponentNames.Input.PostalCodePL
    )(
      AsControl({
        controlType: INPUT_TYPES.TEXT,
        defaultValue: '',
        sanitizeValueForControl: (value: string) => value,
        style: { key: 'Input', styles }
      })(
        WithMask({
          fixedCharacters: [2],
          mask: '__-___',
          regex: POSTAL_CODE_PL_PART_REGEX
        })(
          Input
        )
      )
    ),
    Time: AsComponent(
      ComponentNames.Input.Time
    )(
      AsControl({
        controlType: INPUT_TYPES.TEXT,
        defaultValue: '',
        sanitizeValueForControl: (value: string) => value,
        style: { key: 'Input', styles }
      })(
        WithMask({
          fixedCharacters: [2],
          mask: '__:__',
          regex: TIME_PART_REGEX
        })(
          Input
        )
      )
    )
  }, {
    AUTOCOMPLETE_TYPES,
    INPUT_TYPES
  }): React.ComponentType<InputType> & {
    Date: React.ComponentType<InputType>,
    DateTime: React.ComponentType<InputType>,
    Integer: React.ComponentType<InputType>,
    Number: React.ComponentType<InputType>,
    Password: React.ComponentType<InputType>,
    PhoneNumber: React.ComponentType<InputType>,
    PhoneNumberPL: React.ComponentType<InputType>,
    PostalCodePL: React.ComponentType<InputType>,
    Time: React.ComponentType<InputType>
  } & {
    AUTOCOMPLETE_TYPES: Object,
    INPUT_TYPES: Object
  }
);
