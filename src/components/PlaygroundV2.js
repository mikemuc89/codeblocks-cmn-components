/* @flow */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, unitize } from '@omnibly/codeblocks-cmn-utils';
import LayerRootContainer from '../containers/LayerRootContainer';
import MainSizeContext from '../contexts/MainSizeContext';
import { type ReactForwardedRefType } from '../hocs/components/AsComponent';
import Card from './Card';
import Dialog from './Dialog';
import Field from './Field';
import Hamburger from './Hamburger';
import Label from './Label';
import View from './View';
import styles from './PlaygroundV2.scss';

const PANE_TITLE_HEIGHT = 24;

type ContainerType = {|
  children: ReactChildren
|};

const copyStylesToFrame = (frameDocument: HTMLElement) => {
  document.head.querySelectorAll('link,style').forEach((node: HTMLLinkElement | HTMLStyleElement) => {
    frameDocument.head.appendChild(node.cloneNode(true));
  });
};

const CardContainer = ({ children }: ContainerType) => (
  <View>
    <Card>{children}</Card>
  </View>
);

const DialogMiniContainer = ({ children }: ContainerType) => <Dialog mini>{children}</Dialog>;

const DialogNanoContainer = ({ children }: ContainerType) => <Dialog nano>{children}</Dialog>;

const DialogContainer = ({ children }: ContainerType) => <Dialog>{children}</Dialog>;

const FieldContainer = ({ children }: ContainerType) => <Field>{children}</Field>;

const FieldSideLabelContainer = ({ children }: ContainerType) => (
  <Field label={<Label position={Label.POSITIONS.LEFT}>Label</Label>}>{children}</Field>
);

const FieldTopLabelContainer = ({ children }: ContainerType) => (
  <Field label={<Label position={Label.POSITIONS.TOP}>Label</Label>}>{children}</Field>
);

const HamburgerContainer = ({ children }: ContainerType) => <Hamburger>{children}</Hamburger>;

const ViewContainer = ({ children }: ContainerType) => <View>{children}</View>;

const ViewPaddedContainer = ({ children }: ContainerType) => <View padding={16}>{children}</View>;

const PARENT_ITEM_KEYS = Object.freeze({
  CARD: 'CARD',
  DIALOG_MINI: 'DIALOG_MINI',
  DIALOG_NANO: 'DIALOG_NANO',
  DIALOG_NORMAL: 'DIALOG_NORMAL',
  FIELD: 'FIELD',
  FIELD_LABEL_SIDE: 'FIELD_LABEL_SIDE',
  FIELD_LABEL_TOP: 'FIELD_LABEL_TOP',
  HAMBURGER: 'HAMBURGER',
  VIEW: 'VIEW',
  VIEW_PADDED: 'VIEW_PADDED'
});

const PARENT_ITEM_KEYS_TO_VALUE = Object.freeze({
  CARD: CardContainer,
  DIALOG_MINI: DialogMiniContainer,
  DIALOG_NANO: DialogNanoContainer,
  DIALOG_NORMAL: DialogContainer,
  FIELD: FieldContainer,
  FIELD_LABEL_SIDE: FieldSideLabelContainer,
  FIELD_LABEL_TOP: FieldTopLabelContainer,
  HAMBURGER: HamburgerContainer,
  VIEW: ViewContainer,
  VIEW_PADDED: ViewPaddedContainer
});

const PlaygroundExampleParentContext = React.createContext(PARENT_ITEM_KEYS.VIEW);

export type PlaygroundType = {|
  children: ReactChildren,
  className?: string,
  label?: string,
  parent?: $Values<typeof PARENT_ITEM_KEYS>
|};

const Playground = ({ children, className, label, parent = PARENT_ITEM_KEYS.VIEW }: PlaygroundType) => {
  const { headerHeight, menuHeight } = React.useContext(MainSizeContext);

  return (
    <div className={cx(styles.Playground)}>
      <div className={styles.Title} style={{ top: unitize(headerHeight + menuHeight) }}>
        <div className={styles.Label}>{label}</div>
      </div>
      <PlaygroundExampleParentContext.Provider value={parent}>
        {children}
      </PlaygroundExampleParentContext.Provider>
    </div>
  );
};

export type ExampleType = {|
  children: ReactChildren,
  height?: number,
  spacer?: boolean,
  label?: string
|};

const Example = ({ center, children, height, spacer, label }: ExampleType, forwardedRef: ReactForwardedRefType) => {
  const { footerHeight, headerHeight, menuHeight } = React.useContext(MainSizeContext);

  return (
    <div className={cx(styles.Example, center && styles.Example__Center, '__Playground')} style={{ height: `calc(100vh - ${unitize(footerHeight + headerHeight + menuHeight)})` }}>
      {label && <label className={styles.Label}>{label}</label>}
      <div className={styles.Content} style={{ ...(height ? { height: unitize(height) } : {}) }}>
        {children}
      </div>
      {spacer && <div style={{ height: unitize(spacer) }} />}
    </div>
  );
};

export type SectionType = {|
  children: ReactChildren,
  className?: string,
  label?: string,
  parent?: $Values<typeof PARENT_ITEM_KEYS>
|};

const Section = ({ children, className, label, parent }: SectionType) => {
  const contextParent = React.useContext(PlaygroundExampleParentContext);
  const actualParent = parent || contextParent;
  const frameLoaded = React.useRef(false);
  const { headerHeight, menuHeight, footerHeight } = React.useContext(MainSizeContext);

  const [frameDocument, setFrameDocument] = React.useState(null);

  const handleOnLoad = React.useCallback(
    (e: Event) => {
      if (!e.defaultPrevented && !frameLoaded.current) {
        const frameDocument = e.target.contentDocument;
        setFrameDocument(frameDocument);
        copyStylesToFrame(frameDocument);
        frameLoaded.current = true;
        frameDocument.documentElement.style.overflowX = 'hidden';
      }
    },
    [setFrameDocument]
  );

  const viewHeightDeductions = headerHeight + menuHeight + footerHeight;

  const ParentComponent = PARENT_ITEM_KEYS_TO_VALUE[actualParent];

  const inFrameElement = (
    <LayerRootContainer>
      <ParentComponent>{children}</ParentComponent>
    </LayerRootContainer>
  );

  return (
    <div className={cx(styles.Section)}>
      {label && (
        <div className={styles.Panes}>
          <div className={styles.Label}>{label}</div>
        </div>
      )}
      <div
        className={styles.FrameWrapper}
        style={{
          height: `calc(100vh - ${unitize(viewHeightDeductions + (label ? PANE_TITLE_HEIGHT : 0) + PANE_TITLE_HEIGHT)})`
        }}
      >
        <iframe srcDoc="<!DOCTYPE html>" className={styles.Frame} onLoad={handleOnLoad}>
          {frameDocument && ReactDOM.createPortal(inFrameElement, frameDocument.body)}
        </iframe>
      </div>
    </div>
  );
};

export default (
  Object.assign(Playground, {
    Example,
    Page: ({ children, ...props }: ExampleType) => (
      <Example {...props} center>{children}</Example>
    ),
    Section
  }, {
    PARENT_ITEM_KEYS
  }): React.ComponentType<PlaygroundType> & {
    Example: React.ComponentType<ExampleType>,
    Page: React.ComponentType<ExampleType>,
    Section: React.ComponentType<SectionType>
  } & {
    PARENT_ITEM_KEYS: Object
  }
);
