/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import Dimmer from './Dimmer';
import styles from './Layer.scss';

const LAYER_KINDS = Object.freeze({
  CLOCK: 'Layer.KINDS.CLOCK',
  FAB: 'Layer.KINDS.FAB',
  ITEMS: 'Layer.KINDS.ITEMS',
  TEXT: 'Layer.KINDS.TEXT'
});

export const LAYER_MODES = Object.freeze({
  FAB: 'WithLayer.LAYER_MODES.FAB',
  HORIZONTAL: 'WithLayer.LAYER_MODES.HORIZONTAL',
  ORIGIN: 'WithLayer.LAYER_MODES.ORIGIN',
  VERTICAL: 'WithLayer.LAYER_MODES.VERTICAL'
});

const LAYER_KIND_TO_CLASS = Object.freeze({
  [LAYER_KINDS.CLOCK]: styles.Layer__Clock,
  [LAYER_KINDS.FAB]: styles.Layer__Fab,
  [LAYER_KINDS.ITEMS]: styles.Layer__Items,
  [LAYER_KINDS.TEXT]: styles.Layer__Text
});

const LAYER_MODE_TO_CLASS = Object.freeze({
  [LAYER_MODES.FAB]: styles.Layer__Mode_Fab,
  [LAYER_MODES.HORIZONTAL]: styles.Layer__Mode_Horizontal,
  [LAYER_MODES.ORIGIN]: styles.Layer__Mode_Origin,
  [LAYER_MODES.VERTICAL]: styles.Layer__Mode_Vertical
});

export type LayerContentContextType = ?React.Element<any>;

export const LayerContentContext = React.createContext(null);

type LayerOwnPropsType = {|
  children: ReactChildren,
  contentElementRef: React.ElementRef<HTMLElement>,
  dimmerMode?: $Values<typeof Dimmer.DIMMER_MODES>,
  mode?: $Values<typeof LAYER_MODES>,
  kind: $Values<typeof LAYER_KINDS>
|};

type WrappedLayerType = {|
  ...AsComponentExtensionType,
  ...LayerOwnPropsType
|};

const Layer = React.forwardRef(
  (
    { children, className, contentElementRef, dimmerMode = Dimmer.DIMMER_MODES.MOBILE, kind, mode }: WrappedLayerType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const content = React.useContext(LayerContentContext);

    return (
      <>
        <div ref={forwardedRef} className={cx(styles.Layer, LAYER_KIND_TO_CLASS[kind], LAYER_MODE_TO_CLASS[mode || LAYER_MODES.VERTICAL], className)}>
          {content &&
            React.cloneElement(content, {
              className: styles.Action,
              ref: contentElementRef
            })}
          <div className={styles.Content}>{children}</div>
        </div>
        <Dimmer mode={dimmerMode} />
      </>
    );
  }
);

export type LayerType = {|
  ...AsComponentType,
  ...LayerOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Layer)(Layer), {
    DIMMER_MODES: Dimmer.MODES,
    KINDS: LAYER_KINDS
  }): React.ComponentType<LayerType> & {
    DIMMER_MODES: Object,
    KINDS: Object
  }
);
