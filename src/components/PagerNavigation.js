/* @flow */
import * as React from 'react';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, repeatPattern } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import FontAwesome from './FontAwesome';
import styles from './PagerNavigation.scss';

const PAGE_TYPES = Object.freeze({
  BACKWARD: 'PagerNavigation.PAGE_TYPES.BACKWARD',
  CURRENT: 'PagerNavigation.PAGE_TYPES.CURRENT',
  DIVIDER: 'PagerNavigation.PAGE_TYPES.DIVIDER',
  FAST_BACKWARD: 'PagerNavigation.PAGE_TYPES.FAST_BACKWARD',
  FAST_FORWARD: 'PagerNavigation.PAGE_TYPES.FAST_FORWARD',
  FORWARD: 'PagerNavigation.PAGE_TYPES.FORWARD',
  PAGE: 'PagerNavigation.PAGE_TYPES.PAGE'
});

const PAGE_ICON_COMMON_PROPS = {
  border: false,
  innerSize: 14,
  size: 30
};

const SPECIAL_ELEMENTS = {
  [PAGE_TYPES.BACKWARD]: () => <FontAwesome id="angle-left" {...PAGE_ICON_COMMON_PROPS} />,
  [PAGE_TYPES.DIVIDER]: () => <FontAwesome id="ellipsis-h" {...PAGE_ICON_COMMON_PROPS} />,
  [PAGE_TYPES.FAST_BACKWARD]: () => <FontAwesome id="angle-double-left" {...PAGE_ICON_COMMON_PROPS} />,
  [PAGE_TYPES.FAST_FORWARD]: () => <FontAwesome id="angle-double-right" {...PAGE_ICON_COMMON_PROPS} />,
  [PAGE_TYPES.FORWARD]: () => <FontAwesome id="angle-right" {...PAGE_ICON_COMMON_PROPS} />
};

const getVisiblePages = ({
  boundary,
  current,
  divider,
  fastForward,
  forward,
  maximum,
  span
}: {
  boundary?: number,
  current: number,
  divider?: boolean,
  fastForward?: boolean,
  forward?: boolean,
  maximum?: number,
  span?: number
}) => {
  const fastBackwardVisible = fastForward && current > 2;
  const backwardVisible = forward && current > 1;
  const forwardVisible = forward && current < maximum;
  const fastForwardVisible = fastForward && current < maximum - 1;

  const hasPossibleDivider = divider && boundary !== 0;
  const leftDividerVisible = hasPossibleDivider && current - span - boundary > 2;
  const rightDividerVisible = hasPossibleDivider && maximum - current - span - boundary > 1;

  const allElementsCount = Math.min(
    1 + (fastForward ? 2 : 0) + (forward ? 2 : 0) + (hasPossibleDivider ? boundary * 2 + 2 : 0) + span * 2,
    maximum +
      (fastForwardVisible ? 1 : 0) +
      (forwardVisible ? 1 : 0) +
      (fastBackwardVisible ? 1 : 0) +
      (backwardVisible ? 1 : 0)
  );

  const startElements = [
    ...(fastBackwardVisible ? [{ page: 1, type: PAGE_TYPES.FAST_BACKWARD }] : []),
    ...(backwardVisible ? [{ page: current - 1, type: PAGE_TYPES.BACKWARD }] : []),
    ...(leftDividerVisible ? repeatPattern(boundary, (idx: number) => ({ page: 1 + idx, type: PAGE_TYPES.PAGE })) : []),
    ...(leftDividerVisible ? [{ page: null, type: PAGE_TYPES.DIVIDER }] : [])
  ];

  const endElements = [
    ...(rightDividerVisible ? [{ page: null, type: PAGE_TYPES.DIVIDER }] : []),
    ...(rightDividerVisible
      ? repeatPattern(boundary, (idx: number) => ({ page: maximum - boundary + 1 + idx, type: PAGE_TYPES.PAGE }))
      : []),
    ...(forwardVisible ? [{ page: current + 1, type: PAGE_TYPES.FORWARD }] : []),
    ...(fastForwardVisible ? [{ page: maximum, type: PAGE_TYPES.FAST_FORWARD }] : [])
  ];

  const fillPagesCount = allElementsCount - startElements.length - endElements.length;
  const firstPage = Math.min(
    maximum - fillPagesCount - (rightDividerVisible ? boundary + 1 : 0) + 1,
    Math.max(1, current - span - (hasPossibleDivider && !leftDividerVisible ? boundary + 1 : 0))
  );

  const pagesElements =
    fillPagesCount > 0
      ? repeatPattern(fillPagesCount, (index: number) => {
          const page = firstPage + index;
          return { page, type: page === current ? PAGE_TYPES.CURRENT : PAGE_TYPES.PAGE };
        })
      : [];
  return [...startElements, ...pagesElements, ...endElements];
};

type PageOwnPropsType = {|
  disabled?: boolean,
  onNavigate?: () => void,
  page?: number,
  type: $Values<typeof PAGE_TYPES>
|};

type WrappedPageType = {|
  ...AsComponentExtensionType,
  ...PageOwnPropsType
|};

const Page = React.forwardRef(
  ({ className, disabled, onNavigate, page, type }: WrappedPageType, forwardedRef: ReactForwardedRefType) => {
    const unclickable = disabled || type === PAGE_TYPES.DIVIDER;
    const Component = unclickable ? 'span' : 'a';

    const handleOnNavigate = React.useCallback(() => {
      onNavigate(page);
    }, [onNavigate, page]);

    const childEl = React.useMemo(() => {
      if ([PAGE_TYPES.CURRENT, PAGE_TYPES.PAGE].includes(type)) {
        return <span className={styles.PageNumber}>{page}</span>;
      }

      return SPECIAL_ELEMENTS[type]();
    }, [page, type]);

    return (
      <Component
        ref={forwardedRef}
        className={cx(
          styles.Page,
          cx.control(styles, 'Page', { disabled }),
          type === PAGE_TYPES.DIVIDER && styles.Page__Divider,
          type === PAGE_TYPES.CURRENT && styles.Page__Current,
          className
        )}
        href={unclickable ? undefined : '#'}
        onClick={unclickable ? undefined : handleOnNavigate}
      >
        {childEl}
      </Component>
    );
  }
);

type PagerNavigationOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  boundary?: number,
  current: number,
  disabled?: boolean,
  divider?: boolean,
  fastForward?: boolean,
  forward?: boolean,
  maximum: number,
  onNavigate?: () => void,
  span?: number
|};

type WrappedPagerNavigationType = {|
  ...AsComponentExtensionType,
  ...PagerNavigationOwnPropsType
|};

const PagerNavigation = React.forwardRef(
  (
    {
      align = ALIGN_TYPES.CENTER,
      boundary = 1,
      className, // eslint-disable-line react/prop-types
      current,
      disabled,
      divider = true,
      fastForward = false,
      forward = false,
      maximum,
      onNavigate,
      span = 3
    }: WrappedPagerNavigationType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const visiblePages = React.useMemo(
      () => getVisiblePages({ boundary, current, divider, fastForward, forward, maximum, span }),
      [boundary, current, divider, fastForward, forward, maximum, span]
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(styles.PagerNavigation, cx.alignSingle(styles, 'PagerNavigation', { align }), className)}
      >
        {/* eslint-disable-next-line react/no-unused-prop-types */}
        {visiblePages.map(({ page, type }: { page: number, type: $Values<typeof PAGE_TYPES> }, index: number) => (
          <Page key={index} disabled={disabled} onNavigate={onNavigate} page={page} type={type} />
        ))}
      </div>
    );
  }
);

export type PagerNavigationType = {|
  ...AsComponentType,
  ...PagerNavigationOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.PagerNavigation)(PagerNavigation), {
    ALIGN_TYPES
  }): React.ComponentType<PagerNavigationType> & {
    ALIGN_TYPES: Object
  }
);
