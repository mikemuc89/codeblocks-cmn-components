/* #__flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Form.scss';

type FormOwnPropsType = {|
  border?: boolean,
  children: ReactChildren,
  onChange: (e: Event) => void,
  onSubmit: (e: Event) => void,
  title?: string
|};

type WrappedFormType = {|
  ...AsComponentExtensionType,
  ...FormOwnPropsType
|};

const Form = React.forwardRef(
  ({ border, children, className, onChange, onSubmit, title }: WrappedFormType, forwardedRef: ReactForwardedRefType) => (
    <form className={cx(styles.Form, border && styles.Form__Border, className)} onChange={onChange} onSubmit={onSubmit}>
      {title && <div className={styles.Title}>{title}</div>}
      <div className={styles.Content}>{children}</div>
    </form>
  )
);

export type FormType = {|
  ...AsComponentType,
  ...FormOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Form)(Form), {}): React.ComponentType<FormType>
);
