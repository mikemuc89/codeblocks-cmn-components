/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import useOpenState from '../hooks/useOpenState';
import FontAwesome from './FontAwesome';
import CONFIG from '../components/style-config';
import styles from './Pane.scss';

const {
  colors: {
    background: BACKGROUND_COLORS
  }
} = CONFIG;

type PaneOwnPropsType = {|
  canClose?: boolean,
  children: ReactChildren,
  height?: number,
  onClose: () => void,
  open?: boolean
|};

type WrappedPaneType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...PaneOwnPropsType
|};

const Pane = React.forwardRef(
  ({ backgroundElementRef, border, canClose, children, className, height, onClose, open: initiallyOpen }: WrappedPaneType, forwardedRef: ReactForwardedRefType) => {
    const { open, hide } = useOpenState({ open: initiallyOpen });
    const { innerWidth } = React.useContext(MainSizeContext);

    const handleOnClose = React.useCallback(() => {
      hide();
      if (onClose) {
        onClose();
      }
    }, [hide, onClose]);

    return (
      open && (
        <div
          ref={mergeRefs(forwardedRef, backgroundElementRef)}
          className={cx(styles.Pane, border && styles.Pane__Border, className)}
          style={{ ...(height ? { height: unitize(height) } : {}) }}
        >
          <div className={styles.Content} style={{ ...(innerWidth ? { width: unitize(innerWidth) } : {}) }}>
            {canClose && (
              <FontAwesome className={styles.Close} id="times" size={24} innerSize={16} onClick={handleOnClose} />
            )}
            {children}
          </div>
        </div>
      )
    );
  }
);

const Menu = React.forwardRef(({ children, className, ...props }: WrappedPaneType, forwardedRef: ReactForwardedRefType) => (
  <Pane ref={forwardedRef} className={cx(styles.Menu, className)} border {...props}>{children}</Pane>
));

export type PaneType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...PaneOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Pane)(WithBackground({
      defaultBackgroundColor: BACKGROUND_COLORS.pane.normal,
      defaultBackgroundColorFocus: BACKGROUND_COLORS.pane.focus,
      defaultBackgroundColorHover: BACKGROUND_COLORS.pane.hover
    })(Pane)),
    {
      Menu: AsComponent(ComponentNames.Pane.Menu)(Menu)
    }
  ): React.ComponentType<PaneType> & {
    Menu: React.ComponentType<PaneType>
  }
);
