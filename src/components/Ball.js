/* #__flow */
import * as React from 'react';
import { SVG_COMPONENTS } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, unitize } from '@omnibly/codeblocks-cmn-utils';
import { prepareSvgDefs, prepareSvgLayerParams, type SvgLayerType } from '@omnibly/codeblocks-cmn-utils/src/utils/svg';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Ball.scss';

const DEFAULTS = Object.freeze({
  BACKGROUND_COLOR: '#fff',
  COMPONENT_SIZE: 40,
  SHADOW_COLOR: '#000',
  SHADOW_MODE: 'SourceGraphic',
  SHADOW_OPACITY: 0.5,
  SHADOW_PERCENT: 20,
  SHADOW_STD_DEV: 4,
  STROKE_COLOR: '#000',
  SVG_SIZE: 120
});

const IDS = Object.freeze({
  CLIP_PATH: 'CLIP_PATH',
  CONTENT: 'CONTENT',
  OUTLINE: 'OUTLINE',
  SHADOW_FILTER: 'SHADOW_FILTER'
});

const LAYER_KINDS = Object.freeze({
  CIRCLE: 'CIRCLE',
  CIRCLE_BOTTOM: 'CIRCLE_BOTTOM',
  CIRCLE_BOTTOM_LEFT: 'CIRCLE_BOTTOM_LEFT',
  CIRCLE_BOTTOM_RIGHT: 'CIRCLE_BOTTOM_RIGHT',
  CIRCLE_LEFT: 'CIRCLE_LEFT',
  CIRCLE_RIGHT: 'CIRCLE_RIGHT',
  CIRCLE_TOP: 'CIRCLE_TOP',
  CIRCLE_TOP_LEFT: 'CIRCLE_TOP_LEFT',
  CIRCLE_TOP_RIGHT: 'CIRCLE_TOP_RIGHT',
  CUSTOM_CIRCLE: 'CUSTOM_CIRCLE',
  CUSTOM_LINE: 'CUSTOM_LINE',
  CUSTOM_PATH: 'CUSTOM_PATH',
  DIAGONAL_LINE_LR: 'DIAGONAL_LINE_LR',
  DIAGONAL_LINE_RL: 'DIAGONAL_LINE_RL',
  HORIZONTAL_LINE_BOTTOM: 'HORIZONTAL_LINE_BOTTOM',
  HORIZONTAL_LINE_CENTER: 'HORIZONTAL_LINE_CENTER',
  HORIZONTAL_LINE_TOP: 'HORIZONTAL_LINE_TOP',
  VERTICAL_LINE_CENTER: 'VERTICAL_LINE_CENTER',
  VERTICAL_LINE_LEFT: 'VERTICAL_LINE_LEFT',
  VERTICAL_LINE_RIGHT: 'VERTICAL_LINE_RIGHT'
});

const svgSize = DEFAULTS.SVG_SIZE;
const halfSize = svgSize / 2;
const radius = (svgSize * 5) / 12;
const paddingSize = halfSize - radius;

const LAYER_PROPERTIES = Object.freeze({
  [LAYER_KINDS.CIRCLE]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: halfSize,
    cy: halfSize
  },
  [LAYER_KINDS.CIRCLE_BOTTOM]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: halfSize,
    cy: svgSize
  },
  [LAYER_KINDS.CIRCLE_BOTTOM_LEFT]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: 0,
    cy: svgSize
  },
  [LAYER_KINDS.CIRCLE_BOTTOM_RIGHT]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: svgSize,
    cy: svgSize
  },
  [LAYER_KINDS.CIRCLE_LEFT]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: 0,
    cy: halfSize
  },
  [LAYER_KINDS.CIRCLE_RIGHT]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: svgSize,
    cy: halfSize
  },
  [LAYER_KINDS.CIRCLE_TOP]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: halfSize,
    cy: 0
  },
  [LAYER_KINDS.CIRCLE_TOP_LEFT]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: 0,
    cy: 0
  },
  [LAYER_KINDS.CIRCLE_TOP_RIGHT]: {
    component: SVG_COMPONENTS.CIRCLE,
    cx: svgSize,
    cy: 0
  },
  [LAYER_KINDS.CUSTOM_CIRCLE]: {
    component: SVG_COMPONENTS.CIRCLE
  },
  [LAYER_KINDS.CUSTOM_LINE]: {
    component: SVG_COMPONENTS.LINE
  },
  [LAYER_KINDS.CUSTOM_PATH]: { component: SVG_COMPONENTS.PATH },
  [LAYER_KINDS.DIAGONAL_LINE_LR]: {
    component: SVG_COMPONENTS.LINE,
    x1: 0,
    x2: svgSize,
    y1: 0,
    y2: svgSize
  },
  [LAYER_KINDS.DIAGONAL_LINE_RL]: {
    component: SVG_COMPONENTS.LINE,
    x1: svgSize,
    x2: 0,
    y1: svgSize,
    y2: 0
  },
  [LAYER_KINDS.HORIZONTAL_LINE_BOTTOM]: {
    component: SVG_COMPONENTS.LINE,
    x1: 0,
    x2: svgSize,
    y1: svgSize - paddingSize,
    y2: svgSize - paddingSize
  },
  [LAYER_KINDS.HORIZONTAL_LINE_CENTER]: {
    component: SVG_COMPONENTS.LINE,
    x1: 0,
    x2: svgSize,
    y1: halfSize,
    y2: halfSize
  },
  [LAYER_KINDS.HORIZONTAL_LINE_TOP]: {
    component: SVG_COMPONENTS.LINE,
    x1: 0,
    x2: svgSize,
    y1: paddingSize,
    y2: paddingSize
  },
  [LAYER_KINDS.VERTICAL_LINE_CENTER]: {
    component: SVG_COMPONENTS.LINE,
    x1: halfSize,
    x2: halfSize,
    y1: 0,
    y2: svgSize
  },
  [LAYER_KINDS.VERTICAL_LINE_LEFT]: {
    component: SVG_COMPONENTS.LINE,
    x1: paddingSize,
    x2: paddingSize,
    y1: 0,
    y2: svgSize
  },
  [LAYER_KINDS.VERTICAL_LINE_RIGHT]: {
    component: SVG_COMPONENTS.LINE,
    x1: svgSize - paddingSize,
    x2: svgSize - paddingSize,
    y1: 0,
    y2: svgSize
  }
});

const NOT_DEFINABLE = [
  LAYER_KINDS.CIRCLE,
  LAYER_KINDS.CIRCLE_BOTTOM,
  LAYER_KINDS.CIRCLE_BOTTOM_LEFT,
  LAYER_KINDS.CIRCLE_BOTTOM_RIGHT,
  LAYER_KINDS.CIRCLE_LEFT,
  LAYER_KINDS.CIRCLE_RIGHT,
  LAYER_KINDS.CIRCLE_TOP,
  LAYER_KINDS.CIRCLE_TOP_LEFT,
  LAYER_KINDS.CIRCLE_TOP_RIGHT,
  LAYER_KINDS.CUSTOM_CIRCLE,
  LAYER_KINDS.CUSTOM_LINE,
  LAYER_KINDS.CUSTOM_PATH
];

type BallOwnPropsType = {|
  backgroundColor?: string,
  layers: Array<SvgLayerType>,
  shadowColor?: string,
  size?: number,
  strokeColor?: string
|};

type WrappedBallType = {|
  ...AsComponentExtensionType,
  ...BallOwnPropsType
|};

const NotDefinableComponent = ({ id, params }: { id: string, params: Object }) =>
  (({ component, ...props }: { component: string }) =>
    ({
      circle: () => <circle {...props} {...prepareSvgLayerParams(params, { svgSize })} />,
      line: () => <line {...props} {...prepareSvgLayerParams(params, { svgSize })} />,
      path: () => <path {...props} {...prepareSvgLayerParams(params, { svgSize })} />
    }[component]()))(LAYER_PROPERTIES[id]);

const Ball = React.forwardRef(
  (
    {
      backgroundColor = DEFAULTS.BACKGROUND_COLOR,
      className,
      layers = [],
      shadowColor = DEFAULTS.SHADOW_COLOR,
      size = 40,
      strokeColor = DEFAULTS.STROKE_COLOR
    }: WrappedBallType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div ref={forwardedRef} className={cx(styles.Ball, className)}>
      <div className={styles.Content} style={{ height: unitize(size), width: unitize(size) }}>
        <svg className={styles.Svg} viewBox={`0 0 ${svgSize} ${svgSize}`} height={svgSize} width={svgSize}>
          <defs>
            <circle id={IDS.OUTLINE} cx={halfSize} cy={halfSize} r={radius} />
            <clipPath id={IDS.CLIP_PATH}>
              <use href={`#${IDS.OUTLINE}`} />
            </clipPath>
            <filter
              id={IDS.SHADOW_FILTER}
              y={`-${DEFAULTS.SHADOW_PERCENT}%`}
              height={`${100 + 2 * DEFAULTS.SHADOW_PERCENT}%`}
            >
              <feGaussianBlur in={DEFAULTS.SHADOW_MODE} stdDeviation={DEFAULTS.SHADOW_STD_DEV} />
            </filter>
            {prepareSvgDefs(layers, { LAYER_PROPERTIES, NOT_DEFINABLE })}
          </defs>

          <use
            href={`#${IDS.OUTLINE}`}
            fill={shadowColor}
            filter={`url(#${IDS.SHADOW_FILTER})`}
            opacity={DEFAULTS.SHADOW_OPACITY}
          />
          <use href={`#${IDS.OUTLINE}`} fill={backgroundColor} />

          <g id={IDS.CONTENT} clipPath={`url(#${IDS.CLIP_PATH})`}>
            {layers.map(({ id, params }: { id: string, params: Object }, idx: number) =>
              NOT_DEFINABLE.includes(id) ? (
                <NotDefinableComponent key={idx} id={id} params={params} />
              ) : (
                <use key={idx} href={`#${id}`} {...prepareSvgLayerParams(params, { svgSize })} />
              )
            )}
          </g>

          <use href={`#${IDS.OUTLINE}`} fill="transparent" stroke={strokeColor} />
        </svg>
      </div>
    </div>
  )
);

export type BallType = {|
  ...AsComponentType,
  ...BallOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Ball)(Ball), {
    LAYER_KINDS
  }): React.ComponentType<BallType> & {
    LAYER_KINDS: Object
  }
);
