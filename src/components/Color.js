/* #__flow */
import * as React from 'react';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { color, cx, getColorFormat } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import ParentCacheContext from '../contexts/ParentCacheContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { ControlCacheContext, type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import WithLayer, { LAYER_MODES, type WithLayerExtensionType, type WithLayerType, LAYER_KINDS } from '../hocs/components/WithLayer';
import useSurfaceDrag, { EVENT_KINDS, type SurfaceDragCallbackDataType } from '../hooks/useSurfaceDrag';
import Input from './Input';
import Message from './Message';
import Required from './Required';
import styles from './Color.scss';

type HsvaType = {|
  h: ?number, s: ?number, v: ?number, a: ?number
|};

type ColorOwnPropsType = {|
  border?: boolean,
|};

type WrappedColorType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType<string>,
  ...ColorOwnPropsType
|};

const INITIAL_VALUE = { h: null, s: null, v: null, a: null };

const Color = React.forwardRef(
  (
    {
      border,
      className,
      controlElement,
      disabled,
      messageElement,
      requiredElement,
      setValue,
      value
    }: WrappedColorType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const [hsv, setHsv] = React.useState(INITIAL_VALUE);

    React.useEffect(() => {
      if (getColorFormat(value) === undefined) {
        setHsv(INITIAL_VALUE)
      } else {
        const hsv = color(value).toHsv();
        setHsv(() => hsv);  
      }
    }, [setHsv, value]);

    const updateHue = React.useCallback(({ absolute: { py }, kind }: SurfaceDragCallbackDataType) => {
      const h = Math.round(Math.min(360, Math.max(0, 360 * py)));
      setHsv((old: HsvaType) => {
        const s = old.s === null ? 100 : old.s;
        const v = old.v === null ? 100 : old.v;
        const newHsv = { h, s, v, a: 1 };
        const hex = color(newHsv).toHex();
        if (kind === EVENT_KINDS.DRAG_END) {
          setValue(hex);
        }
        return newHsv;
      });
    }, [setHsv, setValue]);

    const updateColor = React.useCallback(({ absolute: { px, py }, kind }: SurfaceDragCallbackDataType) => {
      const s = Math.round(px * 100);
      const v = Math.round((1 - py) * 100);
      setHsv((old: HsvaType) => {
        const h = old.h === null ? 0 : old.h;
        const newHsv = { h, s, v, a: 1 };
        const hex = color(newHsv).toHex();
        if (kind === EVENT_KINDS.DRAG_END) {
          setValue(hex);
        }
        return newHsv;
      });
    }, [setHsv, setValue]);

    const hueRef = useSurfaceDrag({
      contain: true,
      disabled,
      onDrag: updateHue,
      onDragEnd: updateHue
    });
    const surfaceRef = useSurfaceDrag({
      contain: true,
      disabled,
      onDrag: updateColor,
      onDragEnd: updateColor
    });

    const hexFromHsv = React.useMemo(() => color(hsv).toHex(), [hsv])
    const backgroundColor = React.useMemo(() => `hsl(${hsv.h === null ? 0 : hsv.h},100%,50%)`, [hsv.h]);
    const previewColor = React.useMemo(() => hsv.h === null ? 'transparent' : hexFromHsv, [hexFromHsv, hsv.h]);

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Color,
          border && styles.Color__Border,
          className
        )}
      >
        {controlElement}
        <div className={styles.Content}>
          <div ref={surfaceRef} className={styles.Surface} style={{ backgroundColor }}>
            {hsv.h !== null && (
              <span
                className={styles.Handle}
                style={{ backgroundColor: previewColor, left: `${hsv.s}%`, top: `${100 - hsv.v}%` }}
              />
            )}
          </div>
          <div ref={hueRef} className={styles.Hue}>
            {hsv.h !== null && (
              <span className={styles.Handle} style={{ backgroundColor, top: `${hsv.h / 3.6}%` }} />
            )}
          </div>
        </div>
        {requiredElement}
        {messageElement}
      </div>
    );
  }
);

const WrappedColor = AsComponent(ComponentNames.Color)(
  AsControl({
    controlType: INPUT_TYPES.TEXT,
    defaultValue: '',
    sanitizeValueForControl: (value: string) => value,
    style: { key: 'Color', styles }
   })(Color)
);

type PickerOwnPropsType = {};

type WrappedPickerType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithLayerExtensionType,
  ...PickerOwnPropsType
|};

const PickerBase = React.forwardRef(
  (
    {
      className,
      controlElement,
      coverElement,
      layerElement,
      messageElement,
      requiredElement
    }: WrappedAutocompleteType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Picker,
        className
      )}
    >
      {coverElement}
      {requiredElement}
      {messageElement}
      {layerElement}
    </div>
  )
);

const PickerWithLayer = WithLayer({ kind: LAYER_KINDS.CLOCK, layerClassName: styles.Layer, mode: LAYER_MODES.VERTICAL })(PickerBase);

const Picker = React.forwardRef(({ onChange, ...props }: WrappedAutocompleteWithLayerType, forwardedRef: ReactForwardedRefType) => {
  const [cache, setCache] = React.useState('');
  const { setValue, value } = props;

  const contextValue = React.useMemo(() => ({
    cache,
    setParentCache: setCache
  }), [cache, setCache]);

  React.useEffect(() => {
    setCache(value);
  }, [setCache, value]);

  const handleColorOnChange = React.useCallback((value: string) => {
    setValue(value);
    if (onChange) {
      onChange(value);
    }
  }, [onChange, setValue]);

  const handleInputOnChange = React.useCallback((value: string) => {
    setCache(value);
    if (getColorFormat(value) !== undefined) {
      setValue(value);
      if (onChange) {
        onChange(value);
      }
    }
  }, [onChange, setCache, setValue]);

  const handleInputOnBlur = React.useCallback((e) => {
    const { value: inputValue } = e.target;
    if (getColorFormat(inputValue) === undefined) {
      setCache(value);
    }
  }, [setCache, value]);

  const input = React.useMemo(() => (
    <Input className={styles.Input} onBlur={handleInputOnBlur} onChange={handleInputOnChange} value={cache} />
  ), [cache, handleInputOnBlur, handleInputOnChange]);

  return (
    <ParentCacheContext.Provider value={contextValue}>
      <PickerWithLayer ref={forwardedRef} input={input} {...props} height={306}>
        <WrappedColor onChange={handleColorOnChange} setValue={setValue} value={value} />
      </PickerWithLayer>
    </ParentCacheContext.Provider>
  );
});

export type ColorType = {|
  ...AsComponentType,
  ...AsControlType<string>,
  ...ColorOwnPropsType
|};

export type PickerType = {|
  ...AsComponentType,
  ...AsControlType<string>,
  ...WithLayerType,
  ...PickerOwnPropsType
|};

export default (
  Object.assign(WrappedColor, {
    Picker: AsComponent(ComponentNames.Color.Picker)(
      AsControl({
        style: { key: 'Picker', styles }
      })(Picker)
    )
  }): React.ComponentType<ColorType> & {
    Picker: React.ComponentType<PickerType>
  }
);
