/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Fieldset.scss';

type FieldsetOwnPropsType = {|
  border?: boolean,
  children: ReactChildren,
  legend?: string  
|};

type WrappedFieldsetType = {|
  ...AsComponentExtensionType,
  ...FieldsetOwnPropsType
|};

const Fieldset = React.forwardRef(
  ({ border, children, className, legend: label }: WrappedFieldsetType, forwardedRef: ReactForwardedRefType) => (
    <fieldset ref={forwardedRef} className={cx(styles.Fieldset, border && styles.Fieldset__Border, className)}>
      {label && <legend className={styles.Legend}>{label}</legend>}
      <div className={styles.Content}>{children}</div>
    </fieldset>
  )
);

export type FieldsetType = {|
  ...AsComponentType,
  ...FieldsetOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Fieldset)(Fieldset), {}): React.ComponentType<FieldsetType>
)
