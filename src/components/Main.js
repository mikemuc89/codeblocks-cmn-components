/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import BackgroundColorContext from '../contexts/BackgroundColorContext';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import {
  type FooterPropType,
  type HeaderPropType,
  type MenuPropType
} from '../types';
import CONFIG from '../components/style-config';
import styles from './Main.scss';

const {
  colors: {
    background: BACKGROUND_COLORS
  }
} = CONFIG;

const DEFAULT_HEADER_HEIGHT = 32;
const DEFAULT_MENU_HEIGHT = 56;
const DEFAULT_FOOTER_HEIGHT = 32;

type MainOwnPropsType = {|
  beforeContent?: React.Node,
  children?: ReactChildren,
  footer?: FooterPropType,
  header?: HeaderPropType,
  innerWidth?: string | number,
  menu?: MenuPropType
|};

type WrappedMainType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...MainOwnPropsType
|};

const Main = React.forwardRef(
  (
    { backgroundElementRef, beforeContent, children, className, footer, header, innerWidth = '100%', menu }: WrappedMainType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const headerHeight = header ? header.props.height || DEFAULT_HEADER_HEIGHT : 0;
    const menuHeight = menu ? menu.props.height || DEFAULT_MENU_HEIGHT : 0;
    const footerHeight = footer ? footer.props.height || DEFAULT_FOOTER_HEIGHT : 0;

    const outerContextValue = React.useMemo(
      () => ({ footerHeight, headerHeight, innerWidth, menuHeight }),
      [footerHeight, headerHeight, innerWidth, menuHeight]
    );
    const innerContextValue = React.useMemo(
      () => ({ footerHeight, headerHeight, innerWidth: undefined, menuHeight }),
      [footerHeight, headerHeight, menuHeight]
    );

    return (
      <div
        ref={mergeRefs(forwardedRef, backgroundElementRef)}
        className={cx(styles.Main, className)}
      >
        <MainSizeContext.Provider value={outerContextValue}>
          {header &&
            React.cloneElement(header, {
              className: styles.Header,
              height: headerHeight,
              page: true
            })}
          {menu &&
            React.cloneElement(menu, {
              className: styles.Menu,
              height: menuHeight,
              page: true,
              top: headerHeight
            })}
          <div
            className={styles.Before}
            style={{
              paddingTop: unitize(headerHeight + menuHeight)
            }}
          >
            {beforeContent}
          </div>
          <main
            className={styles.Content}
            style={{
              minHeight: `calc(100vh - ${unitize(headerHeight + menuHeight)})`,
              paddingBottom: unitize(footerHeight),
              width: unitize(innerWidth)
            }}
          >
            <MainSizeContext.Provider value={innerContextValue}>{children}</MainSizeContext.Provider>
          </main>
          {footer &&
            React.cloneElement(footer, {
              className: styles.Footer,
              height: footerHeight,
              page: true
            })}
        </MainSizeContext.Provider>
      </div>
    );
  }
);

export type MainType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...MainOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Main)(WithBackground({
    defaultBackgroundColor: BACKGROUND_COLORS.common.normal,
    defaultBackgroundColorFocus: BACKGROUND_COLORS.common.focus,
    defaultBackgroundColorHover: BACKGROUND_COLORS.common.hover
  })(Main)), {}): React.ComponentType<MainType>
);
