/* @flow */
import * as React from 'react';
import {
  formatAccountNumber,
  formatAmount,
  formatBoolean,
  formatCountry,
  formatDate,
  formatDateIso,
  formatDateTime,
  formatDurationIso,
  formatFileSize,
  formatMoney,
  formatMonthIso,
  formatMonthNameWithYear,
  formatPhoneNumber,
  formatStreetType,
  formatTime,
  formatWeekIso,
  formatYearlessDate,
  formatYearlessDateIso
} from '@omnibly/codeblocks-cmn-formatters';
import { type ReactChildren, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { COUNTRIES, STREET_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { ALIGN_TYPES, KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, padString } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import Message from './Message';
import Required from './Required';
import styles from './Value.scss';

type BaseValueOwnPropsType = {|
  ...WithMessagesType,
  align?: $Values<typeof ALIGN_TYPES>, // eslint-disable-line react/no-unused-prop-types
  children: ReactChildren,
  oneline?: boolean // eslint-disable-line react/no-unused-prop-types
|};

type WrappedBaseValueType = {|
  ...AsComponentExtensionType,
  ...BaseValueOwnPropsType
|};

const BaseValue = React.forwardRef(({
  align,
  children,
  className,
  errors,
  helper,
  hints,
  oneline,
  required,
  summary,
  warnings
}: WrappedBaseValueType,
forwardedRef: ReactForwardedRefType) => (
  <div
    ref={forwardedRef}
    className={cx(
      styles.BaseValue,
      cx.alignSingle(styles, 'BaseValue', { align }),
      cx.control(styles, 'BaseValue', { errors, helper, hints, warnings }),
      oneline && styles.BaseValue__Oneline,
      summary && styles.BaseValue__Summary,
      className
    )}
  >
    {children}
    <Message className={styles.Message} errors={errors} helper={helper} hints={hints} warnings={warnings} />
    {required && <Required className={styles.Required} />}
  </div>
));

type ValueOwnPropsType = {|
  ...WithMessagesType,
  align?: $Values<typeof ALIGN_TYPES>, // eslint-disable-line react/no-unused-prop-types
  oneline?: boolean // eslint-disable-line react/no-unused-prop-types
|};

type WrappedValueType = {|
  ...ValueOwnPropsType,
  children?: string
|};

const Value = React.forwardRef(({ children, className, ...props }: WrappedValueType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Value, className)} {...props}>
    <span className={styles.Content}>{children}</span>
  </BaseValue>
));

type AccountNumberOwnPropsType = {|
  ...ValueOwnPropsType,
  value?: string
|};

type WrappedAccountNumberType = {|
  ...AsComponentExtensionType,
  ...AccountNumberOwnPropsType
|};

const AccountNumber = React.forwardRef(
  ({ className, value, ...props }: WrappedAccountNumberType, forwardedRef: ReactForwardedRefType) => (
    <BaseValue ref={forwardedRef} className={cx(styles.AccountNumber, className)} {...props}>
      <data className={styles.Content} value={formatAccountNumber.sanitize(value)}>
        {formatAccountNumber(value)}
      </data>
    </BaseValue>
  )
);

type AddressOwnPropsType = {|
  ...ValueOwnPropsType,
  apartment?: string | number,
  building?: string | number,
  children?: string,
  city?: string,
  company?: string,
  country?: $Values<typeof COUNTRIES>,
  district?: string,
  name?: string,
  post?: string,
  postCode?: string,
  street?: string,
  streetType?: $Values<typeof STREET_TYPES>
|};

type WrappedAddressType = {|
  ...AsComponentExtensionType,
  ...AddressOwnPropsType
|};

const Address = React.forwardRef(
  (
    {
      apartment,
      building,
      children,
      city,
      className,
      company,
      country,
      district,
      name,
      post,
      postCode,
      street,
      streetType
    }: WrappedAddressType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const lines = React.useMemo(() => {
      if (children) {
        return [children];
      }
      return [
        [name].filter(Boolean).join(' '),
        [company].filter(Boolean).join(' '),
        [streetType && formatStreetType(streetType), street || city, [building, apartment].filter(Boolean).join('/')]
          .filter(Boolean)
          .join(' '),
        [
          street && post && post !== city && city,
          district,
          [postCode, postCode && (post || city)].filter(Boolean).join(' '),
          country && formatCountry(country)
        ]
          .filter(Boolean)
          .join(', ')
      ].filter(Boolean);
    }, [apartment, building, children, city, company, country, district, name, post, postCode, street, streetType]);

    return (
      <BaseValue ref={forwardedRef} className={cx(styles.Address, className)}>
        <address className={styles.Content}>
          {lines.map((line: string, index: number) => (
            <span key={index} className={styles.AddressLine}>
              {line}
            </span>
          ))}
        </address>
      </BaseValue>
    );
  }
);

type AmountOwnPropsType = {|
  ...ValueOwnPropsType,
  value?: string | number
|};

type WrappedAmountType = {|
  ...AsComponentExtensionType,
  ...AmountOwnPropsType
|};

const Amount = React.forwardRef(({ className, value, ...props }: WrappedAmountType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Amount, className)} {...props}>
    <span className={styles.Content}>{formatAmount(value)}</span>
  </BaseValue>
));

type BoolOwnPropsType = {|
  ...ValueOwnPropsType,
  value?: boolean
|};

type WrappedBoolType = {|
  ...AsComponentExtensionType,
  ...BoolOwnPropsType
|};

const Bool = React.forwardRef(({ className, value, ...props }: WrappedBoolType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Bool, className)} {...props}>
    <data
      className={styles.Content}
      value={value === undefined ? '' : value === null ? 'null' : Boolean(value).toString()}
    >
      {formatBoolean(value)}
    </data>
  </BaseValue>
));

type ColorOwnPropsType = {|
  ...ValueOwnPropsType,
  color?: string,
  sample?: string,
  value?: string
|};

type WrappedColorType = {|
  ...AsComponentExtensionType,
  ...ColorOwnPropsType
|};

const Color = React.forwardRef(
  ({ className, color, sample, value = color, ...props }: WrappedColorType, forwardedRef: ReactForwardedRefType) => (
    <BaseValue ref={forwardedRef} className={cx(styles.Color, className)} {...props}>
      <div className={styles.Content}>
        <span className={styles.Sample} style={{ backgroundColor: color }}>
          {sample}
        </span>
        <data value={color} className={styles.Val}>
          {value}
        </data>
      </div>
    </BaseValue>
  )
);

type CountryOwnPropsType = {|
  ...ValueOwnPropsType,
  value?: $Values<typeof COUNTRIES>
|};

type WrappedCountryType = {|
  ...AsComponentExtensionType,
  ...CountryOwnPropsType
|};

const Country = React.forwardRef(({ className, value, ...props }: WrappedCountryType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Country, className)} {...props}>
    <data className={styles.Content} value={value}>
      {formatCountry(value)}
    </data>
  </BaseValue>
));

type CurrencyOwnPropsType = {|
  ...ValueOwnPropsType,
  value?: string
|};

type WrappedCurrencyType = {|
  ...AsComponentExtensionType,
  ...CurrencyOwnPropsType
|};

const Currency = React.forwardRef(({ className, value, ...props }: WrappedCurrencyType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Currency, className)} {...props}>
    <span className={styles.Content}>{value}</span>
  </BaseValue>
));

type DateOwnPropsType = {|
  ...ValueOwnPropsType,
  value?: string
|};

type WrappedDateType = {|
  ...AsComponentExtensionType,
  ...DateOwnPropsType
|};

const Date = React.forwardRef(({ className, value, ...props }: WrappedDateType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Date, className)} {...props}>
    <time className={styles.Content} dateTime={formatDateIso(value, { showTime: false })}>
      {formatDate(value)}
    </time>
  </BaseValue>
));

type DateTimeOwnPropsType = {|
  ...ValueOwnPropsType,
  value?: string
|};

type WrappedDateTimeType = {|
  ...AsComponentExtensionType,
  ...DateTimeOwnPropsType
|};

const DateTime = React.forwardRef(({ className, date, time, ...props }: WrappedDateTimeType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.DateTime, className)} {...props}>
    <time className={styles.Content} dateTime={formatDateIso({ date, time }, { showTime: true })}>
      {formatDateTime({ date, time })}
    </time>
  </BaseValue>
));

type DurationOwnPropsType = {|
  ...ValueOwnPropsType,
  days?: string | number,
  hours?: string | number,
  minutes?: string | number,
  months?: string | number,
  seconds?: string | number,
  years?: string | number
|};

type WrappedDurationType = {|
  ...AsComponentExtensionType,
  ...DurationOwnPropsType
|};

const Duration = React.forwardRef(
  ({ className, days, hours, minutes, months, seconds, years, ...props }: WrappedDurationType, forwardedRef: ReactForwardedRefType) => {
    const formatted = React.useMemo(
      () => formatDurationIso({ days, hours, minutes, months, seconds, years }),
      [days, hours, minutes, months, seconds, years]
    );

    return (
      <BaseValue ref={forwardedRef} className={cx(styles.Duration, className)} {...props}>
        <time className={styles.Content} dateTime={formatted}>
          {formatted}
        </time>
      </BaseValue>
    );
  }
);

type FileSizeOwnPropsType = {|
  ...ValueOwnPropsType,
  value: number
|};

type WrappedFileSizeType = {|
  ...AsComponentExtensionType,
  ...FileSizeOwnPropsType
|};

const FileSize = React.forwardRef(({ className, value, ...props }: WrappedFileSizeType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.FileSize, className)} {...props}>
    <data className={styles.Content} value={value}>
      {formatFileSize(value)}
    </data>
  </BaseValue>
));

type KindOwnPropsType = {|
  ...ValueOwnPropsType,
  kind: $Values<typeof KINDS>,
  value?: string
|};

type WrappedKindType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...KindOwnPropsType
|};

const Kind = React.forwardRef(
  (
    { className, kind, sample = 'Aa', themeElementRef, value = kind, ...props }: WrappedKindType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <BaseValue ref={forwardedRef} className={cx(styles.Kind, className)} {...props}>
      <div className={styles.Content}>
        <span ref={themeElementRef} className={styles.Sample}>
          {sample}
        </span>
        <data value={kind} className={styles.Val}>
          {value}
        </data>
      </div>
    </BaseValue>
  )
);

type MoneyOwnPropsType = {|
  ...ValueOwnPropsType,
  amount: number,
  currency?: string
|};

type WrappedMoneyType = {|
  ...AsComponentExtensionType,
  ...MoneyOwnPropsType
|};

const Money = React.forwardRef(({ amount, className, currency, ...props }: WrappedMoneyType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Money, className)} {...props}>
    <span className={styles.Content}>{formatMoney({ amount, currency })}</span>
  </BaseValue>
));

type MonthOwnPropsType = {|
  ...ValueOwnPropsType,
  month: string | number,
  year: string | number
|};

type WrappedMonthType = {|
  ...AsComponentExtensionType,
  ...MonthOwnPropsType
|};

const Month = React.forwardRef(({ className, month, year, ...props }: WrappedMonthType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Month, className)} {...props}>
    <time className={styles.Content} dateTime={formatMonthIso({ month, year })}>
      {formatMonthNameWithYear({ month, year })}
    </time>
  </BaseValue>
));

type PhoneNumberOwnPropsType = {|
  ...ValueOwnPropsType,
  countryCode: string,
  value: string
|};

type WrappedPhoneNumberType = {|
  ...AsComponentExtensionType,
  ...PhoneNumberOwnPropsType
|};

const PhoneNumber = React.forwardRef(
  ({ className, countryCode, value, ...props }: WrappedPhoneNumberType, forwardedRef: ReactForwardedRefType) => (
    <BaseValue ref={forwardedRef} className={cx(styles.PhoneNumber, className)} {...props}>
      <data className={styles.Content} value={`${countryCode ? `+${countryCode}` : ''}${value}`}>
        {formatPhoneNumber(value, { countryCode })}
      </data>
    </BaseValue>
  )
);

type TimeOwnPropsType = {|
  ...ValueOwnPropsType,
  value: string
|};

type WrappedTimeType = {|
  ...AsComponentExtensionType,
  ...TimeOwnPropsType
|};

const Time = React.forwardRef(({ className, value, ...props }: WrappedTimeType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Time, className)} {...props}>
    <time className={styles.Content} dateTime={value}>
      {formatTime(value)}
    </time>
  </BaseValue>
));

type YearOwnPropsType = {|
  ...ValueOwnPropsType,
  value: string | number
|};

type WrappedYearType = {|
  ...AsComponentExtensionType,
  ...YearOwnPropsType
|};

const Year = React.forwardRef(({ className, value, ...props }: WrappedYearType, forwardedRef: ReactForwardedRefType) => (
  <BaseValue ref={forwardedRef} className={cx(styles.Year, className)} {...props}>
    <time className={styles.Content} dateTime={padString(value, 4, '0')}>
      {value}
    </time>
  </BaseValue>
));

type YearlessDateOwnPropsType = {|
  ...ValueOwnPropsType,
  day: string | number,
  month: string | number
|};

type WrappedYearlessDateType = {|
  ...AsComponentExtensionType,
  ...YearlessDateOwnPropsType
|};

const YearlessDate = React.forwardRef(
  ({ className, day, month, ...props }: WrappedYearlessDateType, forwardedRef: ReactForwardedRefType) => (
    <BaseValue ref={forwardedRef} className={cx(styles.YearlessDate, className)} {...props}>
      <time className={styles.Content} dateTime={formatYearlessDateIso({ day, month })}>
        {formatYearlessDate({ day, month })}
      </time>
    </BaseValue>
  )
);

type WeekOwnPropsType = {|
  ...ValueOwnPropsType,
  week: string | number,
  year: string | number
|};

type WrappedWeekType = {|
  ...AsComponentExtensionType,
  ...WeekOwnPropsType
|};

const Week = React.forwardRef(({ className, week, year, ...props }: WrappedWeekType, forwardedRef: ReactForwardedRefType) => {
  const formatted = React.useMemo(() => formatWeekIso({ week, year }), [week, year]);

  return (
    <BaseValue ref={forwardedRef} className={cx(styles.Week, className)} {...props}>
      <time className={styles.Content} dateTime={formatted}>
        {formatted}
      </time>
    </BaseValue>
  );
});

export type ValueType = {|
  ...AsComponentType,
  ...ValueOwnPropsType
|};

export type AccountNumberType = {|
  ...AsComponentType,
  ...AccountNumberOwnPropsType
|};

export type AddressType = {|
  ...AsComponentType,
  ...AddressOwnPropsType
|};

export type AmountType = {|
  ...AsComponentType,
  ...AmountOwnPropsType
|};

export type BoolType = {|
  ...AsComponentType,
  ...BoolOwnPropsType
|};

export type ColorType = {|
  ...AsComponentType,
  ...ColorOwnPropsType
|};

export type CountryType = {|
  ...AsComponentType,
  ...CountryOwnPropsType
|};

export type CurrencyType = {|
  ...AsComponentType,
  ...CurrencyOwnPropsType
|};

export type DateType = {|
  ...AsComponentType,
  ...DateOwnPropsType
|};

export type DateTimeType = {|
  ...AsComponentType,
  ...DateTimeOwnPropsType
|};

export type DurationType = {|
  ...AsComponentType,
  ...DurationOwnPropsType
|};

export type FileSizeType = {|
  ...AsComponentType,
  ...FileSizeOwnPropsType
|};

export type KindType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...KindOwnPropsType
|};

export type MoneyType = {|
  ...AsComponentType,
  ...MoneyOwnPropsType
|};

export type MonthType = {|
  ...AsComponentType,
  ...MonthOwnPropsType
|};

export type PhoneNumberType = {|
  ...AsComponentType,
  ...PhoneNumberOwnPropsType
|};

export type TimeType = {|
  ...AsComponentType,
  ...TimeOwnPropsType
|};

export type WeekType = {|
  ...AsComponentType,
  ...WeekOwnPropsType
|};

export type YearType = {|
  ...AsComponentType,
  ...YearOwnPropsType
|};

export type YearlessDateType = {|
  ...AsComponentType,
  ...YearlessDateOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Value)(Value), {
    AccountNumber: AsComponent(ComponentNames.Value.AccountNumber)(AccountNumber),
    Address: Object.assign(AsComponent(ComponentNames.Value.Address)(Address), {
      COUNTRIES,
      STREET_TYPES
    }),
    Amount: AsComponent(ComponentNames.Value.Amount)(Amount),
    Bool: AsComponent(ComponentNames.Value.Bool)(Bool),
    Color: AsComponent(ComponentNames.Value.Color)(Color),
    Country: Object.assign(AsComponent(ComponentNames.Value.Country)(Country), {
      COUNTRIES
    }),
    Currency: AsComponent(ComponentNames.Value.Currency)(Currency),
    Date: AsComponent(ComponentNames.Value.Date)(Date),
    DateTime: AsComponent(ComponentNames.Value.DateTime)(DateTime),
    Duration: AsComponent(ComponentNames.Value.Duration)(Duration),
    FileSize: AsComponent(ComponentNames.Value.FileSize)(FileSize),
    Kind: Object.assign(
      AsComponent(ComponentNames.Value.Kind)(
        WithTheme({ defaultKind: KINDS.PLAIN, focusable: false, hoverable: false })(Kind)
      ),
      {
        KINDS
      }
    ),
    Money: AsComponent(ComponentNames.Value.Money)(Money),
    Month: AsComponent(ComponentNames.Value.Month)(Month),
    PhoneNumber: AsComponent(ComponentNames.Value.PhoneNumber)(PhoneNumber),
    Time: AsComponent(ComponentNames.Value.Time)(Time),
    Week: AsComponent(ComponentNames.Value.Week)(Week),
    Year: AsComponent(ComponentNames.Value.Year)(Year),
    YearlessDate: AsComponent(ComponentNames.Value.YearlessDate)(YearlessDate)
  }, {
    ALIGN_TYPES
  }): React.ComponentType<ValueType> & {
    AccountNumber: React.ComponentType<AccountNumberType>,
    Address: React.ComponentType<AddressType> & {
      COUNTRIES: Object,
      STREET_TYPES: Object
    },
    Amount: React.ComponentType<AmountType>,
    Bool: React.ComponentType<BoolType>,
    Color: React.ComponentType<ColorType>,
    Country: React.ComponentType<CountryType> & {
      COUNTRIES: Object
    },
    Currency: React.ComponentType<CurrencyType>,
    Date: React.ComponentType<DateType>,
    DateTime: React.ComponentType<DateTimeType>,
    Duration: React.ComponentType<DurationType>,
    FileSize: React.ComponentType<FileSizeType>,
    Kind: React.ComponentType<KindType> & {
      KINDS: Object
    },
    Money: React.ComponentType<MoneyType>,
    Month: React.ComponentType<MonthType>,
    PhoneNumber: React.ComponentType<PhoneNumberType>,
    Time: React.ComponentType<TimeType>,
    Week: React.ComponentType<WeekType>,
    Year: React.ComponentType<YearType>,
    YearlessDate: React.ComponentType<YearlessDateType>
  } & {
    ALIGN_TYPES: Object
  });
