/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, processChildren } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import useOpenState from '../hooks/useOpenState';
import Dropdown, { type DropdownType, type ItemType, type WrappedDropdownType } from './Dropdown';
import FontAwesome from './FontAwesome';
import styles from './Table.scss';
import boolean from '@omnibly/codeblocks-cmn-schema/src/schema/fields/boolean';

const ActionsCellSubscriberContext = React.createContext(null);
const ActionsCellEnabledContext = React.createContext(false);
const ColumnCountContext = React.createContext(0);

const getChildrenSpan = (children: ReactElementsChildren<any>) =>
  children &&
  React.Children.toArray((Array.isArray(children) ? children[0] : children).props.children).reduce(
    (result: number, child: React.Element<any>) => result + (child.props.span || 1),
    0
  );

const CELL_PRESETS = Object.assign({
  ADDRESS: {
    align: ALIGN_TYPES.LEFT,
    size: 8
  },
  AMOUNT: {
    align: ALIGN_TYPES.RIGHT,
    size: 7
  },
  BOOLEAN: {
    align: ALIGN_TYPES.CENTER,
    size: 4
  },
  CHECKBOX: {
    align: ALIGN_TYPES.CENTER,
    size: 2
  },
  COUNTER: {
    align: ALIGN_TYPES.RIGHT,
    size: 2
  },
  DATE: {
    align: ALIGN_TYPES.CENTER,
    size: 8
  },
  IDENTIFIER: {
    align: ALIGN_TYPES.LEFT,
    size: 6
  },
  NAME: {
    align: ALIGN_TYPES.LEFT,
    size: 8
  },
  NAME_WITH_ICON: {
    align: ALIGN_TYPES.LEFT,
    size: 10
  },
  NUMBER: {
    align: ALIGN_TYPES.RIGHT,
    size: 4
  },
  PERCENT: {
    align: ALIGN_TYPES.RIGHT,
    size: 4
  },
  PRIMARY: {
    align: ALIGN_TYPES.LEFT,
    size: 0
  },
  TOGGLE: {
    align: ALIGN_TYPES.CENTER,
    size: 3
  },
  TYPE: {
    align: ALIGN_TYPES.CENTER,
    size: 4
  },
  USER: {
    align: ALIGN_TYPES.LEFT,
    size: 12
  }
});

type CellOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children?: ReactChildren,
  size?: number,
  span?: number  
|};

type WrappedCellType = {|
  ...AsComponentExtensionType,
  ...CellOwnPropsType
|};

const Cell = React.forwardRef(
  ({ align = ALIGN_TYPES.LEFT, children, className, size, span = 1 }: WrappedCellType, forwardedRef: ReactForwardedRefType) => {
    const childType = ((children) => children[0] && children[0].type && children[0].type.componentId)(React.Children.toArray(children));
    const classBasedOnChild = {
      [ComponentNames.Button]: styles.Cell__WithButton,
      [ComponentNames.Input]: styles.Cell__WithInput,
      [ComponentNames.Option]: styles.Cell__WithOption,
      [ComponentNames.Select]: styles.Cell__WithSelect
    }[childType];

    return (
      <td
        ref={forwardedRef}
        className={cx(
          styles.Cell,
          cx.alignSingle(styles, 'Cell', { align }),
          cx.numericSize(styles, 'Cell', { size }),
          classBasedOnChild,
          className
        )}
        colSpan={span}
      >
          {processChildren(
            children,
            (child, type) => ({
              className: cx(
                child.props.className,
                {
                  [ComponentNames.Button]: styles.Button,
                  [ComponentNames.Checkbox]: styles.Checkbox,
                  [ComponentNames.Input]: styles.Input,
                  [ComponentNames.Option]: styles.Option,
                  [ComponentNames.Select]: styles.Select,
                  [ComponentNames.Toggle]: styles.Toggle
                }[type]
              ),
              ...{
                [ComponentNames.Option]: {
                  border: false,
                  verticalTop: true
                },
                [ComponentNames.Toggle]: {
                  small: true
                }
              }[type]
            }),
          )}
      </td>
    );
  }
);

type WrappedActionsType = WrappedDropdownType;

const Actions = React.forwardRef(({ children, className, ...props }: WrappedActionsType, forwardedRef: ReactForwardedRefType) => {
  const setActionsCellCount = React.useContext(ActionsCellSubscriberContext);
  React.useEffect(() => {
    setActionsCellCount((count: number) => count + 1);
    return () => {
      setActionsCellCount((count: number) => count - 1);
    };
  }, [setActionsCellCount]);

  return (
    <td ref={forwardedRef} className={cx(styles.Actions, className)}>
      {children && (
        <Dropdown
          className={styles.Dropdown}
          icon={<FontAwesome id="ellipsis-v" />}
          border={false}
          shape={Dropdown.SHAPES.CIRCLE}
          {...props}
        >
          {children}
        </Dropdown>
      )}
    </td>
  );
});

type RowOwnPropsType = {|
  children: ReactElementsChildren<CellType | ActionsType>  
|};

type WrappedRowType = {|
  ...AsComponentExtensionType,
  ...RowOwnPropsType
|};

const Row = React.forwardRef(({ children, className }: WrappedRowType, forwardedRef: ReactForwardedRefType) => (
  <tr ref={forwardedRef} className={cx(styles.Row, className)}>
    {children}
  </tr>
));

type HeaderOwnPropsType = {|
  children: ReactElementsChildren<CellType>  
|};

type WrappedHeaderType = {|
  ...AsComponentExtensionType,
  ...HeaderOwnPropsType
|};

const Header = React.forwardRef(({ children, className }: WrappedHeaderType, forwardedRef: ReactForwardedRefType) => {
  const actionsCellEnabled = React.useContext(ActionsCellEnabledContext);

  return (
    <thead ref={forwardedRef} className={cx(styles.Header, className)}>
      <tr className={styles.Content}>
        {children}
        {actionsCellEnabled && <td className={styles.Actions} />}
      </tr>
    </thead>
  );
});

type FooterOwnPropsType = {|
  children: ReactElementsChildren<CellType>  
|};

type WrappedFooterType = {|
  ...AsComponentExtensionType,
  ...FooterOwnPropsType
|};

const Footer = React.forwardRef(({ children, className }: WrappedFooterType, forwardedRef: ReactForwardedRefType) => {
  const actionsCellEnabled = React.useContext(ActionsCellEnabledContext);

  return (
    <tfoot ref={forwardedRef} className={cx(styles.Footer, className)}>
      <tr className={styles.Content}>
        {children}
        {actionsCellEnabled && <td className={styles.Actions} />}
      </tr>
    </tfoot>
  );
});

type BodyOwnPropsType = {|
  children: ReactElementsChildren<RowType>
|};

type WrappedBodyType = {|
  ...AsComponentExtensionType,
  ...BodyOwnPropsType
|};

const Body = React.forwardRef(({ children, className }: WrappedBodyType, forwardedRef: ReactForwardedRefType) => {
  const columnCount = React.useMemo(() => getChildrenSpan(children), [children]);

  return (
    <tbody ref={forwardedRef} className={cx(styles.Body, className)}>
      <ColumnCountContext.Provider value={columnCount}>{children}</ColumnCountContext.Provider>
    </tbody>
  );
});

type GroupOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: ReactElementsChildren<RowType>,
  collapsible?: boolean,
  collapsed?: boolean,
  title?: string  
|};

type WrappedGroupType = {|
  ...AsComponentExtensionType,
  ...GroupOwnPropsType
|};

const Group = React.forwardRef(
  (
    { align = ALIGN_TYPES.LEFT, children, className, collapsible, collapsed: initiallyCollapsed, title }: WrappedGroupType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { onToggle, open } = useOpenState({ disabled: !collapsible, open: collapsible ? !initiallyCollapsed : true });
    const columnCount = getChildrenSpan(children);

    return (
      <tbody
        ref={forwardedRef}
        className={cx(
          styles.Group,
          cx.alignSingle(styles, 'Group', { align }),
          !open && styles.Group__Collapsed,
          collapsible && styles.Group__Collapsible,
          className
        )}
      >
        <tr className={styles.TitleRow} onClick={collapsible ? onToggle : undefined}>
          <td colSpan={columnCount} className={styles.Title}>
            {title}
          </td>
        </tr>
        <ColumnCountContext.Provider value={columnCount}>{children}</ColumnCountContext.Provider>
      </tbody>
    );
  }
);

type DetailsOwnPropsType = {|
  children: ReactElementsChildren<any>  
|};

type WrappedDetailsType = {|
  ...AsComponentExtensionType,
  ...DetailsOwnPropsType
|};

const Details = React.forwardRef(({ children, className }: WrappedDetailsType, forwardedRef: ReactForwardedRefType) => {
  const columnCount = React.useContext(ColumnCountContext);

  return (
    <tr ref={forwardedRef} className={cx(styles.Details, className)}>
      <td colSpan={columnCount} className={styles.Content}>
        {children}
      </td>
    </tr>
  );
});

type TableOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: ReactElementsChildren<BodyType | FooterType | HeaderType | RowType | DetailsType>,
  caption?: string  
|};

type WrappedTableType = {|
  ...AsComponentExtensionType,
  ...TableOwnPropsType
|};

const Table = React.forwardRef(
  (
    { align = ALIGN_TYPES.LEFT, children, className, caption: captionContent }: WrappedTableType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const [actionsCellCount, setActionsCellCount] = React.useState(0);
    const columnCount = React.useMemo(() => getChildrenSpan(children), [children]);
    const actionsCellEnabled = React.useMemo(() => actionsCellCount > 0, [actionsCellCount]);

    return (
      <table ref={forwardedRef} className={cx(styles.Table, cx.alignSingle(styles, 'Table', { align }), className)}>
        {captionContent && <caption className={styles.Caption}>{captionContent}</caption>}
        <ActionsCellSubscriberContext.Provider value={setActionsCellCount}>
          <ActionsCellEnabledContext.Provider value={actionsCellEnabled}>
            <ColumnCountContext.Provider value={columnCount}>{children}</ColumnCountContext.Provider>
          </ActionsCellEnabledContext.Provider>
        </ActionsCellSubscriberContext.Provider>
      </table>
    );
  }
);

export type CellType = {|
  ...AsComponentType,
  ...CellOwnPropsType
|};

export type ActionsType = DropdownType;

export type RowType = {|
  ...AsComponentType,
  ...RowOwnPropsType
|};

export type HeaderType = {|
  ...AsComponentType,
  ...HeaderOwnPropsType
|};

export type FooterType = {|
  ...AsComponentType,
  ...FooterOwnPropsType
|};

export type BodyType = {|
  ...AsComponentType,
  ...BodyOwnPropsType
|};

export type GroupType = {|
  ...AsComponentType,
  ...GroupOwnPropsType
|};

export type DetailsType = {|
  ...AsComponentType,
  ...DetailsOwnPropsType
|};

export type TableType = {|
  ...AsComponentType,
  ...TableOwnPropsType
|};

export type ActionItemType = {|
  ...ItemType,
  children: ReactChildren
|};

export default (
  Object.assign(AsComponent(ComponentNames.Table)(Table), {
    Body: AsComponent(ComponentNames.Table.Body)(Body),
    Cell: Object.assign(AsComponent(ComponentNames.Table.Cell)(Cell), {
      Actions: Object.assign(AsComponent(ComponentNames.Table.Cell.Actions)(Actions), {
        Item: Object.assign(Dropdown.Item, {
          Details: ({ children, ...props }: ActionItemType) => (
            <Dropdown.Item
              align={Dropdown.Item.ALIGN_TYPES.RIGHT}
              {...props}
              icon={<FontAwesome id="info" border={false} />}
            >
              {children}
            </Dropdown.Item>
          ),
          Modify: ({ children, ...props }: ActionItemType) => (
            <Dropdown.Item
              align={Dropdown.Item.ALIGN_TYPES.RIGHT}
              {...props}
              icon={<FontAwesome id="pen" border={false} />}
            >
              {children}
            </Dropdown.Item>
          ),
          Remove: ({ children, ...props }: ActionItemType) => (
            <Dropdown.Item
              align={Dropdown.Item.ALIGN_TYPES.RIGHT}
              {...props}
              icon={<FontAwesome id="trash" border={false} />}
            >
              {children}
            </Dropdown.Item>
          ),
          Schedule: ({ children, ...props }: ActionItemType) => (
            <Dropdown.Item
              align={Dropdown.Item.ALIGN_TYPES.RIGHT}
              {...props}
              icon={<FontAwesome id="calendar-alt" border={false} />}
            >
              {children}
            </Dropdown.Item>
          )
        })
      })
    }, {
      ALIGN_TYPES,
      PRESETS: CELL_PRESETS
    }),
    Details: AsComponent(ComponentNames.Table.Details)(Details),
    Footer: AsComponent(ComponentNames.Table.Footer)(Footer),
    Header: AsComponent(ComponentNames.Table.Header)(Header),
    Row: Object.assign(AsComponent(ComponentNames.Table.Row)(Row), {
      Group: AsComponent(ComponentNames.Table.Row.Group)(Group)
    })
  }, {
    ALIGN_TYPES
  }): React.ComponentType<TableType> & {
    Body: React.ComponentType<BodyType>,
    Cell: React.ComponentType<CellType> & {
      Actions: React.ComponentType<ActionsType> & {
        Item: React.ComponentType<ItemType> & {
          Details: React.ComponentType<ActionItemType>,
          Modify: React.ComponentType<ActionItemType>,
          Remove: React.ComponentType<ActionItemType>,
          Schedule: React.ComponentType<ActionItemType>
        }
      }
    } & {
      ALIGN_TYPES: Object,
      PRESETS: Object
    },
    Details: React.ComponentType<DetailsType>,
    Footer: React.ComponentType<FooterType>,
    Header: React.ComponentType<HeaderType>,
    Row: React.ComponentType<RowType> & {
      Group: React.ComponentType<GroupType>
    }
  } & {
    ALIGN_TYPES: Object
  }
);
