/* #__flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import useEventListener from '../hooks/useEventListener';
import { type IconPropType } from '../types';
import styles from './Action.scss';

const DEFAULT_ICON_SIZE = 80;

type ActionOwnPropsType = {|
  border?: boolean,
  children?: string,
  icon?: IconPropType
|};

type WrappedActionType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...ActionOwnPropsType
|};

const Action = React.forwardRef(
  ({ LinkComponent, border, children, className, icon, linkProps }: WrappedActionType, forwardedRef: ReactForwardedRefType) => {
    const elRef = React.useRef(null);
    const textElRef = React.useRef(null);

    const [iconSize, setIconSize] = React.useState(icon ? icon.props.size || DEFAULT_ICON_SIZE : 0);
    const [wrapped, setWrapped] = React.useState(false);

    const updateElements = React.useCallback(() => {
      const el = elRef.current;
      const textEl = textElRef.current;
      if (!icon) {
        setIconSize(0);
        setWrapped(false);
      }

      if (el && textEl) {
        const parentHeight = el.parentElement.clientHeight;
        const textHeight = textEl.clientHeight;

        setWrapped(textHeight > parentHeight * 0.4);

        const iconSize = parentHeight * 0.6;
        setIconSize(iconSize);
      }
    }, [icon, setIconSize, setWrapped]);

    React.useEffect(() => {
      updateElements();
    }, [updateElements]);

    useEventListener('resize', updateElements);

    return (
      <LinkComponent
        ref={elRef}
        className={cx(styles.Action, border && styles.Action__Border, wrapped && styles.Action__Wrapped)}
        {...linkProps}
      >
        {icon &&
          React.cloneElement(icon, {
            className: styles.Icon,
            innerSize: iconSize * 0.8,
            size: iconSize
          })}
        <span ref={textElRef} className={styles.Text}>
          {children}
        </span>
      </LinkComponent>
    );
  }
);

export type ActionType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...ActionOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Action)(WithLink()(Action)), {}): React.ComponentType<ActionType>
);
