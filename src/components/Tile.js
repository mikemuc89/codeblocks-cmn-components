/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES, KINDS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import { type IconPropType } from '../types';
import styles from './Tile.scss';

type TileOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactChildren,
  disabled?: boolean,
  icon?: IconPropType,
  onClick: () => void  
|};

type WrappedTileType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...WithThemeExtensionType,
  ...TileOwnPropsType
|};


const Tile = React.forwardRef(
  (
    {
      LinkComponent,
      align = ALIGN_TYPES.CENTER,
      border,
      children,
      className,
      disabled,
      icon,
      linkProps,
      onClick,
      themeElementRef
    }: WrappedTileType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const iconElement =
      icon &&
      React.cloneElement(icon, {
        className: styles.Icon,
        focusable: false,
        hoverable: false,
        shape: icon.props.shape || SHAPES.SQUARE
      });

    return (
      <LinkComponent
        ref={mergeRefs(forwardedRef, themeElementRef)}
        className={cx(
          styles.Tile,
          cx.alignSingle(styles, 'Tile', { align }),
          cx.control(styles, 'Tile', { disabled }),
          border && styles.Tile__Border,
          className
        )}
        {...linkProps}
      >
        <div className={styles.Content}>
          {iconElement}
          {children &&
            React.Children.map(
              children,
              (child: React.Node) =>
                child &&
                (isBasicReactChild(child) ? (
                  <span className={styles.Text}>{child}</span>
                ) : (
                  React.cloneElement(child, {
                    className: cx(
                      child.props.className,
                      {
                        [ComponentNames.Button]: styles.Button,
                        [ComponentNames.Link]: styles.Link,
                        [ComponentNames.Markdown]: styles.Text,
                        [ComponentNames.Text]: styles.Text,
                        [ComponentNames.Text.Primary]: styles.Text,
                        [ComponentNames.Text.Secondary]: styles.Text
                      }[child.type.componentId]
                    )
                  })
                ))
            )}
        </div>
      </LinkComponent>
    );
  }
);

export type TileType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...WithThemeType,
  ...TileOwnPropsType
|};

type SetOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: ReactElementsChildren<TileType>,
  rowCapacity?: number  
|};

type WrappedSetType = {|
  ...AsComponentExtensionType,
  ...SetOwnPropsType
|};

const Set = React.forwardRef(
  ({ align = ALIGN_TYPES.JUSTIFY, children, className, rowCapacity }: WrappedSetType, forwardedRef: ReactForwardedRefType) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Set,
        cx.alignSingle(styles, 'Set', { align }),
        styles[`Set__Capacity_${rowCapacity}`],
        className
      )}
    >
      {children}
    </div>
  )
);

export type SetType = {|
  ...AsComponentType,
  ...SetOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Tile)(WithLink()(WithTheme({ defaultKind: undefined })(Tile))),
    {
      Set: Object.assign(AsComponent(ComponentNames.Tile.Set)(Set), {
        ALIGN_TYPES
      })
    },
    {
      ALIGN_TYPES,
      KINDS
    }
  ): React.ComponentType<TileType> & {
    Set: React.ComponentType<SetType>
  } & {
    ALIGN_TYPES: Object,
    KINDS: Object
  }
);
