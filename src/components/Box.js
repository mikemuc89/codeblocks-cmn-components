/* #__flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType } from '../hocs/components/AsComponent';
import styles from './Box.scss';

type BoxOwnPropsType = {|
  align?: $Values<ALIGN_TYPES>,
  bottom?: number,
  children: ReactChildren,
  contentAlign?: $Values<ALIGN_TYPES>,
  height?: number,
  left?: number,
  right?: number,
  top?: number,
  width?: number
|};

export type WrappedBoxType = {|
  ...AsComponentExtensionType,
  ...BoxOwnPropsType
|};

const Box = React.forwardRef(
  ({ align = ALIGN_TYPES.CENTER, bottom, children, className, contentAlign, height, left, right, top, width }: WrappedBoxType, forwardedRef: ReactForwardedRefType) => (
    <div
      ref={forwardedRef}
      className={cx(styles.Box, cx.alignSingle(styles, 'Box', { align }), {
        [ALIGN_TYPES.CENTER]: styles.Box__ContentCenter,
        [ALIGN_TYPES.JUSTIFY]: styles.Box__ContentJustify,
        [ALIGN_TYPES.LEFT]: styles.Box__ContentLeft,
        [ALIGN_TYPES.RIGHT]: styles.Box__ContentRight,
      }[contentAlign], className)}
      style={{
        ...(bottom ? { paddingBottom: unitize(bottom) } : {}),
        ...(height ? { height: unitize(height) } : {}),
        ...(left ? { paddingLeft: unitize(left) } : {}),
        ...(right ? { paddingRight: unitize(right) } : {}),
        ...(top ? { paddingTop: unitize(top) } : {}),
        ...(width ? { width: unitize(width), margin: '0 auto', maxWidth: '100%' } : {})
      }}
    >
      {children}
    </div>
  )
);

export type BoxType = {|
  ...AsComponentType,
  ...BoxOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Box)(Box), {
    ALIGN_TYPES
  }): React.ComponentType<BoxType> & {
    ALIGN_TYPES: Object
  }
);
