/* #__flow */
import * as React from 'react';
import { KINDS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, hexCode } from '@omnibly/codeblocks-cmn-utils';
import { hexToParts, hsvToRgb, rgbToHex, rgbToHsv } from '@omnibly/codeblocks-cmn-utils/src/utils/color';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type IconPropType } from '../types';
import styles from './Badge.scss';

const COLOR_SHIFT_COEFF = 1.2;

type AvatarOwnPropsType = {|
  children: IconPropType,
  hoverable?: boolean,
  seed?: string
|}

type WrappedAvatarType = {|
  ...AsComponentExtensionType,
  ...AvatarOwnPropsType
|};

const Avatar = React.forwardRef(
  ({ children, className, hoverable, seed, ...props }: WrappedAvatarType, forwardedRef: ReactForwardedRefType) => {
    const backgroundColor = React.useMemo(() => (seed ? `#${hexCode(seed)}` : undefined), [seed]);
    const color = React.useMemo(() => {
      if (!backgroundColor) {
        return '#000000';
      }
      const { v } = rgbToHsv(hexToParts(backgroundColor));
      return v < 60 ? '#ffffff' : '#000000';
    }, [backgroundColor]);
    const backgroundColorHover = React.useMemo(() => {
      if (!hoverable) {
        return backgroundColor;
      }
      const { h, s, v } = rgbToHsv(hexToParts(backgroundColor));
      if (v > 45) {
        return rgbToHex(hsvToRgb({ h, s, v: v / COLOR_SHIFT_COEFF }));
      }
      return rgbToHex(hsvToRgb({ h, s, v: Math.min(100, v * COLOR_SHIFT_COEFF) }));
    }, [backgroundColor, hoverable]);

    return React.cloneElement(children, {
      backgroundColor: backgroundColor,
      backgroundColorHover: hoverable ? backgroundColorHover : undefined,
      className: cx(styles.Avatar, children.props.className, className),
      color: color,
      ref: forwardedRef,
      ...props
    });
  }
);

export type AvatarType = {|
  ...AsComponentType,
  ...AvatarOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Avatar)(Avatar), {
    KINDS,
    SHAPES
  }): React.ComponentType<AvatarType> & {
    KINDS: Object,
    SHAPES: Object
  }
);
