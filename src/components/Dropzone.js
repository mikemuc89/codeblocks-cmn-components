/* #__flow */
import * as React from 'react';
import { formatFileSize } from '@omnibly/codeblocks-cmn-formatters';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import WithFileInput, { FileItemContext, getExtensionIcon, type WithFileInputExtensionType, type WithFileInputType } from '../hocs/components/WithFileInput';
import { type IconPropType } from '../types';
import Button from './Button';
import FontAwesome from './FontAwesome';
import styles from './Dropzone.scss';

const ICON_SIZE = 24;
const ICON_INNER_SIZE = 16;

type FileOwnPropsType = {|
  extension: string,
  index: number,
  name: string,
  removable?: boolean,
  size?: number
|};

type WrappedFileType = {|
  ...AsComponentExtensionType,
  ...FileOwnPropsType
|};

const File = React.forwardRef(
  ({ extension, index, name, removable = true, size }: WrappedFileType, forwardedRef: ReactForwardedRefType) => {
    const { disabled, handleOnRemoveByItemIndex, readonly } = React.useContext(FileItemContext);

    const showRemoveIcon = React.useMemo(() => !disabled && !readonly && removable, [disabled, readonly, removable]);

    const handleOnRemove = React.useCallback(() => {
      handleOnRemoveByItemIndex(index);
    }, [index, handleOnRemoveByItemIndex]);

    const fileIconElement = React.useMemo(() => {
      const baseIcon = getExtensionIcon(extension);
      return React.cloneElement(baseIcon, {
        className: cx(baseIcon.props.className, styles.Icon),
        innerSize: baseIcon.props.innerSize || ICON_INNER_SIZE,
        size: baseIcon.props.size || ICON_SIZE
      });
    }, [extension]);

    return (
      <div ref={forwardedRef} className={cx(styles.File)}>
        <span className={styles.Name}>
          {fileIconElement}
          {name}
          <span className={styles.Extension}>
            {extension}
          </span>
        </span>
        <span className={styles.Size}>{formatFileSize(size)}</span>
        <span className={styles.Actions}>
          {showRemoveIcon && (
            <FontAwesome
              className={styles.Remove}
              id="times"
              onClick={handleOnRemove}
              size={ICON_SIZE}
              innerSize={ICON_INNER_SIZE}
            />
          )}
        </span>
      </div>
    );
  }
);

export type FileType = {|
  ...AsComponentType,
  ...FileOwnPropsType
|};

type DropzoneOwnPropsType = {};

type WrappedDropzoneType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithFileInputExtensionType,
  ...DropzoneOwnPropsType
|};

const Dropzone = React.forwardRef(
  (
    {
      buttonElement,
      canAddMore,
      className,
      controlElement,
      multiple,
      onDragProps,
      overElement,
      messageElement = null,
      selectedFiles
    }: WrappedDropzoneType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const emptyText = React.useMemo(() => (multiple ? 'No files selected' : 'No file selected'), [multiple]);
    const text = React.useMemo(() => canAddMore ? multiple ? 'Drop your files here or press button to add them.' : 'Drop your file here or press button to add it.' : 'You added maximum number of files', [canAddMore, multiple]);
    const emptyElement = React.useMemo(() => canAddMore ? (
      <div className={styles.Empty}>{emptyText}</div>
    ) : null, [emptyText]);

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Dropzone,
          className
        )}
        {...onDragProps}
      >
        {overElement}
        <div className={styles.Content}>
          {controlElement}
          <div className={styles.Text}>{text}</div>
          {buttonElement}
          <div className={styles.Files}>
            <div className={styles.Header}>
              <span className={styles.Name}>Name</span>
              <span className={styles.Size}>Size</span>
              <span className={styles.Actions} />
            </div>
            {selectedFiles.length ? selectedFiles.map((params: Object, index: number) => (
              <File key={index} index={index} {...params} />
            )) : emptyElement}
          </div>
        </div>
        {messageElement}
      </div>
    );
  }
);

export type DropzoneType = {|
  ...AsComponentType,
  ...AsControlType,
  ...WithFileInputType,
  ...DropzoneOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(
      ComponentNames.Dropzone
    )(
      AsControl({
        controlType: INPUT_TYPES.FILE,
        defaultValue: null,
        sanitizeValueForControl: () => '',
        style: { key: 'Dropzone', styles }
      })(
        WithFileInput({
          buttonClassName: styles.Button
        })(
          Dropzone
        )
      )
    ),
    {
      File: AsComponent(ComponentNames.Dropzone.File)(File)
    }
  ): React.ComponentType<DropzoneType> & {
    File: React.ComponentType<FileType>
  }
);
