/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES, KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type IconPropType } from '../types';
import Bar from './Bar';
import styles from './List.scss';

const ICON_SIZE = 24;
const ICON_INNER_SIZE = 16;

type ItemOwnPropsType = {|
  align: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactChildren,
  indicator?: string | IconPropType  
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(
  (
    { align = ALIGN_TYPES.LEFT, border = true, children, className, indicator, ...props }: WrappedItemType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const indicatorElement =
      indicator &&
      (typeof indicator === 'string' ? (
        <span className={styles.Indicator}>{indicator}</span>
      ) : (
        React.cloneElement(indicator, {
          className: cx(styles.Indicator, styles.IndicatorIcon),
          ...{
            [ComponentNames.CharIcon]: {
              innerSize: ICON_INNER_SIZE,
              size: ICON_SIZE
            },
            [ComponentNames.ColorIcon]: {
              innerSize: ICON_INNER_SIZE,
              size: ICON_SIZE
            },
            [ComponentNames.FontAwesome]: {
              innerSize: ICON_INNER_SIZE,
              size: ICON_SIZE
            },
            [ComponentNames.FontAwesome.Brand]: {
              innerSize: ICON_INNER_SIZE,
              size: ICON_SIZE
            },
            [ComponentNames.FontAwesome.Regular]: {
              innerSize: ICON_INNER_SIZE,
              size: ICON_SIZE
            },
            [ComponentNames.Icon]: {
              size: ICON_SIZE
            }
          }[indicator.type.componentId]
        })
      ));

    return (
      <li
        ref={forwardedRef}
        className={cx(styles.Item, cx.alignSingle(styles, 'Item', { align }), border && styles.Item__Border, className)}
      >
        <Bar className={styles.Content} align={align} border={border} {...props}>
          {indicatorElement}
          {children}
        </Bar>
      </li>
    );
  }
);

export type ItemType = {|
  ...AsComponentType,
  ...ItemOwnPropsType
|};

type OrderedOwnPropsType = {|
  children: ReactElementsChildren<ItemType>,
  start?: number  
|};

type WrappedOrderedType = {|
  ...AsComponentExtensionType,
  ...OrderedOwnPropsType
|};

const Ordered = React.forwardRef(
  ({ children, className, start = 1, ...props }: WrappedOrderedType, forwardedRef: ReactForwardedRefType) => (
    <ol ref={forwardedRef} className={cx(styles.Ordered, className)}>
      {React.Children.map(children, (child: React.Element<ItemType>, index: number) =>
        React.cloneElement(child, {
          indicator: `${start + index}.`,
          ...props
        })
      )}
    </ol>
  )
);

type UnorderedOwnPropsType = {|
  children: ReactElementsChildren<ItemType>,
  indicator?: string  
|};

type WrappedUnorderedType = {|
  ...AsComponentExtensionType,
  ...UnorderedOwnPropsType
|};

const Unordered = React.forwardRef(
  ({ children, className, indicator = '–', ...props }: WrappedUnorderedType, forwardedRef: ReactForwardedRefType) => (
    <ul ref={forwardedRef} className={cx(styles.Unordered, className)}>
      {React.Children.map(children, (child: React.Element<ItemType>) =>
        React.cloneElement(child, {
          indicator,
          ...props
        })
      )}
    </ul>
  )
);

export type OrderedType = {|
  ...AsComponentType,
  ...OrderedOwnPropsType
|};

export type UnorderedType = {|
  ...AsComponentType,
  ...UnorderedOwnPropsType
|};

export default (
  Object.assign({
    Item: AsComponent(ComponentNames.List.Item)(Item),
    Ordered: AsComponent(ComponentNames.List.Ordered)(Ordered),
    Unordered: AsComponent(ComponentNames.List.Item)(Unordered)
  }, {
    ALIGN_TYPES,
    KINDS
  }): {
    Item: React.ComponentType<ItemType>,
    Ordered: React.ComponentType<OrderedType>,
    Unordered: React.ComponentType<UnorderedType>
  } & {
    ALIGN_TYPES: Object,
    KINDS: Object
  }
);
