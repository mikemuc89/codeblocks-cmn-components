/* @flow */
import * as React from 'react';
import { KINDS, POSITIONS_FULL as POSITIONS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import useOpenState from '../hooks/useOpenState';
import FontAwesome from './FontAwesome';
import styles from './Toast.scss';

type ToastOwnPropsType = {|
  canClose?: boolean,
  duration?: number,
  onClose: () => void,
  open?: boolean,
  position?: $Values<typeof POSITIONS>,
  timeout?: number
|};

type WrappedToastType = {|
  ...AsComponentExtensionType,
  ...WithThemeExtensionType,
  ...ToastOwnPropsType
|};

const Toast = React.forwardRef(
  (
    {
      canClose,
      children,
      className,
      duration = 2000,
      onClose,
      open: initiallyOpen = true,
      position = POSITIONS.TOP,
      themeElementRef,
      timeout = 0
    }: WrappedToastType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const { hide, open, show } = useOpenState({ open: initiallyOpen });
    const { headerHeight, footerHeight, menuHeight } = React.useContext(MainSizeContext);

    React.useEffect(() => {
      if (initiallyOpen) {
        setTimeout(show, timeout);
      }
    }, [initiallyOpen, show, timeout]);

    React.useEffect(() => {
      if (open && duration > 0) {
        setTimeout(hide, duration);
      }
    }, [duration, hide, open]);

    const handleOnClose = React.useCallback(() => {
      hide();
      if (onClose) {
        onClose();
      }
    }, [hide, onClose]);

    return (
      <div
        ref={mergeRefs(forwardedRef, themeElementRef)}
        className={cx(styles.Toast, className)}
        style={
          {
            [POSITIONS.BOTTOM]: {
              bottom: 0 + footerHeight,
              left: '50%',
              transform: 'translateX(-50%)'
            },
            [POSITIONS.BOTTOM_LEFT]: {
              bottom: 0 + footerHeight,
              left: 0
            },
            [POSITIONS.BOTTOM_RIGHT]: {
              bottom: 0 + footerHeight,
              right: 0
            },
            [POSITIONS.LEFT]: {
              left: 0,
              top: '50%',
              transform: 'translateY(-50%)'
            },
            [POSITIONS.RIGHT]: {
              right: 0,
              top: '50%',
              transform: 'translateY(-50%)'
            },
            [POSITIONS.TOP]: {
              left: '50%',
              top: 0 + headerHeight + menuHeight,
              transform: 'translateX(-50%)'
            },
            [POSITIONS.TOP_LEFT]: {
              left: 0,
              top: 0 + headerHeight + menuHeight
            },
            [POSITIONS.TOP_RIGHT]: {
              right: 0,
              top: 0 + headerHeight + menuHeight
            }
          }[position]
        }
      >
        {canClose && (
          <FontAwesome className={styles.Close} size={16} innerSize={10} id="times" onClick={handleOnClose} />
        )}
        <div className={styles.Content}>{children}</div>
      </div>
    );
  }
);

export type ToastType = {|
  ...AsComponentType,
  ...WithThemeType,
  ...ToastOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Toast)(
      WithTheme({ defaultKind: KINDS.PRIMARY, focusable: false, hoverable: false })(Toast)
    ),
    {
      KINDS,
      POSITIONS
    }
  ): React.ComponentType<ToastType> & {
    KINDS: Object,
    POSITIONS: Object
  }
);
