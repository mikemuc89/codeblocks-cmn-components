/* @flow */
import * as React from 'react';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type IconPropType } from '../types';
import styles from './InfoBox.scss';

const ICON_SIZE = 32;
const ICON_INNER_SIZE = 18;

type InfoboxOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  icon?: IconPropType  
|};

type WrappedInfoBoxType = {|
  ...AsComponentExtensionType,
  ...InfoboxOwnPropsType
|};

const InfoBox = React.forwardRef(
  ({ align = ALIGN_TYPES.LEFT, children, className, icon }: WrappedInfoBoxType, forwardedRef: ReactForwardedRefType) => {
    const iconElement =
      icon &&
      React.cloneElement(icon, {
        className: styles.Icon,
        size: ICON_SIZE,
        ...{
          [ComponentNames.CharIcon]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: ICON_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: ICON_INNER_SIZE
          }
        }[icon.type.componentId]
      });

    return (
      <div ref={forwardedRef} className={cx(styles.InfoBox, cx.alignSingle(styles, 'InfoBox', { align }), className)}>
        {iconElement}
        <p className={styles.Text}>{children}</p>
      </div>
    );
  }
);

export type InfoBoxType = {|
  ...AsComponentType,
  ...InfoboxOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.InfoBox)(InfoBox), {
    ALIGN_TYPES
  }): React.ComponentType<InfoBoxType> & {
    ALIGN_TYPES: Object
  }
);
