/* #__flow */
import * as React from 'react';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { KINDS, POSITIONS_CORNERS as POSITIONS, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLayer, {
  LAYER_KINDS,
  LAYER_MODES,
  DIMMER_MODES,
  type WithLayerExtensionType, type WithLayerType
} from '../hocs/components/WithLayer';
import { type IconPropType } from '../types';
import styles from './Fab.scss';

const DEFAULT_ITEM_SIZE = 48;
const DEFAULT_ITEM_INNER_SIZE = 32;

type ItemOwnPropsType = {|
  children: IconPropType,
  disabled?: boolean,
  label?: string,
  onClick: (e: MouseEvent) => void  
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(
  ({ children, className, disabled, label, onClick }: WrappedItemType, forwardedRef: ReactForwardedRefType) => (
    <div ref={forwardedRef} className={cx(styles.Item, className)}>
      {label && <div className={styles.Label}>{label}</div>}
      {React.cloneElement(children, {
        className: styles.Icon,
        disabled,
        kind: children.props.kind || KINDS.PRIMARY,
        onClick: children.props.onClick || onClick,
        shape: children.props.shape || SHAPES.CIRCLE,
        size: children.props.size || DEFAULT_ITEM_SIZE,
        ...{
          [ComponentNames.CharIcon]: {
            innerSize: children.props.innerSize || DEFAULT_ITEM_INNER_SIZE
          },
          [ComponentNames.ColorIcon]: {
            innerSize: children.props.innerSize || DEFAULT_ITEM_INNER_SIZE
          },
          [ComponentNames.FontAwesome]: {
            innerSize: children.props.innerSize || DEFAULT_ITEM_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Brand]: {
            innerSize: children.props.innerSize || DEFAULT_ITEM_INNER_SIZE
          },
          [ComponentNames.FontAwesome.Regular]: {
            innerSize: children.props.innerSize || DEFAULT_ITEM_INNER_SIZE
          }
        }[children.type.componentId]
      })}
    </div>
  )
);

export type ItemType = {|
  ...AsComponentType,
  ...ItemOwnPropsType
|};

type FabOwnPropsType = {|
  children: ReactElementsChildren<ItemType>,
  cover: IconPropType,
  disabled?: boolean,
  label?: string,
  position?: $Values<typeof POSITIONS>
|};

type WrappedFabType = {|
  ...AsComponentExtensionType,
  ...WithLayerExtensionType,
  ...FabOwnPropsType
|};

const Fab = React.forwardRef(
  (
    {
      children,
      className,
      cover,
      disabled,
      label,
      layerElement,
      layerTriggerElementRef,
      position = POSITIONS.BOTTOM_RIGHT
    }: WrappedFabType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div ref={forwardedRef} className={cx(styles.Fab, cx.positionCorners(styles, 'Fab', { position }), className)}>
      {React.cloneElement(cover, {
        className: styles.Cover,
        disabled,
        focusable: true,
        hoverable: true,
        href: '#',
        label: layerElement ? null : label,
        ref: layerTriggerElementRef
      })}
      {layerElement}
    </div>
  )
);

export type FabType = {|
  ...AsComponentType,
  ...WithLayerType,
  ...FabOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Fab)(
      WithLayer({ dimmerMode: DIMMER_MODES.ALWAYS, kind: LAYER_KINDS.FAB, mode: LAYER_MODES.FAB })(Fab)
    ),
    {
      Item: AsComponent(ComponentNames.Fab.Item)(Item)
    }, {
      POSITIONS
    }
  ): React.ComponentType<FabType> & {
    Item: React.ComponentType<ItemType>
  } & {
    POSITIONS: Object
  }
);
