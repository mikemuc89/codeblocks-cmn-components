/* @flow */
import * as React from 'react';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './PropList.scss';

const PROP_LIST_PROP_KINDS = Object.freeze({
  EVENT_HANDLER: 'PropList.PROP_LIST_PROP_KINDS.EVENT_HANDLER',
  PROPERTY: 'PropList.PROP_LIST_PROP_KINDS.PROPERTY'
});

const PROP_LIST_PROP_KIND_TO_CLASS = Object.freeze({
  [PROP_LIST_PROP_KINDS.PROPERTY]: styles.Prop__Prop,
  [PROP_LIST_PROP_KINDS.EVENT_HANDLER]: styles.Prop__EventHandler
});

const formatPropListItemKind = (kind: $Values<typeof PROP_LIST_PROP_KINDS>) =>
  ({
    [PROP_LIST_PROP_KINDS.EVENT_HANDLER]: 'event handler',
    [PROP_LIST_PROP_KINDS.PROPERTY]: 'prop'
  }[kind]);

type PropOwnPropsType = {|
  children?: string,
  defaultValue?: ?string,
  kind?: $Values<typeof PROP_LIST_PROP_KINDS>,
  name: string,
  required?: boolean,
  type?: string  
|};

type WrappedPropType = {|
  ...AsComponentExtensionType,
  ...PropOwnPropsType
|};

const Prop = React.forwardRef(
  (
    { children, className, defaultValue, kind = PROP_LIST_PROP_KINDS.PROPERTY, name, required, type }: WrappedPropType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <li className={cx(styles.Prop, required && styles.Prop__Required, PROP_LIST_PROP_KIND_TO_CLASS[kind], className)}>
      <div className={styles.Name}>{name}</div>
      {kind && <div className={styles.Kind}>{formatPropListItemKind(kind)}</div>}
      {type && <div className={styles.Type}>{type}</div>}
      {defaultValue !== undefined && <div className={styles.Default}>{JSON.stringify(defaultValue)}</div>}
      {children && <div className={styles.Description}>{children}</div>}
    </li>
  )
);

export type PropType = {|
  ...AsComponentType,
  ...PropOwnPropsType
|};

type PropListOwnPropsType = {|
  children: ReactElementsChildren<PropType>,
  description?: string,
  title: string  
|};

type WrappedPropListType = {|
  ...AsComponentExtensionType,
  ...PropListOwnPropsType
|};

const PropList = React.forwardRef(
  ({ children, className, description, title }: WrappedPropListType, forwardedRef: ReactForwardedRefType) => (
    <div className={cx(styles.PropList, className)}>
      <div className={styles.Title}>{title}</div>
      {description && <div className={styles.Description}>{description}</div>}
      <ul className={styles.Props}>{children}</ul>
    </div>
  )
);

export type PropListType = {|
  ...AsComponentType,
  ...PropListOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.PropList)(PropList), {
    Prop: Object.assign(AsComponent(ComponentNames.PropList.Prop)(Prop), {
      KINDS: PROP_LIST_PROP_KINDS
    })
  }): React.ComponentType<PropListType> & {
    Prop: React.ComponentType<PropType> & {
      KINDS: Object
    }
  }
);
