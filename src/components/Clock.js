/* #__flow */
import * as React from 'react';
import { formatHour } from '@omnibly/codeblocks-cmn-formatters';
import { type WithMessagesType, type SetStateCallbackType } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, direction, distance, mergeRefs, padString, repeatPattern } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import ParentCacheContext from '../contexts/ParentCacheContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { ControlCacheContext, type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import WithLayer, {
  LayerContentContext,
  LAYER_KINDS,
  LAYER_MODES,
  type WithLayerExtensionType, type WithLayerType
} from '../hocs/components/WithLayer';
import useHover, { type HoverCallbackDataType } from '../hooks/useHover';
import Input from './Input';
import Message from './Message';
import Required from './Required';
import styles from './Clock.scss';

type ValueType = {|
  hour: string | number,
  minute: string | number
|};

type HandsType = {|
  value: ValueType,
  hour?: number,
  minute?: number
|};

const sanitizeValue = ({ hour, minute }: ValueType) => `${padString(hour, 2)}:${padString(minute, 2)}`;
const parseValue = (value: string | null) => value ? ((hour, minute) => ({
  hour: parseInt(hour) || null,
  minute: parseInt(minute) || null
}))(...value.split(':')) : { hour: null, minute: null };

const isValidHour = (value: string | null) => {
  const { hour, minute } = parseValue(value);
  if (hour === null || minute === null) {
    return false;
  }
  return hour >= 0 && hour < 24 && minute >= 0 && minute < 60;
}

const Hands = React.forwardRef(({ value, hour, minute }: HandsType, forwardedRef: ReactForwardedRefType) => {
  const sanitizedHour = hour === null ? value.hour || 0 : hour;
  const sanitizedMinute = minute === null ? value.minute || 0 : minute;

  const dHour = 60;
  const xHour = 120 + dHour * Math.sin((sanitizedHour * Math.PI) / 6);
  const yHour = 120 - dHour * Math.cos((sanitizedHour * Math.PI) / 6);

  const dMinute = 90;
  const xMinute = 120 + dMinute * Math.sin((sanitizedMinute * Math.PI) / 30);
  const yMinute = 120 - dMinute * Math.cos((sanitizedMinute * Math.PI) / 30);

  return (
    <g ref={forwardedRef}>
      {hour !== undefined && <line x1="120" y1="120" x2={xHour} y2={yHour} stroke="#ddd" strokeWidth="5" />}
      {minute !== undefined && <line x1="120" y1="120" x2={xMinute} y2={yMinute} stroke="#ddd" strokeWidth="3" />}
    </g>
  );
});

type HoursType = {|
  className?: string,
  value: ValueType,
  disabled?: boolean,
  hour?: number,
  resetHoveredHour: () => void,
  setHoveredHour: SetStateCallbackType<number>,
  onChange?: (value: ValueType) => void
|};

const Hours = React.forwardRef(
  (
    { className, value, disabled, hour, resetHoveredHour, setHoveredHour, onChange }: HoursType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const onHoverHandler = React.useCallback(
      ({ absolute: { px, py } }: HoverCallbackDataType) => {
        const dx = px - 0.5;
        const dy = py - 0.5;
        const di = distance({ dx, dy });
        const evening = di > 0.35;
        const angle = (Math.round((direction({ dx, dy }) + 90) / 30) * 30) % 360;
        setHoveredHour((angle / 30 + (evening ? 12 : 0)) % 24);
      },
      [setHoveredHour]
    );

    const surface = useHover({
      disabled,
      onHover: onHoverHandler,
      onHoverEnd: resetHoveredHour
    });

    const handleOnChange = React.useCallback(() => onChange({ ...value, hour }), [value, hour, onChange]);

    return (
      <svg
        ref={mergeRefs(forwardedRef, surface)}
        className={cx(styles.Hours, disabled && styles.Hours__Disabled, className)}
        onClick={disabled ? undefined : handleOnChange}
        height="240"
        width="240"
        viewport="0 0 240 240"
      >
        <circle cx="120" cy="120" r="110" fill="#f8f8f8" stroke="#aaa" strokeWidth="1" />
        <circle cx="120" cy="120" r="83" fill="#fafafa" stroke="#aaa" strokeWidth="1" />
        <Hands value={value} hour={hour} minute={value.minute} />
        <circle cx="120" cy="120" r="1" fill="#000" stroke="#aaa" strokeWidth="1" />
        {repeatPattern(24, (index: number) => {
          const d = index > 11 ? 97 : 70;
          const x = 120 + d * Math.sin((index * Math.PI) / 6);
          const y = 120 - d * Math.cos((index * Math.PI) / 6);

          return (
            <text
              key={index}
              className={cx(styles.HourText, cx.control(styles, 'HourText', { active: index === hour }))}
              x={x}
              y={y}
              dominantBaseline="middle"
              textAnchor="middle"
            >
              {index}
            </text>
          );
        })}
      </svg>
    );
  }
);

type MinutesType = {|
  className?: string,
  value: ValueType,
  disabled?: boolean,
  minute?: number,
  resetHoveredMinute: () => void,
  setHoveredMinute: SetStateCallbackType<number>,
  onChange: (value: ValueType) => void
|};

const Minutes = React.forwardRef(
  (
    { className, value, disabled, minute, resetHoveredMinute, setHoveredMinute, onChange }: MinutesType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const onHoverHandler = React.useCallback(({ absolute: { px, py } }: HoverCallbackDataType) => {
      const dx = px - 0.5;
      const dy = py - 0.5;
      const angle = (Math.round((direction({ dx, dy }) + 90) / 6) * 6) % 360;
      setHoveredMinute((angle / 6) % 60)
    }, [setHoveredMinute]);

    const surface = useHover({
      disabled,
      onHover: onHoverHandler,
      onHoverEnd: resetHoveredMinute
    });

    const handleOnChange = React.useCallback(() => {
      if (onChange) {
        onChange({ ...value, minute });
      }
    }, [value, minute, onChange]);

    return (
      <svg
        ref={mergeRefs(forwardedRef, surface)}
        className={cx(styles.Minutes, disabled && styles.Minutes__Disabled, className)}
        onClick={disabled ? undefined : handleOnChange}
        preserveAspectRatio="none"
        viewport="0 0 240 240"
        width="240"
        height="240"
      >
        <circle cx="120" cy="120" r="110" fill="#f8f8f8" stroke="#aaa" strokeWidth="1" />
        <Hands value={value} hour={value.hour} minute={minute} />
        <circle cx="120" cy="120" r="1" fill="#000" stroke="#aaa" strokeWidth="1" />
        {repeatPattern(12, (index: number) => {
          const i = index * 5;
          const d = 97;
          const x = 120 + d * Math.sin((i * Math.PI) / 30);
          const y = 120 - d * Math.cos((i * Math.PI) / 30);

          return (
            <text
              key={index}
              className={cx(styles.MinuteText, cx.control(styles, 'MinuteText', { active: i === minute }))}
              x={x}
              y={y}
              dominantBaseline="middle"
              textAnchor="middle"
            >
              {i}
            </text>
          );
        })}
      </svg>
    );
  }
);

type ClockOwnPropsType = {|
  ...WithMessagesType,
  border?: boolean,
  hideTime?: boolean
|};

type WrappedClockType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType<string>,
  ...ClockOwnPropsType
|};

const Clock = React.forwardRef(
  (
    {
      border,
      className,
      controlElement,
      disabled,
      hideTime,
      messageElement,
      requiredElement,
      value,
      setValue
    }: WrappedClockType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const [valueObj, setValueObj] = React.useState(parseValue(value));
    const [showMinutes, setShowMinutes] = React.useState(false);

    const [hoveredHour, setHoveredHour] = React.useState(null);
    const [hoveredMinute, setHoveredMinute] = React.useState(null);

    const resetHoveredHour = React.useCallback(() => setHoveredHour(null), [setHoveredHour]);
    const resetHoveredMinute = React.useCallback(() => setHoveredMinute(null), [setHoveredMinute]);

    const onChangeHour = React.useCallback((newValue: ValueType) => {
      setValueObj(newValue);
      setShowMinutes(true);
    }, [setValueObj, setShowMinutes]);

    const onChangeMinute = React.useCallback((newValue: ValueType) => {
      setValueObj(newValue);
      setShowMinutes(false);
    }, [setValueObj, setShowMinutes]);

    const handleShowMinutes = React.useCallback(() => setShowMinutes(true), [setShowMinutes]);
    const handleHideMinutes = React.useCallback(() => setShowMinutes(false), [setShowMinutes]);

    const padddedHour = React.useMemo(() => padString(hoveredHour === null ? valueObj.hour || '00' : hoveredHour, 2), [hoveredHour, valueObj.hour]);
    const padddedMinute = React.useMemo(() => padString(hoveredMinute === null ? valueObj.minute || '00' : hoveredMinute, 2), [hoveredMinute, valueObj.minute]);

    React.useEffect(() => setValue(() => sanitizeValue(valueObj)), [setValue, valueObj]);

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Clock,
          border && styles.Clock__Border,
          showMinutes && styles.Clock__ShowMinutes,
          hideTime && styles.Clock__HideTime,
          className
        )}
      >
        {controlElement}
        <div className={styles.Content}>
          {!hideTime && (
            <div className={styles.Time}>
              <button onClick={disabled ? undefined : handleHideMinutes} className={styles.Hour}>
                {padddedHour}
              </button>
              <div>:</div>
              <button onClick={disabled ? undefined : handleShowMinutes} className={styles.Minute}>
                {padddedMinute}
              </button>
            </div>
          )}
          {showMinutes ? (
            <Minutes
              value={valueObj}
              disabled={disabled}
              hour={hoveredHour}
              minute={hoveredMinute}
              resetHoveredMinute={resetHoveredMinute}
              setHoveredMinute={setHoveredMinute}
              onChange={onChangeMinute}
            />
          ) : (
            <Hours
              value={valueObj}
              disabled={disabled}
              hour={hoveredHour}
              minute={hoveredMinute}
              resetHoveredHour={resetHoveredHour}
              setHoveredHour={setHoveredHour}
              onChange={onChangeHour}
            />
          )}
        </div>
        {requiredElement}
        {messageElement}
      </div>
    );
  }
);

const WrappedClock = AsComponent(ComponentNames.Clock)(
  AsControl({
    style: { key: 'Clock', styles },
    defaultValue: null,
    controlType: INPUT_TYPES.HIDDEN,
    sanitizeValueForControl: (value: string) => value
  })(Clock)
);

type PickerOwnPropsType = {};

type WrappedPickerType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithLayerExtensionType,
  ...PickerOwnPropsType
|};

const PickerBase = React.forwardRef(
  (
    {
      className,
      controlElement,
      coverElement,
      layerElement,
      messageElement,
      requiredElement
    }: WrappedPickerWrapperType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Picker,
        className
      )}
    >
      {coverElement}
      {requiredElement}
      {messageElement}
      {layerElement}
    </div>
  )
);

const PickerWithLayer = WithLayer({ kind: LAYER_KINDS.CLOCK, layerClassName: styles.Layer, mode: LAYER_MODES.VERTICAL })(PickerBase);

const Picker = React.forwardRef(({ onChange, ...props }: WrappedPickerType, forwardedRef: ReactForwardedRefType) => {
  const [cache, setCache] = React.useState('');
  const { setValue, value } = props;

  const contextValue = React.useMemo(() => ({
    cache,
    setParentCache: setCache
  }), [cache, setCache]);

  React.useEffect(() => {
    setCache(value);
  }, [setCache, value]);

  const handleClockOnChange = React.useCallback((value: string) => {
    setValue(value);
  }, [onChange, setValue]);

  const handleInputOnChange = React.useCallback((value: string) => {
    setCache(value);
    if (isValidHour(value)) {
      setValue(value);
    }
  }, [onChange, setCache, setValue]);

  const handleInputOnBlur = React.useCallback((e) => {
    const { value: inputValue } = e.target;
    if (isValidHour(inputValue)) {
      setCache(value);
    }
  }, [setCache, value]);

  const input = React.useMemo(() => (
    <Input.Time className={styles.Input} onBlur={handleInputOnBlur} onChange={handleInputOnChange} value={cache} />
  ), [cache, handleInputOnBlur, handleInputOnChange]);

  return (
    <ParentCacheContext.Provider value={contextValue}>
      <PickerWithLayer ref={forwardedRef} input={input} {...props} height={298}>
        <WrappedClock onChange={handleClockOnChange} setValue={setValue} value={value} hideTime />
      </PickerWithLayer>
    </ParentCacheContext.Provider>
  );
});

export type ClockType = {|
  ...AsComponentType,
  ...AsControlType<string>,
  ...ClockOwnPropsType
|};

export type PickerType = {|
  ...AsComponentType,
  ...AsControlType<ValueType>,
  ...WithLayerType,
  ...PickerOwnPropsType
|};

export default (
  Object.assign(WrappedClock, {
    Picker: AsComponent(ComponentNames.Clock.Picker)(
      AsControl({
        style: { key: 'Picker', styles }
      })(Picker)
    )
  }): React.ComponentType<ClockType> & {
    Picker: React.ComponentType<PickerType>
  }
);
