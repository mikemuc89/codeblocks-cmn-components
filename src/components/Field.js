/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { LABEL_POSITIONS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type LabelPropType } from '../types';
import Label from './Label';
import styles from './Field.scss';

export type FieldGroupContextType = boolean;

const FieldGroupContext = React.createContext(false);

type FieldOwnPropsType = {|
  children: React.Node,
  label?: LabelPropType,
  size?: number  
|};

type WrappedFieldType = {|
  ...AsComponentExtensionType,
  ...FieldOwnPropsType
|};

const Field = React.forwardRef(({ children, className, label, size }: WrappedFieldType, forwardedRef: ReactForwardedRefType) => {
  const grouped = React.useContext(FieldGroupContext);
  const hasStringLabel = typeof label === 'string';
  const labelPosition = hasStringLabel ? LABEL_POSITIONS.TOP : label && label.props.position;

  const { childElements, hasCheckboxLikeChild, rootClassNames } = React.Children.toArray(children).reduce(({ childElements, hasCheckboxLikeChild, rootClassNames }, child) => {
    if (isBasicReactChild(child)) {
      const childEl = (
        <span className={styles.Text}>{child}</span>
      );

      return {
        childElements: [
          ...childElements,
          childEl          
        ],
        hasCheckboxLikeChild,
        rootClassNames
      }
    }

    const childEl = React.cloneElement(child, {
      className: {
        [ComponentNames.Button]: styles.Button,
        [ComponentNames.Checkbox]: styles.Checkbox,
        [ComponentNames.Option]: styles.Option,
        [ComponentNames.Option.Selectable]: styles.Option,
        [ComponentNames.Option.Selectable.Group]: styles.Option,
        [ComponentNames.Text]: styles.Text,
        [ComponentNames.Toggle]: styles.Toggle,
      }[child.type.componentId]
    });
    const newRootClassName = {
      [ComponentNames.Checkbox]: styles.Field__Checkbox_Child,
      [ComponentNames.Option]: styles.Field__Option_Child,
      [ComponentNames.Option.Selectable]: styles.Field__Option_Child,
      [ComponentNames.Option.Selectable.Group]: styles.Field__Option_Child,
      [ComponentNames.Toggle]: styles.Field__Toggle_Child
    }[child.type.componentId];

    return {
      childElements: [
        ...childElements,
        childEl          
      ],
      hasCheckboxLikeChild: hasCheckboxLikeChild || [ComponentNames.Checkbox, ComponentNames.Toggle].includes(child.type.componentId),
      rootClassNames: [
        ...rootClassNames,
        newRootClassName
      ]
    }
  }, {
    childElements: [],
    hasCheckboxLikeChild: false,
    rootClassNames: []
  });

  return (
    children && (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Field,
          grouped && size === undefined ? styles.Field__NoSize : styles[`Field__Size_${size}`],
          [LABEL_POSITIONS.LEFT, LABEL_POSITIONS.RIGHT].includes(labelPosition) && styles.Field__Label_Alongside,
          labelPosition === LABEL_POSITIONS.RIGHT && styles.Field__Label_After,
          [LABEL_POSITIONS.BOTTOM, LABEL_POSITIONS.BOTTOM_CENTER, LABEL_POSITIONS.BOTTOM_RIGHT].includes(
            labelPosition
          ) && styles.Field__Label_Bottom,
          [LABEL_POSITIONS.TOP, LABEL_POSITIONS.TOP_CENTER, LABEL_POSITIONS.TOP_RIGHT].includes(
            labelPosition
          ) && styles.Field__Label_Top,
          ...rootClassNames,
          className
        )}
      >
        {label && (hasStringLabel
          ? <Label className={styles.Label} position={hasCheckboxLikeChild ? Label.POSITIONS.RIGHT : Label.POSITIONS.TOP} oneline>{label}</Label>
          : React.cloneElement(label, {
            className: styles.Label
          }))
        }
        <div className={styles.Content}>
          {childElements}
        </div>
      </div>
    )
  );
});

export type FieldType = {|
  ...AsComponentType,
  ...FieldOwnPropsType
|};

type GroupOwnPropsType = {|
  children: ReactElementsChildren<FieldType>,
  label?: LabelPropType  
|};

type WrappedGroupType = {|
  ...AsComponentExtensionType,
  ...GroupOwnPropsType
|};

const Group = React.forwardRef(({ children, className, label }: WrappedGroupType, forwardedRef: ReactForwardedRefType) => (
  <div ref={forwardedRef} className={cx(styles.Group, className)}>
    {label && (typeof label === 'string'
      ? <Label className={styles.Label} position={Label.POSITIONS.TOP} oneline>{label}</Label>
      : React.cloneElement(label, {
        className: styles.GroupLabel
      }))}
    <div className={styles.GroupContent}>
      <FieldGroupContext.Provider value>{children}</FieldGroupContext.Provider>
    </div>
  </div>
));

export type GroupType = {|
  ...AsComponentType,
  ...GroupOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Field)(Field), {
    Group: AsComponent(ComponentNames.Field.Group)(Group)
  }): React.ComponentType<FieldType> & {
    Group: React.ComponentType<GroupType>
  }
);
