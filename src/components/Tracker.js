/* @flow */
import * as React from 'react';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import Status, { STATUSES, type StatusType, type WrappedStatusType } from './Status';
import styles from './Tracker.scss';

type ItemOwnPropsType = {|
  emphasized?: boolean
|};

type WrappedItemType = {|
  ...WrappedStatusType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(({ children, className, emphasized, ...props }: WrappedItemType, forwardedRef: ReactForwardedRefType) => (
  <Status ref={forwardedRef} className={cx(styles.Item, emphasized && styles.Item__Emphasized, className)} {...props}>
    {children}
  </Status>
));

export type ItemType = {|
  ...StatusType,
  ...ItemOwnPropsType
|};

type TrackerOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: ReactElementsChildren<ItemType>
|};

type WrappedTrackerType = {|
  ...AsComponentExtensionType,
  ...TrackerOwnPropsType
|};

const Tracker = React.forwardRef(
  ({ align = ALIGN_TYPES.LEFT, children, className }: WrappedTrackerType, forwardedRef: ReactForwardedRefType) => (
    <div
      ref={forwardedRef}
      className={cx(styles.Tracker, align && cx.alignSingle(styles, 'Tracker', { align }), className)}
    >
      {children}
    </div>
  )
);

export type TrackerType = {|
  ...AsComponentType,
  ...TrackerOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Tracker)(Tracker), {
    Item: AsComponent(ComponentNames.Tracker.Item)(Item)
  }, {
    ALIGN_TYPES,
    STATUSES
  }): React.ComponentType<TrackerType> & {
    Item: React.ComponentType<ItemType>
  } & {
    ALIGN_TYPES: Object,
    STATUSES: Object
  }
);
