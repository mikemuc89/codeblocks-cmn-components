/* #__flow */
import * as React from 'react';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { ALIGN_TYPES, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, guid, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLayer, { LAYER_MODES, type WithLayerExtensionType, type WithLayerType } from '../hocs/components/WithLayer';
import WithShape, { type WithShapeExtensionType, type WithShapeType } from '../hocs/components/WithShape';
import useKeyboardSubmit from '../hooks/useKeyboardSubmit';
import { type IconPropType } from '../types';
import Option, { type OptionType, type WrappedOptionType } from './Option';
import styles from './Dropdown.scss';

const ICON_SIZE = 30;
const ICON_INNER_SIZE = 16;

type DropdownContextType = {|
  name: string
|};

const DropdownContext = React.createContext({
  name: ''
});

type WrappedItemType = WrappedOptionType;

export const Item = React.forwardRef(
  ({ action, children, className, disabled, onClick, ...props }: WrappedItemType, forwardedRef: ReactForwardedRefType) => {
    const { name } = React.useContext(DropdownContext);
    const onKeyPress = useKeyboardSubmit(onClick);

    const controlElement = (
      <input
        className={styles.Control}
        name={name}
        disabled={disabled}
        value=""
        type={INPUT_TYPES.RADIO}
        onKeyPress={disabled ? undefined : onKeyPress}
      />
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(styles.Item, cx.control(styles, 'Item', { disabled }), className)}
        onClick={disabled ? undefined : onClick}
      >
        <Option
          action={action}
          className={styles.Option}
          clickable={false}
          disabled={disabled}
          {...props}
          border={false}
          onClick={disabled ? undefined : onClick}
        >
          {children}
        </Option>
        {controlElement}
      </div>
    );
  }
);

export type ItemType = OptionType;

type DropdownOwnPropsType = {|
  accent?: boolean,
  border?: boolean,
  children: ReactElementsChildren<ItemType>,
  disabled?: boolean,
  focus?: boolean,
  icon: IconPropType,
  label?: string,
  name?: string
|};

export type WrappedDropdownType = {|
  ...AsComponentExtensionType,
  ...WithLayerExtensionType,
  ...WithShapeExtensionType,
  ...DropdownOwnPropsType
|};

const Dropdown = React.forwardRef(
  (
    {
      accent,
      border = true,
      children,
      className,
      disabled,
      focus,
      icon,
      label,
      layerElement,
      layerTriggerElementRef,
      name,
      shapeElementRef
    }: WrappedDropdownType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const inputName = React.useMemo(() => name || guid(), [name]);

    const iconElement =
      icon &&
      React.cloneElement(icon, {
        className: styles.Icon,
        innerSize: icon.props.innerSize || ICON_INNER_SIZE,
        size: icon.props.size || ICON_SIZE
      });

    const contextValue = React.useMemo(() => ({ name: inputName }), [inputName]);

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Dropdown,
          cx.control(styles, 'Dropdown', { disabled, focus }),
          accent && styles.Dropdown__Accent,
          border && styles.Dropdown__Border,
          className
        )}
      >
        <div
          ref={mergeRefs(layerTriggerElementRef, shapeElementRef)}
          className={styles.Cover}
          {...(disabled ? {} : { tabIndex: 0 })}
        >
          {iconElement}
          {label && <span className={styles.Label}>{label}</span>}
        </div>
        <DropdownContext.Provider value={contextValue}>{layerElement}</DropdownContext.Provider>
      </div>
    );
  }
);

export type DropdownType = {|
  ...AsComponentType,
  ...WithLayerType,
  ...WithShapeType,
  ...DropdownOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Dropdown)(WithLayer({ mode: LAYER_MODES.ORIGIN })(WithShape()(Dropdown))),
    {
      Item: Object.assign(AsComponent(ComponentNames.Dropdown.Item)(Item), {
        ALIGN_TYPES
      })
    }, {
      SHAPES
    }
  ): React.ComponentType<DropdownType> & {
    Item: React.ComponentType<ItemType> & {
      ALIGN_TYPES: Object
    }
  } & {
    SHAPES: Object
  }
);
