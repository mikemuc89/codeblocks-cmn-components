/* #__flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { INPUT_TYPES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { ALIGN_TYPES, KINDS, SOCIAL_MEDIA as MEDIA, SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, processChildren } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import DialogContext from '../contexts/DialogContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import AsControlWithItems, { ControlWithItemsContext, GroupItemContext, type AsControlWithItemsExtensionType, type AsControlWithItemsType } from '../hocs/components/AsControlWithItems';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import WithTheme, { type WithThemeExtensionType, type WithThemeType } from '../hocs/components/WithTheme';
import { type BadgePropType } from '../types';
import FontAwesome from './FontAwesome';
import DefaultBox, { type BoxType, type WrappedBoxType } from './Box';
import styles from './Button.scss';

const DEFAULT_BADGE_KIND = KINDS.PLAIN;
const DEFAULT_BADGE_SHAPE = SHAPES.CIRCLE;
const DEFAULT_BADGE_SIZE = 24;
const DEFAULT_ICON_SIZE = 24;
const DEFAULT_ICON_INNER_SIZE = 14;

const MEDIUM_TO_KIND = Object.freeze({
  [MEDIA.FACEBOOK]: KINDS.SOCIAL_FACEBOOK,
  [MEDIA.GOOGLE]: KINDS.SOCIAL_GOOGLE,
  [MEDIA.INSTAGRAM]: KINDS.SOCIAL_INSTAGRAM,
  [MEDIA.LINKEDIN]: KINDS.SOCIAL_LINKEDIN,
  [MEDIA.MAIL]: KINDS.SOCIAL_LINKEDIN,
  [MEDIA.PHONE]: KINDS.SOCIAL_PHONE,
  [MEDIA.PINTEREST]: KINDS.SOCIAL_PINTEREST,
  [MEDIA.SKYPE]: KINDS.SOCIAL_SKYPE,
  [MEDIA.SNAPCHAT]: KINDS.SOCIAL_SNAPCHAT,
  [MEDIA.SPOTIFY]: KINDS.SOCIAL_SPOTIFY,
  [MEDIA.TIKTOK]: KINDS.SOCIAL_TIKTOK,
  [MEDIA.TWITTER]: KINDS.SOCIAL_TWITTER,
  [MEDIA.YOUTUBE]: KINDS.SOCIAL_YOUTUBE
});

const MEDIUM_TO_COMPONENT = Object.freeze({
  [MEDIA.FACEBOOK]: <FontAwesome.Brand id="facebook" />,
  [MEDIA.GOOGLE]: <FontAwesome.Brand id="google" />,
  [MEDIA.INSTAGRAM]: <FontAwesome.Brand id="instagram" />,
  [MEDIA.LINKEDIN]: <FontAwesome.Brand id="linkedin" />,
  [MEDIA.MAIL]: <FontAwesome id="at" />,
  [MEDIA.PHONE]: <FontAwesome id="phone" />,
  [MEDIA.PINTEREST]: <FontAwesome.Brand id="pinterest" />,
  [MEDIA.SKYPE]: <FontAwesome.Brand id="skype" />,
  [MEDIA.SNAPCHAT]: <FontAwesome.Brand id="snapchat" />,
  [MEDIA.SPOTIFY]: <FontAwesome.Brand id="spotify" />,
  [MEDIA.TIKTOK]: <FontAwesome.Brand id="tiktok" />,
  [MEDIA.TWITTER]: <FontAwesome.Brand id="twitter" />,
  [MEDIA.YOUTUBE]: <FontAwesome.Brand id="youtube" />
});

const SOCIAL_ICON_TYPES = Object.freeze({
  LEFT: 'BUTTON.SOCIAL.ICON_TYPES.LEFT',
  NONE: 'BUTTON.SOCIAL.ICON_TYPES.NONE',
  RIGHT: 'BUTTON.SOCIAL.ICON_TYPES.RIGHT'
});

type ButtonOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  badge?: BadgePropType,
  children: React.Node,
  disabled?: boolean,
  focusable?: boolean,
  narrow?: boolean,
  onBlur?: (e: SyntheticFocusEvent<HTMLButtonElement>) => void,
  onFocus?: (e: SyntheticFocusEvent<HTMLButtonElement>) => void,
  value?: string
|};

type WrappedButtonType = {|
  ...AsComponentExtensionType,
  ...WithLinkExtensionType,
  ...ButtonOwnPropsType
|};

const BasicButtonChild = ({ children }) => (
  <span className={styles.Content}>{children}</span>
);

const Button = React.forwardRef(
  (
    { align, badge, children, className, disabled, focusable, linkProps, narrow, onBlur, onFocus, onKeyDown, themeElementRef, value }: WrappedButtonType,
    forwardedRef: ReactForwardedRefType
  ) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Button,
        cx.control(styles, 'Button', { disabled }),
        cx.alignSingle(styles, 'Button', { align }),
        narrow && styles.Button__Narrow,
        className
      )}
    >
      <button ref={themeElementRef} className={styles.Control} {...(focusable ? {} : { tabIndex: -1 })} type="button" value={value} onKeyDown={disabled ? undefined : onKeyDown} {...linkProps}>
        {badge &&
          React.cloneElement(badge, {
            className: cx(styles.Badge, !badge.props.position && styles.Badge__Inline, badge.props.className),
            kind: badge.props.kind || badge.props.children.props.kind || DEFAULT_BADGE_KIND,
            shape: badge.props.shape || badge.props.children.props.shape || DEFAULT_BADGE_SHAPE,
            size: badge.props.size || badge.props.children.props.size || DEFAULT_BADGE_SIZE
          })}
        {processChildren(
          children,
          (child, type) => ({
            className: cx(
              child.props.className,
              {
                [ComponentNames.CharIcon]: styles.Icon,
                [ComponentNames.ColorIcon]: styles.Icon,
                [ComponentNames.FontAwesome]: styles.Icon,
                [ComponentNames.FontAwesome.Brand]: styles.Icon,
                [ComponentNames.FontAwesome.Regular]: styles.Icon,
                [ComponentNames.Icon]: styles.Icon
              }[type]
            ),
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: child.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.ColorIcon]: {
                innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: child.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.FontAwesome]: {
                innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: child.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: child.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: child.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.Icon]: {
                size: child.props.size || DEFAULT_ICON_SIZE
              }
            }[type]
          }),
          BasicButtonChild
        )}
      </button>
    </div>
  )
);

type SocialOwnPropsType = {|
  ...ButtonOwnPropsType,
  icon?: $Values<typeof SOCIAL_ICON_TYPES>,
  medium?: $Values<typeof MEDIA>
|};

type WrappedSocialType = {|
  ...AsComponentExtensionType,
  ...SocialOwnPropsType,
|};

const Social = React.forwardRef(
  (
    { children, className, icon = SOCIAL_ICON_TYPES.LEFT, medium, ...props }: WrappedSocialType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const brandEl = React.cloneElement(MEDIUM_TO_COMPONENT[medium], {
      className: cx(styles.SocialIcon, brand.props.className)
    });

    return (
      <Button ref={forwardedRef} className={cx(styles.Social, className)} {...props} kind={MEDIUM_TO_KIND[medium]}>
        {icon === SOCIAL_ICON_TYPES.LEFT && brandEl}
        {children}
        {icon === SOCIAL_ICON_TYPES.RIGHT && brandEl}
      </Button>
    );
  }
);

type GroupOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: ReactElementsChildren<ButtonType>,
  kind?: $Values<typeof KINDS>,
  vertical?: boolean
|};

type WrappedGroupType = {|
  ...AsComponentExtensionType,
  ...GroupOwnPropsType
|};

const Group = React.forwardRef(
  ({ align, children, className, kind, vertical }: WrappedGroupType, forwardedRef: ReactForwardedRefType) => (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Group,
        cx.alignSingle(styles, 'Group', { align }),
        cx.orientation(styles, 'Group', { vertical }),
        className
      )}
    >
      {processChildren(
        children,
        (child) => ({
          kind: child.props.kind || kind
        })
      )}
    </div>
  )
);

type WrappedButtonBoxType = WrappedBoxType;

const Box = React.forwardRef(({ children, className, ...props }: WrappedButtonBoxType, forwardedRef) => {
  const { isDialog } = React.useContext(DialogContext);

  return (
    <DefaultBox className={cx(styles.Box, className)} bottom={isDialog ? 0 : 8} top={16} {...props}>
      {children}
    </DefaultBox>
  )
});

const WrappedButton = AsComponent(ComponentNames.Button)(WithTheme({ defaultKind: KINDS.PLAIN })(WithLink()(Button)));

type SelectableOwnPropsType = {|
  notSelectedIcon?: React.Node,
  notSelectedProps?: React.Node,
  selectedIcon?: React.Node,
  selectedProps?: React.Node
|};

type WrappedSelectableType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithThemeExtensionType,
  ...SelectableOwnPropsType,
  ...OptionOwnPropsType
|};

const Selectable = React.forwardRef(({
  border = true,
  children,
  className,
  controlElement,
  disabled,
  messageElement = null,
  notSelectedIcon,
  notSelectedProps,
  readonly,
  requiredElement = null,
  selectedIcon,
  selectedProps,
  setValue,
  value,
  ...props
}: WrappedSelectableType, forwardedRef) => {
  const { handleGroupOnChange } = React.useContext(GroupItemContext);
  const toggleValue = React.useCallback(() => setValue((oldValue) => !oldValue), [setValue]);
  const valueUpdater = React.useMemo(() => handleGroupOnChange || toggleValue, [handleGroupOnChange, toggleValue]);
  const valueBasedProps = React.useMemo(() => value ? selectedProps || { kind: KINDS.PRIMARY } : notSelectedProps || { kind: KINDS.PLAIN }, [notSelectedProps, selectedProps, value]);
  const icon = React.useMemo(() => value ? selectedIcon || <FontAwesome id="check" /> : notSelectedIcon || <FontAwesome id="" />, [notSelectedIcon, selectedIcon, value]);

  const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const { keyCode } = e;

    if ([KEY_CODES.ENTER].includes(keyCode)) {
      valueUpdater();
    }

    if (controlElement.props.onKeyDown) {
      controlElement.props.onKeyDown(e);
    }
  }, [controlElement.props.onKeyDown, valueUpdater]);

  const handleOnClick = React.useCallback((e: SnytheticEvent<HTMLElement>) => {
    e.stopPropagation();
    valueUpdater();
  }, [valueUpdater]);

  return (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Selectable,
        className
      )}
    >
      {controlElement}
      <WrappedButton
        {...props}
        onClick={(disabled || readonly) ? undefined : handleOnClick}
        onKeyDown={(disabled || readonly) ? undefined : handleOnKeyDown}
        border={border}
        focusable={false}
        {...valueBasedProps}
      >
        {icon}
        {children}
      </WrappedButton>
      {messageElement}
      {requiredElement}
    </div>
  );
});

export type SelectableType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType,
  ...WithThemeExtensionType,
  ...SelectableOwnPropsType,
  ...OptionOwnPropsType
|};

type ItemOwnPropsType = {|
  id: string  
|};

type WrappedItemType = {|
  ...WrappedOptionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(({
  className,
  disabled: itemDisabled,
  id,
  ...props
}: WrappedItemType, forwardedRef: ReactForwardedRefType) => {
  const itemRootRef = React.useRef(null);
  const { controlSelector, itemControlElement, onChangeByItemId, value, ...contextProps } = React.useContext(ControlWithItemsContext);
  const selected = React.useMemo(() => value.includes(id), [id, value]);

  const handleOnChange = React.useCallback(() => {
    onChangeByItemId(id);
  }, [id, onChangeByItemId]);

  const focusNextItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const el = itemRootRef.current;
    if (el) {
      const nextEl = el.nextElementSibling;
      if (nextEl) {
        e.preventDefault();
        nextEl.querySelector(controlSelector).focus();
      }
    }
  }, [controlSelector]);

  const focusPrevItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    e.preventDefault();
    const el = itemRootRef.current;
    if (el) {
      const nextEl = el.previousElementSibling;
      if (nextEl) {
        nextEl.querySelector(controlSelector).focus();
      }
    }
  }, [controlSelector]);

  const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const { keyCode } = e;

    if ([KEY_CODES.UP_ARROW, KEY_CODES.LEFT_ARROW].includes(keyCode)) {
      focusPrevItem(e);
    }
    if ([KEY_CODES.DOWN_ARROW, KEY_CODES.RIGHT_ARROW].includes(keyCode)) {
      focusNextItem(e);
    }

    if (itemControlElement.props.onKeyDown) {
      itemControlElement.props.onKeyDown(e);
    }
  }, [itemControlElement.props.onKeyDown, focusNextItem, focusPrevItem]);

  const disabled = React.useMemo(() => itemDisabled || itemControlElement.props.disabled, [itemDisabled, itemControlElement.props.disabled]);

  const controlElement = React.cloneElement(itemControlElement, {
    disabled,
    id,
    onChange: disabled ? undefined : handleOnChange,
    onKeyDown: disabled ? undefined : handleOnKeyDown
  });

  const contextValue = React.useMemo(() => ({
    handleGroupOnChange: handleOnChange
  }), [handleOnChange]);

  return (
    <div ref={mergeRefs(forwardedRef, itemRootRef)} className={cx(styles.Item, className)}>
      <GroupItemContext.Provider value={contextValue}>
        <Selectable {...props} {...contextProps} disabled={disabled} controlElement={controlElement} value={selected} />
      </GroupItemContext.Provider>
    </div>
  );
});

export type ItemType = {|
  ...WrappedCheckboxType,
  ...ItemOwnPropsType
|};

type SelectableGroupOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  border?: boolean,
  children: ReactElementsChildren<ItemType>
|};

type WrappedSelectableGroupType = {|
  ...AsComponentExtensionType,
  ...AsControlWithItemsExtensionType,
  ...SelectableGroupOwnPropsType
|};

const SelectableGroup = React.forwardRef(
  (
    {
      align = ALIGN_TYPES.LEFT,
      children,
      className,
      controlElement,
      messageElement,
      onBlur,
      onFocus,
      requiredElement,
      setValue,
      value
    }: WrappedSelectableGroupType,
    forwardedRef: ReactForwardedRefType
  ) => {
    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.SelectableGroup,
          className
        )}
        onBlur={onBlur}
        onFocus={onFocus}
      >
        {controlElement}
        <div className={styles.Content}>
          {processChildren(
            children,
            (child) => ({
              align: child.props.align || align
            })
          )}
        </div>
        {messageElement}
        {requiredElement}
      </div>
    );
  }
);

export type ButtonType = {|
  ...AsComponentType,
  ...WithLinkType,
  ...ButtonOwnPropsType
|};

export type SocialType = {|
  ...AsComponentType,
  ...SocialOwnPropsType,
|};

export type GroupType = {|
  ...AsComponentType,
  ...GroupOwnPropsType
|};

export type SelectableGroupType = {|
  ...AsComponentType,
  ...AsControlWithItemsType,
  ...SelectableGroupOwnPropsType
|};

export type ButtonBoxType = BoxType;

export default (
  Object.assign(
    WrappedButton,
    {
      Box: AsComponent(ComponentNames.Button.Box)(Box),
      Group: Object.assign(AsComponent(ComponentNames.Button.Group)(Group), {
        ALIGN_TYPES
      }),
      Selectable: Object.assign(AsComponent(ComponentNames.Button.Selectable)(AsControl({
        controlType: INPUT_TYPES.CHECKBOX,
        defaultValue: false,
        sanitizeValueForControl: () => '',
        style: { key: 'Selectable', styles }
      })(WithTheme()(Selectable))), {
        Group: Object.assign(AsComponent(ComponentNames.Button.Selectable.Group)(AsControlWithItems({
          changeTypeMulti: AsControlWithItems.MULTI_CHANGE_TYPES.TOGGLE,
          defaultMaximum: 1,
          sanitizeValueForControl: JSON.stringify,
          style: { key: 'SelectableGroup', styles }
        })(SelectableGroup)), {
          Item: AsComponent(ComponentNames.Button.Selectable.Group.Item)(Item)
        })
      }),
      Social: Object.assign(AsComponent(ComponentNames.Button.Social)(Social), {
        ALIGN_TYPES,
        ICON_TYPES: SOCIAL_ICON_TYPES,
        MEDIA
      })
    }, {
      ALIGN_TYPES,
      KINDS
    }
  ): React.ComponentType<ButtonType> & {
    Box: React.ComponentType<ButtonBoxType>,
    Group: React.ComponentType<GroupType> & {
      ALIGN_TYPES: Object
    },
    Social: React.ComponentType<SocialType> & {
      ALIGN_TYPES: Object,
      ICON_TYPES: Object,
      MEDIA: Object
    }
  } & {
    ALIGN_TYPES: Object,
    KINDS: Object
  }
);
