/* @flow */
import * as React from 'react';
import { type ReactChildren, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs, repeatPattern } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLink, { type WithLinkExtensionType, type WithLinkType } from '../hocs/components/WithLink';
import useFocusState from '../hooks/useFocusState';
import useHoverState from '../hooks/useHoverState';
import Message from './Message';
import Required from './Required';
import styles from './Rating.scss';

const DEFAULT_ICON_SIZE = 32;
const DEFAULT_ICON_INNER_SIZE = 24;

type SectionType = {|
  ...WithLinkExtensionType,
  className?: string,
  children: ReactChildren,
  hovering?: boolean,
  onChange?: (value: number) => void,
  onFocus?: (e: FocusEvent) => void,
  onHover?: (value?: number) => void,
  value: number
|};

const Section = WithLink({ forceClickable: true })(
  React.forwardRef(
    (
      {
        LinkComponent,
        className,
        children,
        hovering,
        linkProps,
        value,
        onChange,
        onFocus,
        onHover,
        selected
      }: SectionType,
      forwardedRef: ReactForwardedRefType
    ) => {
      const handleOnChange = React.useCallback(
        (e: Event) => {
          e.preventDefault();
          e.stopPropagation();

          if (onChange) {
            onChange(value);
          }
        },
        [onChange, value]
      );

      const { focusElementRef, focused } = useFocusState();
      const { hoverElementRef, hovered } = useHoverState();

      React.useEffect(() => {
        if (onHover) {
          onHover(hovered ? value : null);
        }
      }, [hovered, onHover, value]);

      React.useEffect(() => {
        if (onFocus) {
          onFocus(focused ? value : null);
        }
      }, [focused, onFocus, value]);

      return (
        <LinkComponent
          ref={mergeRefs(forwardedRef, focusElementRef, hoverElementRef)}
          className={cx(
            styles.Section,
            hovering && styles.Section__Hovering,
            selected && styles.Section__Selected,
            className
          )}
          {...linkProps}
          onClick={handleOnChange}
        >
          {children}
        </LinkComponent>
      );
    }
  )
);

type RatingOwnPropsType = {|
  ...WithMessagesType,
  align?: $Values<typeof ALIGN_TYPES>,
  children: React.Node,
  disabled?: boolean,
  focus?: boolean,
  halves?: boolean,
  onBlur?: (e: FocusEvent) => void,
  onChange?: (value: number) => void,
  onFocus?: (e: FocusEvent) => void,
  value: number  
|};

type WrappedRatingType = {|
  ...AsComponentExtensionType,
  ...RatingOwnPropsType
|};

const Rating = React.forwardRef(
  (
    {
      align = ALIGN_TYPES.LEFT,
      children,
      className,
      disabled,
      focus,
      errors,
      halves = false,
      helper,
      hints,
      onBlur,
      onChange,
      onFocus,
      required,
      value,
      warnings
    }: WrappedRatingType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const [controlValue, setControlValue] = React.useState(value);
    const [hoveringValue, setHoveringValue] = React.useState(null);

    React.useEffect(() => {
      setControlValue(value);
    }, [value]);

    const handleOnChange = React.useCallback(
      (value: number) => {
        if (onChange) {
          onChange(value);
        }
        setControlValue(value);
      },
      [onChange, setControlValue]
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Rating,
          cx.control(styles, 'Rating', { disabled, errors, focus, hints, required, warnings }),
          cx.alignSingle(styles, 'Rating', { align }),
          className
        )}
      >
        <div className={styles.HoverSurface}>
          {React.Children.map(children, (child: React.Node, index: number) => {
            const elementValue = (index + 1) / (halves ? 2 : 1);
            const hovering = hoveringValue !== null && hoveringValue >= elementValue;
            const selected = controlValue >= elementValue;
            return (
              <Section
                key={index}
                value={elementValue}
                disabled={disabled}
                hovering={hovering}
                selected={selected}
                onChange={disabled ? undefined : handleOnChange}
                onHover={disabled ? undefined : setHoveringValue}
                onFocus={disabled ? undefined : setHoveringValue}
              >
                {React.cloneElement(child, {
                  className: cx(
                    child.props.className,
                    {
                      [ComponentNames.CharIcon]: styles.Icon,
                      [ComponentNames.ColorIcon]: styles.Icon,
                      [ComponentNames.FontAwesome]: styles.Icon,
                      [ComponentNames.FontAwesome.Brand]: styles.Icon,
                      [ComponentNames.FontAwesome.Regular]: styles.Icon,
                      [ComponentNames.Icon]: styles.Icon
                    }[child.type.componentId]
                  ),
                  ...{
                    [ComponentNames.CharIcon]: {
                      innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                      size: child.props.size || DEFAULT_ICON_SIZE
                    },
                    [ComponentNames.ColorIcon]: {
                      innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                      size: child.props.size || DEFAULT_ICON_SIZE
                    },
                    [ComponentNames.FontAwesome]: {
                      innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                      size: child.props.size || DEFAULT_ICON_SIZE
                    },
                    [ComponentNames.FontAwesome.Brand]: {
                      innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                      size: child.props.size || DEFAULT_ICON_SIZE
                    },
                    [ComponentNames.FontAwesome.Regular]: {
                      innerSize: child.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                      size: child.props.size || DEFAULT_ICON_SIZE
                    },
                    [ComponentNames.Icon]: {
                      size: child.props.size || DEFAULT_ICON_SIZE
                    }
                  }[child.type.componentId]
                })}
              </Section>
            );
          })}
        </div>
        <Message className={styles.Message} errors={errors} helper={helper} hints={hints} warnings={warnings} />
        {required && <Required className={styles.Required} />}
      </div>
    );
  }
);

type NumbersOwnPropsType = {|
  max?: number  
|};

type WrappedNumbersType = {|
  ...WrappedRatingType,
  ...NumbersOwnPropsType
|};

const Numbers = React.forwardRef(({ className, max = 10, ...props }: WrappedNumbersType, forwardedRef: ReactForwardedRefType) => (
  <Rating className={cx(styles.Numbers, className)} {...props}>
    {repeatPattern(max, (index: number) => (
      <span key={index} className={styles.Number}>
        {index + 1}
      </span>
    ))}
  </Rating>
));

type StarsOwnPropsType = {|
  max?: number  
|};

type WrappedStarsType = {|
  ...WrappedRatingType,
  ...StarsOwnPropsType
|};

const Stars = React.forwardRef(({ className, halves, max = 10, ...props }: StarsType, forwardedRef: ReactForwardedRefType) => {
  const numberOfSections = React.useMemo(() => max * (halves ? 2 : 1), [halves, max]);

  return (
    <Rating className={cx(styles.Stars, halves && styles.Stars__Halves, className)} {...props}>
      {repeatPattern(numberOfSections, (index: number) => (
        <span key={index} className={styles.Star} />
      ))}
    </Rating>
  );
});

export type RatingType = {|
  ...AsComponentType,
  ...RatingOwnPropsType
|};

export type NumbersType = {|
  ...RatingType,
  ...NumbersOwnPropsType
|};

export type StarsType = {|
  ...RatingType,
  ...StarsOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Rating)(Rating), {
    Numbers: AsComponent(ComponentNames.Rating.Numbers)(Numbers),
    Stars: AsComponent(ComponentNames.Rating.Stars)(Stars)
  }, {
    ALIGN_TYPES
  }): React.ComponentType<RatingType> & {
    Numbers: React.ComponentType<NumbersType>,
    Stars: React.ComponentType<StarsType>
  } & {
    ALIGN_TYPES: Object
  }
);
