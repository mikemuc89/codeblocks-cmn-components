import Input from '../Input';

/* SNAPSHOTS */

/* empty value */

it('Input: Null value', () => {
  const tree = renderer.create(<Input value={null} />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Empty value', () => {
  const tree = renderer.create(<Input value="" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Empty value, custom classname', () => {
  const tree = renderer.create(<Input value="" className="testingPurposesOnly" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Empty value, placeholder', () => {
  const tree = renderer.create(<Input value="" placeholder="Placeholder" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Empty value, cannot clear', () => {
  const tree = renderer.create(<Input value="" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Empty value, focus', () => {
  const tree = renderer.create(<Input value="" focus />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Empty value, required', () => {
  const tree = renderer.create(<Input value="" required />).toJSON();
  expect(tree).toMatchSnapshot();
});

/* with value */

it('Input: Non empty value', () => {
  const tree = renderer.create(<Input value="Something" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Non empty, placeholder', () => {
  const tree = renderer.create(<Input value="Something" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Non empty value, cannot clear', () => {
  const tree = renderer.create(<Input value="Something" canClear={false} />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Non empty value, focus', () => {
  const tree = renderer.create(<Input value="Something" focus />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Non empty value, required', () => {
  const tree = renderer.create(<Input value="Something" required />).toJSON();
  expect(tree).toMatchSnapshot();
});

/* actionIcon */

it('Input: Empty value, actionIcon', () => {
  const tree = renderer.create(<Input value="" actionIcon="tick" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Empty value, actionIcon, cannot clear', () => {
  const tree = renderer.create(<Input value="" canClear={false} actionIcon="tick" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Non empty value, actionIcon', () => {
  const tree = renderer.create(<Input value="Something" actionIcon="tick" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Non empty value, actionIcon, cannot clear', () => {
  const tree = renderer.create(<Input value="Something" canClear={false} actionIcon="tick" />).toJSON();
  expect(tree).toMatchSnapshot();
});

/* messages */

it('Input: Errors', () => {
  const tree = renderer.create(<Input value="" errors="test" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Hints', () => {
  const tree = renderer.create(<Input value="" hints="test" />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('Input: Warnings', () => {
  const tree = renderer.create(<Input value="" warnings="test" />).toJSON();
  expect(tree).toMatchSnapshot();
});

/* name */

it('Input: name', () => {
  const tree = renderer.create(<Input value="" name="test" />).toJSON();
  expect(tree).toMatchSnapshot();
});

/* EVENT CALLBACKS */

it('Input: onAction', () => {
  const onAction = jest.fn();
  const tree = shallow(<Input actionIcon="tick" onAction={onAction} />);

  tree.find('.ActionIcon').simulate('click');
  expect(onAction).toBeCalled();
});

it('Input: onChange', () => {
  const onChange = jest.fn();
  const tree = shallow(<Input value="" onChange={onChange} />);
  const input = tree.find('.Input');

  input.simulate('change', { target: input });
  expect(onChange).toBeCalled();
});

// it('Input: onClear', () => {
//   const onClear = jest.fn();
//   const tree = shallow(<Input value="Something" onClear={onClear} />);

//   tree.find('.ActionIcon').simulate('click');
//   expect(onClear).toBeCalled();
// });

it('Input: onEnter', () => {
  const onEnter = jest.fn();
  const tree = shallow(<Input value="Something" onEnter={onEnter} />);

  tree.find('.Input').simulate('keydown', { keyCode: 13 });
  expect(onEnter).toBeCalled();
});

it('Input: onFocus, onBlur', () => {
  const onBlur = jest.fn();
  const onFocus = jest.fn();
  const tree = shallow(<Input placeholder="test" value="" onBlur={onBlur} onFocus={onFocus} />);
  const input = tree.find('.Input');

  input.simulate('focus', { target: input });
  expect(onFocus).toBeCalled();
  expect(input.placeholder).toBe('');

  input.simulate('blur', { target: input });
  expect(onBlur).toBeCalled();
  expect(input.placeholder).toBe('test');
});
