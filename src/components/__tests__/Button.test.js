import Button from '../Button';

it('Button: Test click', () => {
  const onClick = jest.fn();
  const tree = shallow(<Button onClick={onClick}>Test</Button>);

  tree.find('button').simulate('click');

  expect(onClick).toBeCalled();
});
