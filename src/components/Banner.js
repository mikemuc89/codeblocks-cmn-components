/* #__flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import { type ImagePropType } from '../types';
import styles from './Banner.scss';

type BannerOwnPropsType = {|
  border?: boolean,
  children: ReactChildren,
  height?: number,
  image: ImagePropType
|};

type WrappedBannerType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...BannerOwnPropsType
|};

const Banner = React.forwardRef(
  ({ backgroundElementRef, border, children, className, height = 240, image }: WrappedBannerType, forwardedRef: ReactForwardedRefType) => {
    const { innerWidth } = React.useContext(MainSizeContext);

    return (
      <div
        ref={mergeRefs(forwardedRef, backgroundElementRef)}
        className={cx(styles.Banner, border && styles.Banner__Border, className)}
        style={{
          ...(height ? { height: unitize(height) } : {})
        }}
      >
        {image &&
          React.cloneElement(image, {
            className: cx(
              image.props.className,
              {
                [ComponentNames.CharIcon]: styles.Icon,
                [ComponentNames.ColorIcon]: styles.Icon,
                [ComponentNames.FontAwesome]: styles.Icon,
                [ComponentNames.FontAwesome.Brand]: styles.Icon,
                [ComponentNames.FontAwesome.Regular]: styles.Icon,
                [ComponentNames.Icon]: styles.Icon,
                [ComponentNames.Image]: styles.Image
              }[image.type.componentId]
            ),
            ...{
              [ComponentNames.CharIcon]: {
                size: image.props.size || height
              },
              [ComponentNames.ColorIcon]: {
                size: image.props.size || height
              },
              [ComponentNames.FontAwesome]: {
                size: image.props.size || height
              },
              [ComponentNames.FontAwesome.Brand]: {
                size: image.props.size || height
              },
              [ComponentNames.FontAwesome.Regular]: {
                size: image.props.size || height
              },
              [ComponentNames.Icon]: {
                size: image.props.size || height
              },
              [ComponentNames.Image]: {
                height: image.props.height || height
              }
            }[image.type.componentId]
          })}
        <div className={styles.Content} style={{ width: unitize(innerWidth) }}>
          {children}
        </div>
      </div>
    );
  }
);

export type BannerType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...BannerOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Banner)(WithBackground()(Banner)), {}): React.ComponentType<BannerType>
);
