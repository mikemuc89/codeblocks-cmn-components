/* @flow */
import * as React from 'react';
import { KINDS } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type ReactForwardedRefType } from '../hocs/components/AsComponent';
import Bar, { type BarType, type WrappedBarType } from './Bar';
import styles from './Notification.scss';

const Notification = React.forwardRef(
  ({ children, className, ...props }: WrappedBarType, forwardedRef: ReactForwardedRefType) => (
    <Bar ref={forwardedRef} className={cx(styles.Notification, className)} {...props}>
      {children}
    </Bar>
  )
);

export type NotificationType = BarType;

export default (
  Object.assign(AsComponent(ComponentNames.Notification)(Notification), {
    KINDS
  }): React.ComponentType<NotificationType> & {
    KINDS: Object
  }
);
