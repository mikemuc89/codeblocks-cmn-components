/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type ImagePropType } from '../types';
import styles from './GraphicPreview.scss';

type GraphicPreviewOwnPropsType = {|
  children: ReactChildren,
  image?: ImagePropType
|};

type WrappedGraphicPreviewType = {|
  ...AsComponentExtensionType,
  ...GraphicPreviewOwnPropsType
|};

const GraphicPreview = React.forwardRef(
  ({ children, className, image }: WrappedGraphicPreviewType, forwardedRef: ReactForwardedRefType) => (
    <div ref={forwardedRef} className={cx(styles.GraphicPreview, className)}>
      {image &&
        React.cloneElement(image, {
          className: styles.Image,
          height: image.props.height || 240
        })}
      <div className={styles.Content}>{children}</div>
    </div>
  )
);

export type GraphicPreviewType = {|
  ...AsComponentType,
  ...GraphicPreviewOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.GraphicPreview)(GraphicPreview), {}): React.ComponentType<GraphicPreviewType>
);
