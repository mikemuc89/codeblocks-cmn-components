/* @flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type ImagePropType } from '../types';
import styles from './Error.scss';

const ICON_SIZE = 80;
const ICON_INNER_SIZE = 100;

type ErrorOwnPropsType = {|
  errors?: Object,
  image?: ImagePropType
|};

type WrappedErrorType = {|
  ...AsComponentExtensionType,
  ...ErrorOwnPropsType
|};

const Error = React.forwardRef(({ className, errors = {}, image }: WrappedErrorType, forwardedRef: ReactForwardedRefType) => {
  if (!errors) {
    return null;
  }
  const {
    title = 'Błąd aplikacji',
    description,
    exception,
    stack,
    traceback = stack
  } = typeof errors === 'object' ? errors : {
    description: '',
    title: errors,
    exception: '',
    stack: '',
    traceback: ''
  };

  return (
    <div ref={forwardedRef} className={cx(styles.Error, className)}>
      {image &&
        React.cloneElement(image, {
          alt: 'Brak danych',
          className: styles.Image,
          size: ICON_SIZE,
          ...{
            [ComponentNames.CharIcon]: {
              innerSize: ICON_INNER_SIZE
            },
            [ComponentNames.Coloricon]: {
              innerSize: ICON_INNER_SIZE
            },
            [ComponentNames.FontAwesome]: {
              innerSize: ICON_INNER_SIZE
            },
            [ComponentNames.FontAwesome.Brand]: {
              innerSize: ICON_INNER_SIZE
            },
            [ComponentNames.FontAwesome.Regular]: {
              innerSize: ICON_INNER_SIZE
            }
          }[image.type.componentId]
        })}
      <div className={styles.Title}>{title}</div>
      {description ? (
        <div className={styles.Description}>{description}</div>
      ) : (
        exception && <div className={styles.Exception}>{exception}</div>
      )}
      {traceback && (
        <div className={styles.Traceback}>
          <pre>{traceback}</pre>
        </div>
      )}
    </div>
  );
});

export type ErrorType = {|
  ...AsComponentType,
  ...ErrorOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Error)(Error), {}): React.ComponentType<ErrorType>
);
