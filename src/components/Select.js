/* @flow */
import * as React from 'react';
import { formatBooleanId } from '@omnibly/codeblocks-cmn-formatters';
import { type ReactChildren, type ReactElementsChildren, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { BOOL_IDS, INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { cx, guid, mergeRefs, repeatPattern } from '@omnibly/codeblocks-cmn-utils';
import { numberToTimeString, timeStringToNumber } from '@omnibly/codeblocks-cmn-utils/src/utils/calendar';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControlWithItems, { ControlWithItemsContext, GroupItemContext, type AsControlWithItemsExtensionType, type AsControlWithItemsType } from '../hocs/components/AsControlWithItems';
import WithLayer, { LayerContext, LAYER_MODES, type WithLayerExtensionType, type WithLayerType } from '../hocs/components/WithLayer';
import useKeyboardSubmit from '../hooks/useKeyboardSubmit';
import FontAwesome from './FontAwesome';
import Option, { type OptionType, type WrappedOptionType } from './Option';
import styles from './Select.scss';

type ItemOwnPropsType = {|
  id: string  
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...OptionType,
  ...ItemOwnPropsType
|};

export const Item = React.forwardRef((
  { className, disabled: itemDisabled, id, ...props }: WrappedItemType,
  forwardedRef: ReactForwardedRefType
) => {
  const itemRootRef = React.useRef(null);
  const { controlSelector, itemControlElement, multiple, onChangeByItemId, value, ...contextProps } = React.useContext(ControlWithItemsContext);
  const { hideLayer, sustainOnPick } = React.useContext(LayerContext);
  const selected = React.useMemo(() => multiple ? value.includes(id) : value === id, [id, multiple, value]);

  const handleOnChange = React.useCallback(() => {
    onChangeByItemId(id);
    if (!sustainOnPick) {
      hideLayer();
    }
  }, [hideLayer, id, onChangeByItemId, sustainOnPick]);

  const focusNextItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    e.preventDefault();

    const el = itemRootRef.current;
    if (el) {
      const nextEl = el.nextElementSibling || el.parentElement.firstChild;
      if (nextEl) {
        nextEl.querySelector(controlSelector).focus();
      }
    }
  }, [controlSelector]);

  const focusPrevItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    e.preventDefault();

    const el = itemRootRef.current;
    if (el) {
      const nextEl = el.previousElementSibling || el.parentElement.lastChild;
      if (nextEl) {
        nextEl.querySelector(controlSelector).focus();
      }
    }
  }, [controlSelector]);

  const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const { keyCode } = e;

    if ([KEY_CODES.UP_ARROW, KEY_CODES.LEFT_ARROW].includes(keyCode)) {
      focusPrevItem(e);
    }
    if ([KEY_CODES.DOWN_ARROW, KEY_CODES.RIGHT_ARROW].includes(keyCode)) {
      focusNextItem(e);
    }

    if (itemControlElement.props.onKeyDown) {
      itemControlElement.props.onKeyDown(e);
    }
  }, [itemControlElement.props.onKeyDown, focusNextItem, focusPrevItem]);

  const disabled = React.useMemo(() => itemDisabled || itemControlElement.props.disabled, [itemDisabled, itemControlElement.props.disabled]);

  const controlElement = React.cloneElement(itemControlElement, {
    disabled,
    id,
    onChange: disabled ? undefined : handleOnChange,
    onKeyDown: disabled ? undefined : handleOnKeyDown
  });

  const contextValue = React.useMemo(() => ({
    handleGroupOnChange: handleOnChange
  }), [handleOnChange]);

  return (
    <div ref={mergeRefs(forwardedRef, itemRootRef)} className={cx(styles.Item, className)}>
      <GroupItemContext.Provider value={contextValue}>
        <Option.Selectable
          {...props}
          {...contextProps}
          className={styles.Option}
          disabled={disabled}
          controlElement={controlElement}
          value={selected}
        />
      </GroupItemContext.Provider>
    </div>
  );
});

type RemoveIconType = {|
  id: string
|};

const RemoveIcon = React.forwardRef(({ id }: RemoveIconType, forwardedRef: ReactForwardedRefType) => {
  const { onRemoveByItemId } = React.useContext(ControlWithItemsContext);

  const handleOnChange = React.useCallback((e) => {
    e.stopPropagation();
    onRemoveByItemId(id);
  }, [id, onRemoveByItemId]);

  return (
    <FontAwesome id="times" onClick={handleOnChange} size={38} innerSize={16} />
  );
});

export type ItemType = {|
  ...AsComponentType,
  ...OptionType,
  ...ItemOwnPropsType
|};

type SelectOwnPropsType = {|
  emptyElement: string | React.Element<OptionType>,
  groupMultiple?: boolean,
  groupMultipleElement: string | React.Element<OptionType>
|};

type WrappedSelectType = {|
  ...AsComponentExtensionType,
  ...AsControlWithItemsExtensionType,
  ...WithLayerExtensionType,
  ...SelectOwnPropsType
|};

const Select = React.forwardRef(
  (
    {
      children,
      className,
      controlElement,
      coverElement,
      disabled,
      emptyElement = 'Wybierz',
      groupMultiple,
      groupMultipleElement = 'Wybrano wiele elementów',
      layerElement,
      messageElement,
      multiple,
      onClear,
      requiredElement,
      setCover,
      setValue,
      value
    }: SelectType,
    forwardedRef: WrappedSelectType
  ) => {
    const { showLayer } = React.useContext(LayerContext);

    const selectedElement = React.useMemo(() => {
      if (multiple) {
        const selectedItemsCount = value.length;
        if (selectedItemsCount === 1) {
          const [id] = value;
          const selectedChild = children && React.Children.toArray(children).find((child: React.Element<ItemType>) => child.props.id === id);
          if (selectedChild) {
            const action = onClear ? <RemoveIcon id={id} /> : undefined;
            return (
              <Option
                {...selectedChild.props}
                action={action}
                arrow={Option.ARROW_KINDS.OPEN}
                className={styles.Selected}
                disabled={disabled}
                onClick={showLayer}
                border
              />
            );
          }
        } else if (selectedItemsCount > 1) {
          if (groupMultiple) {
            const action = onClear ? <FontAwesome id="times" onClick={onClear} /> : undefined;
            if (typeof groupMultipleElement === 'string') {
              return (
                <Option
                  className={styles.Group}
                  action={action}
                  arrow={Option.ARROW_KINDS.OPEN}
                  border={true}
                  disabled={disabled}
                  onClick={showLayer}
                >
                  {groupMultipleElement}
                </Option>
              );
            }
            return React.cloneElement(groupMultipleElement, {
              action,
              arrow: groupMultipleElement.props.arrow || Option.ARROW_KINDS.OPEN,
              border: false,
              className: styles.Group,
              disabled,
              onClick: showLayer
            });
          }
          if (children) {
            return (
              <>
                {React.Children.toArray(children)
                  .filter((child: React.Element<ItemType>) => value.includes(child.props.id))
                  .map((child: React.Element<ItemType>, index: number) =>
                    child
                      ? React.cloneElement(child, {
                          action: onClear ? <RemoveIcon id={child.props.id} /> : undefined,
                          arrow: index === 0 ? Option.ARROW_KINDS.OPEN : null,
                          border: true,
                          arrow: null,
                          className: styles.Selected,
                          disabled,
                          key: child.props.id,
                          onClick: showLayer
                        })
                      : null
                  )}
              </>
            );
          }
        }
      } else if (value) {
        const selectedChild =
          children && React.Children.toArray(children).find((child: React.Element<ItemType>) => child.props.id === value);
        if (selectedChild) {
          const action = onClear ? <RemoveIcon id={value} /> : undefined;
          return (
            <Option
              {...selectedChild.props}
              action={action}
              arrow={Option.ARROW_KINDS.OPEN}
              className={styles.Selected}
              disabled={disabled}
              onClick={showLayer}
              border
            />
          );
        }
      }
      if (typeof emptyElement === 'string') {
        return (
          <Option
            className={styles.Empty}
            arrow={Option.ARROW_KINDS.OPEN}
            border={true}
            disabled={disabled}
            onClick={showLayer}
          >
            {emptyElement}
          </Option>
        );
      }
      return React.cloneElement(emptyElement, {
        arrow: emptyElement.props.arrow || Option.ARROW_KINDS.OPEN,
        className: styles.Empty,
        disabled
      });
    }, [
      children,
      disabled,
      emptyElement,
      groupMultiple,
      groupMultipleElement,
      multiple,
      onClear,
      showLayer,
      value
    ]);

    React.useEffect(() => {
      setCover(selectedElement);
    }, [selectedElement, setCover]);

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Select,
          className
        )}
      >
        {controlElement}
        {coverElement}
        {requiredElement}
        {messageElement}
        {layerElement}
      </div>
    );
  }
);

const WrappedSelect = AsComponent(ComponentNames.Select)(AsControlWithItems({
  changeTypeMulti: AsControlWithItems.MULTI_CHANGE_TYPES.TOGGLE,
  defaultMaximum: 1,
  defaultValue: null,
  sanitizeValueForControl: JSON.stringify,
  style: { key: 'Select', styles }
})(WithLayer({
  mode: LAYER_MODES.VERTICAL,
  layerClassName: styles.Layer
})(Select)));

type WrappedBoolType = WrappedSelectType;

const Bool = React.forwardRef((props: WrappedBoolType, forwardedRef: ReactForwardedRefType) => (
  <WrappedSelect ref={forwardedRef} {...props} multiple={false}>
    {[BOOL_IDS.YES, BOOL_IDS.NO].map((id: $Values<typeof BOOL_IDS>) => (
      <Item key={id} id={id}>
        {formatBooleanId(id)}
      </Item>
    ))}
  </WrappedSelect>
));

type NumberOwnPropsType = {|
  available?: Array<number>,
  every?: number,
  max?: number,
  min?: number
|};

type WrappedNumberType = {|
  ...WrappedSelectType,
  ...NumberOwnPropsType
|};

const Number = React.forwardRef(
  ({ available, every = 1, max = 10, min = 0, ...props }: WrappedNumberType, forwardedRef: ReactForwardedRefType) => {
    const everyNum = React.useMemo(() => parseInt(every), [every]);
    const minNum = React.useMemo(() => parseInt(min), [min]);
    const maxNum = React.useMemo(() => parseInt(max), [max]);
    const nElements = React.useMemo(() => Math.ceil((maxNum - minNum) / everyNum), [maxNum, minNum, everyNum]);

    return (
      <WrappedSelect ref={forwardedRef} {...props}>
        {available
          ? available.map((item: string) => (
              <Item key={item} id={item}>
                {item}
              </Item>
            ))
          : repeatPattern(nElements, (index: number) => {
              const item = minNum + index * everyNum;
              return (
                <Item key={item} id={item}>
                  {item}
                </Item>
              );
            })}
      </WrappedSelect>
    );
  }
);

type TimeOwnPropsType = {|
  frequency?: number,
  maxTime?: string,
  minTime?: string
|};

type WrappedTimeType = {|
  ...WrappedSelectType,
  ...TimeOwnPropsType
|};

const Time = React.forwardRef(
  ({ frequency = 15, maxTime = '23:59', minTime = '00:00', ...props }: WrappedTimeType, forwardedRef: ReactForwardedRefType) => {
    const freq = React.useMemo(() => parseInt(frequency), [frequency]);
    const minTimeNum = React.useMemo(() => timeStringToNumber(minTime), [minTime]);
    const maxTimeNum = React.useMemo(() => timeStringToNumber(maxTime), [maxTime]);
    const nElements = React.useMemo(() => Math.ceil((maxTimeNum - minTimeNum) / freq), [maxTimeNum, minTimeNum, freq]);

    return (
      <WrappedSelect ref={forwardedRef} {...props}>
        {repeatPattern(nElements, (index: number) => {
          const id = numberToTimeString(minTimeNum + index * freq);
          return (
            <Item key={id} id={id}>
              {id}
            </Item>
          );
        })}
      </WrappedSelect>
    );
  }
);

export type SelectType = {|
  ...AsComponentType,
  ...AsControlWithItemsType,
  ...WithLayerType,
  ...SelectOwnPropsType
|};

export type BoolType = SelectType;

export type NumberType = {|
  ...SelectType,
  ...NumberOwnPropsType
|};

export type TimeType = {|
  ...SelectType,
  ...TimeOwnPropsType
|};

export default (
  Object.assign(WrappedSelect, {
    Bool: Object.assign(AsComponent(ComponentNames.Select.Bool)(Bool), {
      OPTIONS: BOOL_IDS
    }),
    Item: AsComponent(ComponentNames.Select.Item)(Item),
    Number: AsComponent(ComponentNames.Select.Number)(Number),
    Time: AsComponent(ComponentNames.Select.Time)(Time)
  }): React.ComponentType<SelectType> & {
    Bool: React.ComponentType<BoolType> & {
      OPTIONS: Object
    },
    Item: React.ComponentType<ItemType>,
    Number: React.ComponentType<NumberType>,
    Time: React.ComponentType<TimeType>
  }
);
