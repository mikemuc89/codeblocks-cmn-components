/* @flow */
import * as React from 'react';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControl, { type AsControlExtensionType, type AsControlType } from '../hocs/components/AsControlV2';
import useDrag, { EVENT_KINDS, type DragCallbackDataType } from '../hooks/useDrag';
import Input from './Input';
import Message from './Message';
import Required from './Required';
import styles from './Slider.scss';

type SliderGenericParamsContextType = {|
  disabled?: boolean,
  max?: number,
  min?: number,
  range?: number,
  step?: number
|};

const SliderGenericParamsContext = React.createContext({
  disabled: false,
  max: 100,
  min: 0,
  range: 100,
  step: 1
});

const VALUE_KINDS = Object.freeze({ BOTTOM: 'BOTTOM', TOP: 'TOP' });
const CHANGE_KINDS = Object.freeze({ DECREMENT: 'DECREMENT', INCREMENT: 'INCREMENT' });
const VALUE_KEYS = Object.freeze({ [VALUE_KINDS.BOTTOM]: 'bottom', [VALUE_KINDS.TOP]: 'top' });

type HandleType = {|
  value?: number
|};

const Handle = React.forwardRef(({ value }: HandleType, forwardedRef: ReactForwardedRefType) => {
  const { disabled, max, min, range, step } = React.useContext(SliderGenericParamsContext);
  const style = React.useMemo(() => {
    const boundValue = Math.min(Math.max(min, (value / range) * 100), max);
    const left = step ? Math.round(boundValue / step) * step : boundValue;
    return { left: `${left}%` };
  }, [min, max, value, range, step]);

  return <span className={styles.Handle} ref={forwardedRef} style={style} {...(!disabled && { tabIndex: 0 })} />;
});

type SliderOwnPropsType = {|
  ...WithMessagesType,
  decimals?: number,
  input?: boolean,
  max?: number,
  min?: number,
  step?: number  
|};

type WrappedSliderType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType<number>,
  ...SliderOwnPropsType
|};

const Slider = React.forwardRef(
  (
    {
      className,
      controlElement,
      decimals = 0,
      disabled,
      input,
      max = 100,
      messageElement,
      min = 0,
      onBlur,
      onFocus,
      required,
      requiredElement,
      setValue,
      step = 1,
      value
    }: WrappedSliderType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const [cache, setCache] = React.useState(value ? value.toString() : '');
    const [position, setPosition] = React.useState(value || 0);
    const range = React.useMemo(() => max - min, [max, min]);

    React.useEffect(() => {
      const newCache = [null, undefined].includes(value) ? '' : value.toString();
      setCache(newCache);
      setPosition(value);
    }, [value, setCache, setPosition]);

    const prepareValue = React.useCallback((value: string | number) => {
      const parsed = parseFloat(value);
      if (isNaN(parsed)) {
        return null;
      }

      const multiplier = 10 ** decimals;
      const constraintValue = Math.max(min, Math.min(max, parsed));
      return step ? Math.round(constraintValue / step) * step : Math.round(constraintValue * multiplier) / multiplier;
    }, [decimals, max, min, step]);

    const updateValue = React.useCallback(({ absolute: { px }, kind }: DragCallbackDataType) => {
      const newValue = prepareValue(min + range * px);
      setPosition(newValue);
      if (kind === EVENT_KINDS.DRAG_END) {
        setValue(newValue);
      } else {
        setCache(newValue.toString());
      }
    }, [range, prepareValue, setCache, setPosition, setValue]);

    const updateValueByUnit = React.useCallback(
      (changeKind: $Values<typeof CHANGE_KINDS>) => {
        const delta = {
          [CHANGE_KINDS.DECREMENT]: -(step || 1),
          [CHANGE_KINDS.INCREMENT]: step || 1
        }[changeKind];

        setValue((oldValue) => prepareValue(oldValue + delta));
      },
      [step, prepareValue, setValue]
    );

    const onDecrement = React.useCallback(() => updateValueByUnit(CHANGE_KINDS.DECREMENT), [updateValueByUnit]);
    const onIncrement = React.useCallback(() => updateValueByUnit(CHANGE_KINDS.INCREMENT), [updateValueByUnit]);

    const onInputChange = React.useCallback((value: string) => {
      setCache(value);
    }, [setCache]);

    const onInputBlur = React.useCallback((e: FocusEvent) => {
      const sanitized = prepareValue(e.target.value);
      setValue((oldValue) => {
        if (sanitized === null) {
          setCache(oldValue ? oldValue.toString() : '');
          return oldValue
        }
        setValue(sanitized);
      });
      if (onBlur) {
        onBlur(e);
      }
    }, [onBlur, prepareValue, setCache, setValue]);

    const handleRef = useDrag({
      disabled,
      onDecrement,
      onDrag: updateValue,
      onDragEnd: updateValue,
      onIncrement
    });

    const contextValue = React.useMemo(
      () => ({
        disabled,
        max,
        min,
        range,
        step
      }),
      [disabled, max, min, range, step]
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Slider,
          input && styles.Slider__WithInput,
          className
        )}
      >
        {controlElement}
        <div className={styles.Content}>
          <div className={styles.Track}>
            <SliderGenericParamsContext.Provider value={contextValue}>
              <div className={styles.HandleWrapper}>
                <Handle ref={handleRef} value={position} />
              </div>
            </SliderGenericParamsContext.Provider>
          </div>
        </div>
        {input ? (
          <Input
            className={styles.Input}
            disabled={disabled}
            value={cache}
            onBlur={onInputBlur}
            onChange={onInputChange}
            onFocus={onFocus}
            required={required}
            canClear={false}
          />
        ) : requiredElement}
        {messageElement}
      </div>
    );
  }
);

type RangeValueType = {|
  bottom: number,
  top: number
|};

type RangeOwnPropsType = {|
  ...WithMessagesType,
  decimals?: number,
  focus?: boolean,
  input?: boolean,
  max?: number,
  min?: number,
  onBlur: (e: FocusEvent) => void,
  onChange: (value: RangeValueType) => void,
  onFocus: (e: FocusEvent) => void,
  step?: number  
|};

type WrappedRangeType = {|
  ...AsComponentExtensionType,
  ...AsControlExtensionType<RangeValueType>,
  ...RangeOwnPropsType
|};

const Range = React.forwardRef((
  {
    className,
    controlElement,
    decimals = 0,
    disabled,
    input,
    max = 100,
    messageElement,
    min = 0,
    onBlur,
    onChange,
    onFocus,
    required,
    requiredElement,
    setValue,
    step = 1,
    value
  }: WrappedRangeType,
  forwardedRef: ReactForwardedRefType
) => {
  const maxForBottom = React.useRef(value?.top || max - step);
  const minForTop = React.useRef(value?.bottom || min + step);
  const [cacheBottom, setCacheBottom] = React.useState(value?.bottom ? value.bottom.toString() : '');
  const [cacheTop, setCacheTop] = React.useState(value?.top ? value.top.toString() : '');
  const [positionBottom, setPositionBottom] = React.useState((value?.bottom) || 0);
  const [positionTop, setPositionTop] = React.useState((value?.top) || 0);
  const range = React.useMemo(() => max - min, [max, min]);

  React.useEffect(() => {
    const newCacheBottom = [null, undefined].includes(value?.bottom) ? '' : value.bottom.toString();
    const newCacheTop = [null, undefined].includes(value?.top) ? '' : value.top.toString();
    setCacheBottom(newCacheBottom);
    setCacheTop(newCacheTop);
    setPositionBottom(value.bottom);
    setPositionTop(value.top);
    maxForBottom.current = value.top - step;
    minForTop.current = value.bottom + step;
  }, [value, setCacheBottom, setCacheTop, setPositionBottom, setPositionTop, step]);

  const prepareValue = React.useCallback((changeValue: number, kind: $Values<typeof VALUE_KINDS>) => {
    const parsed = parseFloat(changeValue);

    if (isNaN(parsed)) {
      return null;
    }

    const actualMax = {
      [VALUE_KINDS.BOTTOM]: maxForBottom.current,
      [VALUE_KINDS.TOP]: max
    }[kind];

    const actualMin = {
      [VALUE_KINDS.BOTTOM]: min,
      [VALUE_KINDS.TOP]: minForTop.current
    }[kind];

    const multiplier = 10 ** decimals;
    const constraintValue = Math.max(actualMin, Math.min(actualMax, parsed));

    return step ? Math.round(constraintValue / step) * step : Math.round(constraintValue * multiplier) / multiplier;
  }, [decimals, max, min, step]);

  const updateValue = React.useCallback(({ absolute: { px }, kind }: DragCallbackDataType, valueKind: $Values<typeof VALUE_KINDS>) => {
    const newValuePart = prepareValue(min + range * px, valueKind);
    const key = VALUE_KEYS[valueKind];
    const [updateCache, updatePosition] = {
      [VALUE_KINDS.BOTTOM]: [setCacheBottom, setPositionBottom],
      [VALUE_KINDS.TOP]: [setCacheTop, setPositionTop]
    }[valueKind];
    updatePosition(newValuePart);
    if (kind === EVENT_KINDS.DRAG_END) {
      setValue((old: RangeValueType) => {
        const newValue = {
          ...old,
          [key]: newValuePart
        };
        return newValue;
      });  
    } else {
      updateCache(newValuePart.toString());
    }
  }, [min, prepareValue, range, setCacheBottom, setCacheTop, setPositionBottom, setPositionTop, setValue]);

  const updateBottom = React.useCallback((params: Object) => updateValue(params, VALUE_KINDS.BOTTOM), [updateValue]);
  const updateTop = React.useCallback((params: Object) => updateValue(params, VALUE_KINDS.TOP), [updateValue]);

  const updateValueByUnit = React.useCallback((valueKind: $Values<typeof VALUE_KINDS>, changeKind: $Values<typeof CHANGE_KINDS>) => {
    const key = VALUE_KEYS[valueKind];
    const delta = {
      [CHANGE_KINDS.DECREMENT]: -(step || 1),
      [CHANGE_KINDS.INCREMENT]: step || 1
    }[changeKind];

    setValue((oldValue: RangeValueType) => ({ ...oldValue, [key]: prepareValue(oldValue[key] + delta, valueKind) }));
  }, [prepareValue, setValue, step]);

  const decrementBottom = React.useCallback(() => updateValueByUnit(VALUE_KINDS.BOTTOM, CHANGE_KINDS.DECREMENT), [updateValueByUnit]);
  const incrementBottom = React.useCallback(() => updateValueByUnit(VALUE_KINDS.BOTTOM, CHANGE_KINDS.INCREMENT), [updateValueByUnit]);
  const decrementTop = React.useCallback(() => updateValueByUnit(VALUE_KINDS.TOP, CHANGE_KINDS.DECREMENT), [updateValueByUnit]);
  const incrementTop = React.useCallback(() => updateValueByUnit(VALUE_KINDS.TOP, CHANGE_KINDS.INCREMENT),[updateValueByUnit]);

  const onInputChange = React.useCallback((value: number, valueKind: $Values<typeof VALUE_KINDS>) => {
    const key = VALUE_KEYS[valueKind];
    const updateCache = {
      [VALUE_KINDS.BOTTOM]: setCacheBottom,
      [VALUE_KINDS.TOP]: setCacheTop
    }[valueKind];
    updateCache(value);
  }, [setCacheBottom, setCacheTop]);

  const onBottomInputChange = React.useCallback((value: number) => onInputChange(value, VALUE_KINDS.BOTTOM), [onInputChange]);
  const onTopInputChange = React.useCallback((value: number) => onInputChange(value, VALUE_KINDS.TOP), [onInputChange]);

  const onInputBlur = React.useCallback((e: FocusEvent, valueKind: $Values<typeof VALUE_KINDS>) => {
    const key = VALUE_KEYS[valueKind];
    const sanitized = prepareValue(e.target.value, valueKind);
    const updateCache = {
      [VALUE_KINDS.BOTTOM]: setCacheBottom,
      [VALUE_KINDS.TOP]: setCacheTop
    }[valueKind];
    setValue((oldValue) => {
      if (sanitized === null) {
        updateCache(oldValue ? oldValue[key].toString() : '');
        return oldValue
      }
      return {
        ...oldValue,
        [key]: sanitized
      }
    });
    if (onBlur) {
      onBlur(e);
    }
  }, [onBlur, prepareValue, setCacheBottom, setCacheTop, setValue]);

  const onBottomInputBlur = React.useCallback((e: FocusEvent) => onInputBlur(e, VALUE_KINDS.BOTTOM), [onInputBlur]);
  const onTopInputBlur = React.useCallback((e: FocusEvent) => onInputBlur(e, VALUE_KINDS.TOP), [onInputBlur]);

  const bottomHandleRef = useDrag({
    disabled,
    onDecrement: decrementBottom,
    onDrag: updateBottom,
    onDragEnd: updateBottom,
    onIncrement: incrementBottom
  });

  const topHandleRef = useDrag({
    disabled,
    onDecrement: decrementTop,
    onDrag: updateTop,
    onDragEnd: updateTop,
    onIncrement: incrementTop
  });

  const contextValue = React.useMemo(() => ({ disabled, max, min, range, step }), [disabled, max, min, range, step]);

  return (
    <div
      ref={forwardedRef}
      className={cx(
        styles.Range,
        input && styles.Range__WithInput,
        className
      )}
    >
      {controlElement}
      {input && (
        <Input
          className={styles.Input}
          disabled={disabled}
          value={cacheBottom}
          onBlur={onBottomInputBlur}
          onChange={onBottomInputChange}
          onFocus={onFocus}
          canClear={false}
        />
      )}
      <div className={styles.Content}>
        <div className={styles.Track}>
          <SliderGenericParamsContext.Provider value={contextValue}>
            <div className={styles.HandleWrapper}>
              <Handle ref={bottomHandleRef} value={positionBottom} />
              <Handle ref={topHandleRef} value={positionTop} />
            </div>
          </SliderGenericParamsContext.Provider>
        </div>
      </div>
      {input ? (
        <Input
          className={styles.Input}
          disabled={disabled}
          value={cacheTop}
          onBlur={onTopInputBlur}
          onChange={onTopInputChange}
          onFocus={onFocus}
          required={required}
          canClear={false}
        />
      ) : requiredElement}
      {messageElement}
    </div>
  );
});

export type SliderType = {|
  ...AsComponentType,
  ...AsControlType<number>,
  ...SliderOwnPropsType
|};

export type RangeType = {|
  ...AsComponentType,
  ...AsControlType<RangeValueType>,
  ...RangeOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Slider)(AsControl({
      style: { key: 'Slider', styles }
    })(Slider)),
    {
      Range: AsComponent(ComponentNames.Slider.Range)(
        AsControl({
          defaultValue: { bottom: null, top: null },
          sanitizeValueForControl: JSON.stringify
        })(Range)
      )
    }
  ): React.ComponentType<SliderType> & {
    Range: React.ComponentType<RangeType>
  }
);
