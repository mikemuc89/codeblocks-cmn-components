/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { SHAPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithLayer, { LAYER_MODES, LAYER_KINDS, type WithLayerExtensionType, type WithLayerType } from '../hocs/components/WithLayer';
import WithShape, { type WithShapeExtensionType, type WithShapeType } from '../hocs/components/WithShape';
import { type IconPropType } from '../types';
import FontAwesome from './FontAwesome';
import styles from './Infotip.scss';

const ICON_SIZE = 30;
const ICON_INNER_SIZE = 16;

const DefaultCover = () => (
  <FontAwesome className={styles.Icon} size={ICON_SIZE} innerSize={ICON_INNER_SIZE} id="info" />
);

type InfotipOwnPropsType = {|
  accent?: boolean,
  border?: boolean,
  children: ReactChildren,
  disabled?: boolean,
  focus?: boolean,
  icon?: IconPropType  
|};

type WrappedInfotipType = {|
  ...AsComponentExtensionType,
  ...WithLayerExtensionType,
  ...WithShapeExtensionType,
  ...InfotipOwnPropsType
|};

const Infotip = React.forwardRef(
  (
    {
      accent,
      border = true,
      children,
      className,
      disabled,
      focus,
      icon,
      layerElement,
      layerTriggerElementRef,
      shapeElementRef
    }: WrappedInfotipType,
    forwardedRef: ReactForwardedRefType
  ) => {
    const iconElement = icon ? (
      React.cloneElement(icon, {
        className: styles.Icon,
        innerSize: icon.props.innerSize || ICON_INNER_SIZE,
        size: icon.props.size || ICON_SIZE
      })
    ) : (
      <DefaultCover />
    );

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Infotip,
          cx.control(styles, 'Infotip', { disabled, focus }),
          accent && styles.Infotip__Accent,
          border && styles.Infotip__Border,
          className
        )}
      >
        <div
          ref={mergeRefs(layerTriggerElementRef, shapeElementRef)}
          className={styles.Cover}
          {...(disabled ? {} : { tabIndex: 0 })}
        >
          {iconElement}
        </div>
        {layerElement}
      </div>
    );
  }
);

export type InfotipType = {|
  ...AsComponentType,
  ...WithLayerType,
  ...WithShapeType,
  ...InfotipOwnPropsType
|};

export default (
  Object.assign(
    AsComponent(ComponentNames.Infotip)(
      WithLayer({ kind: LAYER_KINDS.TEXT, mode: LAYER_MODES.ORIGIN })(WithShape()(Infotip))
    ),
    {
      SHAPES
    }
  ): React.ComponentType<InfotipType> & {
    SHAPES: Object
  }
);
