/* @flow */
import * as React from 'react';
import { type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import useOpenState from '../hooks/useOpenState';
import FontAwesome from './FontAwesome';
import styles from './Tree.scss';

const ICON_INNER_SIZE = 14;
const ICON_INNER_SIZE_EMPTY = 4;
const ICON_SIZE = 24;

const ICONS = Object.freeze({
  CLOSE: 'angle-up',
  EMPTY: 'circle',
  OPEN: 'angle-down'
});

type ItemOwnPropsType = {|
  children?: ReactElementsChildren<any>,
  disabled?: boolean,
  open?: boolean,
  title: string
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef(
  ({ children, className, disabled, open: initiallyOpen = false, title }: WrappedItemType, forwardedRef: ReactForwardedRefType) => {
    const openable = children && !disabled;
    const { open, onToggle } = useOpenState({ disabled: !openable, open: initiallyOpen });
    const Component = openable ? 'a' : 'span';

    return (
      <div className={cx(styles.Item, openable && styles.Item__Openable, className)}>
        {openable ? (
          <FontAwesome
            className={styles.Arrow}
            id={open ? ICONS.CLOSE : ICONS.OPEN}
            border={false}
            innerSize={ICON_INNER_SIZE}
            size={ICON_SIZE}
            onClick={onToggle}
          />
        ) : (
          <FontAwesome
            className={styles.Arrow}
            id={ICONS.EMPTY}
            border={false}
            innerSize={ICON_INNER_SIZE_EMPTY}
            size={ICON_SIZE}
          />
        )}
        <Component className={styles.Title} onClick={openable ? onToggle : undefined}>
          {title}
        </Component>
        {open && <div className={styles.Subitems}>{children}</div>}
      </div>
    );
  }
);

export type ItemType = {|
  ...AsComponentType,
  ...ItemOwnPropsType
|};

type TreeOwnPropsType = {|
  children: ReactElementsChildren<ItemType>
|};

type WrappedTreeType = {|
  ...AsComponentExtensionType,
  ...TreeOwnPropsType
|};

const Tree = React.forwardRef(({ children, className }: WrappedTreeType, forwardedRef: ReactForwardedRefType) => (
  <div className={cx(styles.Tree, className)}>{children}</div>
));

export type TreeType = {|
  ...AsComponentType,
  ...TreeOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Tree)(Tree), {
    Item: AsComponent(ComponentNames.Tree.Item)(Item)
  }): React.ComponentType<TreeType> & {
    Item: React.ComponentType<ItemType>
  }
);
