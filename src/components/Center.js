/* #__flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs, setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType } from '../hocs/components/AsComponent';
import styles from './Center.scss';

const CENTER_TYPES = Object.freeze({
  BOTH: 'BOTH',
  BOTTOM_CENTER: 'BOTTOM_CENTER',
  HORIZONTAL: 'HORIZONTAL',
  LEFT_CENTER: 'LEFT_CENTER',
  RIGHT_CENTER: 'RIGHT_CENTER',
  TOP_CENTER: 'TOP_CENTER',
  VERTICAL: 'VERTICAL'
});

const POSITION_TYPES = Object.freeze({
  ABSOLUTE: 'absolute',
  FIXED: 'fixed'
});

type CenterOwnPropsType = {|
  bottom?: number,
  children: ReactChildren,
  center?: $Values<typeof CENTER_TYPES>,
  height?: number,
  left?: number,
  position?: $Values<typeof POSITION_TYPES>,
  right?: number,
  top?: number,
  width?: number
|};

type WrappedCenterType = {|
  ...AsComponentExtensionType,
  ...CenterOwnPropsType
|};

const Center = React.forwardRef(({
  bottom,
  children,
  className,
  center = CENTER_TYPES.HORIZONTAL,
  height,
  left,
  position = POSITION_TYPES.ABSOLUTE,
  right,
  top,
  width
}: WrappedCenterType, forwardedRef: ReactForwardedRefType) => {
  const absoluteElRef = React.useRef(null);

  React.useEffect(() => {
    const el = absoluteElRef.current;
    if (el) {
      el.style.position = position;
      if (height) {
        el.style.height = unitize(height);
      }
      if (width) {
        el.style.width = unitize(width);
      }
      if (center) {
        const style = {
          [CENTER_TYPES.BOTH]: () => ({
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)'
          }),
          [CENTER_TYPES.BOTTOM_CENTER]: () => ({
            left: '50%',
            top: '100%',
            transform: 'translate(-50%, -50%)'
          }),
          [CENTER_TYPES.HORIZONTAL]: () => ({
            left: '50%',
            transform: 'translateX(-50%)'
          }),
          [CENTER_TYPES.LEFT_CENTER]: () => ({
            right: '100%',
            top: '50%',
            transform: 'translate(50%, -50%)'
          }),
          [CENTER_TYPES.RIGHT_CENTER]: () => ({
            left: '100%',
            top: '50%',
            transform: 'translate(-50%, -50%)'
          }),
          [CENTER_TYPES.TOP_CENTER]: () => ({
            bottom: '100%',
            left: '50%',
            transform: 'translate(-50%, 50%)'
          }),
          [CENTER_TYPES.VERTICAL]: () => ({
            top: '50%',
            transform: 'translateY(-50%)'
          })
        }[center]();
        if (bottom) {
          style.transform += ` translateY(${unitize(-bottom)})`
        }
        if (left) {
          style.transform += ` translateX(${unitize(left)})`
        }
        if (right) {
          style.transform += ` translateX(${unitize(-right)})`
        }
        if (top) {
          style.transform += ` translateY(${unitize(top)})`
        }
        setElementStyle(el, style);
      } else {
        const style = {
          ...(bottom === undefined ? {} : { bottom: unitize(bottom) }),
          ...(left === undefined ? {} : { left: unitize(left) }),
          ...(right === undefined ? {} : { right: unitize(right) }),
          ...(top === undefined ? {} : { top: unitize(top) })
        };
        setElementStyle(el, style);
      }
    }
  }, [bottom, center, left, position, right, top]);

  return React.cloneElement(
    children,
    {
      className: cx(styles.Center, className),
      ref: mergeRefs(absoluteElRef, forwardedRef)
    }
  );
});

export type CenterType = {|
  ...AsComponentType,
  ...CenterOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Center)(Center), {
    CENTER_TYPES,
    POSITION_TYPES
  }): React.ComponentType<CenterType> & {
    CENTER_TYPES: Object,
    POSITION_TYPES: Object
  }
);
