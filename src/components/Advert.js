/* #__flow */
import * as React from 'react';
import { cx } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Advert.scss';

type AdvertOwnPropsType = {|
  border?: boolean,
  id?: string,
  label?: string
|};

type WrappedAdvertType = {|
  ...AsComponentExtensionType,
  ...AdvertOwnPropsType
|};

const Advert = React.forwardRef(
  ({ border, className, id, label = 'Reklama' }: WrappedAdvertType, forwardedRef: ReactForwardedRefType) => (
    <div ref={forwardedRef} className={cx(styles.Advert, border && styles.Advert__Border, className)}>
      <div className={styles.Label}>{label}</div>
      <div id={id} className={styles.Content} />
    </div>
  )
);

export type AdvertType = {|
  ...AsComponentType,
  ...AdvertOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Advert)(Advert), {}): React.ComponentType<AdvertType>
);
