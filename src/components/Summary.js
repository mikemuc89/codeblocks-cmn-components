/* @flow */
import * as React from 'react';
import { RESULTS, RESULT_TO_ICON } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import { type IconPropType } from '../types';
import FontAwesome from './FontAwesome';
import styles from './Summary.scss';

const DEFAULT_ICON_SIZE = 80;
const DEFAULT_ICON_INNER_SIZE = 56;

type SummaryOwnPropsType = {|
  icon?: IconPropType,
  result?: $Values<typeof RESULTS>,
  title?: string
|};

type WrappedSummaryType = {|
  ...AsComponentExtensionType,
  ...SummaryOwnPropsType
|};

const Summary = React.forwardRef(
  ({ children, className, color, icon, result, title, verticalCenter }: WrappedSummaryType, forwardedRef: ReactForwardedRefType) => (
    <div className={cx(styles.Summary, cx.result(styles, 'Summary', { result }), verticalCenter && styles.Summary__VerticalCenter, className)}>
      {icon
        ? React.cloneElement(icon, {
            className: styles.Icon,
            color: icon.props.color || color,
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: icon.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.ColorIcon]: {
                innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: icon.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.FontAwesome]: {
                innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: icon.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: icon.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: icon.props.innerSize || DEFAULT_ICON_INNER_SIZE,
                size: icon.props.size || DEFAULT_ICON_SIZE
              },
              [ComponentNames.Icon]: {
                size: icon.props.size || DEFAULT_ICON_SIZE
              }
            }[icon.type.componentId]
          })
        : result && (
            <FontAwesome
              className={styles.Icon}
              innerSize={DEFAULT_ICON_INNER_SIZE}
              size={DEFAULT_ICON_SIZE}
              id={RESULT_TO_ICON[result]}
            />
          )}
      {title && <div style={{ ...(color ? { color } : {}) }} className={styles.Title}>{title}</div>}
      <div className={styles.Content}>
        {React.Children.map(
          children,
          (child: React.Node) =>
            child &&
            (isBasicReactChild(child) ? (
              <span className={styles.Text}>{child}</span>
            ) : (
              React.cloneElement(child, {
                className: cx(
                  child.props.className,
                  {
                    [ComponentNames.Button]: styles.Button,
                    [ComponentNames.Markdown]: styles.Text,
                    [ComponentNames.Text]: styles.Text,
                    [ComponentNames.Text.Primary]: styles.Text,
                    [ComponentNames.Text.Secondary]: styles.Text
                  }[child.type.componentId]
                )
              })
            ))
        )}
      </div>
    </div>
  )
);

export type SummaryType = {|
  ...AsComponentType,
  ...SummaryOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Summary)(Summary), {
    RESULTS
  }): React.ComponentType<SummaryType> & {
    RESULTS: Object
  }
);
