/* @flow */
import * as React from 'react';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, isBasicReactChild } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import styles from './Heading.scss';

const headingHandler = ({ color, id }: { id?: string }) => ({ id, style: { ...(color ? { color } : {}) } });

type HeadingOwnPropsType = {|
  align?: $Values<typeof ALIGN_TYPES>,
  children: string | React.ComponentType<any> | Array<React.ComponentType<any>>,
  color?: string,
  counter?: boolean,
  id?: string  
|};

type WrappedHeadingType = {|
  ...AsComponentExtensionType,
  ...HeadingOwnPropsType
|};

const H1 = React.forwardRef(
  ({ align, children, className, counter, ...props }: WrappedHeadingType, forwardedRef: ReactForwardedRefType) => (
    <h1
      ref={forwardedRef}
      {...headingHandler(props)}
      className={cx(styles.H1, cx.alignSingle(styles, 'H1', { align }), counter && styles.H1__Counter, className)}
    >
      {React.Children.map(children, (child: React.Node) =>
        isBasicReactChild(child) ? (
          <span className={styles.Text}>{child}</span>
        ) : (
          React.cloneElement(child, {
            className: cx(
              child.props.className,
              {
                [ComponentNames.CharIcon]: styles.Icon,
                [ComponentNames.ColorIcon]: styles.Icon,
                [ComponentNames.FontAwesome]: styles.Icon,
                [ComponentNames.FontAwesome.Brand]: styles.Icon,
                [ComponentNames.FontAwesome.Regular]: styles.Icon,
                [ComponentNames.Icon]: styles.Icon
              }[child.type.componentId] || styles.Content
            ),
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: 24,
                size: 24
              },
              [ComponentNames.ColorIcon]: {
                innerSize: 24,
                size: 24
              },
              [ComponentNames.FontAwesome]: {
                innerSize: 24,
                size: 24
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: 24,
                size: 24
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: 24,
                size: 24
              },
              [ComponentNames.Icon]: {
                size: 24
              }
            }[child.type.componentId]
          })
        )
      )}
    </h1>
  )
);

const H2 = React.forwardRef(
  ({ align, children, className, counter, ...props }: WrappedHeadingType, forwardedRef: ReactForwardedRefType) => (
    <h2
      ref={forwardedRef}
      {...headingHandler(props)}
      className={cx(styles.H2, cx.alignSingle(styles, 'H2', { align }), counter && styles.H2__Counter, className)}
    >
      {React.Children.map(children, (child: React.Node) =>
        isBasicReactChild(child) ? (
          <span className={styles.Text}>{child}</span>
        ) : (
          React.cloneElement(child, {
            className: cx(
              child.props.className,
              {
                [ComponentNames.CharIcon]: styles.Icon,
                [ComponentNames.ColorIcon]: styles.Icon,
                [ComponentNames.FontAwesome]: styles.Icon,
                [ComponentNames.FontAwesome.Brand]: styles.Icon,
                [ComponentNames.FontAwesome.Regular]: styles.Icon,
                [ComponentNames.Icon]: styles.Icon
              }[child.type.componentId] || styles.Content
            ),
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: 20,
                size: 20
              },
              [ComponentNames.ColorIcon]: {
                innerSize: 20,
                size: 20
              },
              [ComponentNames.FontAwesome]: {
                innerSize: 20,
                size: 20
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: 20,
                size: 20
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: 20,
                size: 20
              },
              [ComponentNames.Icon]: {
                innerSize: 20,
                size: 20
              }
            }[child.type.componentId]
          })
        )
      )}
    </h2>
  )
);

const H3 = React.forwardRef(
  ({ align, children, className, counter, ...props }: WrappedHeadingType, forwardedRef: ReactForwardedRefType) => (
    <h3
      ref={forwardedRef}
      {...headingHandler(props)}
      className={cx(styles.H3, cx.alignSingle(styles, 'H3', { align }), counter && styles.H3__Counter, className)}
    >
      {React.Children.map(children, (child: React.Node) =>
        isBasicReactChild(child) ? (
          <span className={styles.Text}>{child}</span>
        ) : (
          React.cloneElement(child, {
            className: cx(
              child.props.className,
              {
                [ComponentNames.CharIcon]: styles.Icon,
                [ComponentNames.ColorIcon]: styles.Icon,
                [ComponentNames.FontAwesome]: styles.Icon,
                [ComponentNames.FontAwesome.Brand]: styles.Icon,
                [ComponentNames.FontAwesome.Regular]: styles.Icon,
                [ComponentNames.Icon]: styles.Icon
              }[child.type.componentId] || styles.Content
            ),
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: 18,
                size: 18
              },
              [ComponentNames.ColorIcon]: {
                innerSize: 18,
                size: 18
              },
              [ComponentNames.FontAwesome]: {
                innerSize: 18,
                size: 18
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: 18,
                size: 18
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: 18,
                size: 18
              },
              [ComponentNames.Icon]: {
                innerSize: 18,
                size: 18
              }
            }[child.type.componentId]
          })
        )
      )}
    </h3>
  )
);

const H4 = React.forwardRef(
  ({ align, children, className, counter, ...props }: WrappedHeadingType, forwardedRef: ReactForwardedRefType) => (
    <h4
      ref={forwardedRef}
      {...headingHandler(props)}
      className={cx(styles.H4, cx.alignSingle(styles, 'H4', { align }), counter && styles.H4__Counter, className)}
    >
      {React.Children.map(children, (child: React.Node) =>
        isBasicReactChild(child) ? (
          <span className={styles.Text}>{child}</span>
        ) : (
          React.cloneElement(child, {
            className: cx(
              child.props.className,
              {
                [ComponentNames.CharIcon]: styles.Icon,
                [ComponentNames.ColorIcon]: styles.Icon,
                [ComponentNames.FontAwesome]: styles.Icon,
                [ComponentNames.FontAwesome.Brand]: styles.Icon,
                [ComponentNames.FontAwesome.Regular]: styles.Icon,
                [ComponentNames.Icon]: styles.Icon
              }[child.type.componentId] || styles.Content
            ),
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: 14,
                size: 14
              },
              [ComponentNames.ColorIcon]: {
                innerSize: 14,
                size: 14
              },
              [ComponentNames.FontAwesome]: {
                innerSize: 14,
                size: 14
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: 14,
                size: 14
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: 14,
                size: 14
              },
              [ComponentNames.Icon]: {
                innerSize: 14,
                size: 14
              }
            }[child.type.componentId]
          })
        )
      )}
    </h4>
  )
);

const H5 = React.forwardRef(
  ({ align, children, className, counter, ...props }: WrappedHeadingType, forwardedRef: ReactForwardedRefType) => (
    <h5
      ref={forwardedRef}
      {...headingHandler(props)}
      className={cx(styles.H5, cx.alignSingle(styles, 'H5', { align }), counter && styles.H5__Counter, className)}
    >
      {React.Children.map(children, (child: React.Node) =>
        isBasicReactChild(child) ? (
          <span className={styles.Text}>{child}</span>
        ) : (
          React.cloneElement(child, {
            className: cx(
              child.props.className,
              {
                [ComponentNames.CharIcon]: styles.Icon,
                [ComponentNames.ColorIcon]: styles.Icon,
                [ComponentNames.FontAwesome]: styles.Icon,
                [ComponentNames.FontAwesome.Brand]: styles.Icon,
                [ComponentNames.FontAwesome.Regular]: styles.Icon,
                [ComponentNames.Icon]: styles.Icon
              }[child.type.componentId] || styles.Content
            ),
            ...{
              [ComponentNames.CharIcon]: {
                innerSize: 11,
                size: 11
              },
              [ComponentNames.ColorIcon]: {
                innerSize: 11,
                size: 11
              },
              [ComponentNames.FontAwesome]: {
                innerSize: 11,
                size: 11
              },
              [ComponentNames.FontAwesome.Brand]: {
                innerSize: 11,
                size: 11
              },
              [ComponentNames.FontAwesome.Regular]: {
                innerSize: 11,
                size: 11
              },
              [ComponentNames.Icon]: {
                innerSize: 11,
                size: 11
              }
            }[child.type.componentId]
          })
        )
      )}
    </h5>
  )
);

export type HeadingType = {|
  ...AsComponentType,
  ...HeadingOwnPropsType
|};

export default (
  Object.assign(
    {
      H1: Object.assign(AsComponent(ComponentNames.Heading.H1)(H1), {
        ALIGN_TYPES
      }),
      H2: Object.assign(AsComponent(ComponentNames.Heading.H2)(H2), {
        ALIGN_TYPES
      }),
      H3: Object.assign(AsComponent(ComponentNames.Heading.H3)(H3), {
        ALIGN_TYPES
      }),
      H4: Object.assign(AsComponent(ComponentNames.Heading.H4)(H4), {
        ALIGN_TYPES
      }),
      H5: Object.assign(AsComponent(ComponentNames.Heading.H5)(H5), {
        ALIGN_TYPES
      })
    },
    {
      ALIGN_TYPES
    }
  ): {
    H1: React.ComponentType<HeadingType> & {
      ALIGN_TYPES: Object
    },
    H2: React.ComponentType<HeadingType> & {
      ALIGN_TYPES: Object
    },
    H3: React.ComponentType<HeadingType> & {
      ALIGN_TYPES: Object
    },
    H4: React.ComponentType<HeadingType> & {
      ALIGN_TYPES: Object
    },
    H5: React.ComponentType<HeadingType> & {
      ALIGN_TYPES: Object
    },
  } & {
    ALIGN_TYPES: Object
  }
);
