/* @flow */
import * as React from 'react';
import { type ReactChildren, type ReactElementsChildren } from '@omnibly/codeblocks-cmn-types';
import { cx, mergeRefs, unitize } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import MainSizeContext from '../contexts/MainSizeContext';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import WithBackground, { type WithBackgroundExtensionType, type WithBackgroundType } from '../hocs/components/WithBackground';
import styles from './Flipper.scss';

type PageOwnPropsType = {|
  children?: ReactChildren  
|};

type WrappedPageType = {|
  ...AsComponentExtensionType,
  ...WithBackgroundExtensionType,
  ...PageOwnPropsType
|};

const Page = React.forwardRef(({ backgroundElementRef, children, className }: WrappedPageType, forwardedRef: ReactForwardedRefType) => (
  <li
    ref={mergeRefs(forwardedRef, backgroundElementRef)}
    className={cx(styles.Page, className)}
  >
    {children}
  </li>
));

export type PageType = {|
  ...AsComponentType,
  ...WithBackgroundType,
  ...PageOwnPropsType
|};

type FlipperOwnPropsType = {|
  children: ReactElementsChildren<PageType>  
|};

type WrappedFlipperType = {|
  ...AsComponentExtensionType,
  ...FlipperOwnPropsType
|};

const Flipper = React.forwardRef(({ children, className }: WrappedFlipperType, forwardedRef: ReactForwardedRefType) => {
  const { headerHeight, menuHeight, footerHeight } = React.useContext(MainSizeContext);

  return (
    <div
      className={cx(styles.Flipper, className)}
      style={{ height: `calc(100vh - ${unitize(headerHeight + menuHeight + footerHeight)})` }}
    >
      <ul className={styles.Items}>{children}</ul>
    </div>
  );
});

export type FlipperType = {|
  ...AsComponentType,
  ...FlipperOwnPropsType
|};

export default (
  Object.assign(AsComponent(ComponentNames.Flipper)(Flipper), {
    Page: AsComponent(ComponentNames.Flipper.Page)(WithBackground()(Page))
  }): React.ComponentType<FlipperType> & {
    Page: React.ComponentType<PageType>
  }
);
