/* @flow */
import * as React from 'react';
import { formatBooleanId } from '@omnibly/codeblocks-cmn-formatters';
import { type ReactChildren, type ReactElementsChildren, type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import { BOOL_IDS, INPUT_TYPES, KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { ALIGN_TYPES } from '@omnibly/codeblocks-cmn-types/src/enums';
import { cx, guid, isBasicReactChild, mergeRefs } from '@omnibly/codeblocks-cmn-utils';
import ComponentNames from '../componentNames';
import AsComponent, { type AsComponentExtensionType, type AsComponentType, type ComponentType, type ReactForwardedRefType } from '../hocs/components/AsComponent';
import AsControlWithItems, { ControlWithItemsContext, GroupItemContext, type AsControlWithItemsExtensionType, type AsControlWithItemsType } from '../hocs/components/AsControlWithItems';
import styles from './Picker.scss';

const ICONS_ICON_SIZE = 38;
const ICONS_ICON_INNER_SIZE = 18;

type ItemOwnPropsType = {|
  id: string  
|};

type WrappedItemType = {|
  ...AsComponentExtensionType,
  ...OptionType,
  ...ItemOwnPropsType
|};

const Item = React.forwardRef((
  { children, className, disabled: itemDisabled, id, ...props }: WrappedItemType,
  forwardedRef: ReactForwardedRefType
) => {
  const itemRootRef = React.useRef(null);
  const { controlSelector, itemControlElement, multiple, onChangeByItemId, value, ...contextProps } = React.useContext(ControlWithItemsContext);
  const selected = React.useMemo(() => multiple ? value.includes(id) : value === id, [id, multiple, value]);

  const handleOnChange = React.useCallback(() => {
    onChangeByItemId(id);
  }, [id, onChangeByItemId]);

  const focusNextItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    e.preventDefault();

    const el = itemRootRef.current;
    if (el) {
      const nextEl = el.nextElementSibling || el.parentElement.firstChild;
      if (nextEl) {
        nextEl.querySelector(controlSelector).focus();
      }
    }
  }, [controlSelector]);

  const focusPrevItem = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    e.preventDefault();

    const el = itemRootRef.current;
    if (el) {
      const nextEl = el.previousElementSibling || el.parentElement.lastChild;
      if (nextEl) {
        nextEl.querySelector(controlSelector).focus();
      }
    }
  }, [controlSelector]);

  const handleOnKeyDown = React.useCallback((e: SyntheticKeyboardEvent<HTMLInputElement>) => {
    const { keyCode } = e;

    if ([KEY_CODES.UP_ARROW, KEY_CODES.LEFT_ARROW].includes(keyCode)) {
      focusPrevItem(e);
    }
    if ([KEY_CODES.DOWN_ARROW, KEY_CODES.RIGHT_ARROW].includes(keyCode)) {
      focusNextItem(e);
    }

    if (itemControlElement.props.onKeyDown) {
      itemControlElement.props.onKeyDown(e);
    }
  }, [itemControlElement.props.onKeyDown, focusNextItem, focusPrevItem]);

  const disabled = React.useMemo(() => itemDisabled || itemControlElement.props.disabled, [itemDisabled, itemControlElement.props.disabled]);

  const controlElement = React.cloneElement(itemControlElement, {
    disabled,
    id,
    onChange: disabled ? undefined : handleOnChange,
    onKeyDown: disabled ? undefined : handleOnKeyDown
  });

  const contextValue = React.useMemo(() => ({
    handleGroupOnChange: handleOnChange
  }), [handleOnChange]);

  return (
    <div ref={mergeRefs(forwardedRef, itemRootRef)} className={cx(styles.Item, disabled && styles.Item__Disabled, selected && styles.Item__Selected, className)}>
      {controlElement}
      <div className={styles.Label} onClick={disabled ? undefined : handleOnChange}>
        {React.Children.map(
          children,
          (child: React.Node) =>
            child &&
            (isBasicReactChild(child) ? (
              <span className={styles.Content}>{child}</span>
            ) : (
              React.cloneElement(child, {
                className: cx(
                  child.props.className,
                  {
                    [ComponentNames.CharIcon]: styles.Icon,
                    [ComponentNames.ColorIcon]: styles.Icon,
                    [ComponentNames.FontAwesome]: styles.Icon,
                    [ComponentNames.FontAwesome.Brand]: styles.Icon,
                    [ComponentNames.FontAwesome.Regular]: styles.Icon,
                    [ComponentNames.Icon]: styles.Icon
                  }[child.type.componentId]
                ),
                ...{
                  [ComponentNames.CharIcon]: {
                    innerSize: ICONS_ICON_INNER_SIZE,
                    size: ICONS_ICON_SIZE
                  },
                  [ComponentNames.ColorIcon]: {
                    innerSize: ICONS_ICON_INNER_SIZE,
                    size: ICONS_ICON_SIZE
                  },
                  [ComponentNames.FontAwesome]: {
                    innerSize: ICONS_ICON_INNER_SIZE,
                    size: ICONS_ICON_SIZE
                  },
                  [ComponentNames.FontAwesome.Brand]: {
                    innerSize: ICONS_ICON_INNER_SIZE,
                    size: ICONS_ICON_SIZE
                  },
                  [ComponentNames.FontAwesome.Regular]: {
                    innerSize: ICONS_ICON_INNER_SIZE,
                    size: ICONS_ICON_SIZE
                  },
                  [ComponentNames.Icon]: {
                    size: ICONS_ICON_SIZE
                  }
                }[child.type.componentId]
              })
            ))
        )}
      </div>
    </div>
  );
});

export type ItemType = {|
  ...AsComponentType,
  ...OptionType,
  ...ItemOwnPropsType
|};

type PickerOwnPropsType = {||};

type WrappedPickerType = {|
  ...AsComponentExtensionType,
  ...AsControlWithItemsExtensionType,
  ...PickerOwnPropsType
|};

const Picker = React.forwardRef(
  (
    {
      children,
      className,
      controlElement,
      disabled,
      messageElement,
      requiredElement
    }: SelectType,
    forwardedRef: WrappedPickerType
  ) => {
    const count = React.Children.count(children);

    return (
      <div
        ref={forwardedRef}
        className={cx(
          styles.Picker,
          styles[`Picker__Size_${count}`],
          className
        )}
      >
        {controlElement}
        <div className={styles.Inner}>
          {children}
        </div>
        {requiredElement}
        {messageElement}
      </div>
    );
  }
);

const WrappedPicker = AsComponent(ComponentNames.Picker)(AsControlWithItems({
  changeTypeMulti: AsControlWithItems.MULTI_CHANGE_TYPES.TOGGLE,
  defaultMaximum: 1,
  defaultValue: null,
  sanitizeValueForControl: JSON.stringify,
  style: { key: 'Picker', styles }
})(Picker));

type WrappedBoolType = WrappedPickerType;

const Bool = React.forwardRef((props: WrappedBoolType, forwardedRef: ReactForwardedRefType) => (
  <WrappedPicker ref={forwardedRef} {...props} multiple={false}>
    {[BOOL_IDS.YES, BOOL_IDS.NO].map((id: $Values<typeof BOOL_IDS>) => (
      <Item key={id} id={id}>
        {formatBooleanId(id)}
      </Item>
    ))}
  </WrappedPicker>
));

export type PickerType = {|
  ...AsComponentType,
  ...AsControlWithItemsType,
  ...PickerOwnPropsType
|};

export type BoolType = PickerType;

export default (
  Object.assign(WrappedPicker, {
    Bool: Object.assign(AsComponent(ComponentNames.Picker.Bool)(Bool), {
      OPTIONS: BOOL_IDS
    }),
    Item: AsComponent(ComponentNames.Picker.Item)(Item)
  }): React.ComponentType<PickerType> & {
    Bool: React.ComponentType<BoolType> & {
      OPTIONS: Object
    },
    Item: React.ComponentType<ItemType>
  }
);
