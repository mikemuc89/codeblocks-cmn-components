/* @flow */
import React from 'react';
import { type BadgeType } from './components/Badge';
import { type ButtonType } from './components/Badge';
import { type CharIconType } from './components/CharIcon';
import { type ColorIconType } from './components/ColorIcon';
import { type FontAwesomeType } from './components/FontAwesome';
import { type FooterType } from './components/Footer';
import { type HeaderType } from './components/Header';
import { type IconType } from './components/Icon';
import { type ImageType } from './components/Image';
import { type LabelType } from './components/Label';
import { type LinkType } from './components/Link';
import { type MenuType } from './components/Menu';
import { type StepsType } from './components/Steps';
import { type TabsType } from './components/Tabs';

export type BadgePropType = React.ElementRef<BadgeType>;
export type FooterPropType = React.ElementRef<FooterType>;
export type HeaderPropType = React.ElementRef<HeaderType>;
export type IconPropType = React.ElementRef<CharIconType | ColorIconType | FontAwesomeType | IconType>;
export type ActionPropType = IconPropType | React.ElementRef<ButtonType | LinkType>;
export type ImagePropType = React.ElementRef<ImageType>;
export type LabelPropType = React.ElementRef<LabelType>;
export type MenuPropType = React.ElementRef<MenuType>;
export type TabsPropType = React.ElementRef<StepsType | TabsType>;
