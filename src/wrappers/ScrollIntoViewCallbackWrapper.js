/* @flow */
import * as React from 'react';
import { type ReactChildren } from '@omnibly/codeblocks-cmn-types';
import useScrollIntoViewCallback from '../hooks/useScrollIntoViewCallback';

export type ScrollIntoViewCallbackWrapperType = {|
  children: ReactChildren,
  onScrollIntoView: (e: Event) => void
|};

const ScrollIntoViewCallbackWrapper = ({ children, onScrollIntoView }: ScrollCallbackWrapperType) => {
  const ref = useScrollIntoViewCallback({ onScrollIntoView });

  return React.cloneElement(React.Children.only(children), { ref });
};

export default ScrollIntoViewCallbackWrapper;
