/* @flow */
import * as React from 'react';
import { KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';

export default (submitFunction: (any) => void) =>
  React.useCallback(
    (e: KeyboardEvent) => {
      const key = e.which || e.keyCode;
      if ([KEY_CODES.ENTER, KEY_CODES.SPACE].includes(key) && submitFunction) {
        submitFunction(e);
      }
    },
    [submitFunction]
  );
