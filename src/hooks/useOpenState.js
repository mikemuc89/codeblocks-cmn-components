/* @flow */
import * as React from 'react';
import { AnimationStateContext } from '../components/Animation';

export default ({ disabled, open: initiallyOpen, onClose, onOpen }: { disabled?: boolean, open?: boolean, onClose: () => void, onOpen: () => void } = {}) => {
  const [open, setOpen] = React.useState(initiallyOpen);
  const { setAnimatedElementVisibility } = React.useContext(AnimationStateContext);

  React.useLayoutEffect(() => {
    if (open) {
      if (onOpen) {
        onOpen();
      }
    } else {
      if (onClose) {
        onClose();
      }
    }
    if (setAnimatedElementVisibility) {
      setAnimatedElementVisibility(open);
    }
  }, [onClose, onOpen, open, setAnimatedElementVisibility]);

  const hide = React.useCallback(
    (e?: Event) => {
      if (e) {
        e.stopPropagation();
        e.preventDefault();
      }
      setOpen(false);
    },
    [setOpen]
  );

  const show = React.useCallback(
    (e?: Event) => {
      if (e) {
        e.stopPropagation();
        e.preventDefault();
      }
      if (!disabled) {
        setOpen(true);
      }
    },
    [disabled, setOpen]
  );

  const toggle = React.useCallback(
    (e?: Event) => {
      if (e) {
        e.stopPropagation();
        e.preventDefault();
      }
      if (!disabled) {
        setOpen((open?: boolean) => !open);
      }
    },
    [disabled, setOpen]
  );

  return { hide, open, show, toggle };
};
