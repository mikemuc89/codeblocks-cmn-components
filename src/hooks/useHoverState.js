/* @flow */
import * as React from 'react';

export default ({
  disabled,
  hover
}: {
  disabled?: boolean,
  hover?: boolean
} = {}) => {
  const hoverElementRef = React.useRef(null);
  const [hoverState, setHoverState] = React.useState(!disabled && hover ? 1 : 0);

  const onEnter = React.useCallback((e: MouseEvent) => {
    setHoverState((state: number) => state + 1);
  }, []);

  const onLeave = React.useCallback((e: MouseEvent) => {
    setHoverState((state: number) => state - 1);
  }, []);

  React.useEffect(() => {
    const disableListener = () => {
      const el = hoverElementRef.current;

      if (el) {
        hoverElementRef.current.removeEventListener('mouseenter', onEnter);
        hoverElementRef.current.removeEventListener('mouseleave', onLeave);
      }
    };

    const enableListener = () => {
      const el = hoverElementRef.current;

      if (el) {
        hoverElementRef.current.addEventListener('mouseenter', onEnter);
        hoverElementRef.current.addEventListener('mouseleave', onLeave);
      }
    };

    if (disabled) {
      disableListener();
    } else {
      enableListener();
    }

    return disableListener;
  }, [disabled, onEnter, onLeave]);

  return { hoverElementRef, hovered: hoverState > 0 };
};
