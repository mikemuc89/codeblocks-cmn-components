/* @flow */
import * as React from 'react';
import { dequal as deepEqual } from 'dequal';

type DependencyListType = Array<any>;

const isPrimitive = (val: any) => val === null || /^[sbn]/.test(typeof val);

const checkDeps = (deps: DependencyListType) => {
  if (!deps || !deps.length) {
    throw new Error('useDeepCompareEffect should not be used with no dependencies. Use React.useEffect instead.');
  }
  if (deps.every(isPrimitive)) {
    throw new Error(
      'useDeepCompareEffect should not be used with dependencies that are all primitive values. Use React.useEffect instead.'
    );
  }
};

export const useDeepCompareMemoize = (deps: DependencyListType) => {
  const ref = React.useRef<DependencyListType>();
  const signalRef = React.useRef<number>(0);

  if (!deepEqual(deps, ref.current)) {
    ref.current = deps;
    signalRef.current += 1;
  }

  return [signalRef.current];
};

export const useDeepCompareCallback = (callback: (args: Array<any>) => any, dependencies: DependencyListType) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return React.useCallback(callback, useDeepCompareMemoize(dependencies));
};

export const useDeepCompareEffect = (callback: () => void, dependencies: DependencyListType) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return React.useEffect(callback, useDeepCompareMemoize(dependencies));
};

export const useDeepCompareMemo = (callback: () => any, dependencies: DependencyListType) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return React.useMemo(callback, useDeepCompareMemoize(dependencies));
};

export const useDeepMemoizedValue = (notMemoizedValue: any) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return useDeepCompareMemo(() => notMemoizedValue, [notMemoizedValue]);
}
