/* @flow */
import * as React from 'react';
import { debounced } from '@omnibly/codeblocks-cmn-utils';
import useCache from './useCache';
import { type ItemType } from './useItems';

const DEBOUNCE_TIME = 500;

export default ({
  getItemText,
  items,
  value
}: {
  getItemText: (item: ItemType) => string,
  items: Array<ItemType>,
  value: string
}) => {
  const {
    cache,
    onCacheChange: defaultOnCacheChange,
    onClearCache: defaultOnClearCache
  } = useCache({
    value: ((item: ?ItemType) => (item ? getItemText(item) : ''))(
      items.find((item: ItemType) => item.id === value) || null
    )
  });
  const [cacheItems, setCacheItems] = React.useState(items);

  const onFilter = debounced((value: string) => {
    if (value) {
      const filteredItems = items.filter((item: ItemType) => getItemText(item).includes(value));
      setCacheItems(filteredItems);
    } else {
      setCacheItems(items);
    }
  }, DEBOUNCE_TIME);

  const onCacheChange = (newValue: string) => {
    defaultOnCacheChange(newValue);
    onFilter(newValue);
  };

  const onClearCache = () => {
    defaultOnClearCache();
    setCacheItems(items);
  };

  const onSelectItem = (id: string) => {
    const selectedItem = items.find((item: ItemType) => item.id === id);

    onCacheChange(getItemText(selectedItem));
  };

  return { cache, cacheItems, onCacheChange, onClearCache, onSelectItem };
};
