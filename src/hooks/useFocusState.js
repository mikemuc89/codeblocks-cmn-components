/* @flow */
import * as React from 'react';

export default ({
  disabled,
  focus = false
}: {
  disabled?: boolean,
  focus?: boolean
} = {}) => {
  const focusElementRef = React.useRef(null);
  const [focusState, setFocusState] = React.useState(!disabled && focus);

  const onFocus = React.useCallback((e: Event) => {
    setFocusState(true);
  }, []);

  const onBlur = React.useCallback((e: Event) => {
    setFocusState(false);
  }, []);

  React.useEffect(() => {
    const disableListener = () => {
      const el = focusElementRef.current;
      if (el) {
        focusElementRef.current.removeEventListener('focusin', onFocus);
        focusElementRef.current.removeEventListener('focusout', onBlur);
      }
    };

    const enableListener = () => {
      const el = focusElementRef.current;
      if (el) {
        focusElementRef.current.addEventListener('focusin', onFocus);
        focusElementRef.current.addEventListener('focusout', onBlur);
      }
    };

    if (disabled) {
      disableListener();
    } else {
      enableListener();
    }

    return disableListener;
  }, [disabled, onFocus, onBlur]);

  return { focusElementRef, focused: focusState };
};
