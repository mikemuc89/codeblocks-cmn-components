/* @flow */
import * as React from 'react';

export default (event: string, handler: (e: Event) => void, el: any = window) => {
  const savedHandler = React.useRef(null);

  React.useEffect(() => {
    const isSupported = el && el.addEventListener;
    if (!isSupported) {
      return;
    }

    const oldHandler = savedHandler.current;
    if (oldHandler) {
      el.removeEventListener(event, oldHandler);
    }
    savedHandler.current = handler;
    el.addEventListener(event, savedHandler.current);

    return () => {
      if (el) {
        const oldHandler = savedHandler.current;
        if (oldHandler) {
          el.removeEventListener(event, oldHandler);
        }
      }
    };
  }, [el, event, handler]);

  const pauseHandler = React.useCallback(
    ({ delay }: { delay?: number } = {}) => {
      if (el) {
        const handler = savedHandler.current;
        if (handler) {
          if (delay === undefined) {
            el.removeEventListener(event, handler);
          } else {
            setTimeout(() => {
              el.removeEventListener(event, handler);
            }, delay);
          }
        }
      }
    },
    [el, event]
  );

  const resumeHandler = React.useCallback(
    ({ delay }: { delay?: number } = {}) => {
      if (el) {
        const handler = savedHandler.current;
        if (handler) {
          if (delay === undefined) {
            el.addEventListener(event, handler);
          } else {
            setTimeout(() => {
              el.addEventListener(event, handler);
            }, delay);
          }
        }
      }
    },
    [el, event]
  );

  return { pauseHandler, resumeHandler };
};
