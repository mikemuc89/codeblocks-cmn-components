/* @flow */
import * as React from 'react';
import { EVENT_NAMES } from '@omnibly/codeblocks-cmn-types/src/constants';
import useEventListener from './useEventListener';

export default () => {
  const [windowDimensions, setWindowDimensions] = React.useState({
    height: window.innerHeight,
    width: window.innerWidth
  });

  const onResize = React.useCallback((e: Event) => {
    const { innerHeight, innerWidth } = e.target;
    setWindowDimensions({ height: innerHeight, width: innerWidth });
  }, []);

  useEventListener(EVENT_NAMES.RESIZE, onResize);

  return windowDimensions;
};
