/* @flow */
import * as React from 'react';
import { BREAKPOINT_NAMES, EVENT_NAMES } from '@omnibly/codeblocks-cmn-types/src/constants';
import useEventListener from './useEventListener';
import CONFIG from '../components/style-config';

const { breakpoints: BREAKPOINTS } = CONFIG;

export { BREAKPOINT_NAMES, BREAKPOINTS };

const findBreakpointByWidth = (width: number) =>
  Object.keys(BREAKPOINTS).find((key: string) => BREAKPOINTS[key].from <= width && BREAKPOINTS[key].to >= width);

export default () => {
  const [breakpoint, setBreakpoint] = React.useState(findBreakpointByWidth(window.innerWidth));

  const onResize = React.useCallback(
    (e: Event) => {
      const newBreakpoint = findBreakpointByWidth(e.target.innerWidth);
      if (newBreakpoint !== breakpoint) {
        setBreakpoint(newBreakpoint);
      }
    },
    [breakpoint]
  );

  useEventListener(EVENT_NAMES.RESIZE, onResize);

  return { breakpoint };
};
