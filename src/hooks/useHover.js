/* @flow */
import * as React from 'react';

const EVENT_KINDS = Object.freeze({
  HOVER: Symbol('useHover.EVENT_KINDS.HOVER'),
  HOVER_END: Symbol('useHover.EVENT_KINDS.HOVER_END'),
  HOVER_START: Symbol('useHover.EVENT_KINDS.HOVER_START')
});

type AbsolutePositionType = {|
  px: number,
  py: number,
  x: number,
  y: number
|};

type DeltaPositionType = {|
  dpx: number,
  dpy: number,
  dx: number,
  dy: number
|};

export type HoverCallbackDataType = {|
  absolute: AbsolutePositionType,
  fromLast: DeltaPositionType,
  fromStart: DeltaPositionType,
  kind: $Values<typeof EVENT_KINDS>
|};

export default ({
  disabled,
  onHover,
  onHoverEnd,
  onHoverStart
}: {
  disabled?: boolean,
  onHover?: (data: HoverCallbackDataType) => void,
  onHoverEnd?: (data: HoverCallbackDataType) => void,
  onHoverStart?: (data: HoverCallbackDataType) => void
}) => {
  const surface = React.useRef(null);
  const rect = React.useRef({ height: null, left: null, top: null, width: null });
  const startValue = React.useRef({ x: null, y: null });
  const lastValue = React.useRef({ x: null, y: null });

  const start = React.useCallback(
    (e: MouseEvent) => {
      surface.current.addEventListener('mousemove', hovering);
      surface.current.addEventListener('mouseout', finish);
      surface.current.removeEventListener('mouseover', start);

      const { height, left, top, width } = surface.current.getBoundingClientRect();
      rect.current = { height, left, top, width };

      const x = e.clientX;
      const y = e.clientY;

      if (onHoverStart) {
        const px = (x - left) / width;
        const py = (y - top) / height;

        onHoverStart({
          absolute: { px, py, x, y },
          fromLast: { dpx: 0, dpy: 0, dx: 0, dy: 0 },
          fromStart: { dpx: 0, dpy: 0, dx: 0, dy: 0 },
          kind: EVENT_KINDS.HOVER_START
        });
      }

      lastValue.current = { x, y };
      startValue.current = { x, y };
    },
    [finish, hovering, onHoverStart]
  );

  const hovering = React.useCallback(
    (e: MouseEvent) => {
      e.preventDefault();

      const { height, left, top, width } = rect.current;

      const x = e.clientX;
      const y = e.clientY;

      if (onHover) {
        const px = (x - left) / width;
        const py = (y - top) / height;

        const dxLast = x - lastValue.current.x;
        const dyLast = y - lastValue.current.y;

        const dxStart = x - startValue.current.x;
        const dyStart = y - startValue.current.y;

        onHover({
          absolute: { px, py, x, y },
          fromLast: { dpx: dxLast / width, dpy: dyLast / width, dx: dxLast, dy: dyLast },
          fromStart: { dpx: dxStart / width, dpy: dyStart / width, dx: dxStart, dy: dyStart },
          kind: EVENT_KINDS.HOVER
        });
      }

      lastValue.current = { x, y };
    },
    [onHover]
  );

  const finish = React.useCallback(
    (e: MouseEvent) => {
      surface.current.removeEventListener('mousemove', hovering);
      surface.current.removeEventListener('mouseout', finish);
      surface.current.addEventListener('mouseover', start);

      const { height, left, top, width } = rect.current;

      const x = e.clientX;
      const y = e.clientY;

      if (onHoverEnd) {
        const px = (x - left) / width;
        const py = (y - top) / height;

        const dxLast = x - lastValue.current.x;
        const dyLast = y - lastValue.current.y;

        const dxStart = x - startValue.current.x;
        const dyStart = y - startValue.current.y;

        onHoverEnd({
          absolute: { px, py, x, y },
          fromLast: { dpx: dxLast / width, dpy: dyLast / width, dx: dxLast, dy: dyLast },
          fromStart: { dpx: dxStart / width, dpy: dyStart / width, dx: dxStart, dy: dyStart },
          kind: EVENT_KINDS.HOVER_END
        });
      }

      lastValue.current = { x: null, y: null };
      startValue.current = { x: null, y: null };
    },
    [hovering, onHoverEnd, start]
  );

  React.useEffect(() => {
    const el = surface.current;
    if (el) {
      if (disabled) {
        el.removeEventListener('mouseover', start);
        el.removeEventListener('mousemove', hovering);
      } else {
        el.addEventListener('mouseover', start);
      }
    }
    return () => {
      if (el) {
        el.removeEventListener('mouseover', start);
        el.removeEventListener('mousemove', hovering);
      }
    };
  }, [disabled, hovering, start]);

  return surface;
};
