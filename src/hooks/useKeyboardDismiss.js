/* @flow */
import * as React from 'react';
import { KEY_CODES } from '@omnibly/codeblocks-cmn-types/src/constants';

export default (dismissFunction: (any) => void) =>
  React.useCallback(
    (e: KeyboardEvent) => {
      const key = e.which || e.keyCode;
      if ([KEY_CODES.ESC].includes(key) && dismissFunction) {
        dismissFunction(e);
      }
    },
    [dismissFunction]
  );
