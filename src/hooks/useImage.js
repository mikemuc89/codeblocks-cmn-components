/* @flow */
import * as React from 'react';
import { setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';
import useEventListener from '../hooks/useEventListener';

const getDimension = (prop?: string | number, parentDimension: number) => {
  if (prop === undefined) {
    return parentDimension;
  }
  const match = prop.toString().match(/^(\d+(.\d+)?)(%?)$/);
  if (!match) {
    return parentDimension;
  }
  const [_, value, __, unit] = match;
  const numValue = parseFloat(value);
  if (['%'].includes(unit)) {
    return numValue * parentDimension / 100;
  }
  return numValue;
}

const updateImageDimensions = (
  containerEl: HTMLElement,
  image: typeof Image,
  { fill, height, width }: { fill?: boolean, height?: number, width?: number } = {}
) => {
  const imageEl = containerEl && containerEl.querySelector('img');

  if (!imageEl) {
    return;
  }

  if (!image) {
    const style = { height: unitize(0), width: '100%' };

    setElementStyle(containerEl, style);
    setElementStyle(imageEl, style);
  }

  if (fill) {
    const imageRatio = image.height / image.width;

    const { clientHeight: parentHeight, clientWidth: parentWidth } = containerEl.parentElement;
    const containerHeight = getDimension(height, parentHeight);
    const containerWidth = getDimension(width, parentWidth);
    const containerRatio = containerHeight / containerWidth;

    const containerStyle = {
      height: height === undefined ? '100%' : unitize(height),
      width: width === undefined ? '100%' : unitize(width)
    };
    const imgStyle = imageRatio > containerRatio ? {
      height: 'auto',
      width: '100%'
    } : {
      height: '100%',
      width: 'auto'
    };

    setElementStyle(containerEl, containerStyle);
    setElementStyle(imageEl, imgStyle);
    return;
  }

  if (height === undefined && width === undefined) {
    const style = { height: unitize(image.height), width: unitize(image.width) };

    setElementStyle(containerEl, style);
    setElementStyle(imageEl, style);
    return;
  }

  if (height !== undefined && width !== undefined) {
    const style = { height: unitize(height), width: unitize(width) };    

    setElementStyle(containerEl, style);
    setElementStyle(imageEl, style);
    return;
  }

  const dimension = height === undefined ? width : height;
  const { primaryProp, secondaryProp } = height === undefined ? {
    primaryProp: 'width',
    secondaryProp: 'height'
  } : {
    primaryProp: 'height',
    secondaryProp: 'width'
  };

  const style = {
    [primaryProp]: unitize(dimension),
    [secondaryProp]: 'auto'
  };

  setElementStyle(containerEl, style);
  setElementStyle(imageEl, style);
};

export default (
  src: string,
  {
    fill,
    size,
    height = size,
    width = size
  }: {
    fill?: boolean,
    size?: number,
    height?: number,
    width?: number
  }
) => {
  const [image, setImage] = React.useState(null);
  const imageContainerRef = React.useRef(null);

  const updateImage = React.useCallback(() => {
    updateImageDimensions(imageContainerRef.current, image, { fill, height, width });
  }, [image, fill, height, width]);

  const updateFillImage = React.useCallback(() => {
    if (fill) {
      updateImage();
    }
  }, [fill, updateImage]);

  React.useEffect(() => {
    if (!image) {
      const initializedImage = new Image();
      initializedImage.onload = () => {
        setImage(initializedImage);
      };
      initializedImage.src = src;
    }
  }, [image, setImage, src]);

  React.useEffect(() => {
    updateImage();
  }, [updateImage]);

  useEventListener('resize', updateFillImage, window);

  return { image, imageContainerRef };
};
