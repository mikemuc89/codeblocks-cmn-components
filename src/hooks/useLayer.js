/* @flow */
import * as React from 'react';
import useOpenState from './useOpenState';

export default ({ disabled, open }: { disabled?: boolean, open?: boolean }) => {
  const wrapperRef = React.useRef(null);
  const { hide: defaultHide, show: defaultShow, ...openState } = useOpenState({ disabled, open });
  const { left, top, width } = wrapperRef.current ? wrapperRef.current.getBoundingClientRect() : {};

  const hide = React.useCallback(
    (e?: MouseEvent, force?: boolean) => {
      if (force || !(e && wrapperRef.current && wrapperRef.current.contains(e.target))) {
        defaultHide(e);
        document.removeEventListener('click', hide);
      }
    },
    [defaultHide]
  );

  const show = React.useCallback(
    (e: MouseEvent) => {
      e.stopPropagation();
      if (!disabled) {
        defaultShow(e);
        document.addEventListener('click', hide);
      }
    },
    [defaultShow, disabled, hide]
  );

  return { hide, position: { left, top, width }, show, wrapperRef, ...openState };
};
