/* @flow */
import * as React from 'react';

type UseControlFocusType<ValueType> = {
  focus?: boolean,
  onBlur?: (e: SyntheticFocusEvent<HTMLInputElement | HTMLSelectElement>) => void,
  onFocus?: (e: SyntheticFocusEvent<HTMLInputElement | HTMLSelectElement>) => void
};

export default ({
  focus: initialFocus,
  onBlur,
  onFocus
}) => {
  const [focus, setFocus] = React.useState(initialFocus);

  React.useEffect(() => setFocus(initialFocus), [initialFocus, setFocus]);

  const handleOnBlur = React.useCallback((e: SyntheticFocusEvent<HTMLInputElement | HTMLSelectElement>) => {
    if (onBlur) {
      onBlur(e);
    }
    setFocus(false);
  }, [onBlur, setFocus]);

  const handleOnFocus = React.useCallback((e: SyntheticFocusEvent<HTMLInputElement | HTMLSelectElement>) => {
    if (onFocus) {
      onFocus(e);
    }            
    setFocus(true);
  }, [onFocus, setFocus]);

  return { focus, handleOnBlur, handleOnFocus };
};
