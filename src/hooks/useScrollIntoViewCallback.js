/* @flow */
import * as React from 'react';
import { EVENT_NAMES } from '@omnibly/codeblocks-cmn-types/src/constants';
import { debounced } from '@omnibly/codeblocks-cmn-utils';

const DEBOUNCE_TIME = 1000;

export default ({ onScrollIntoView }: { onScrollIntoView: (e: Event) => void }) => {
  const ref = React.useRef(null);
  const inView = React.useRef(false);

  const onScroll = React.useMemo(
    () =>
      debounced((e: Event) => {
        const el = document.scrollingElement || document.documentElement || {};
        const { scrollTop = 0, offsetHeight = 0 } = el;
        const { offsetTop = 0 } = ref.current || {};

        const scrolledIntoView = offsetTop < scrollTop + offsetHeight && offsetTop > scrollTop;

        if (scrolledIntoView !== inView.current) {
          if (scrolledIntoView) {
            onScrollIntoView(e);
          }
          inView.current = scrolledIntoView;
        }
      }, DEBOUNCE_TIME),
    [onScrollIntoView]
  );

  React.useEffect(() => {
    window.addEventListener(EVENT_NAMES.SCROLL, onScroll);
    return () => window.removeEventListener(EVENT_NAMES.SCROLL, onScroll);
  }, [onScroll]);

  return ref;
};
