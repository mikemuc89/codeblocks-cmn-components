/* @flow */
import * as React from 'react';
import { setElementStyle, unitize } from '@omnibly/codeblocks-cmn-utils';

export const ENTER_ANIMATIONS = Object.freeze({
  EXPAND: 'EXPAND',
  EXPAND_WIDTH: 'EXPAND_WIDTH',
  FADE_IN: 'FADE_IN',
  GROW_FROM_BOTTOM_LEFT: 'GROW_FROM_BOTTOM_LEFT',
  GROW_FROM_BOTTOM_RIGHT: 'GROW_FROM_BOTTOM_RIGHT',
  GROW_FROM_CENTER: 'GROW_FROM_CENTER',
  GROW_FROM_TOP_LEFT: 'GROW_FROM_TOP_LEFT',
  GROW_FROM_TOP_RIGHT: 'GROW_FROM_TOP_RIGHT',
  SHOW: 'SHOW',
  SLIDE_FROM_BOTTOM: 'SLIDE_FROM_BOTTOM',
  SLIDE_FROM_LEFT: 'LIDE_FROM_LEFT',
  SLIDE_FROM_RIGHT: 'SLIDE_FROM_RIGHT',
  SLIDE_FROM_TOP: 'SLIDE_FROM_TOP'
});

export const EXIT_ANIMATIONS = Object.freeze({
  COLLAPSE: 'COLLAPSE',
  COLLAPSE_WIDTH: 'COLLAPSE_WIDTH',
  FADE_OUT: 'FADE_OUT',
  HIDE: 'HIDE',
  SHRINK_TO_BOTTOM_LEFT: 'SHRINK_TO_BOTTOM_LEFT',
  SHRINK_TO_BOTTOM_RIGHT: 'SHRINK_TO_BOTTOM_RIGHT',
  SHRINK_TO_CENTER: 'SHRINK_TO_CENTER',
  SHRINK_TO_TOP_LEFT: 'SHRINK_TO_TOP_LEFT',
  SHRINK_TO_TOP_RIGHT: 'SHRINK_TO_TOP_RIGHT',
  SLIDE_TO_BOTTOM: 'SLIDE_TO_BOTTOM',
  SLIDE_TO_LEFT: 'SLIDE_TO_LEFT',
  SLIDE_TO_RIGHT: 'SLIDE_TO_RIGHT',
  SLIDE_TO_TOP: 'SLIDE_TO_TOP'
});

const ENTER_ANIMATION_PARAMS = Object.freeze({
  [ENTER_ANIMATIONS.EXPAND]: {},
  [ENTER_ANIMATIONS.FADE_IN]: {
    opacity: [0, 1]
  },
  [ENTER_ANIMATIONS.GROW_FROM_BOTTOM_LEFT]: {},
  [ENTER_ANIMATIONS.GROW_FROM_BOTTOM_RIGHT]: {},
  [ENTER_ANIMATIONS.GROW_FROM_CENTER]: {},
  [ENTER_ANIMATIONS.GROW_FROM_TOP_LEFT]: {},
  [ENTER_ANIMATIONS.GROW_FROM_TOP_RIGHT]: {},
  [ENTER_ANIMATIONS.SHOW]: {
    duration: 1,
    opacity: [0, 1]
  },
  [ENTER_ANIMATIONS.SLIDE_FROM_BOTTOM]: {},
  [ENTER_ANIMATIONS.SLIDE_FROM_LEFT]: {},
  [ENTER_ANIMATIONS.SLIDE_FROM_RIGHT]: {},
  [ENTER_ANIMATIONS.SLIDE_FROM_TOP]: {}
});

const EXIT_ANIMATION_PARAMS = Object.freeze({
  [EXIT_ANIMATIONS.COLLAPSE]: {},
  [EXIT_ANIMATIONS.FADE_OUT]: {
    opacity: [1, 0]
  },
  [EXIT_ANIMATIONS.HIDE]: {
    duration: 1,
    opacity: [1, 0]
  },
  [EXIT_ANIMATIONS.SHRINK_TO_BOTTOM_LEFT]: {},
  [EXIT_ANIMATIONS.SHRINK_TO_BOTTOM_RIGHT]: {},
  [EXIT_ANIMATIONS.SHRINK_TO_CENTER]:{},
  [EXIT_ANIMATIONS.SHRINK_TO_TOP_LEFT]: {},
  [EXIT_ANIMATIONS.SHRINK_TO_TOP_RIGHT]: {},
  [EXIT_ANIMATIONS.SLIDE_TO_BOTTOM]: {},
  [EXIT_ANIMATIONS.SLIDE_TO_LEFT]: {},
  [EXIT_ANIMATIONS.SLIDE_TO_RIGHT]: {},
  [EXIT_ANIMATIONS.SLIDE_TO_TOP]: {}
});

const ANIMATION_STATES = Object.freeze({
  BEFORE_ENTER: 'BEFORE_ENTER',
  ENTER: 'ENTER',
  ENTERING: 'ENTERING',
  ENTERED: 'ENTERED',
  EXIT: 'EXIT',
  EXITING: 'EXITING',
  EXITED: 'EXITED'
});

export default ({ animateOnMount = true, enter, exit, lifetime, unmountOnExit, visible: initiallyVisible = false }) => {
  const [visible, setVisible] = React.useState(false);

  const [state, setState] = React.useState(visible ? animateOnMount ? ANIMATION_STATES.BEFORE_ENTER : ANIMATION_STATES.ENTERED : ANIMATION_STATES.EXITED);
  const animationElementRef = React.useRef(null);
  const timeoutRef = React.useRef(null);
  const lifetimeTimeoutRef = React.useRef(null);

  React.useEffect(() => {
    const el = animationElementRef.current;
    if (el) {
      el.style.opacity = visible ? 1 : 0;
    }
  }, [visible]);

  const enterParams = React.useMemo(() => {
    if (!enter) {
      return null;
    }
    return (Array.isArray(enter) ? enter : [enter]).reduce((result, { delay, duration, kind, timing, ...properties }) => {
      const { delay: defaultDelay = 0, duration: defaultDuration = 0, timing: defaultTiming = 'ease-in-out', ...defaultProperties } = ENTER_ANIMATION_PARAMS[kind] || {};
      const computedProperties = { ...defaultProperties, ...properties };

      const entryDelay = delay === undefined ? defaultDelay : delay;
      const entryDuration = duration === undefined ? defaultDuration : duration;
      const entryTiming = timing === undefined ? defaultTiming : timing;
      const adjustHeight = result.adjustHeight || [ENTER_ANIMATIONS.EXPAND].includes(kind);
      const adjustWidth = result.adjustWidth || [ENTER_ANIMATIONS.EXPAND_WIDTH].includes(kind);

      return Object.entries(computedProperties).reduce((res, [propertyKey, propertyValue]) => ({
        adjustHeight,
        adjustWidth,
        delay: [...res.delay, entryDelay],
        duration: [...res.duration, entryDuration],
        properties: [...res.properties, propertyKey],
        timing: [...res.timing, entryTiming],
        values: {
          start: {
            ...res.values.start,
            [propertyKey]: propertyValue[0]
          },
          finish: {
            ...res.values.finish,
            [propertyKey]: propertyValue[1]
          }
        }
      }), result);
    }, {
      adjustHeight: false,
      adjustWidth: false,
      delay: [],
      duration: [],
      properties: [],
      timing: [],
      values: {
        start: {},
        finish: {}
      }
    });
  }, [enter]);

  const exitParams = React.useMemo(() => {
    if (!exit) {
      return null;
    }
    return (Array.isArray(exit) ? exit : [exit]).reduce((result, { delay, duration, kind, timing, ...properties }) => {
      const { delay: defaultDelay = 0, duration: defaultDuration = 0, timing: defaultTiming = 'ease-in-out', ...defaultProperties } = EXIT_ANIMATION_PARAMS[kind] || {};
      const computedProperties = { ...defaultProperties, ...properties };

      const entryDelay = delay === undefined ? defaultDelay : delay;
      const entryDuration = duration === undefined ? defaultDuration : duration;
      const entryTiming = timing === undefined ? defaultTiming : timing;
      const adjustHeight = result.adjustHeight || [ENTER_ANIMATIONS.COLLAPSE].includes(kind);
      const adjustWidth = result.adjustWidth || [ENTER_ANIMATIONS.COLLAPSE_WIDTH].includes(kind);

      return Object.entries(computedProperties).reduce((res, [propertyKey, propertyValue]) => ({
        adjustHeight,
        adjustWidth,
        delay: [...res.delay, entryDelay],
        duration: [...res.duration, entryDuration],
        properties: [...res.properties, propertyKey],
        timing: [...res.timing, entryTiming],
        values: {
          start: {
            ...res.values.start,
            [propertyKey]: propertyValue[0]
          },
          finish: {
            ...res.values.finish,
            [propertyKey]: propertyValue[1]
          }
        }
      }), result);
    }, {
      adjustHeight: false,
      adjustWidth: false,
      delay: [],
      duration: [],
      properties: [],
      timing: [],
      values: {
        start: {},
        finish: {}
      }
    });
  }, [exit]);

  const enterDelay = React.useMemo(() => Math.min(...enterParams.delay), [enterParams]);
  const exitDelay = React.useMemo(() => Math.min(...exitParams.delay), [exitParams]);

  const enterDuration = React.useMemo(() => Math.max(...enterParams.duration), [enterParams]);
  const exitDuration = React.useMemo(() => Math.max(...exitParams.duration), [exitParams]);

  const hide = React.useCallback((useDelay?: boolean) => {
    // clearTimeout(timeoutRef.current);
    timeoutRef.current = setTimeout(() => {
      setState(ANIMATION_STATES.EXIT);
      timeoutRef.current = setTimeout(() => {
        setState(ANIMATION_STATES.EXITING);  
        timeoutRef.current = setTimeout(() => {
          setState(ANIMATION_STATES.EXITED); 
        }, exitDuration);        
      }, 0);
    }, useDelay ? exitDelay : 0);
  }, [exitDelay, exitDuration, setState]);

  const show = React.useCallback((useDelay?: boolean) => {
    // clearTimeout(timeoutRef.current);
    timeoutRef.current = setTimeout(() => {
      setState(ANIMATION_STATES.ENTER);
      timeoutRef.current = setTimeout(() => {
        setState(ANIMATION_STATES.ENTERING);
        timeoutRef.current = setTimeout(() => {
          setState(ANIMATION_STATES.ENTERED); 
        }, enterDuration);
      }, 0);
    }, useDelay ? enterDelay : 0);
  }, [enterDelay, enterDuration, setState]);

  React.useEffect(() => {
    if (visible) {
      setState((oldState) => {
        if (oldState === ANIMATION_STATES.BEFORE_ENTER) {
          show();
        }
        return oldState;
      })
    }
  }, [visible, show, setState]);

  React.useLayoutEffect(() => {
    const isDuringEnter = [ANIMATION_STATES.ENTER, ANIMATION_STATES.ENTERING, ANIMATION_STATES.ENTERED].includes(state);
    const isDuringExit = [ANIMATION_STATES.EXIT, ANIMATION_STATES.EXITING, ANIMATION_STATES.EXITED].includes(state);

    if (!visible && isDuringEnter) {
      hide();
    } else if (visible && isDuringExit) {
      show();
    }

    // return () => clearTimeout(timeoutRef.current);
  }, [hide, show, state, visible]);
  
  const onEnter = React.useCallback((node) => {
    node.style.opacity = 0;
    setElementStyle(node, enterParams.values.start);

    node.style.transitionProperty = enterParams.properties.join(', ');
    node.style.transitionDuration = enterParams.duration.map((value) => `${value}ms`).join(', ');
    node.style.transitionTimingFunction = enterParams.timing.join(', ');
    node.style.transitionDelay = enterParams.delay.map((value) => `${value}ms`).join(', ');
    
    node.dataset.minHeight = node.style.minHeight;
    node.dataset.minWidth = node.style.minWidth;
    
    if (enterParams.adjustHeight) {
      node.style.minHeight = unitize(0);
      node.style.height = unitize(0);
      node.style.overflowY = 'hidden';
    }
    
    if (enterParams.adjustWidth) {
      node.style.minWidth = unitize(0);
      node.style.width = unitize(0);
      node.style.overflowX = 'hidden';
    }
  }, [enterParams]);

  const onEntering = React.useCallback((node) => {
    if (enterParams.adjustHeight) {
      const { scrollHeight } = node;
      const { maxHeight } = getComputedStyle(node);
      const height = Math.max(scrollHeight, unitize.revert(maxHeight) || Infinity);
      node.style.height = unitize(height);
    }
    
    if (enterParams.adjustWidth) {
      const { scrollWidth } = node;
      const { maxWidth } = getComputedStyle(node);
      const width = Math.max(scrollWidth, unitize.revert(maxWidth) || Infinity);
      node.style.width = unitize(width);
    }

    setElementStyle(node, enterParams.values.finish);
  }, [enterParams]);

  const onEntered = React.useCallback((node) => {
    node.style.transitionDelay = '';
    node.style.transitionDuration = '';
    node.style.transitionProperty = '';
    node.style.transitionTimingFunction = '';

    node.style.minHeight = node.dataset.minHeight;
    node.style.minWidth = node.dataset.minWidth;
  }, []);
  
  const onExit = React.useCallback((node) => {
    setElementStyle(node, exitParams.values.start);

    node.style.transitionProperty = enterParams.properties.join(', ');
    node.style.transitionDuration = enterParams.duration.map((value) => `${value}ms`).join(', ');
    node.style.transitionTimingFunction = enterParams.timing.join(', ');
    node.style.transitionDelay = enterParams.delay.map((value) => `${value}ms`).join(', ');
    
    node.dataset.boxSizing = node.style.boxSizing;
    node.dataset.minHeight = node.style.minHeight;
    node.dataset.minWidth = node.style.minWidth;

    if (exitParams.adjustHeight) {
      node.style.minHeight = unitize(0);
      node.style.overflowY = 'hidden';
      const { clientHeight: height } = node;
      node.style.boxSizing = 'border-box';
      node.style.height = unitize(height);
    }

    if (exitParams.adjustWidth) {
      node.style.minWidth = unitize(0);
      node.style.overflowX = 'hidden';
      const { clientWidth: width } = node;
      node.style.boxSizing = 'border-box';
      node.style.width = unitize(width);
    }
  }, [exitParams]);

  const onExiting = React.useCallback((node) => {
    if (exitParams.adjustHeight) {
      node.style.height = unitize(0);
    }

    if (exitParams.adjustWidth) {
      node.style.width = unitize(0);
    }

    setElementStyle(node, exitParams.values.finish);
}, [exitParams]);

  const onExited = React.useCallback((node) => {
    if (node) {
      node.style.transitionDelay = '';
      node.style.transitionDuration = '';
      node.style.transitionProperty = '';
      node.style.transitionTimingFunction = '';

      node.style.boxSizing = node.dataset.boxSizing;
      node.style.minHeight = node.dataset.minHeight;
      node.style.minWidth = node.dataset.minWidth;
    }
  }, []);

  React.useEffect(() => {
    if (state === ANIMATION_STATES.ENTERED && lifetime) {
      lifetimeTimeoutRef.current = setTimeout(() => {
        hide(false);
      }, lifetime);
    }
  }, [hide, lifetime, state]);
  
  React.useEffect(() => {
    const el = animationElementRef.current;
    if (el && state && state !== ANIMATION_STATES.BEFORE_ENTER) {
      (() => ({
        [ANIMATION_STATES.ENTER]: onEnter,
        [ANIMATION_STATES.ENTERING]: onEntering,
        [ANIMATION_STATES.ENTERED]: onEntered,
        [ANIMATION_STATES.EXIT]: onExit,
        [ANIMATION_STATES.EXITING]: onExiting,
        [ANIMATION_STATES.EXITED]: onExited
      }[state]))()(el);
    }
  }, [onEnter, onEntered, onEntering, onExit, onExited, onExiting, state]);

  return {
    animationElementRef,
    setVisible,
    visible
  };
};
