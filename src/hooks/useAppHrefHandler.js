/* @flow */
import * as React from 'react';
import { REGEXP } from '@omnibly/codeblocks-cmn-types/src/enums';
import { createAppLinkFromRef, createUrlFromIdAndParams, getParamsFromLink } from '@omnibly/codeblocks-cmn-utils';
import AppNavigationContext from '../contexts/AppNavigationContext';

const CLICKABLE_STYLE = { cursor: 'pointer' };
const NON_CLICKABLE_STYLE = { cursor: 'inherit' };

export default ({
  button,
  disabled,
  focusable,
  forceClickable,
  href: propsHref,
  hrefData: data,
  onClick
}: {
  button?: boolean,
  disabled?: boolean,
  focusable?: boolean,
  forceClickable?: boolean,
  href: string,
  hrefData?: Object,
  onClick?: (e: MouseEvent) => void
}) => {
  const navigation = React.useContext(AppNavigationContext);

  const Component = React.useMemo(
    () => (propsHref || forceClickable ? (button ? 'button' : 'a') : 'span'),
    [button, forceClickable, propsHref]
  );

  const handler = () => {
    if (disabled) {
      return {
        'data-href': undefined,
        disabled,
        href: undefined,
        onClick: undefined,
        style: NON_CLICKABLE_STYLE,
        target: undefined
      };
    }

    const target = propsHref ? (REGEXP.HTTPS.test(propsHref) ? '_blank' : '_self') : undefined;
    const style = propsHref || onClick || forceClickable ? CLICKABLE_STYLE : NON_CLICKABLE_STYLE;

    const { appLink, href } = (() => {
      if (!propsHref) {
        const href = onClick || forceClickable ? '#' : undefined;
        return {
          appLink: href,
          href,
          style
        };
      }

      if (propsHref in navigation) {
        return {
          appLink: createAppLinkFromRef(propsHref, { data }, navigation),
          href: createUrlFromIdAndParams(propsHref, { data }, navigation),
          style
        };
      }

      if (propsHref.startsWith('#/')) {
        const { id, params } = getParamsFromLink(propsHref.split('#')[1]);
        return {
          appLink: createAppLinkFromRef(id, { data: { ...params, ...data } }, navigation),
          href: createUrlFromIdAndParams(id, { data: { ...params, ...data } }, navigation),
          style
        };
      }

      return { appLink: propsHref, href: propsHref, style };
    })();

    return {
      'data-href': Component === 'span' ? undefined : appLink,
      disabled,
      href: button || Component === 'span' ? undefined : href,
      onClick,
      style,
      ...(onClick && focusable ? { tabIndex: 0 } : {}),
      target: button || Component === 'span' ? undefined : target
    };
  };

  return Object.assign(handler, {
    Component
  });
};
