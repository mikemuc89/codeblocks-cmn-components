/* @flow */
import * as React from 'react';

export default ({ value }: { value: string }) => {
  const [cache, setCache] = React.useState(value || '');
  const onCacheChange = (newValue: string) => setCache(newValue);
  const onClearCache = () => setCache('');

  return { cache, onCacheChange, onClearCache };
};
