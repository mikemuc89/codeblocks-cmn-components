/* @flow */
import * as React from 'react';

export type ItemType = {|
  id: string,
  data: Object
|};

export default ({
  children,
  items: propsItems,
  multiple,
  value
}: {
  children?: Array<any>,
  items?: Array<ItemType>,
  multiple?: boolean,
  value?: string | Array<string>
}) => {
  const items = React.useMemo(
    () =>
      propsItems ||
      React.Children.toArray(children).map((child: Object) => {
        const { id, children, text, ...data } = child.props;
        return {
          data: { text: children || text, ...data },
          id
        };
      }),
    [propsItems, children]
  );

  const selected = React.useMemo(
    () =>
      multiple
        ? (value && items.filter((item: ItemType) => value.includes(item.id))) || null
        : items.find((item: ItemType) => item.id === value) || null,
    [items, multiple, value]
  );

  return { items, selected };
};
