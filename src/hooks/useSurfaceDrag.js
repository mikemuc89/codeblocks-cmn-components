/* @flow */
import * as React from 'react';

const containBetween = (value: number, min: number, max: number) => Math.max(min, Math.min(max, value));

export const EVENT_KINDS = Object.freeze({
  DRAG: Symbol('useSurfaceDrag.EVENT_KINDS.DRAG'),
  DRAG_END: Symbol('useSurfaceDrag.EVENT_KINDS.DRAG_END'),
  DRAG_START: Symbol('useSurfaceDrag.EVENT_KINDS.DRAG_START')
});

type AbsolutePositionType = {|
  px: number,
  py: number,
  x: number,
  y: number
|};

type DeltaPositionType = {|
  dpx: number,
  dpy: number,
  dx: number,
  dy: number
|};

export type SurfaceDragCallbackDataType = {|
  absolute: AbsolutePositionType,
  fromLast: DeltaPositionType,
  fromStart: DeltaPositionType,
  kind: $Values<typeof EVENT_KINDS>
|};

export default ({
  contain,
  disabled,
  onDrag,
  onDragEnd,
  onDragStart
}: {
  contain?: boolean,
  disabled?: boolean,
  onDrag: (data: SurfaceDragCallbackDataType) => void,
  onDragEnd: (data: SurfaceDragCallbackDataType) => void,
  onDragStart: (data: SurfaceDragCallbackDataType) => void
}) => {
  const surface = React.useRef(null);
  const rect = React.useRef({ height: null, left: null, top: null, width: null });
  const startValue = React.useRef({ x: null, y: null });
  const lastValue = React.useRef({ x: null, y: null });

  const start = React.useCallback(
    (e: MouseEvent) => {
      document.addEventListener('mousemove', dragging);
      document.addEventListener('mouseup', finish);
      surface.current.removeEventListener('mousedown', start);

      const { height, left, top, width } = surface.current.getBoundingClientRect();
      rect.current = { height, left, top, width };

      const x = e.clientX;
      const y = e.clientY;

      if (onDragStart) {
        const px = (x - left) / width;
        const py = (y - top) / height;

        onDragStart({
          absolute: { px, py, x, y },
          fromLast: { dpx: 0, dpy: 0, dx: 0, dy: 0 },
          fromStart: { dpx: 0, dpy: 0, dx: 0, dy: 0 },
          kind: EVENT_KINDS.DRAG_START
        });
      }

      lastValue.current = { x, y };
      startValue.current = { x, y };
    },
    [dragging, finish, onDragStart]
  );

  const dragging = React.useCallback(
    (e: MouseEvent) => {
      e.preventDefault();

      const { height, left, top, width } = rect.current;

      const x = e.clientX;
      const y = e.clientY;

      if (onDrag) {
        const _px = (x - left) / width;
        const _py = (y - top) / height;
        const px = contain ? containBetween(_px, 0, 1) : _px;
        const py = contain ? containBetween(_py, 0, 1) : _py;

        const dxLast = x - lastValue.current.x;
        const dyLast = y - lastValue.current.y;

        const dxStart = x - startValue.current.x;
        const dyStart = y - startValue.current.y;

        onDrag({
          absolute: { px, py, x, y },
          fromLast: { dpx: dxLast / width, dpy: dyLast / width, dx: dxLast, dy: dyLast },
          fromStart: { dpx: dxStart / width, dpy: dyStart / width, dx: dxStart, dy: dyStart },
          kind: EVENT_KINDS.DRAG
        });
      }

      lastValue.current = { x, y };
    },
    [contain, onDrag]
  );

  const finish = React.useCallback(
    (e: MouseEvent) => {
      document.removeEventListener('mousemove', dragging);
      document.removeEventListener('mouseup', finish);
      surface.current.addEventListener('mousedown', start);

      const { height, left, top, width } = rect.current;

      const x = e.clientX;
      const y = e.clientY;

      if (onDragEnd) {
        const _px = (x - left) / width;
        const _py = (y - top) / height;
        const px = contain ? containBetween(_px, 0, 1) : _px;
        const py = contain ? containBetween(_py, 0, 1) : _py;

        const dxLast = x - lastValue.current.x;
        const dyLast = y - lastValue.current.y;

        const dxStart = x - startValue.current.x;
        const dyStart = y - startValue.current.y;

        onDragEnd({
          absolute: { px, py, x, y },
          fromLast: { dpx: dxLast / width, dpy: dyLast / width, dx: dxLast, dy: dyLast },
          fromStart: { dpx: dxStart / width, dpy: dyStart / width, dx: dxStart, dy: dyStart },
          kind: EVENT_KINDS.DRAG_END
        });
      }

      lastValue.current = { x: null, y: null };
      startValue.current = { x: null, y: null };
    },
    [contain, dragging, onDragEnd, start]
  );

  React.useEffect(() => {
    const el = surface.current;
    if (el) {
      if (disabled) {
        el.removeEventListener('mousedown', start);
      } else {
        el.addEventListener('mousedown', start);
      }
    }
    return () => {
      if (el) {
        el.removeEventListener('mousedown', start);
      }
    };
  }, [disabled, start]);

  return surface;
};
