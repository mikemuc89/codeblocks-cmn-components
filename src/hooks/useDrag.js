/* @flow */
import * as React from 'react';

const containBetween = (value: number, min: number, max: number) => Math.max(min, Math.min(max, value));

export const EVENT_KINDS = Object.freeze({
  DRAG: Symbol('useDrag.EVENT_KINDS.DRAG'),
  DRAG_END: Symbol('useDrag.EVENT_KINDS.DRAG_END'),
  DRAG_START: Symbol('useDrag.EVENT_KINDS.DRAG_START')
});

type AbsolutePositionType = {|
  px: number,
  py: number,
  x: number,
  y: number
|};

type DeltaPositionType = {|
  dpx: number,
  dpy: number,
  dx: number,
  dy: number
|};

export type DragCallbackDataType = {|
  absolute: AbsolutePositionType,
  fromLast: DeltaPositionType,
  fromStart: DeltaPositionType,
  kind: $Values<typeof EVENT_KINDS>
|};

export default ({
  contain,
  disabled,
  onDrag,
  onDragEnd,
  onDragStart,
  onIncrement,
  onDecrement
}: {
  contain?: boolean,
  disabled?: boolean,
  onDrag: (data: DragCallbackDataType) => void,
  onDragEnd: (data: DragCallbackDataType) => void,
  onDragStart: (data: DragCallbackDataType) => void,
  onIncrement: () => void,
  onDecrement: () => void
}) => {
  const handle = React.useRef(null);
  const rect = React.useRef({ height: null, left: null, top: null, width: null });
  const startValue = React.useRef({ x: null, y: null });
  const lastValue = React.useRef({ x: null, y: null });

  React.useEffect(() => {
    const el = handle.current;
    if (el) {
      el.style.cursor = disabled ? 'inherit' : 'pointer';
    }
  }, [disabled]);

  const start = React.useCallback(
    (e: MouseEvent) => {
      const el = handle.current;
      document.addEventListener('mousemove', dragging);
      document.addEventListener('mouseup', finish);
      el.removeEventListener('mousedown', start);

      const { height, left, top, width } = el.parentElement.getBoundingClientRect();
      rect.current = { height, left, top, width };

      const x = e.clientX;
      const y = e.clientY;

      if (onDragStart) {
        const px = (x - left) / width;
        const py = (y - top) / height;

        onDragStart({
          absolute: { px, py, x, y },
          fromLast: { dpx: 0, dpy: 0, dx: 0, dy: 0 },
          fromStart: { dpx: 0, dpy: 0, dx: 0, dy: 0 },
          kind: EVENT_KINDS.DRAG_START
        });
      }

      lastValue.current = { x, y };
      startValue.current = { x, y };
    },
    [dragging, finish, onDragStart]
  );

  const dragging = React.useCallback(
    (e: MouseEvent) => {
      e.preventDefault();

      const { height, left, top, width } = rect.current;

      const x = e.clientX;
      const y = e.clientY;

      if (onDrag) {
        const _px = (x - left) / width;
        const _py = (y - top) / height;
        const px = contain ? containBetween(_px, 0, 1) : _px;
        const py = contain ? containBetween(_py, 0, 1) : _py;

        const dxLast = x - lastValue.current.x;
        const dyLast = y - lastValue.current.y;

        const dxStart = x - startValue.current.x;
        const dyStart = y - startValue.current.y;

        onDrag({
          absolute: { px, py, x, y },
          fromLast: { dpx: dxLast / width, dpy: dyLast / width, dx: dxLast, dy: dyLast },
          fromStart: { dpx: dxStart / width, dpy: dyStart / width, dx: dxStart, dy: dyStart },
          kind: EVENT_KINDS.DRAG
        });
      }

      lastValue.current = { x, y };
    },
    [contain, onDrag]
  );

  const finish = React.useCallback(
    (e: MouseEvent) => {
      document.removeEventListener('mousemove', dragging);
      document.removeEventListener('mouseup', finish);
      handle.current.addEventListener('mousedown', start);

      const { height, left, top, width } = rect.current;

      const x = e.clientX;
      const y = e.clientY;

      if (onDragEnd) {
        const _px = (x - left) / width;
        const _py = (y - top) / height;
        const px = contain ? containBetween(_px, 0, 1) : _px;
        const py = contain ? containBetween(_py, 0, 1) : _py;

        const dxLast = x - lastValue.current.x;
        const dyLast = y - lastValue.current.y;

        const dxStart = x - startValue.current.x;
        const dyStart = y - startValue.current.y;

        onDragEnd({
          absolute: { px, py, x, y },
          fromLast: { dpx: dxLast / width, dpy: dyLast / width, dx: dxLast, dy: dyLast },
          fromStart: { dpx: dxStart / width, dpy: dyStart / width, dx: dxStart, dy: dyStart },
          kind: EVENT_KINDS.DRAG_END
        });
      }

      lastValue.current = { x: null, y: null };
      startValue.current = { x: null, y: null };
    },
    [contain, dragging, onDragEnd, start]
  );

  const keying = React.useCallback(
    (e: Event) => {
      const key = e.which || e.keyCode;

      switch (key) {
        case 38:
        case 39:
          e.preventDefault();
          if (onIncrement) {
            onIncrement();
          }
          break;
        case 37:
        case 40:
          e.preventDefault();
          if (onDecrement) {
            onDecrement();
          }
          break;
        default:
          break;
      }
    },
    [onDecrement, onIncrement]
  );

  React.useEffect(() => {
    const el = handle.current;
    if (disabled) {
      el.removeEventListener('mousedown', start);
      el.removeEventListener('keydown', keying);
    } else {
      el.addEventListener('mousedown', start);
      el.addEventListener('keydown', keying);
    }
    return () => {
      el.removeEventListener('mousedown', start);
      el.removeEventListener('keydown', keying);
    };
  }, [disabled, keying, start]);

  return handle;
};
