/* @flow */
import * as React from 'react';
import { type ErrorsType } from '@omnibly/codeblocks-cmn-types';
import { GLOBAL_CLASS_NAMES } from '@omnibly/codeblocks-cmn-types/src/constants';
import Message from '../components/Message';

type UseControlMessagesType<ValueType> = {
  errors?: ErrorsType,
  handleOnFocus: (e: SyntheticFocusEvent<HTMLInputElement | HTMLSelectElement>) => void,
  helper?: ErrorsType,
  hints?: ErrorsType,
  warnings?: ErrorsType
};

export default ({
  errors: initialErrors,
  handleOnFocus: handleOnFocusIntermediate,
  helper: initialHelper,
  hints: initialHints,
  warnings: initialWarnings
}) => {
  const [errors, setErrors] = React.useState(initialErrors);
  const [helper, setHelper] = React.useState(initialHelper);
  const [hints, setHints] = React.useState(initialHints);
  const [warnings, setWarnings] = React.useState(initialWarnings);

  React.useEffect(() => setErrors(initialErrors), [initialErrors, setErrors]);
  React.useEffect(() => setHelper(initialHelper), [initialHelper, setHelper]);
  React.useEffect(() => setHints(initialHints), [initialHints, setHints]);
  React.useEffect(() => setWarnings(initialWarnings), [initialWarnings, setWarnings]);

  const handleOnFocus = React.useCallback((e: SyntheticFocusEvent<HTMLInputElement | HTMLSelectElement>) => {
    if (handleOnFocusIntermediate) {
      handleOnFocusIntermediate(e);
    }
    setErrors(null);
  }, [handleOnFocusIntermediate, setErrors]);

  const messageElement = React.useMemo(() => (
    <Message className={GLOBAL_CLASS_NAMES.MESSAGE} errors={errors} helper={helper} hints={hints} warnings={warnings} />
  ), [errors, helper, hints, warnings]);

return { handleOnFocus, messageElement, setErrors, setHelper, setHints, setWarnings };
};
