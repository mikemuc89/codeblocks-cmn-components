/* @flow */
import * as React from 'react';
import { useDeepMemoizedValue } from './useDeepCompareHooks';

type UseControlValueType<ValueType> = {
  canClear?: boolean,
  multiple?: boolean,
  onChange?: (e: SyntheticChangeEvent<HTMLInputElement | HTMLSelectElement>) => void,
  onClear?: (value: ValueType) => void,
  paramsDefaultValue: ValueType,
  sanitizeValueForControl: (value: ValueType, { multiple?: boolean }) => string,
  value: ValueType
};

export default ({
  canClear = true,
  maximum,
  onChange,
  onClear,
  paramsDefaultValue,
  readonly,
  sanitizeValueForControl,
  value: propsValue
}) => {
  const multiple = React.useMemo(() => maximum > 1, [maximum]);
  const emptyValue = React.useMemo(() => multiple ? [] : '', [multiple]);
  const defaultValue = React.useMemo(() => {
    if (paramsDefaultValue !== undefined) {
      return paramsDefaultValue;
    }
    return emptyValue;
  }, [emptyValue]);

  const propsValueMemo = useDeepMemoizedValue(propsValue);
  const [value, setValue] = React.useState(propsValueMemo);

  
  const sanitizedControlValue = React.useMemo(() => sanitizeValueForControl(valueMemo === undefined ? defaultValue : valueMemo, { multiple }), [defaultValue, multiple, sanitizeValueForControl, valueMemo]);

  React.useEffect(() => setValue(propsValueMemo), [propsValueMemo, setValue]);

  React.useEffect(() => {
    if (onChange) {
      onChange(valueMemo);
    }
  }, [onChange, valueMemo])

  const handleOnChange = React.useCallback((e: SyntheticChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const el = e.target;
    if (el) {
      const newValue = el.dataset.value || el.value;
      setValue(newValue);
    }
  }, [setValue]);

  const handleOnClear = React.useCallback(() => {
    if (!readonly) {
      setValue((oldValue) => {
        if (onClear) {
          onClear(oldValue);
        }
        return emptyValue;
      });
    }
  }, [emptyValue, onClear, readonly, setValue]);

  return {
    handleOnChange,
    handleOnClear: canClear ? handleOnClear : undefined,
    multiple,
    sanitizedControlValue,
    setValue,
    value: valueMemo
  };
};
